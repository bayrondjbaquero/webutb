<!DOCTYPE html>
<html lang="es">
<head>
    <?php include "head.php"; ?>
</head>
<body class="">
    <div id="app" class="mx-auto">
            <nav class="navbar navbar-expand-lg navbar-light justify-content-center fixed-top">
                 <?php include "menu.php"; ?>
            </nav>
            <br><br><br>
        <div class="container pt-3">
            <div class="row justify-content-center mt-3">
                <div class="col-12 col-sm-12 col-md-12 col-lg-12 col-xl-12 bg-white box pt-4 pl-4 pr-4 pb-4 animated fadeInUp mb-2">
                    <img class="d-block w-100 img-fluid" src="http://www.utb.edu.co/sites/web.unitecnologica.edu.co/files/banner-trafico2.jpg" alt="First slide" style="max-height: 635px;"><br>
                    <h4 class="w-100 text-regular pb-2 m-0 rounded-top primary">
                        Información del Evento
                    </h4>
                    <!-- <figure>
                        <img src="images/Energia Solar.jpg" alt="" class="img-fluid d-block mx-auto">
                    </figure> -->
                    <p class="text panel bg-white " style="font-size: 15px;">
                        El Congreso Colombiano de Ingeniería de Transporte y Tránsito, anteriormente llamado
Simposio Colombiano de Ingeniería de Tránsito y Transporte, lleva 30 años reuniendo
académicos, estudiantes, consultores, empresarios y autoridades nacionales e
internacionales, con el fin de conocer y discutir los avances de los últimos años en
investigaciones y aplicaciones en las diferentes áreas en ingeniería de tránsito, transporte
y logística.
                    </p>
                    <p class="text panel bg-white " style="font-size: 15px;">
                        Este evento se ha llevado a cabo en las siguientes universidades de Colombia:
                    </p>
                    <ul>
                        <li>Universidad del Cauca: 1986, 1988, 1990, 1992. </li>
                        <li>Universidad Nacional de Colombia - Sede Medellín: 2001.</li>
                        <li>Universidad del Cauca: 2003.</li>
                        <li>Universidad Nacional de Colombia - Sede Bogotá 2005.</li>
                        <li>Universidad del Cauca 2007.</li>
                        <li>Universidad Pedagógica y Tecnológica de Colombia 2009.</li>
                        <li>Universidad Nacional de Colombia - Sede Medellín y Universidad del Cauca 2011.</li>
                        <li>Universidad del Norte 2015</li>
                        <li>Universidad de los Andes 2017</li>
                    </ul>
                    <p class="text panel bg-white " style="font-size: 15px;">
                        Con el eslogan <em>“Movilizando el futuro de Colombia”</em>, la décimo tercera versión del
Congreso Colombiano de Tránsito y Transporte, se desarrollará en la ciudad de Cartagena
de Indias los días 26, 27 y 28 de junio del 2019.
                    </p>
                    <p class="text panel bg-white " style="font-size: 15px;">
                        En el marco del evento, se contará con la presencia de invitados nacionales e
internacionales y se realizarán cursos especializados y visitas técnicas a diferentes
sectores del área de tránsito y transporte. Adicionalmente, se estará realizando la gestión
para que en la selección de artículos destacados en el evento, estos sean publicados en
volúmenes especiales de revistas de prestigio nacional e internacional, donde se
emplearán los comités temáticos creados en el CCTT2017.
                    </p>
                    <hr>
                </div>    
            </div>
        </div>
       <?php include "footer.php"; ?>
    </div>

    <!-- Scripts -->
    <?php include "script.php"; ?>
</body>
</html>

