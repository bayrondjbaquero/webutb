<!DOCTYPE html>
<html lang="es">
<head>
    <?php include "head.php"; ?>
</head>
<body class="">
    <div id="app" class="mx-auto">
            <nav class="navbar navbar-expand-lg navbar-light justify-content-center fixed-top">
                <?php include "menu.php"; ?>
            </nav>
            <br><br><br>
            <!-- <div id="carouselExampleControls" class="carousel slide carousel-fade row" data-ride="carousel">
                <ol class="carousel-indicators">
                    <li data-target="#carouselExampleIndicators" data-slide-to="0" class="active"></li>
                    <li data-target="#carouselExampleIndicators" data-slide-to="1"></li>
                    <li data-target="#carouselExampleIndicators" data-slide-to="2"></li>
                    <li data-target="#carouselExampleIndicators" data-slide-to="3"></li>
                </ol>
                <div class="carousel-inner animated fadeIn">
                    <div class="carousel-item active">
                        <img class="d-block w-100 img-fluid" src="http://www.utb.edu.co/sites/web.unitecnologica.edu.co/files/banner_home_mesa_de_trabajo_1.jpg" alt="First slide">
                    </div>
                    <div class="carousel-item">
                        <img class="d-block w-100 img-fluid" src="http://www.utb.edu.co/sites/web.unitecnologica.edu.co/files/banner_grammy.png" alt="Second slide">
                    </div>
                </div>
                <a class="carousel-control-prev" href="#carouselExampleControls" role="button" data-slide="prev">
                    <span class="carousel-control-prev-icon" aria-hidden="true"></span>
                    <span class="sr-only">Previous</span>
                </a>
                <a class="carousel-control-next" href="#carouselExampleControls" role="button" data-slide="next">
                    <span class="carousel-control-next-icon" aria-hidden="true"></span>
                    <span class="sr-only">Next</span>
                </a>
            </div> -->
        <div class="container pt-3">
            <div class="row justify-content-center">
                <div class="col-12 col-sm-12 col-md-12 col-lg-12 col-xl-12 bg-white box pt-4 pl-4 pr-4 pb-4 animated fadeInUp mb-2">
                    <h4 class="w-100 text-regular pb-2 m-0 rounded-top primary">
                        Visitas Técnicas
                    </h4>
                    <figure>
                        <img src="http://www.utb.edu.co/sites/web.unitecnologica.edu.co/files/construccion_0.png" alt="" class="img-fluid d-block mx-auto">
                    </figure>
                </div>                
                <hr>
            </div>
        </div>
        <?php include "footer.php"; ?>
    </div>

    <!-- Scripts -->
    <?php include "script.php"; ?>
</body>
</html>

