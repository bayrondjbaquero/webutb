<!DOCTYPE html>
<html lang="es">
<head>
    <?php include "head.php"; ?>
</head>
<body class="">
    <div id="app" class="mx-auto">
            <nav class="navbar navbar-expand-lg navbar-light justify-content-center fixed-top">
                <?php include "menu.php"; ?>
            </nav>
            <br><br><br>
            <!-- <div id="carouselExampleControls" class="carousel slide carousel-fade row" data-ride="carousel">
                <ol class="carousel-indicators">
                    <li data-target="#carouselExampleIndicators" data-slide-to="0" class="active"></li>
                    <li data-target="#carouselExampleIndicators" data-slide-to="1"></li>
                    <li data-target="#carouselExampleIndicators" data-slide-to="2"></li>
                    <li data-target="#carouselExampleIndicators" data-slide-to="3"></li>
                </ol>
                <div class="carousel-inner animated fadeIn">
                    <div class="carousel-item active">
                        <img class="d-block w-100 img-fluid" src="http://www.utb.edu.co/sites/web.unitecnologica.edu.co/files/banner_home_mesa_de_trabajo_1.jpg" alt="First slide">
                    </div>
                    <div class="carousel-item">
                        <img class="d-block w-100 img-fluid" src="http://www.utb.edu.co/sites/web.unitecnologica.edu.co/files/banner_grammy.png" alt="Second slide">
                    </div>
                </div>
                <a class="carousel-control-prev" href="#carouselExampleControls" role="button" data-slide="prev">
                    <span class="carousel-control-prev-icon" aria-hidden="true"></span>
                    <span class="sr-only">Previous</span>
                </a>
                <a class="carousel-control-next" href="#carouselExampleControls" role="button" data-slide="next">
                    <span class="carousel-control-next-icon" aria-hidden="true"></span>
                    <span class="sr-only">Next</span>
                </a>
            </div> -->
        <div class="container pt-3 " style="font-size: 13px;">
            <div class="row justify-content-center mt-3">
                <div class="col-12 col-sm-12 col-md-12 col-lg-12 col-xl-12 bg-white box pt-4 pl-4 pr-4 pb-4 animated fadeInUp mb-2">
                    <h4 class="w-100 text-regular pb-2 m-0 rounded-top text-left mb-3 text-azul">
                        Programa del evento
                    </h4>
                    <div class="row justify-content-center">

                <div class="col-md-4">
                    <!--Pricing card-->
                    <div class="pricing-card text-center z-depth-1 hoverable">
                        <!--Label-->
                        <div class="bg-primary p-2 text-white darken-2 z-depth-1">
                            <p class="white-text extra-padding-05"><strong>Miercoles</strong></p>
                        </div>
                        <!--Price-->
                        <div class="card-up bg-primary p-3 text-white">
                            <h4>26 Junio</h4>
                            <p style="font-size: 17px;">
                                Universidad Tecnológica de Bolívar (UTB)<br>
                                Campus Casa Lemaitre-Calle del Bouquet<br>
                                Cra.21 #25-92, barrio Manga<br>
                            </p>
                            <hr>
                            <!-- <p style="font-size: 17px;">
                                Centro de Formación de la<br>
                                Cooperación Española (CFCE)<br>
                                Cra. 3 #274, barrio Centro<br>
                            </p> -->
                        </div>

                        <!-- <div class="card-content">
                            <ul class="features-list" style="text-align: left;">
                                <li>
                                    <p><i class="fa fa-check green-text" aria-hidden="true"></i> 7:30-15:00 Registro (UTB)</p>
                                </li>
                                <li>
                                    <p><i class="fa fa-check green-text" aria-hidden="true"></i> 8:00-9:30 Sesiones paralelas/paneles 1 (UTB) </p>
                                </li>
                                <li>
                                    <p><i class="fa fa-check green-text" aria-hidden="true"></i> 9:30-11:00 Sesiones paralelas/paneles 2 (UTB)</p>
                                </li>
                                <li>
                                    <p><i class="fa fa-check green-text" aria-hidden="true"></i> 11:00-11:30 Intervalo (Coffe break)</p>
                                </li>
                                <li>
                                    <p><i class="fa fa-check green-text" aria-hidden="true"></i> 11:30-13:00: Conferencia 1: Eduardo Posada Carbó - Latin American Centre –
Universidad de Oxford (UTB)</p>
                                </li>
                                <li>
                                    <p><i class="fa fa-check green-text" aria-hidden="true"></i> 13:00-14:30 Ceremonia de Apertura y Almuerzo de bienvenida (UTB)</p>
                                </li>
                                <li>
                                    <p><i class="fa fa-check green-text" aria-hidden="true"></i> 14:30-15:45 Sesiones paralelas/paneles 3 (UTB)</p>
                                </li>
                                <li>
                                    <p><i class="fa fa-check green-text" aria-hidden="true"></i> 17:00-17.30 Presentación Proyecto Editorial 1 (Investigación Económica, UNAM,
México) (CFCE)</p>
                                </li>
                                <li>
                                    <p><i class="fa fa-check green-text" aria-hidden="true"></i> 17.30-18:30 Conferencia 2: Michael Piore - MIT (CFCE)</p>
                                </li>
                            </ul>
                            <hr>
                        </div> -->
                    </div>
                    <!--/.Pricing card-->

                </div>

                <div class="col-md-4">
                    <!--Pricing card-->
                    <div class="pricing-card text-center z-depth-1 hoverable">
                        <!--Label-->
                        <div class="bg-primary p-2 text-white darken-2 z-depth-1">
                            <p class="white-text extra-padding-05"><strong>Jueves</strong></p>
                        </div>
                        <!--Price-->
                        <div class="card-up bg-primary p-3 text-white">
                            <h4>27 Junio</h4>
                            <p style="font-size: 17px;">
                                Universidad Tecnológica de Bolívar (UTB)<br>
                                Campus Casa Lemaitre-Calle del Bouquet<br>
                                Cra.21 #25-92, barrio Manga<br>
                            </p>
                            <hr>
                            <!-- <p style="font-size: 17px;">
                                Centro de Formación de la<br>
                                Cooperación Española (CFCE)<br>
                                Cra. 3 #274, barrio Centro<br>
                            </p> -->
                        </div>

                        <!-- <div class="card-content">
                            <ul class="features-list" style="text-align: left;">
                                <li>
                                    <p><i class="fa fa-check green-text" aria-hidden="true"></i> 8:00-12:00 Registro (UTB)</p>
                                </li>
                                <li>
                                    <p><i class="fa fa-check green-text" aria-hidden="true"></i> 8:00-9:30 Sesiones paralelas/paneles 4 (UTB)</p>
                                </li>
                                <li>
                                    <p><i class="fa fa-check green-text" aria-hidden="true"></i> 9:30-11:00 Sesiones paralelas/paneles 5 (UTB)</p>
                                </li>
                                <li>
                                    <p><i class="fa fa-check green-text" aria-hidden="true"></i> 11:00-11.30 Intervalo (Coffe Break)</p>
                                </li>
                                <li>
                                    <p><i class="fa fa-check green-text" aria-hidden="true"></i> 11:30-13:00 Sesiones paralelas/paneles 6 (UTB)</p>
                                </li>
                                <li>
                                    <p><i class="fa fa-check green-text" aria-hidden="true"></i> 13:00-14:00 Almuerzo libre</p>
                                </li>
                                <li>
                                    <p><i class="fa fa-check green-text" aria-hidden="true"></i> 14:30-15:30 Conferencia 3: María de los Ángeles Durán – Centro Superior de
Investigaciones Científicas - CSIC (CFCE)</p>
                                </li>
                                <li>
                                    <p><i class="fa fa-check green-text" aria-hidden="true"></i> 15:30-16:00 Presentación Proyecto Editorial 2 (Cuadernos de Desarrollo Rural, U.
Javeriana, Colombia)</p>
                                </li>
                                <li>
                                    <p><i class="fa fa-check green-text" aria-hidden="true"></i> 16:00-17:00 Conferencia 4: Josep Borrell – Catedrático de Economía de la Universidad
Complutense de Madrid and Expresidente del Parlamento Europeo (CFCE) </p>
                                </li>
                                <li>
                                    <p><i class="fa fa-check green-text" aria-hidden="true"></i> 17:00-18:00 Muestra Cultural - Tambores de Cabildo - Cóctel (CFCE)</p>
                                </li>
                            </ul>
                            <hr>
                        </div> -->
                    </div>
                    <!--/.Pricing card-->

                </div>

                <div class="col-md-4">
                    <!--Pricing card-->
                    <div class="pricing-card text-center z-depth-1 hoverable">
                        <!--Label-->
                        <div class="bg-primary p-2 text-white darken-2 z-depth-1">
                            <p class="white-text extra-padding-05"><strong>Viernes</strong></p>
                        </div>
                        <!--Price-->
                        <div class="card-up bg-primary p-3 text-white">
                            <h4>28 Junio</h4>
                            <p style="font-size: 17px;">
                                Universidad Tecnológica de Bolívar (UTB)<br>
                                Campus Casa Lemaitre-Calle del Bouquet<br>
                                Cra.21 #25-92, barrio Manga<br>
                            </p>
                            <hr>
                        </div>

                        <!-- <div class="card-content">
                            <ul class="features-list" style="text-align: left;">
                                <li>
                                    <p><i class="fa fa-check green-text" aria-hidden="true"></i> 08:00-10:00 Registro (UdC)</p>
                                </li>
                                <li>
                                    <p><i class="fa fa-check green-text" aria-hidden="true"></i> 08:00-9:30 sesiones paralelas/paneles 9</p>
                                </li>
                                <li>
                                    <p><i class="fa fa-check green-text" aria-hidden="true"></i> 09:30-11:00 (UdC) Sesiones paralelas / paneles 10</p>
                                </li>
                                <li>
                                    <p><i class="fa fa-check green-text" aria-hidden="true"></i> 11:00-12:30 (UdC) Sesiones paralelas / paneles 11</p>
                                </li>
                                <li>
                                    <p><i class="fa fa-check green-text" aria-hidden="true"></i> 12:30-12:45 (UdC) Ceremonia de cierre</p>
                                </li>
                                <li>
                                    <p><i class="fa fa-check green-text" aria-hidden="true"></i> 12.45-14.00 Conferencia (UdC) Alberto Abello Vives</p>
                                </li>
                                <li>
                                    <p><i class="fa fa-check green-text" aria-hidden="true"></i> 14.00. Cóctel de despedida</p>
                                </li>
                            </ul>
                            <hr>
                        </div> -->
                    </div>
                    <!--/.Pricing card-->

                </div>

            </div>
                </div>                 
                <hr>
            </div>
        </div>
        <?php include "footer.php"; ?>
    </div>

    <!-- Scripts -->
    <?php include "script.php"; ?>
</body>
</html>

