<!DOCTYPE html>
<html lang="es">
<head>
   <?php include "head.php"; ?>
</head>
<body class="">
    <div id="app" class="mx-auto">
            <nav class="navbar navbar-expand-lg navbar-light justify-content-center fixed-top">
                 <?php include "menu.php"; ?>
            </nav>
            <br><br><br>
        <div class="container pt-3">
            <div class="row justify-content-center mt-3">
                <div class="col-12 col-sm-12 col-md-12 col-lg-12 col-xl-12 bg-white box pt-4 pl-4 pr-4 pb-4 animated fadeInUp mb-2">
                    <img class="d-block w-100 img-fluid" src="http://www.utb.edu.co/sites/web.unitecnologica.edu.co/files/banner-trafico1.jpg" alt="First slide" style="max-height: 635px;"><br>
                    
                    <h4 class="w-100 text-regular pb-2 m-0 rounded-top primary">
                        Organización
                    </h4>
                    <figure>
                        <img src="http://www.utb.edu.co/sites/web.unitecnologica.edu.co/files/construccion_0.png" alt="" class="img-fluid d-block mx-auto">
                    </figure>
                    <!-- <figure>
                        <img src="images/Energia Solar.jpg" alt="" class="img-fluid d-block mx-auto">
                    </figure> -->
                    <!-- <p class="text panel bg-white " style="font-size: 15px;">
                        Lorem ipsum dolor sit amet, consectetur adipisicing elit. Iste adipisci ab quibusdam? Officia, quaerat. Numquam voluptatibus fuga maxime minus atque. Ullam repellendus quia nemo vitae! Incidunt fugiat dolor mollitia iusto.
                    </p>
                    <p class="text panel bg-white " style="font-size: 15px;">
                        Lorem ipsum dolor sit amet, consectetur adipisicing elit. Iste adipisci ab quibusdam? Officia, quaerat. Numquam voluptatibus fuga maxime minus atque. Ullam repellendus quia nemo vitae! Incidunt fugiat dolor mollitia iusto.
                    </p> -->
                </div>
            </div>
        </div>
        <?php include "footer.php"; ?>
    </div>

    <!-- Scripts -->
    <?php include "script.php"; ?>
</body>
</html>

