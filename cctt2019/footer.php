<div class="bg-azul pt-2" style="border-top: 2px solid #64c542; "><br>
    <div class="container foot">
        <div class="row justify-content-center">
            <div class="col-xs-12 text-left col-sm-12 col-md-10 col-lg-10 mb-sm-3 mb-3 text-bold text-center">
                <h2 class="text-dark text-regular text-azul-cielo">XIII Congreso Colombiano de Transporte y Tránsito 2019</h2><br>
                <div class="text-dorado h4 text-regular text-dark mb-2">Siguenos en:</div>
                <div class="text-regular text-dark d-inline-flex text-center mx-auto">
                    <a href="http://www.facebook.com" target="_blank" class="ml">
                        <figure>
                            <img src="http://cosmoid.co/pagina-web/public/images/facebook.png" alt="" class="img-fluid d-block">
                        </figure>
                    </a>
                    <a href="http://www.instagram.com" target="_blank" class="ml-3">
                        <figure>
                            <img src="http://cosmoid.co/pagina-web/public/images/instagram.png" alt="" class="img-fluid d-block">
                        </figure>
                    </a>
                </div>
            </div>
        </div>
    </div>
</div>
<div class="copyright text-center bg-azul text-white text-regular small p-2">
    <div class="row justify-content-center">
        <div class="col-md-9">
            Universidad Tecnológica de Bolívar - 2017 Institución de Educación Superior sujeta a inspección y vigilancia por el Ministerio de Educación Nacional.
            Resolución No 961 del 26 de octubre de 1970 a través de la cual la Gobernación de Bolívar otorga la Personería Jurídica a la Universidad Tecnológica de Bolívar. <br><br>
            Copyright 2018. Todos los derechos reservados
        </div>
    </div>
</div>
