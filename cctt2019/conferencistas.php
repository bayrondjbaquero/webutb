<!DOCTYPE html>
<html lang="es">
<head>
    <?php include "head.php"; ?>
</head>
<body class="">
    <div id="app" class="mx-auto">
            <nav class="navbar navbar-expand-lg navbar-light justify-content-center fixed-top">
                <?php include "menu.php"; ?>
            </nav>
            <br><br><br>
            <!-- <div id="carouselExampleControls" class="carousel slide carousel-fade row" data-ride="carousel">
                <ol class="carousel-indicators">
                    <li data-target="#carouselExampleIndicators" data-slide-to="0" class="active"></li>
                    <li data-target="#carouselExampleIndicators" data-slide-to="1"></li>
                    <li data-target="#carouselExampleIndicators" data-slide-to="2"></li>
                    <li data-target="#carouselExampleIndicators" data-slide-to="3"></li>
                </ol>
                <div class="carousel-inner animated fadeIn">
                    <div class="carousel-item active">
                        <img class="d-block w-100 img-fluid" src="http://www.utb.edu.co/sites/web.unitecnologica.edu.co/files/banner_home_mesa_de_trabajo_1.jpg" alt="First slide">
                    </div>
                    <div class="carousel-item">
                        <img class="d-block w-100 img-fluid" src="http://www.utb.edu.co/sites/web.unitecnologica.edu.co/files/banner_grammy.png" alt="Second slide">
                    </div>
                </div>
                <a class="carousel-control-prev" href="#carouselExampleControls" role="button" data-slide="prev">
                    <span class="carousel-control-prev-icon" aria-hidden="true"></span>
                    <span class="sr-only">Previous</span>
                </a>
                <a class="carousel-control-next" href="#carouselExampleControls" role="button" data-slide="next">
                    <span class="carousel-control-next-icon" aria-hidden="true"></span>
                    <span class="sr-only">Next</span>
                </a>
            </div> -->
        <div class="container pt-3">
            <div class="row justify-content-center">
                <div class="col-12 col-sm-12 col-md-12 col-lg-12 col-xl-12 bg-white box pt-4 pl-4 pr-4 pb-4 animated fadeInUp mb-2">
                    <h4 class="w-100 text-regular pb-2 m-0 rounded-top primary">
                        Conferencistas invitados
                    </h4>
                    <hr>
                    <div class="row justify-content-center">
                        <div class="col-md-4">
                            <figure>
                                <img src="http://www.utb.edu.co/sites/web.unitecnologica.edu.co/files/img_8511.jpg" alt="" class="img-fluid img-responsive d-block">
                            </figure>
                            <div class="text-center text-dark">Servio Rodriguez Díaz</div>
                        </div>
                        <div class="col-md-4">
                            <figure>
                                <img src="http://www.utb.edu.co/sites/web.unitecnologica.edu.co/files/maria-claudia-penas-arana-834x1024.jpg" alt="" class="img-fluid img-responsive d-block">
                            </figure>
                            <div class="text-center text-dark">María Claudia Peñas Arana </div>
                        </div>
                        <div class="col-md-4">
                            <figure>
                                <img src="http://www.utb.edu.co/sites/web.unitecnologica.edu.co/files/javier_campillo_consejo_superior_4.jpg" alt="" class="img-fluid img-responsive d-block">
                            </figure>
                            <div class="text-center text-dark">Alejandro Espinosa Harris </div><br>
                        </div>
                        <div class="col-md-4">
                            <figure>
                                <img src="http://www.utb.edu.co/sites/web.unitecnologica.edu.co/files/ALVARO%20CUBAS%20MONTES.JPG" alt="" class="img-fluid img-responsive d-block">
                            </figure>
                            <div class="text-center text-dark">Álvaro Luis Cubas Montes</div>
                        </div>
                        <div class="col-md-4">
                            <figure>
                                <img src="http://www.utb.edu.co/sites/web.unitecnologica.edu.co/files/img_6031.jpg" alt="" class="img-fluid img-responsive d-block">
                            </figure>
                            <div class="text-center text-dark">Jaime Hernández Herrera </div>
                        </div>
                        <div class="col-md-4">
                            <figure>
                                <img src="http://www.utb.edu.co/sites/web.unitecnologica.edu.co/files/la_directora_de_internacionalizacion_erika_duncan_y_el_director_-_ericka_duncan.jpg" alt="" class="img-fluid img-responsive d-block">
                            </figure>
                            <div class="text-center text-dark">Ericka Duncan Ortega </div>
                        </div>
                    </div>
                    <!-- <figure>
                        <img src="images/Energia Solar.jpg" alt="" class="img-fluid d-block mx-auto">
                    </figure> -->
                </div>              
                <hr>
            </div>
        </div>
        <?php include "footer.php"; ?>
    </div>

    <!-- Scripts -->
   <?php include "script.php"; ?>
</body>
</html>

