<!DOCTYPE html>
<html lang="es">
<head>
    <?php include "head.php"; ?>
</head>
<body class="">
    <div id="app" class="mx-auto">
            <nav class="navbar navbar-expand-lg navbar-light justify-content-center fixed-top">
                <?php include "menu.php"; ?>
            </nav>
            <br><br><br>
            <!-- <div id="carouselExampleControls" class="carousel slide carousel-fade row" data-ride="carousel">
                <ol class="carousel-indicators">
                    <li data-target="#carouselExampleIndicators" data-slide-to="0" class="active"></li>
                    <li data-target="#carouselExampleIndicators" data-slide-to="1"></li>
                    <li data-target="#carouselExampleIndicators" data-slide-to="2"></li>
                    <li data-target="#carouselExampleIndicators" data-slide-to="3"></li>
                </ol>
                <div class="carousel-inner animated fadeIn">
                    <div class="carousel-item active">
                        <img class="d-block w-100 img-fluid" src="http://www.utb.edu.co/sites/web.unitecnologica.edu.co/files/banner_home_mesa_de_trabajo_1.jpg" alt="First slide">
                    </div>
                    <div class="carousel-item">
                        <img class="d-block w-100 img-fluid" src="http://www.utb.edu.co/sites/web.unitecnologica.edu.co/files/banner_grammy.png" alt="Second slide">
                    </div>
                </div>
                <a class="carousel-control-prev" href="#carouselExampleControls" role="button" data-slide="prev">
                    <span class="carousel-control-prev-icon" aria-hidden="true"></span>
                    <span class="sr-only">Previous</span>
                </a>
                <a class="carousel-control-next" href="#carouselExampleControls" role="button" data-slide="next">
                    <span class="carousel-control-next-icon" aria-hidden="true"></span>
                    <span class="sr-only">Next</span>
                </a>
            </div> -->
        <div class="container pt-3"><br>
            <div class="col-12 col-sm-12 col-md-12 col-lg-12 col-xl-12 bg-white box pt-4 pl-4 pr-4 pb-4 animated fadeInUp mb-2">
                <div class="row justify-content-center">
                    <div class="col-md-6 mb-4">
                        <a href="#" data-toggle="modal" data-target="#myModalHOTELES">
                            <h3 class="w-100 text-regular m-0 rounded-top text-center mb-3 text-azul">
                                Hoteles
                            </h3>
                                <img src="http://www.unitecnologica.edu.co/newsletter/Conectate/institucional/2017/hoteles.jpg" alt="" class="img-responsive center-block img-fluid">
                        </a>
                    </div>
                    <div class="col-md-6 mb-4">
                        <a href="#" data-toggle="modal" data-target="#myModalRESTAURANTES">
                            <h3 class="w-100 text-regular m-0 rounded-top text-center mb-3 text-azul">
                                Restaurantes
                            </h3>
                                <img src="http://www.unitecnologica.edu.co/newsletter/Conectate/institucional/2017/restaurante.jpg" alt="" class="img-responsive center-block img-fluid">
                        </a>
                    </div>
                    <div class="col-md-6 mb-4">
                        <a href="#" data-toggle="modal" data-target="#myModalCULTURA">
                            <h3 class="w-100 text-regular m-0 rounded-top text-center mb-3 text-azul">
                                Cultura
                            </h3>
                                <img src="http://www.unitecnologica.edu.co/newsletter/Conectate/institucional/2017/cultura.jpg" alt="" class="img-responsive center-block img-fluid">
                        </a>
                    </div>
                    <div class="col-md-6 mb-4">
                        <a href="#" data-toggle="modal" data-target="#myModalUTB">
                            <h3 class="w-100 text-regular m-0 rounded-top text-center mb-3 text-azul">
                                UTB
                            </h3>
                                <img src="http://www.unitecnologica.edu.co/newsletter/Conectate/institucional/2017/utb.jpg" alt="" class="img-responsive center-block img-fluid">
                        </a>
                    </div>
                </div>              
                <hr>
            </div>
        </div>
        <?php include "footer.php"; ?>
    </div>
    <div class="modal fade in" id="myModalUTB" role="dialog" >
        <div class="modal-dialog modal-lg">

            <!-- Modal content-->
            <div class="modal-content">
                <div class="modal-header">
                    <h4 class="modal-title">Información</h4>
                    <button type="button" class="close" data-dismiss="modal">×</button>
                </div>
                <div class="modal-body">


                    <div class="row">

                        <div class="col-md-6">
                            <h3 class="section-title st-blue">Universidad Tecnológica de Bolívar</h3>
                            <img src="http://www.unitecnologica.edu.co/newsletter/Conectate/institucional/2017/utb.jpg" class="img-responsive img-fluid"><br>
                            <p>La Universidad Tecnológica de Bolívar (UTB)  es una institución privada sin ánimo de lucro, fundada en 1970 en Cartagena, Colombia  y dirigida por los principales gremios económicos de la ciudad. La UTB fue la primera universidad en la ciudad en recibir la acreditación de alta calidad que es el máximo reconocimiento de calidad otorgado por el Ministerio de Educación de Colombia.</p>
                            <p>La oferta académica de la UTB está compuesta por 18 programas de pregrado, 21 especializaciones, 20 maestrías y un doctorado. Este portafolio está organizado en seis facultades: Ingeniería, Economía y Negocios, Ciencias Sociales y Humanas, Ciencias Básicas, Educación y Técnicas y Tecnológicas.</p>
                            <p>La universidad tiene una población de 6.000 estudiantes de pregrado y más de 13.000 egresados. A lo largo de sus de 46 años se ha convertido en una institución de docencia con  investigación y  vocación empresarial, para compartir un proyecto educativo responsable, que contribuye a la calidad de vida de toda la región.  El desarrollo social, humano y empresarial es una prioridad misional, por ello los esfuerzos en formación, investigación y extensión se orientan con el propósito de generar impacto social.</p>
                        </div>


                        <div class="col-md-6">
                            <h3 class="section-title st-blue">Cartagena de Indias</h3>
                            <img src="http://www.unitecnologica.edu.co/newsletter/Conectate/institucional/2017/cartagena.jpg" class="img-responsive img-fluid"><br>

                            <p>"Cartagena de Indias está localizada en la Costa Caribe colombiana, fue declarada en 1984 por la UNESCO como patrimonio histórico de la humanidad y es una de las ciudades más bellas de América.</p>

                            <p>Actualmente, la ciudad es escenario de importantes eventos culturales de talla internacional como el Festival Internacional de Cine, Hay Festival y el Festival Internacional de Música, entre otros. Todo lo anterior hace que la ciudad se convierta en un excelente lugar para la realización de eventos académicos internacionales.</p>

                            <p>Cartagena combina su riqueza histórica y cultural con un notable complejo industrial y un puerto eficiente. La Zona Industria de Mamonal, localizada al sureste de la ciudad, alberga más de 100 empresas manufactureras entre las que se incluyen plantas petroquímicas, de biocombustible y agroindustriales. Además de ser considerada la primera ciudad industrial del Caribe Colombiano y la quinta del país, Cartagena cuenta con una excelente infraestructura de puertos logísticos, haciéndola así uno de los destinos de inversión más competitivos del Caribe Colombiano. Cuenta, además, con el puerto más eficiente del país y el de mayor confiabilidad en el Caribe.</p>

                            <p>También es considerada el destino turístico más importante de Colombia y el tercer mejor lugar para el turismo en América Latina. La ciudad ha desarrollado una infraestructura hotelera de talla mundial y ha atraído cadenas internacionales como Sheraton, Hilton, Hyatt, Radisson, Holiday Inn, entre otras. </p>

                            <p>Todo lo anterior hace que la ciudad se convierta en un excelente lugar para que estudiantes, docentes y académicos internacionales que visiten la ciudad tengan una experiencia académica y cultural inolvidable.</p>

                            <p> <i>Me bastó dar un paso dentro de la muralla, para verla en toda su grandeza a la luz malva de las seis de la tarde y no pude reprimir el sentimiento de haber vuelto a nacer</i> -Gabriel García Marquez"</p>
                        </div>

                    </div>

                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-default" data-dismiss="modal">Cerrar</button>
                </div>
            </div>

        </div>
    </div>
    <div class="modal fade in" id="myModalRESTAURANTES" role="dialog" >
        <div class="modal-dialog modal-lg">

            <!-- Modal content-->
            <div class="modal-content">
                <div class="modal-header">
                    <h4 class="modal-title">Restaurantes</h4>
                    <button type="button" class="close" data-dismiss="modal">×</button>
                </div>
                <div class="modal-body">


                    <div class="row">

                        <div class="col-md-12">
                            <h3 class="section-title st-blue">RESTAURANTES<small> <br>Fuente: <a href="https://cartagena.restorando.com.co/" target="_blank">https://cartagena.restorando.com.co/</a></small></h3>

                            <strong>Marea by Rausch</strong><br>
                            <a href="http://www.mareabyrausch.com" target="_blank">http://www.mareabyrausch.com/</a><br>
                            Centro de Convenciones Cartagena de Indias<br>
                            +57 (5) 654 4205<br><br>

                            <strong>El Gobernador by Rausch</strong><br>
                            <a href="http://www.criterion.com.co/el-gobernador.html" target="_blank">http://www.criterion.com.co/el-gobernador.html</a><br>
                            Calle del Sargento Mayor #6 - 68‎<br>
                            +57 (5) 642 4100<br><br>

                            <strong>Quebracho</strong><br>
                            <a href="http://www.restaurantequebracho.com/" target="_blank">http://www.restaurantequebracho.com/</a><br>
                            Zona - Centro Amurallado Calle Baloco Nº 2-69<br>
                            +57 (5) 6641300<br><br>

                            <strong>Vera </strong><br>
                            <a href="http://www.tcherassihotels.com/es-es/dining.htm" target="_blank">http://www.tcherassihotels.com/es-es/dining.htm</a><br>
                            Calle Plaza Fernández de Madrid, Cl. 38 #6-21 <br>
                            +57 (5) 6644445<br><br>

                            <strong>La Girolata</strong><br>
                            <a href="http://www.lagirolata.co/menu" target="_blank"> http://www.lagirolata.co/menu</a><br>
                            Calle No 36-86, Playa de la Artilleria<br>
                            +57 (5) 6602399<br><br>

                            <strong>La Perla</strong><br>
                            <a href="https://goo.gl/MGk7fV" target="_blank">https://goo.gl/MGk7fV</a><br>
                            Centro, Calle de Ayos #4-42<br>
                            +57 (5) 6642157<br><br>

                            <strong>Restaurante 1621</strong><br>
                            <a href="https://goo.gl/hmfHhW" target="_blank">https://goo.gl/hmfHhW</a><br>
                            Sofitel Legend Santa Clara<br>
                            +57 (5) 6504741<br><br>

                            <strong>Don Juan</strong><br>
                            <a href="http://donjuancartagena.com/" target="_blank">http://donjuancartagena.com/</a><br>
                            Calle del Colegio # 34-60 Local 1 <br>
                            +57 (5) 6643678<br><br>

                            <strong>Agua de Mar Tapas Gourmet &amp; Gin Bar</strong><br>
                            <a href="http://www.aguademar.com.co/" target="_blank">http://www.aguademar.com.co/</a><br>
                            Centro, Calle del Santísimo #8-15<br>
                            +57 (5) 6645798<br><br>

                            <strong>Brujas de Cartagena</strong><br>
                            <a href="http://www.brujasdecartagena.com.co/" target="_blank">http://www.brujasdecartagena.com.co/</a><br>
                            Playa de la Artilliera #36-38, Cartagena<br>
                            +57 (5) 6604772<br><br>


                            <strong>La Cocina de Pepina</strong><br>
                            Barrio Getsemaní Callejón Vargas Numero 9A – 06<br>
                            +57 3008565189<br><br>

                            <strong>El Burlador de Sevilla</strong><br>
                            <a href="http://elburladordesevilla.com/" target="_blank">http://elburladordesevilla.com/</a><br>
                            Carrera 3 # 33-88, Calle Santo Domingo<br>
                            +57 (5) 6600866<br><br>

                            <strong>Di Silvio Trattoria</strong><br>
                            <a href="http://www.disilviotrattoria.com/" target="_blank">http://www.disilviotrattoria.com/</a><br>
                            Centro Histórico Cl. 29 #9A-08<br>
                            +57 (5) 6602205<br><br>

                            <strong>Demente Tapas Bar</strong><br>
                            Centro Histórico Cl. 39 #7 14<br>
                            +57 (5) 6604226<br><br>


                            <strong>La Cevichería </strong><br>
                            <a href="http://lacevicheriacartagena.com/" target="_blank">http://lacevicheriacartagena.com/</a><br>
                            Cl. 39 #7 14<br>
                            +57 (5) 6601492<br><br>

                            <strong>CoCo Club Social</strong><br>
                            <a href="https://www.facebook.com/cococlubsocial/" target="_blank">https://www.facebook.com/cococlubsocial/</a><br>
                            Calle Gastelbondo No 02-124 Local 8<br>
                            +57 (5) 6602740<br><br>

                            <strong>La Mulata</strong><br>
                            <a href="http://lamulatacartagena.blogspot.com.co/p/menu.html" target="_blank">http://lamulatacartagena.blogspot.com.co/p/menu.html</a><br>
                            Calle Quero #9 - 58 San diego<br>
                            +57 (5) 6646222<br><br>

                            <strong>Perú Fusión</strong><br>
                            Centro Histórico Cl. 35 #4<br>
                            +57 (5) 6605243<br><br>

                            <strong>Restaurante 241</strong><br>
                            Centro Histórico Tv. 54 #41<br>
                            +57 (5) 6670791<br><br>

                            <strong>Club de Pesca</strong><br>
                            <a href="http://clubdepesca.com/es/" target="_blank">http://clubdepesca.com/es/</a><br>
                            Avenida Miramar, Barrio Manga<br>
                            +57 (5) 6604594<br><br>

                            <strong>De Oliva</strong><br>
                            <a href="http://www.deolivarestaurante.com/" target="_blank">http://www.deolivarestaurante.com/</a><br>
                            Cra. 17 #24-116, Barrio Manga<br>
                            +57 (5) 6606861<br><br>

                            <strong>Restaurante Árabe Internacional</strong><br>
                            <a href="http://www.restaurantearabeinternacional.com/" target="_blank">http://www.restaurantearabeinternacional.com/</a><br>
                            Cra. 3#8-83, Bocagrande<br>
                            +57 (5) 6654365<br><br>

                            <strong>Chico y Rita</strong><br>
                            Calle 2da de Badillo #36-96<br>
                            +57 3176566352<br><br>

                            <strong>Juan del Mar Pizzería Gourmet</strong><br>
                            <a href="http://www.juandelmar.com/es" target="_blank">http://www.juandelmar.com/es</a><br>
                            Calle Plaza de San diego, Cl. 39 #38-18<br>
                            +57 (5) 6642782<br><br>

                            <strong>Carmen Cartagena</strong><br>
                            <a href="www.carmencartagena.com/" target="_blank">www.carmencartagena.com/</a><br>
                            Calle 38 # 8-19, Calle del Santísimo<br>
                            +57 (5) 6645116 <br><br>

                            <strong>Az-zahr</strong><br>
                            <a href="http://www.lurecartagena.com/az-zahr/" target="_blank">http://www.lurecartagena.com/az-zahr/</a><br>
                            Calle de la Artillería No 33-24<br>
                            +57 (5) 6600820<br><br>

                            <strong>María</strong><br>
                            <a href="http://www.mariacartagena.com/" target="_blank">http://www.mariacartagena.com/</a><br>
                            Calle del colegio No. 34-64 Local 2<br>
                            +57 3165247046<br><br>

                            <strong>Red Knife Parrilla de Autor</strong><br>
                            <a href="http://www.allurechocolathotel.com/restaurante-red-knife/" target="_blank">http://www.allurechocolathotel.com/restaurante-red-knife/</a><br>
                            Calle Larga No 8B-58<br>
                            +57 (5) 6605831<br><br>

                            <strong>La Bruschetta</strong><br>
                            <a href="http://www.labruschetta.com.co/" target="_blank">http://www.labruschetta.com.co/</a><br>
                            Centro Calle del Curato No 38 – 135<br>
                            +57 (5) 6600016<br><br>

                            <strong>La Tinaja</strong><br>
                            <a href="http://latinajacartagena.blogspot.com.co/" target="_blank">http://latinajacartagena.blogspot.com.co/</a><br>
                            Carrera 10 No. 10 – 36<br>
                            +57 (5) 6641019<br><br>

                            <strong>Plaza Majagua</strong><br>
                            <a href="http://plazamajagua.com/" target="_blank">http://plazamajagua.com/</a><br>
                            Parque Fernández Madrid, Calle de la Tablada No. 7-12<br>
                            +57 (5) 6647958<br><br>

                            <strong>Bacco Trattoria</strong><br>
                            <a href="https://baccotrattoria.com.co/" target="_blank">https://baccotrattoria.com.co/</a><br>
                            Calle Quero #9-14<br>
                            +57 (5) 6640159<br><br>

                            <strong>Candé</strong><br>
                            <a href="http://restaurantecande.com/" target="_blank">http://restaurantecande.com/</a><br>
                            Calle Estanco del Tabaco, No. 35-30<br>
                            +57 (5) 6685291<br><br>

                            <strong>El Kilo</strong><br>
                            Calle 2 de Badillo #36-51<br>
                            +57 (5) 6641779<br><br>


                            <strong>Zaitún</strong><br>
                            Calle de Ayos, Cra 4 No. 34-37<br>
                            +57 (5) 6606204<br><br>

                            <strong>Bohemia</strong><br>
                            <a href="http://www.bohemia.com.co/index2.html" target="_blank"> http://www.bohemia.com.co/index2.html</a><br>
                            Calle Nuestra Señora del Carmen #33-41<br>
                            +57 5 6644438<br><br>

                            <strong>La Diva</strong><br>
                            Calle Ricaurte 31-38, Centro Histórico<br>
                            +57 3218587424<br><br>

                            <strong>Donde Olano</strong><br>
                            <a href="http://www.dondeolano.com/" target="_blank">http://www.dondeolano.com/</a><br>
                            Calle Santo Domingo No 33-81<br>
                            +57 (5) 6647099<br><br>

                            <strong>Señor Toro</strong><br>
                            Calle Santo Domingo Cra 3 No. 35-55 <br>
                            +57 (5) 6601740<br><br>

                            <strong>El Bistro</strong><br>
                            <a href="http://elbistrocartagena.wixsite.com/elbistro" target="_blank">http://elbistrocartagena.wixsite.com/elbistro</a><br>
                            Calle de Ayos No 4-46<br>
                            +57 (5) 6602065<br><br>


                            <strong>Oh La La</strong><br>
                            Barrio Getsemaní Calle de Ayos Cr 5 No 4-48<br>
                            +57 (5) 6644321<br><br>

                            <strong>La Vitrola</strong><br>
                            Calle Baloco No 33-201<br>
                            +57 (5) 6600711<br>


                        </div>


                    </div>

                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-default" data-dismiss="modal">Cerrar</button>
                </div>
            </div>

        </div>
    </div>
    <div class="modal fade in" id="myModalCULTURA" role="dialog" >
        <div class="modal-dialog modal-lg">

            <!-- Modal content-->
            <div class="modal-content">
                <div class="modal-header">
                    <h4 class="modal-title">Cartagena</h4>
                    <button type="button" class="close" data-dismiss="modal">×</button>
                </div>
                <div class="modal-body">


                    <div class="row">

                        <div class="col-md-12">
                            <h3 class="section-title st-blue">¿Qué conocer?<small><br>Fuentes: Cartagena Caribe y Colombia Travel</small></h3>
                            <hr>
                            <h4 class="section-title st-blue">Museos</h4>
                            <h5>Palacio de La Inquisición -  Museo de Historia de Cartagena</h5>
                            <p>
                                El Palacio de la Inquisición de Cartagena de Indias, es una típica casona de la época de la colonia. Fue la sede del Tribunal de Penas del Santo Oficio de la Inquisición, donde se juzgaban los delitos contra la fé cristiana. El Palacio incluía cárceles y cámaras de tortura. Hoy en día se puede visitar su interior y recorrer el museo, donde se pueden ver objetos de tortura de la colonia, y recorrer las salas donde se muestra cómo vivían los indígenas de la zona antes de la llegada de los españoles y cómo fue el proceso de ocupación española, los años de la Inquisición y la Independencia, cuyo movimiento desplazó a los inquisidores de Cartagena.
                                <br>El Palacio de la Inquisición de Cartagena de Indias está ubicado dentro del recinto amurallado frente al Parque Bolivar y puede visitarse de lunes a sábados de 9:00 a 19:00 hs. y los domingos y feriados de 10:00 a 16:00 hs.
                            </p>
                            <hr>
                            <h5>Museo del Oro</h5>
                            <p> El Museo del Oro Zenú hace parte de la red de museos del Oro del Banco de la República y abrió sus puertas al público en el año 1982. A través de sus salas se presenta una muestra representativa de las sociedades de las distintas zonas arqueológicas orfebres del territorio colombiano, sus sistemas de pensamiento, formas de organización social y de adaptación a las condiciones del medio ambiente, en particular en las llanuras del Caribe colombiano. Ubicación: Centro, Carrera 4 N° 33-26 Parque de Bolívar Horarios de atención: Martes a Sábados De 9:00 a.m. a 5:00 p.m; Domingos, de 10:00 a.m. a 3:00 p.m. Cerrado los lunes. Precio: Entrada gratuita</p>

                            <hr>
                            <h5>Museo de Arte Moderno</h5>
                            <p>Exposiciones permanentes e itinerantes de arte pictórico y escultórico. Frente a él se encuentra la Plaza de San Pedro, en donde hacen alusión al museo varias esculturas (chatarras) de hierro que representan los oficios del hombre costeño.</p>

                            <hr>
                            <h5>Museo Naval</h5>
                            <p>Ubicado en la calle San Juan de Dios, este singular museo contiene piezas y elementos relacionados con la historia naval colombiana. A su costado se encuentra la oficina del Festival Internacional de Cine de Cartagena, reconocible por una escultura de la India Catalina, imagen del galardón que año tras año premia a lo mejor del cine.</p>

                            <hr>
                            <h5>Casa Museo Rafael Núñez</h5>
                            <p>En esta Casa vivió el cuatro veces presidente y poeta colombiano Rafael Núñez. Es una muestra de la arquitectura criolla y de estilo antillano en madera del siglo XIX que hoy tiene las puertas abiertas al público como museo en donde están exhibidos objetos personales y algunas obras de su autoría.</p>

                            <hr>
                            <h5>Museo de las Fortificaciones</h5>
                            <p>Fue inagurado en 1994. Reúne piezas de los monumentos fortificados de Cartagena y museos de sitio en el Baluarte de Santa Catalina y Fuerte de San Bernando de Bocachica, Castillo San Felipe de Barajas, Batería del Ángel San Rafael, Cordón Amurallado, Fuerte de San Fernando, Fuerte de San josé y el Parque Histórico Isla de Tierrabomba. Este museo está ubicado en el inferior del baluarte de Santa Catalina. Es una muestra del interior de la muralla que bordea la ciudad.</p>
                            <hr>
                            <h4 class="section-title st-blue">Monumentos</h4>
                            <h5>Castillo de San Felipe de Barajas</h5>
                            <p>
                                El Castillo de San Felipe de Barajas es una fortaleza ubicada fuera de la&nbsp;Ciudad Amurallada, en un punto estratégico desde donde antaño se controlaban las invasiones, tanto por tierra como por mar. Esta construcción del siglo XVII tardó más un siglo en completarse. Resistió varios asaltos y ataques a la ciudad. Al visitar la fortaleza,&nbsp;se recomienda acudir con un guía o rentar una de las audio-guías que se ofrecen en el ingreso o que pueden ser contratadas previamente.
                                <br> Detrás del Castillo de San Felipe de Barajas&nbsp;está el&nbsp;Monumento a los Zapatos Viejos. Es un homenaje a Luis Carlos López,&nbsp;uno de los más grandes poetas de Cartagena de Indias.
                            </p>

                            <hr>
                            <h5>La Torre del Reloj</h5>
                            <p>
                                La Torre del Reloj es&nbsp;el símbolo más representativo de la Ciudad Amurallada. La torre, llamada también&nbsp;Boca del Puente, fue construida sobre la muralla en el siglo XIX. Está sobre la&nbsp;entrada principal del centro histórico&nbsp;de Cartagena de Indias, dando acceso a la&nbsp;Plaza de los Coches. Antiguamente era único ingreso hacia el interior de las murallas.
                                <br>  Cercana a la&nbsp;Torre del Reloj, se alza la estatua de&nbsp;Pedro de Heredia, fundador de la ciudad.
                            </p>

                            <hr>
                            <h5>Cerro y Convento de La Popa</h5>
                            <p>
                                El&nbsp;Cerro de La Popa&nbsp;es el accidente geográfico más elevado de la ciudad de Cartagena de Indias. Se encuentra&nbsp;a 10 minutos del&nbsp;centro histórico&nbsp;de la ciudad y desde allí puede verse el Mar Caribe,&nbsp;la Isla de Tierrabomba, el centro amurallado, La Boquilla, la zona norte&nbsp;y Bocagrande.&nbsp;
                            </p><p>
                                Los españoles que lo descubrieron en 1510 se imaginaron ver una enorme galera que sobresalía del mar, por lo que lo llamaron Cerro de la Galera, y a la cima, La Popa de la Galera. Los antiguos cartageneros también lo llamaron Cerro de la Cruz por la cruz que lo corona.&nbsp;Sobre su cima se encuentra una hermosa iglesia&nbsp;y convento colonial construidos entre 1609 y 1611. El claustro del convento de la Popa es&nbsp;uno de los más bellos de la ciudad colonial de Cartagena&nbsp;y de toda Colombia.
                                <br> El paseo al&nbsp;Cerro La Popa&nbsp;suele estar incluido en los tours&nbsp;que se hacen alrededor de la ciudad. El horario de atención es de lunes a domingo de 8:30 a 17:30 hs.
                            </p>

                            <hr>
                            <h5>Plaza de Santo Domingo</h5>
                            <p>
                                La Plaza de Santo Domingo es uno de los sitios más animados de la ciudad, tanto de día como de noche. El lugar está rodeado de restaurantes y cafés, donde puedes sentarte a tomar una copa, mientras escuchas a los músicos que van por las mesas tocando ritmos locales a cambio de una propina.  En una de las esquinas de la plaza de Santo Domingo está la Iglesia de Santo Domingo, el templo más antiguo de la ciudad el cual da nombre a la plaza.
                                <br>  Cuando visites la plaza, no te olvides de buscar a la gorda Gertrudis. No es una persona, es una escultura de Botero de una mujer desnuda recostada. Se dice que al tocar los senos de Gertrudis, te aseguras un largo romance con tu pareja. Además se cree que eso también asegura tu regreso a la ciudad de Cartagena.
                            </p>

                            <hr>
                            <h5> Plaza de los Coches</h5>
                            <p>
                                El Portal de los Dulces es el lugar ideal para saborear los dulces más típicos de la ciudad.
                                <br>  El Portal de los Dulces se encuentra frente a la Torre del Reloj, al otro lado de la Plaza de los Coches y es la primera vista que recibe al visitante al ingresar a la Ciudad Amurallada.
                            </p>

                            <hr>
                            <h5>Las Bóvedas</h5>
                            <p>
                                Las Bóvedas de Santa Clara fueron construidas a finales del siglo XVIII como depósitos de municiones. Más tarde fueron utilizadas como cárceles.  Las 23 bóvedas que conforman las Bóvedas de Santa Clara se encuentran en el extremo noreste de la ciudad amurallada donde actualmente funciona allí un conjunto de tiendas de artesanías y objetos típicos de la región.
                                <br> Los precios que se encuentran en sus negocios pueden ser un poco más elevados que en otras zonas de Cartagena, pero es un bonito paseo y podrás encontrar mayor variedad de suvenires que en otros sitios.
                            </p>

                            <hr>
                            <h5>Monumento a la India Catalina </h5>
                            <p>
                                Se trata de una de las obras maestras del escultor Eladio Gil. La india simboliza la raza nativa. Se dice que Catalina, bella y valerosa guerrera nacida en la vecina localidad de Galerazamba, fue capturada por Alonso de Ojeda una vez terminado el motín en que murió el cartagenero Juan de la Cosa, en 1509. Posteriormente, Diego de Nicuesa se la llevó para Centroamérica y la vendió como esclava en Santo Domingo. Don Pedro de Heredia la trajo nuevamente a Cartagena como intérprete, en 1533.
                            </p>

                            <hr>
                            <h5>Monumento a Gabriel García Márquez</h5>
                            <p>
                                En el Claustro de la Merced, en el Centro Histórico de Cartagena, se encuentran las cenizas de Gabriel García Márquez, fallecido a los 87 años el 17 de abril de 2014. Las cenizas están contenidas en una plataforma flotante sobre la que se encuentra un busto de la imagen de Gabo. Este monumento fue esculpido por la artista británica Katie Murray.
                            </p>

                            <hr>
                            <h5>Teatro Heredia</h5>
                            <p>
                                Construido sobre las ruinas de la antigua Iglesia de La Merced (1625) para conmemorar el Centenario de la Independencia en 1911. En 1998 fue restaurado bajo la dirección del arquitecto cartagenero Alberto Samudio Trallero como centro cultural de artes escénicas y musicales. Teatro construido en forma de herradura con palcos y balcones divididos por celosías de cero, que parecen encajes, originalmente servían para ventilación. Escaleras y esculturas de mármol italiano con telón de Boca y techo bellamente creado por el artista cartagenero Enrique Grau.
                            </p>

                            <hr>
                            <h4 class="section-title st-blue">Playas</h4>
                            <h5>Islas del Rosario</h5>
                            <p>
                                Las&nbsp;Islas del Rosario&nbsp;son un archipiélago de arenas blancas y aguas cristalinas y templadas. Estas islas se encuentran a una hora en bote desde Cartagena de Indias. Es un&nbsp;sitio ideal para relajarse y disfrutar del mar Caribe, y también para practicar&nbsp;buceo o snorkel. Si se va con tiempo, puede visitarse también el&nbsp;Oceanario. La playa más visitada de las islas es la&nbsp;Playa Blanca, de&nbsp;Isla Barú. Es necesario llevar bloqueador solar, comidas, bebidas y en lo posible, una sombrilla. Los tours diarios a&nbsp;Islas del Rosario&nbsp;salen desde Cartagena a primera hora de la mañana y vuelven comenzando la noche.&nbsp;Los tours suelen incluir el transporte, las comidas y bebidas, los chalecos salvavidas y el servicio de los guías.
                            </p>

                            <hr>
                            <h5>Isla de Barú&nbsp;</h5>
                            <p>
                                Isla Barú&nbsp;es en realidad una paradisíaca península en el&nbsp;mar Caribe. Barú es donde se encuentra la playa más visitada de todas las&nbsp;Islas del Rosario:&nbsp;Playa Blanca. Otros puntos de interés en&nbsp;Isla Barú&nbsp;son Puerto Naito, Playa Bobo, Playa de Cholón, Playa Mar Azul, Punta Iguana y Playa de los muertos. Desde la ciudad de Cartagena de Indias salen&nbsp;tours diarios&nbsp;a las islas, incluyendo el paseo por&nbsp;Isla Barú.
                            </p>
                            <hr>
                            <h4 class="section-title st-blue">Otros</h4>
                            <h5>Barrio Getsemaní</h5>
                            <p>
                                Getsemaní es uno de los barrios más representativos de la ciudad, dado que allí se dio el Grito de Independencia en 1811. Getsemaní es conocido por recibir gran cantidad de mochileros, por sus tiendas de oficios (modistas, zapateros) y por su Parque Centenario. Era el barrio donde habitaban los esclavos en la época de la Colonia.
                                <br> Getsemaní se ubica cerca del centro y a la Ciudad Amurallada.
                            </p>

                            <hr>
                            <h5>Café del Mar</h5>
                            <p>
                                Café del Mar Cartagena de Indias es el bar más profesional y mejor dotado, con más de 50 cocktails, las mejores cervezas, vinos y champañas. El restaurante brilla por una cocina delicadamente decorada, en la que solo se trabaja con los mejores ingredientes. En su menú se pueden encontrar desde pequeñas golosinas y finas ensaladas hasta las más exquisitas carnes y mariscos. En el campo Nacional o Internacional, sus visitantes pueden seleccionar platos de una variada carta, e incluso gourmets experimentados estarán satisfechos con su repertorio de comidas. El atractivo principal de este lugar es su ubicación estratégica sobre la muralla y en frente del mar que en el horario de 4:30 – 05:30 pm se puede disfrutar del hermoso atardecer del corralito de piedra.
                                <br> Sus horarios son de domingos a jueves, de 17:00 a 3:00 hs. de Viernes a sábados de 17:00 a 4:00 hs.
                            </p>

                            <hr>
                            <h5> Volcán del Totumo</h5>
                            <p>
                                El Volcán del Totumo es un volcán de lodo de 15 metros de altura. En la cima del volcán se ha formado una especie de piscina de lodo, tibio y denso, donde los visitantes pueden bañarse y gozar además de sus propiedades terapéuticas para la salud de la piel.
                                <br>A la cima se accede a través de una escalera apoyada sobre uno de los lados del volcán. Es importante llevar traje de baño y zapatos antideslizantes para poder ingresar. El Volcán del Totumo se encuentra a 1 hora en auto del centro de Cartagena de Indias y es considerado un paseo muy recomendable y divertido.
                            </p>


                        </div>


                    </div>

                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-default" data-dismiss="modal">Cerrar</button>
                </div>
            </div>

        </div>
    </div>
    <div class="modal fade" id="myModalHOTELES" role="dialog" >
        <div class="modal-dialog modal-lg">

            <!-- Modal content-->
            <div class="modal-content">
                <div class="modal-header">
                    <h4 class="modal-title">HOTELES</h4>
                    <button type="button" class="close" data-dismiss="modal">×</button>
                </div>
                <div class="modal-body">


                    <div class="row">

                        <div class="col-md-12">
                            <h3 class="section-title st-blue">HOTELES<small><br>Fuente: Cartagena Caribe - Mapa Cartagena</small></h3>
                            <strong>Cartagena Caribe</strong><a href="http://www.cartagenacaribe.com/hoteles/hoteles.htm" target="_blank"> http://www.cartagenacaribe.com/hoteles/hoteles.htm </a><br>
                            <strong>Mapa Cartagena</strong><a href="https://goo.gl/Oh5nLc" target="_blank"> https://goo.gl/Oh5nLc </a><br><br>

                            <strong>Hilton Cartagena</strong><br>
                            http://www.hiltonhotels.com/es_XM/colombia/hilton-cartagena-hotel/ <br>
                            Avenida Almirante Brion, El Laguito<br>
                            +57 (5) 6948000<br><br>

                            <strong>Charleston Cartagena Hotel Santa Teresa</strong><br>
                            http://www.hotelcharlestonsantateresa.com/ <br>
                            Carrera 3 # 31-23 Centro Plaza de Santa Teresa<br>
                            +57 (5) 6649494 / 6649547<br><br>

                            <strong>Hotel Las Américas Torre del Mar</strong><br>
                            http://www.hotellasamericas.com.co/ <br>
                            Anillo vial, sector Cielo Mar<br>
                            +57 (5) 6723344 / +57 (5) 693 0592<br><br>

                            <strong>San Lázaro Art Lifestyle Hotel</strong><br>
                            http://www.sanlazaroarthotel.com/ <br>
                            Barrio El Espinal- Frente al Castillo de San Felipe Carrera 15 #31-110<br>
                            +57 (5) 6475000<br><br>

                            <strong>Atlantic Lux Hotel</strong><br>
                            http://www.hotelcasinoatlantic.com/es/ <br>
                            Avenida San Martín No. 8-20, Bocagrande<br>
                            +57 (5) 6659023<br><br>

                            <strong>Hotel Caribe</strong><br>
                            http://www.hotelcaribe.com/ <br>
                            Carrera 1 # 2-87, Bocagrande<br>
                            +57 (5) 6501160<br><br>

                            <strong>Hampton by Hilton Cartagena</strong><br>
                            https://goo.gl/3ZYpLK <br>
                            Calle 8 Avenida San Martin, Bocagrande<br>
                            +57 (5) 6945000<br><br>

                            <strong>Hotel Casa San Agustín </strong><br>
                            http://www.hotelcasasanagustin.com/ <br>
                            Centro, Calle de la Universidad No. 36 – 44<br>
                            +57 (5) 681 0000<br><br>

                            <strong>Sofitel Legend Santa Clara </strong><br>
                            https://goo.gl/wEn0rp <br>
                            Calle del Torno 39-29 Barrio San Diego<br>
                            +57 (5) 6504700<br><br>

                            <strong>Hotel Almirante Cartagena</strong><br>
                            http://www.hotelalmirantecartagena.com.co/ <br>
                            Bocagrande, Avenida San Martín, Calle 6 esquina<br>
                            +57 (5) 665 8811 Ext. 5100 - 5146<br><br>

                            <strong>Hotel Dann Cartagena</strong><br>
                            http://www.hoteldanncartagena.com/ <br>
                            Calle 1A # 1-60 El Laguito<br>
                            +57 (5) 6650000 - 6650510<br><br>

                            <strong>Hotel Capilla del Mar</strong><br>
                            http://capilladelmar.com/es/ <br>
                            Cra 1 No. 8 -12, Bocagrande<br>
                            +57 (5) 6501500 <br><br>

                            <strong>Corales de Indias</strong><br>
                            http://www.coralesdeindias.com/ <br>
                            Carrera 1 No 62- 198, Crespo<br>
                            +57 (5) 6810500<br><br>

                            <strong>Movich Hotels </strong><br>
                            https://goo.gl/JVprII <br>
                            Centro Histórico, Calle de Vélez Danies No. 4 – 39<br>
                            +57 (5) 664 2995<br><br>

                            <strong>Aguamarina Hotel Boutique</strong><br>
                            https://www.aguamarinahotel.com/es/ <br>
                            Calle Sto. Domingo 33-16<br>
                            +57 (5) 6640797<br><br>

                            <strong>Anandá Hotel Boutique</strong><br>
                            http://anandacartagena.com/ <br>
                            Calle del Cuartel # 36-77<br>
                            +57 305 4159590<br><br>

                            <strong>Sonesta Hotel</strong><br>
                            http://www.sonestacartagena.com/ <br>
                            Carrera 9 # 35-104 Anillo Vial, Zona Norte<br>
                            +57 (5) 6535656<br><br>

                            <strong>Allure Chocolat Hotel By Karisma Hotels &amp; Resorts</strong><br>
                            http://www.allurechocolathotel.com/ <br>
                            Calle del Arsenal, Calle 24 # 8B-58<br>
                            +57 (5) 660 5831<br><br>

                            <strong> Bantú</strong><br>
                            http://www.bantuhotel.com/ <br>
                            Calle de La Tablada #7-62, San Diego<br>
                            +57 (5) 6643362<br><br>

                            <strong>Tcherassi Hotel Spa</strong><br>
                            http://www.tcherassihotels.com/ <br>
                            Calle Plaza Fernández de Madrid, Cl. 38 #6-21<br>
                            +57 (5) 6517050<br><br>

                            <strong>Cartagena Plaza</strong><br>
                            http://www.hotelcartagenaplaza.com.co/ <br>
                            Bocagrande Cra 1ra No 6-154<br>
                            +57 (5) 6654-000<br><br>

                            <strong>Armeria Real </strong><br>
                            http://www.armeriarealhotel.com/ <br>
                            Calle del Pedregal No 25-28, Getsemaní <br>
                            +57 (5) 651 74 60<br><br>

                            <strong>Hotel IBIS Cartagena </strong><br>
                            http://ibis-cartagena-marbella.cartagena-hotels-co.com/es/ <br>
                            Avenida Santander #4890 # 47- 47<br>
                            +57 5 6932424<br><br>

                            <strong>Alfiz Hotel Boutique</strong><br>
                            http://www.alfizhotel.com/ <br>
                            Calle Cochera Del Gobernador 3, Centro Histórico<br>
                            +57 (5) 6600006<br><br>

                            <strong>San Pedro Hotel Spa</strong><br>
                            http://sanpedrohotelspa.com.co/es/ <br>
                            Calle San Pedro Mártir #10-85<br>
                            +57 (5) 664 5800<br><br>


                            <strong>Casa Pestagua Hotel Boutique</strong><br>
                            http://www.hotelboutiquecasapestagua.com/<br>
                            Calle de Santo Domingo # 33-63<br>
                            +57 (5) 664 9510<br><br>

                            <strong>Bastion Luxury Hotel</strong><br>
                            http://www.bastionluxuryhotel.com/ <br>
                            Calle del Sargento Mayor No. 6 – 87<br>
                            +57 (5) 6424100<br><br>

                            <hr>

                            <strong>Opciones para estudiantes</strong><br><br>
                            <strong>Opción 1:</strong><br>
                            Apartahotel ubicado en el centro.<br>
                            La ventaja del lugar es que está ubicado en pleno centro histórico de Cartagena.<br>
                            Las fotos de los apartamentos/habitaciones las puedes encontrar en el siguiente enlace <a href="http://hotel-marie.cartagena-hotels-co.com/en/#photo">http://hotel-marie.cartagena-hotels-co.com/en/#photo.</a><br>
                            El contacto es:  Maria Stella Figueroa,  <a href="mailto:gerencia@catalinareal.com">gerencia@catalinareal.com</a> y <a href="mailto:ventas@catalinareal.com">ventas@catalinareal.com</a><br>
                            El promedio de noche por persona es $91.000 (no incluye desayuno).<br><br>

                            <strong>Opción 2:</strong><br>
                            Hotel San Felipe. También en el centro histórico.<br>
                            Acomodación doble: $90.000 por persona (Incluye desayuno e impuestos).<br>
                            Estos valores corresponden a la semana. Dirección: Centro Barrio Getsemaní Cra 9 Nº 31 - 72 PBX: (575) 664 0205.<br>
                            Las reservas deben confirmarlas a través de este correo: <a href="mailto:sanfelipe@doradoplaza.com">sanfelipe@doradoplaza.com</a><br><br>

                        </div>


                    </div>

                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-default" data-dismiss="modal">Cerrar</button>
                </div>
            </div>

        </div>
    </div>
    <!-- Scripts -->
    <?php include "script.php"; ?>
</body>
</html>

