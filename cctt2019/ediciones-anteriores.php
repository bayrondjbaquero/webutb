<!DOCTYPE html>
<html lang="es">
<head>
    <?php include "head.php"; ?>
</head>
<body class="">
    <div id="app" class="mx-auto">
            <nav class="navbar navbar-expand-lg navbar-light justify-content-center fixed-top">
                <?php include "menu.php"; ?>
            </nav>
            <br><br><br>
        <div class="container pt-3">
            <div class="row justify-content-center mt-3">              
                <div class="col-12 col-sm-12 col-md-12 col-lg-12 col-xl-12 bg-white box pt-4 pl-4 pr-4 pb-4 animated fadeInUp mb-2">
                    <h4 class="w-100 text-regular pb-2 m-0 rounded-top text-azul-cielo">
                        Ediciones Anteriores
                    </h4>
                    <!-- <figure>
                        <img src="images/Energia Solar.jpg" alt="" class="img-fluid d-block mx-auto">
                    </figure> -->
                    <div class="row justify-content-center" id="responsive">
                        <div class="col-12 col-sm-12 col-md-12 col-lg-12 col-xl-12">
                            <!-- <h5 id="servicioss" class="w-100 text-white bg-info text-regular p-2 m-0 rounded-top border-primary">
                                Nombre del programa
                            </h5> -->
                            <figure>
                                <img src="images/Redacción.jpg" alt="" class="img-fluid d-block mx-auto">
                            </figure>
                            <p class="text panel bg-white p-2 " style="font-size: 15px;">
                             Lorem ipsum dolor sit amet, consectetur adipisicing elit. Tempora ratione quibusdam eveniet adipisci temporibus dignissimos quo pariatur nihil blanditiis quod esse magnam maiores culpa, quaerat consectetur iste porro neque asperiores.
                            </p>
                        </div>
                        <div class="col-12 col-sm-12 col-md-12 col-lg-12 col-xl-12">
                            <!-- <h5 id="servicioss" class="w-100 text-white bg-info text-regular p-2 m-0 rounded-top border-primary">
                                Nombre del programa
                            </h5> -->
                            <figure>
                                <img src="images/Propiedad Horizontales.jpg" alt="" class="img-fluid d-block mx-auto">
                            </figure>
                            <p class="text panel bg-white p-2 " style="font-size: 15px;">
                             Lorem ipsum dolor sit amet, consectetur adipisicing elit. Tempora ratione quibusdam eveniet adipisci temporibus dignissimos quo pariatur nihil blanditiis quod esse magnam maiores culpa, quaerat consectetur iste porro neque asperiores.
                            </p>
                        </div>
                        <div class="col-12 col-sm-12 col-md-12 col-lg-12 col-xl-12">
                            <!-- <h5 id="servicioss" class="w-100 text-white bg-info text-regular p-2 m-0 rounded-top border-primary">
                                Nombre del programa
                            </h5> -->
                            <figure>
                                <img src="images/Propiedad Horizontales.jpg" alt="" class="img-fluid d-block mx-auto">
                            </figure>
                            <p class="text panel bg-white p-2 " style="font-size: 15px;">
                             Lorem ipsum dolor sit amet, consectetur adipisicing elit. Tempora ratione quibusdam eveniet adipisci temporibus dignissimos quo pariatur nihil blanditiis quod esse magnam maiores culpa, quaerat consectetur iste porro neque asperiores.
                            </p>
                        </div>
                        <div class="col-12 col-sm-12 col-md-12 col-lg-12 col-xl-12">
                            <!-- <h5 id="servicioss" class="w-100 text-white bg-info text-regular p-2 m-0 rounded-top border-primary">
                                Nombre del programa
                            </h5> -->
                            <figure>
                                <img src="images/Propiedad Horizontales.jpg" alt="" class="img-fluid d-block mx-auto">
                            </figure>
                            <p class="text panel bg-white p-2 " style="font-size: 15px;">
                             Lorem ipsum dolor sit amet, consectetur adipisicing elit. Tempora ratione quibusdam eveniet adipisci temporibus dignissimos quo pariatur nihil blanditiis quod esse magnam maiores culpa, quaerat consectetur iste porro neque asperiores.
                            </p>
                        </div>
                        <div class="col-12 col-sm-12 col-md-12 col-lg-12 col-xl-12">
                            <!-- <h5 id="servicioss" class="w-100 text-white bg-info text-regular p-2 m-0 rounded-top border-primary">
                                Nombre del programa
                            </h5> -->
                            <figure>
                                <img src="images/Propiedad Horizontales.jpg" alt="" class="img-fluid d-block mx-auto">
                            </figure>
                            <p class="text panel bg-white p-2 " style="font-size: 15px;">
                             Lorem ipsum dolor sit amet, consectetur adipisicing elit. Tempora ratione quibusdam eveniet adipisci temporibus dignissimos quo pariatur nihil blanditiis quod esse magnam maiores culpa, quaerat consectetur iste porro neque asperiores.
                            </p>
                        </div>
                    </div>
                </div> 
            </div>
        </div>
        <?php include "footer.php"; ?>
    </div>

    <!-- Scripts -->
    <?php include "script.php"; ?>
</body>
</html>

