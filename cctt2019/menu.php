<div class="container">
                    <a class="navbar-brand " href="index.php">
                        <img src="http://www.utb.edu.co/sites/web.unitecnologica.edu.co/files/logo_utb_0.png" alt="" class="img-fluid d-block animated fadeInDown mx-auto" style="max-width: 150px;float: left;">
                        <img src="http://www.utb.edu.co/sites/web.unitecnologica.edu.co/files/logo_congreso_xiii_cctt_2019_b.jpg" alt="" class="img-fluid d-block animated fadeInDown mx-auto" style="max-width: 265px; margin-top: 12px;">
                    </a>
                    <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
                        <span class="navbar-toggler-icon"></span>
                    </button>

                    <div class="collapse navbar-collapse my-2 " id="navbarSupportedContent">
                        <ul class="navbar-nav nav navbar-nav ml-auto">
                            <li class="nav-item  ">
                                <a class="nav-link active text-center text-secondary text-regular dropdown-toggle" href="#">Inicio </a>
                                <ul class="dropdown-menu" aria-labelledby="navbarDropdownMenuLink">
                                    <li>
                                        <a class="dropdown-item" href="informacion-del-evento.php">Información del Evento</a>
                                    </li>
                                    <li>
                                        <a class="dropdown-item" href="organizacion.php">Organización</a>
                                    </li>
                                    <li>
                                        <a class="dropdown-item" href="sponsors.php">Sponsors</a>
                                    </li>
                                    <!-- <li>
                                        <a class="dropdown-item" href="noticias.php">Noticias</a>
                                    </li>
                                    <li>
                                        <a class="dropdown-item" href="ediciones-anteriores.php">Ediciones anteriores</a>
                                    </li>
                                    <li>
                                        <a class="dropdown-item" href="fototeca.php">Fototeca</a>
                                    </li> -->
                                </ul>
                            </li>
                            <li class="nav-item ">
                                <a class="nav-link text-center text-secondary text-regular dropdown-toggle" href="#">Inscripción</a>
                                <ul class="dropdown-menu" aria-labelledby="navbarDropdownMenuLink">
                                    <li>
                                        <a class="dropdown-item" href="costos-precios.php">Costos y precios</a>
                                    </li>
                                    <li>
                                        <a class="dropdown-item" href="cursos-tecnicos.php">Cursos técnicos</a>
                                    </li>
                                    <li>
                                        <a class="dropdown-item" href="visitas-tecnicas.php">Visitas técnicas</a>
                                    </li>
                                </ul>
                            </li>
                            <li class="nav-item ">
                                <a class="nav-link text-center text-secondary text-regular dropdown-toggle" href="#">Información Científica</a>
                                <ul class="dropdown-menu" aria-labelledby="navbarDropdownMenuLink">
                                    <li>
                                        <a class="dropdown-item" href="resumen.php">Envío de resumen</a>
                                    </li>
                                    <li>
                                        <a class="dropdown-item" href="programa-evento.php">Programa del evento</a>
                                    </li>
                                    <li>
                                        <a class="dropdown-item" href="areastematicas-comite.php">Áreas temáticas y comité científico</a>
                                    </li>
                                    <!-- <li>
                                        <a class="dropdown-item" href="conferencistas.php">Conferencistas invitados</a>
                                    </li> -->
                                    <!-- <li>
                                        <a class="dropdown-item" href="memorias.php">Memorias y volumen especial en revistas</a>
                                    </li> -->
                                </ul>
                            </li>
                            <li class="nav-item ">
                                <a class="nav-link text-center text-secondary text-regular dropdown-toggle" href="#">Información General</a>
                                <ul class="dropdown-menu" aria-labelledby="navbarDropdownMenuLink">
                                    <li>
                                        <a class="dropdown-item" href="cartagena.php">Cartagena cultural e historica</a>
                                    </li>
                                    <li>
                                        <a class="dropdown-item" href="acomodacion-hoteles.php">Acomodación y hoteles</a>
                                    </li>
                                   <!--  <li>
                                        <a class="dropdown-item" href="como-llegar.php">Cómo llegar</a>
                                    </li> -->
                                    <li>
                                        <a class="dropdown-item" href="lugar-evento.php">Lugar del evento</a>
                                    </li>
                                </ul>
                            </li>
                            <li class="nav-item ">
                                <a class="nav-link text-center text-secondary text-regular" href="contacto.php">Contacto</a>
                            </li>
                        </ul>
                        
                    </div>
                </div>
