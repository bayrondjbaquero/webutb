<meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1.0, user-scalable=no">

    <!-- CSRF Token -->
    <meta name="csrf-token" content="">
    <meta meta name="description" content="Universidad Tecnológica de Bolívar">
    <meta name="robots" content="Index, Follow">
    <meta name="keywords" content="Universidad Tecnologica de bolívar, programas utb, programas pregrados, cursos utb">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="author" content="Mercadeo y comunicaciones utb">
    <meta name="language" content="es">
    <meta name="generator" content="HTML5, CCS3, PHP">

    <title>Universidad Tecnologica de Bolívar</title>

    <!-- Styles -->
    <link rel="shortcut icon" href="http://www.utb.edu.co/sites/web.unitecnologica.edu.co/files/favicon.ico">
    <!-- <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/css/bootstrap.min.css" integrity="sha384-Gn5384xqQ1aoWXA+058RXPxPg6fy4IWvTNh0E263XmFcJlSAwiGgFAW/dAiS6JXm" crossorigin="anonymous"> -->
    <link href="http://www.utb.edu.co/landing-pregrados/css/bootstrap.min.css" rel="stylesheet">
    <link href="css/landing.css" rel="stylesheet">
    <link href="http://www.utb.edu.co/landing-pregrados/css/lato.css" rel="stylesheet">
    <link href="http://www.utb.edu.co/landing-pregrados/css/animate.css" rel="stylesheet">
    <link href="http://www.utb.edu.co/landing-pregrados/css/font-awesome.css" rel="stylesheet">
    
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/slick-carousel/1.8.1/slick.min.css" />
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/slick-carousel/1.8.1/slick-theme.min.css" />
