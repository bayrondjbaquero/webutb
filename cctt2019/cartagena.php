<!DOCTYPE html>
<html lang="es">
<head>
    <?php include "head.php"; ?>
</head>
<body class="">
    <div id="app" class="mx-auto">
            <nav class="navbar navbar-expand-lg navbar-light justify-content-center fixed-top">
                <?php include "menu.php"; ?>
            </nav>
            <br><br><br>
            <!-- <div id="carouselExampleControls" class="carousel slide carousel-fade row" data-ride="carousel">
                <ol class="carousel-indicators">
                    <li data-target="#carouselExampleIndicators" data-slide-to="0" class="active"></li>
                    <li data-target="#carouselExampleIndicators" data-slide-to="1"></li>
                    <li data-target="#carouselExampleIndicators" data-slide-to="2"></li>
                    <li data-target="#carouselExampleIndicators" data-slide-to="3"></li>
                </ol>
                <div class="carousel-inner animated fadeIn">
                    <div class="carousel-item active">
                        <img class="d-block w-100 img-fluid" src="http://www.utb.edu.co/sites/web.unitecnologica.edu.co/files/banner_home_mesa_de_trabajo_1.jpg" alt="First slide">
                    </div>
                    <div class="carousel-item">
                        <img class="d-block w-100 img-fluid" src="http://www.utb.edu.co/sites/web.unitecnologica.edu.co/files/banner_grammy.png" alt="Second slide">
                    </div>
                </div>
                <a class="carousel-control-prev" href="#carouselExampleControls" role="button" data-slide="prev">
                    <span class="carousel-control-prev-icon" aria-hidden="true"></span>
                    <span class="sr-only">Previous</span>
                </a>
                <a class="carousel-control-next" href="#carouselExampleControls" role="button" data-slide="next">
                    <span class="carousel-control-next-icon" aria-hidden="true"></span>
                    <span class="sr-only">Next</span>
                </a>
            </div> -->
        <div class="container pt-3">
            <div class="row justify-content-center mt-3">
                <div class="col-12 col-sm-12 col-md-12 col-lg-12 col-xl-12 bg-white box pt-4 pl-4 pr-4 pb-4 animated fadeInUp mb-2">
                    <h4 class="w-100 text-regular pb-2 m-0 rounded-top primary">
                        Cartagena cultural e histórica
                    </h4>
                    <div class="row">
                        <div class="col-md-6">
                        <figure>
                            <img src="https://d2yoo3qu6vrk5d.cloudfront.net/images/20180713172603/gettyimages-cartagenat1-900x485.jpg" alt="" class="img-fluid d-block mx-auto">
                        </figure>
                    </div>
                    <div class="col-md-6">
                        <figure>
                            <img src="http://www.utb.edu.co/sites/web.unitecnologica.edu.co/files/cartagena2.jpg" alt="" class="img-fluid d-block mx-auto" style="max-height: 285px;">
                        </figure>
                    </div>
                    </div>
                    <p class="text panel bg-white " style="font-size: 15px;">La brisa marina y los atardeceres que transforman los colores de las fachadas de casas de más de 400 años hechizan a los visitantes en Cartagena de Indias. Esta ciudad, Patrimonio Histórico de la Humanidad, con tesoros coloniales y fundada en 1533 por Pedro de Heredia, conserva la arquitectura colonial de sus construcciones y el conjunto de fortificaciones más completo de Suramérica. </p>
                    <p class="text panel bg-white " style="font-size: 15px;">Cartagena, Colombia, suma a los encantos de su arquitectura colonial, republicana y moderna, los atractivos de una intensa vida nocturna, festivales culturales, paisajes exuberantes, magníficas playas, excelente oferta gastronómica y una importante infraestructura hotelera y turística. Es una ciudad fantástica que guarda los secretos de la historia en sus murallas y balcones en sus construcciones y en sus angostos caminos de piedra.</p>
                    <p class="text panel bg-white " style="font-size: 15px;">Enmarcada por una hermosa bahía, Cartagena de Indias es una de las ciudades más bellas y conservadas de América.</p> 
                </div>
                <div class="col-12 col-sm-12 col-md-12 col-lg-12 col-xl-12 bg-white box pt-4 pl-4 pr-4 pb-4 animated fadeInUp mb-2">
                    <h4 class="w-100 text-regular pb-2 m-0 rounded-top primary">
                        ¿QUÉ HACER EN CARTAGENA DE INDIAS?
                    </h4>
                    <!-- <div class="row">
                        <div class="col-md-6">
                        <figure>
                            <img src="https://d2yoo3qu6vrk5d.cloudfront.net/images/20180713172603/gettyimages-cartagenat1-900x485.jpg" alt="" class="img-fluid d-block mx-auto">
                        </figure>
                    </div>
                    <div class="col-md-6">
                        <figure>
                            <img src="images/Cartagena2.jpg" alt="" class="img-fluid d-block mx-auto" style="max-height: 285px;">
                        </figure>
                    </div>
                    </div> -->
                    <p class="text panel bg-white " style="font-size: 15px;">El centro histórico, encerrado por las murallas de Cartagena, es el alma de esta ciudad. Le sirvió de inspiración a Gabriel García Márquez, ganador del premio Nobel de Literatura en 1982. Aquí, además de palpar la historia de siglos en calles empedradas, se puede explorar el Castillo de San Felipe, asombrarse con un recorrido por iglesias antiguas y, cerca, hasta nadar y flotar en un volcán de lodo. </p>
                    <p class="text panel bg-white " style="font-size: 15px;">Es placentero recorrer las calles y observar los tesoros coloniales. El Palacio de la Inquisición y la Torre del Reloj son un claro ejemplo. Es posible, además, disfrutar la brisa cálida y tranquila desde sus parques y plazas. </p>
                    <p class="text panel bg-white " style="font-size: 15px;">La gastronomía es también una fiesta en la ciudad. Las alternativas se multiplican para los viajeros que buscan experimentar sabores nuevos y exóticos de la cocina local e internacional.
Las opciones de alojamiento son diversas. Es posible escoger tradicionales hoteles coloniales o exclusivos hoteles boutique que proporcionan una experiencia única por sus detalles y servicios personalizados. </p> 
                </div>
                               
                <hr>
            </div>
        </div>
        <?php include "footer.php"; ?>
    </div>

    <!-- Scripts -->
    <?php include "script.php"; ?>
</body>
</html>

