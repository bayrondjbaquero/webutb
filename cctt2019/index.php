<!DOCTYPE html>
<html lang="es">
<head>
    <?php include "head.php"; ?>
</head>
<body class="">
    <div id="app" class="mx-auto">
            <nav class="navbar navbar-expand-lg navbar-light justify-content-center fixed-top">
                <?php include "menu.php"; ?>
                
            </nav>
            <div id="carouselExampleControls" style="box-shadow: 0px 0px 8px 3px gray;" class="carousel slide carousel-fade row" data-ride="carousel">
                <ol class="carousel-indicators">
                    <li data-target="#carouselExampleIndicators" data-slide-to="0" class="active"></li>
                    <li data-target="#carouselExampleIndicators" data-slide-to="1"></li>
                    <li data-target="#carouselExampleIndicators" data-slide-to="2"></li>
                    <li data-target="#carouselExampleIndicators" data-slide-to="3"></li>
                </ol>
                <div class="carousel-inner animated fadeIn">
                    <div class="carousel-item active">
                        <img class="d-block w-100 img-fluid" src="http://www.utb.edu.co/sites/web.unitecnologica.edu.co/files/banner-trafico.jpg" alt="First slide" style="max-height: 700px;">
                        <div class="carousel-caption d-none d-md-block text-left bg-dark p-3" style="background: #68c2c799 !important;left: 0;

padding-left: 4% !important;

max-width: 590px;">
                            <h3>Universidad Tecnológica de Bolívar</h3>
                            <p>
                                Cartagena de Indias - Colombia <h2>26, 27 y 28 de Junio de 2019</h2>
                            </p>
                            
                      </div>
                    </div>
                    <div class="carousel-item">
                        <img class="d-block w-100 img-fluid" src="http://www.utb.edu.co/sites/web.unitecnologica.edu.co/files/banner-trafico1.jpg" alt="First slide" style="max-height: 700px;">
                        <div class="carousel-caption d-none d-md-block text-left bg-dark p-3" style="background: #68c2c799 !important;left: 0;

padding-left: 4% !important;

max-width: 590px;">
                            <h3>Universidad Tecnológica de Bolívar</h3>
                            <p>
                                Cartagena de Indias - Colombia <h2>26, 27 y 28 de Junio de 2019</h2>
                            </p>
                            
                      </div>

                    </div>
                    <div class="carousel-item">
                        <img class="d-block w-100 img-fluid" src="http://www.utb.edu.co/sites/web.unitecnologica.edu.co/files/banner-trafico2.jpg" alt="First slide" style="max-height: 700px;">
                       <div class="carousel-caption d-none d-md-block text-left bg-dark p-3" style="background: #68c2c799 !important;left: 0;

padding-left: 4% !important;

max-width: 590px;">
                            <h3>Universidad Tecnológica de Bolívar</h3>
                            <p>
                                Cartagena de Indias - Colombia <h2>26, 27 y 28 de Junio de 2019</h2>
                            </p>
                            
                      </div>

                    </div>
                </div>
                <a class="carousel-control-prev" href="#carouselExampleControls" role="button" data-slide="prev">
                    <span class="carousel-control-prev-icon" aria-hidden="true"></span>
                    <span class="sr-only">Previous</span>
                </a>
                <a class="carousel-control-next" href="#carouselExampleControls" role="button" data-slide="next">
                    <span class="carousel-control-next-icon" aria-hidden="true"></span>
                    <span class="sr-only">Next</span>
                </a>
            </div>
            <div class="container">
                
                <div class="row justify-content-center mt-5 mb-5">
<div class="col-md-3 col-xs-3 col-lg-3 col-xl-3 col-12">
                        <a href="informacion-del-evento.php" class="text-dark">
                            <figure>
                                <img src="https://cdn4.iconfinder.com/data/icons/finance-and-banking-free/64/Finance_financial_planning-512.png" alt="" class="img-responsive img-fluid center-block d-block mx-auto" style="max-width: 150px;">
                            </figure>
                            <h4 class="text-center text-dark">
                                    Información del Evento
                            </h4>
                        </a>
                    </div>
                    <div class="col-md-3 col-xs-3 col-lg-3 col-xl-3 col-12">
                        <a href="costos-precios.php" class="text-dark">
                            <figure>
                                <img src="https://cdn3.iconfinder.com/data/icons/art-and-design-tools-1/128/Colored_Pencil-512.png" alt="" class="img-responsive img-fluid center-block d-block mx-auto" style="max-width: 150px;">
                            </figure>
                            <h4 class="text-center text-dark">
                                    Inscripción
                            </h4>
                        </a>
                    </div>
                    <div class="col-md-3 col-xs-3 col-lg-3 col-xl-3 col-12">
                        <a href="resumen.php" class="text-dark">
                            <figure>
                                <img src="https://cdn0.iconfinder.com/data/icons/user-collection-4/512/user_information-512.png" alt="" class="img-responsive img-fluid center-block d-block mx-auto" style="max-width: 150px;">
                            </figure>
                            <h4 class="text-center text-dark">
                                    Información Científica
                            </h4>
                        </a>
                    </div>
                    <div class="col-md-3 col-xs-3 col-lg-3 col-xl-3 col-12">
                        <a href="cartagena.php" class="text-dark">
                            <figure>
                                <img src="https://cdn3.iconfinder.com/data/icons/signs-symbols-and-social-media/100/chat_comment_feedback_review_messanger-512.png" alt="" class="img-responsive img-fluid center-block d-block mx-auto" style="max-width: 150px;">
                            </figure>
                            <h4 class="text-center text-dark">
                                    Información General
                            </h4>
                        </a>
                    </div>
                </div>
            </div>
            <?php include "footer.php"; ?>
    </div>

    <!-- Scripts -->
    <?php include "script.php"; ?>
    
</body>
</html>

