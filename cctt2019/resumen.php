<!DOCTYPE html>
<html lang="es">
<head>
    <?php include "head.php"; ?>
</head>
<body class="">
    <div id="app" class="mx-auto">
            <nav class="navbar navbar-expand-lg navbar-light justify-content-center fixed-top">
                <?php include "menu.php"; ?>
            </nav>
            <br><br><br>
            <!-- <div id="carouselExampleControls" class="carousel slide carousel-fade row" data-ride="carousel">
                <ol class="carousel-indicators">
                    <li data-target="#carouselExampleIndicators" data-slide-to="0" class="active"></li>
                    <li data-target="#carouselExampleIndicators" data-slide-to="1"></li>
                    <li data-target="#carouselExampleIndicators" data-slide-to="2"></li>
                    <li data-target="#carouselExampleIndicators" data-slide-to="3"></li>
                </ol>
                <div class="carousel-inner animated fadeIn">
                    <div class="carousel-item active">
                        <img class="d-block w-100 img-fluid" src="http://www.utb.edu.co/sites/web.unitecnologica.edu.co/files/banner_home_mesa_de_trabajo_1.jpg" alt="First slide">
                    </div>
                    <div class="carousel-item">
                        <img class="d-block w-100 img-fluid" src="http://www.utb.edu.co/sites/web.unitecnologica.edu.co/files/banner_grammy.png" alt="Second slide">
                    </div>
                </div>
                <a class="carousel-control-prev" href="#carouselExampleControls" role="button" data-slide="prev">
                    <span class="carousel-control-prev-icon" aria-hidden="true"></span>
                    <span class="sr-only">Previous</span>
                </a>
                <a class="carousel-control-next" href="#carouselExampleControls" role="button" data-slide="next">
                    <span class="carousel-control-next-icon" aria-hidden="true"></span>
                    <span class="sr-only">Next</span>
                </a>
            </div> -->
        <div class="container pt-3">
            <div class="row justify-content-center mt-3">
                <div class="col-12 col-sm-12 col-md-12 col-lg-12 col-xl-12 bg-white box pt-4 pl-4 pr-4 pb-4 animated fadeInUp mb-2">
                    <img src="https://easychair.org/images/logoEC.png" alt="" class="img-fluid img-responsive d-block mx-auto">
                    <h4 class="w-100 text-regular pb-2 m-0 rounded-top primary">
                        Envío de Resumen
                    </h4>
                    <!-- <figure>
                        <img src="images/Energia Solar.jpg" alt="" class="img-fluid d-block mx-auto">
                    </figure> -->
                    <p class="text-justify panel bg-white text-regular" style="font-size: 17px;">La recepción de los trabajos para el CCTT2019 se realizará mediante la plataforma
EasyChair suministrada por los diversos canales de difusión del evento -principalmente
página web-.</p>
                    <p class="text-justify panel bg-white text-regular" style="font-size: 17px;">En general, los autores deberán tener presente la siguiente información mínima para el
sometimiento:</p>
                    <ul class="text-regular text-justify">
                        <li>Nombres y filiación de los autores, incluyendo sus direcciones de correo
electrónico. </li>
                        <li>La modalidad en la que desean presentar su trabajo en el marco del evento: Oral
y poster. No obstante, la decisión final del tipo de intervención podría estar sujeta
a la disponibilidad de la agenda, relevancia del tema y las valoraciones recibidas
por los pares.</li>
                        <li>Identificar una de las áreas temáticas en la que desea presentar su trabajo.</li>
                        <li>Resumen del trabajo a presentar, el cual debe contener mínimo:</li>
                        <p class="panel bg-white">
                            <strong>Introducción y objetivos:</strong> Describir el propósito de la investigación. Esta puede
ser una breve descripción introductoria de la problemática y/o importancia de la
misma, además, identificar la(s) hipótesis u objetivo(s).
                        </p>
                        <p class="panel bg-white">
                            <strong>Materiales y/o métodos:</strong> Describir los materiales y/o variables en estudio, así
como un resumen o indicación de los procedimientos o estándares aplicados,
haciendo hincapié en los puntos clave o pasos críticos establecidos dentro de la
investigación.
                        </p>
                        <p class="panel bg-white">
                            <strong>Resultados relevantes:</strong> Presentar de manera clara los resultados más relevantes
y su importancia en el contexto de la investigación presentada.
                        </p>
                        <p class="panel bg-white">
                            <strong>Conclusiones:</strong> Concluir los objetivos presentados, a la luz de los resultados
relevantes de la investigación.
                        </p>
                        <p class="bg-white panel">
                            El resumen debe ser no estructurado (un solo párrafo), máximo 500 palabras y
debe evitar el uso de abreviaturas, tablas, gráficos y referencias bibliográficas, en
la medida de lo posible.
                        </p>
                        <p class="bg-white panel">
                            Solo se garantiza el proceso de recepción, evaluación y potencial aceptación de los
trabajos presentados por la plataforma EasyChair.
                        </p>
                    </ul>
                </div>             
                <hr>
            </div>
        </div>
        <?php include "footer.php"; ?>
    </div>

    <!-- Scripts -->
    <?php include "script.php"; ?>
</body>
</html>

