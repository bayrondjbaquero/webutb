<!DOCTYPE html>
<html lang="es">
<head>
    <?php include "head.php"; ?>
</head>
<body class="">
    <div id="app" class="mx-auto">
            <nav class="navbar navbar-expand-lg navbar-light justify-content-center fixed-top">
                <?php include "menu.php"; ?>
            </nav>
            <br><br><br>
        <div class="container pt-3">
            <div class="row justify-content-center mt-3">
                <div class="col-12 col-sm-12 col-md-12 col-lg-12 col-xl-12 bg-white box pt-4 pl-4 pr-4 pb-4 animated fadeInUp mb-2">
                    <!-- 
                    <p class="text panel bg-white p-2 " style="font-size: 15px;">
                     Lorem ipsum dolor sit amet, consectetur adipisicing elit. Tempora ratione quibusdam eveniet adipisci temporibus dignissimos quo pariatur nihil blanditiis quod esse magnam maiores culpa, quaerat consectetur iste porro neque asperiores.
                    </p>
                    <ul>
                        <li>Lorem ipsum dolor sit amet, consectetur adipisicing elit. </li>
                        <li>Ipsa expedita repellat dolore officia accusamus modi, corporis sed perspiciatis, odio veniam ex laborum commodi! </li>
                        <li>Ipsa expedita repellat dolore officia accusamus modi, corporis sed perspiciatis, odio veniam ex laborum commodi! </li>
                    </ul> -->
                    <h4 class="w-100 text-regular pb-2 m-0 rounded-top primary">
                        Lugar del Evento
                    </h4>
                    <iframe src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d245.25656243152338!2d-75.53820562869154!3d10.413234919765655!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x8ef625936b260037%3A0x9308af08f0ad9383!2sUTB!5e0!3m2!1ses-419!2sco!4v1520351529937" width="100%" height="480"></iframe>
                </div>                 
                <hr>
            </div>
        </div>
        <?php include "footer.php"; ?>
    </div>

    <!-- Scripts -->
    <?php include "script.php"; ?>
</body>
</html>

