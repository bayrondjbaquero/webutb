<!DOCTYPE html>
<html lang="es">
<head>
    <?php include "head.php"; ?>
</head>
<body class="">
    <div id="app" class="mx-auto">
            <nav class="navbar navbar-expand-lg navbar-light justify-content-center fixed-top">
                <?php include "menu.php"; ?>
            </nav>
            <br><br><br>
        <div class="container pt-3">
            <div class="row justify-content-center mt-3">
                <div class="col-12 col-sm-12 col-md-12 col-lg-12 col-xl-12 bg-white box pt-4 pl-4 pr-4 pb-4 animated fadeInUp mb-2"><br>
                    <figure>
                        <img src="http://www.utb.edu.co/sites/web.unitecnologica.edu.co/files/construccion_0.png" alt="" class="img-fluid d-block mx-auto">
                    </figure>
                </div>      
            </div>
        </div>
        <?php include "footer.php"; ?>
    </div>

    <!-- Scripts -->
   <?php include "script.php"; ?>
</body>
</html>

