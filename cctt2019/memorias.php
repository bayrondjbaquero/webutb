<!DOCTYPE html>
<html lang="es">
<head>
    <?php include "head.php"; ?>
</head>
<body class="">
    <div id="app" class="mx-auto">
            <nav class="navbar navbar-expand-lg navbar-light justify-content-center fixed-top">
                <?php include "menu.php"; ?>
            </nav>
            <br><br><br>
            <!-- <div id="carouselExampleControls" class="carousel slide carousel-fade row" data-ride="carousel">
                <ol class="carousel-indicators">
                    <li data-target="#carouselExampleIndicators" data-slide-to="0" class="active"></li>
                    <li data-target="#carouselExampleIndicators" data-slide-to="1"></li>
                    <li data-target="#carouselExampleIndicators" data-slide-to="2"></li>
                    <li data-target="#carouselExampleIndicators" data-slide-to="3"></li>
                </ol>
                <div class="carousel-inner animated fadeIn">
                    <div class="carousel-item active">
                        <img class="d-block w-100 img-fluid" src="http://www.utb.edu.co/sites/web.unitecnologica.edu.co/files/banner_home_mesa_de_trabajo_1.jpg" alt="First slide">
                    </div>
                    <div class="carousel-item">
                        <img class="d-block w-100 img-fluid" src="http://www.utb.edu.co/sites/web.unitecnologica.edu.co/files/banner_grammy.png" alt="Second slide">
                    </div>
                </div>
                <a class="carousel-control-prev" href="#carouselExampleControls" role="button" data-slide="prev">
                    <span class="carousel-control-prev-icon" aria-hidden="true"></span>
                    <span class="sr-only">Previous</span>
                </a>
                <a class="carousel-control-next" href="#carouselExampleControls" role="button" data-slide="next">
                    <span class="carousel-control-next-icon" aria-hidden="true"></span>
                    <span class="sr-only">Next</span>
                </a>
            </div> -->
        <div class="container pt-3">
            <div class="row justify-content-center mt-3">
                <div class="col-12 col-sm-12 col-md-12 col-lg-12 col-xl-12 bg-white box pt-4 pl-4 pr-4 pb-4 animated fadeInUp mb-2">
                    <h4 class="w-100 text-regular pb-2 m-0 rounded-top primary">
                        Memorias y volumen especial en revistas
                    </h4>
                    <p class="text panel bg-white p-2 " style="font-size: 15px;">
                     Los trabajos recibidos, evaluados y aceptados serán publicados en las memorias del
evento con registro ISBN. No obstante, se realizará el acercamiento con revista de alto
impacto nacional e internacional para la consecución de un volumen especial en el que se
podrían presentar los trabajos con mejor valoración en el proceso de sometimiento de
resumen y presentación en el evento.
                    </p>
                    <p class="text panel bg-white p-2 " style="font-size: 15px;">
                     El listado de revistas potenciales a las cual se realizará la invitación, se resume a
continuación:
                    </p>
                    <span><strong>Internacionales:</strong></span><br>
                    <ul>
                        <li>Advances in Transportation Studies</li>
                        <li>International Journal of Sustainable Transportation</li>
                        <li>International Journal of Vehicle Systems Modelling and Testing </li>
                        <li>Journal of Advanced Transportation</li>
                        <li>Journal of Intelligent Transportation Systems</li>
                        <li>Journal of Traffic and Transportation Engineering</li>
                        <li>Transport</li>
                        <li>Transport Problems</li>
                        <li>Transportation Research Part D: Transport and environment</li>
                        <li>Transportation Research Part F: Traffic Psychology and Behaviour</li>
                    </ul>
                    <br>
                    <span><strong>Nacionales:</strong></span><br>
                    <ul>
                        <li>Caldasia</li>
                        <li>Cuadernos de Vivienda y Urbanismo</li>
                        <li>INGE CUC</li>
                        <li>Ingeniería e Investigación</li>
                        <li>Ingeniería y Ciencia</li>
                        <li>Ingeniería y Desarrollo</li>
                        <li>Revista Ingenierías Universidad de Medellín</li>
                        <li>Scientia Et Technica</li>
                    </ul>
                    <br>
                    <p class="text bg-white">
                        La posibilidad del volumen está supeditada a la aceptación por parte del editor de la
revista y el cumplimiento por parte de los autores de las condiciones establecidas para la
publicación.
                    </p>
                    <div class="row">
                        <div class="col-md-3">
                            <figure>
                                <img src="images/libro-digital.jpg" alt="" class="img-fluid d-block mx-auto">
                            </figure>
                        </div>
                        <div class="col-md-3">
                            <figure>
                                <img src="images/libro-digital.jpg" alt="" class="img-fluid d-block mx-auto">
                            </figure>
                        </div>
                        <div class="col-md-3">
                            <figure>
                                <img src="images/libro-digital.jpg" alt="" class="img-fluid d-block mx-auto">
                            </figure>
                        </div>
                        <div class="col-md-3">
                            <figure>
                                <img src="images/libro-digital.jpg" alt="" class="img-fluid d-block mx-auto">
                            </figure>
                        </div>
                    </div>
                </div>                
                <hr>
            </div>
        </div>
        <?php include "footer.php"; ?>
    </div>

    <!-- Scripts -->
    <?php include "script.php"; ?>
</body>
</html>

