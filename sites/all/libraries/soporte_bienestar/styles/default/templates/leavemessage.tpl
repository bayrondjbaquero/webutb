<!DOCTYPE html>
<html lang="es">
<head>
<title>${msg:leavemessage.title}</title>
<link rel="shortcut icon" href="${mibewroot}/images/favicon.ico" type="image/x-icon"/>
<link rel="stylesheet" type="text/css" href="${tplroot}/chat.css" />

<!-- Bootstrap css -->
<meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1, user-scalable=0 ">
<link href="${mibewroot}/bootstrap-3.0.3/css/bootstrap.min.css" rel="stylesheet">
<!-- fin Bootstrap css -->

<!-- jQuery (necessary for Bootstrap's JavaScript plugins) -->
<!-- <script type="text/javascript" language="javascript" src="http://code.jquery.com/jquery.min.js"></script> -->
<script type="text/javascript" language="javascript" src="${mibewroot}/js/jquery-1.10.2.min.js"></script>
<!-- Include all compiled plugins (below), or include individual files as needed -->
<script type="text/javascript" language="javascript" src="${mibewroot}/bootstrap-3.0.3/js/bootstrap.min.js"></script>

<!-- jQuery (necessary for Validate plugins) -->
<script type="text/javascript" language="javascript" src="${mibewroot}/jquery.validate-1.11.1/jquery.validate.min.js"></script>
<script type="text/javascript" language="javascript" src="${mibewroot}/jquery.validate-1.11.1/messages_es.js"></script>

<style type="text/css">
#header{
	height:50px;
	background:url(${tplroot}/images/bg_domain.gif) repeat-x top;
	background-color:#5AD66B;
	width:99.6%;
	margin:0px 0px 20px 0px;
}
#header .mmimg{
	background:url(${tplroot}/images/quadrat.gif) bottom left no-repeat;
}
.form td{
	background-color:#f4f4f4;
	color:#525252;
}
.but{
	font-family:Verdana !important;
	font-size:11px;
	background:url(${tplroot}/images/butbg.gif) no-repeat top left;
	display:block;
	text-align:center;
	padding-top:2px;
	color:white;
	width:80px;
	height:18px;
	text-decoration:none;
	position:relative;top:1px;
}

.text-center {
	text-align: center;
}

.titulo{
	padding-left:0px;
	padding-top: 5px;
	font-weight: bold;
}

#sndmessagelnk{/* boton enviar */
	margin-right: 20px;
}

/* mensajes de error del formulario */
.error{
	color: #F00;
	font-style: italic;
}
/* fin mensajes de error del formulario */

</style>


<script type="text/javascript">
$(document).ready(function(){
	// validate the comment form when it is submitted
	$(document).ready(function(){
				// validate the comment form when it is submitted
				$("#formDejarMensaje").validate();
			});
});
</script>
</head>

<body bgcolor="#FFFFFF" text="#000000" link="#C28400" vlink="#C28400" alink="#C28400" style="margin:0px 15px 10px 15px">

	<!-- header -->
	<div class="row">
		<div class="col-xs-6 wrap-logo">
			<div class="pull-right">
				${if:ct.company.chatLogoURL}
					${if:mibewHost}
						<a onclick="window.open('${page:mibewHost}');return false;" href="${page:mibewHost}">
							<img src="${page:ct.company.chatLogoURL}" border="0" alt=""/>
						</a>
					${else:mibewHost}
						<img src="${page:ct.company.chatLogoURL}" border="0" alt=""/>
					${endif:mibewHost}
				${else:ct.company.chatLogoURL}
					${if:mibewHost}
						<a onclick="window.open('${page:mibewHost}');return false;" href="${page:mibewHost}">
							<img src="${mibewroot}/images/mibewlogo.gif" border="0" alt=""/>
						</a>
					${else:mibewHost}
						<img src="${mibewroot}/images/mibewlogo.gif" border="0" alt=""/>
					${endif:mibewHost}
				${endif:ct.company.chatLogoURL}
			</div>
		</div>
		<div class="col-xs-6 titulo" >
		  	<div class="pull-left" >
		  		Asesoría Virtual de
		  		<br />
		  		Bienestar Universitario
		   </div>
		</div>
		
	</div><!-- fin row -->
	<!-- fin header -->
		
	<div class="panel panel-danger">
		<div class="panel-heading">
			<h2 class="panel-title">
		
				${if:formgroupname}
					${form:groupname}: 
				${endif:formgroupname}
				${msg:leavemessage.title}.
				<!-- <div style="padding:10px;background-color:#f9f9f9;border-bottom: #579f73 1px solid;"> -->
		
			</h2><!-- fin panel-title -->
			¡${msg:leavemessage.descr}!
		</div><!-- fin panel-heading -->
		<div class="panel-body">
			${if:errors}
				${errors}
			${endif:errors}
			
				<!-- Campos email y Nombre del formulario cuando no hay operador conectado. Se rellenan con el email del usuario
		 que se obtiene desde la url
	-->
	<script type="text/javascript">
		function getParam(name){  
			name = name.replace(/[\[]/,"\\\[").replace(/[\]]/,"\\\]");  
			var regexS = "[\\?&]"+name+"=([^&#]*)";  
			var regex = new RegExp( regexS );  
			var results = regex.exec(window.location.href);
			if(results == null)
				return "";  
			else    
				return results[1];
			}
			//alert(getParam("mail_usuario"));
	</script>

	<form id="formDejarMensaje" class="form-horizontal cmxform" role="form" name="leaveMessageForm" method="post" action="${mibewroot}/leavemessage.php" onsubmit="return validateForm()"> 

		<input type="hidden" name="style" value="${styleid}"/>
		<input type="hidden" name="info" value="${form:info}"/>
		<input type="hidden" name="referrer" value="${page:referrer}"/>
		${if:formgroupid}<input type="hidden" name="group" value="${form:groupid}"/>${endif:formgroupid}

		<div class="form-group">
			<!-- ${msg:form.field.email}: -->
			<label class="col-sm-2 control-label hidden">${msg:form.field.email}:</label>
			<div class="col-sm-10">
				<script>
					document.write('<input type="email" id="email" name="email" value="'+getParam("mail_usuario")+'" class="form-control hidden" placeholder="Ingrese su correo" readonly required />');
				</script>
				<!-- <input type="text" name="email" size="50" value="${form:email}" class="username"/> -->
			</div>
		</div>
			
		<div class="form-group" style="display:none">
			<label class="col-sm-2 control-label">${msg:form.field.name}:</label>
			<div class="col-sm-10">
				<script type="text/javascript">
					document.write('<input id="name" type="text" name="name" value="'+getParam("mail_usuario")+'" class="form-control" placeholder="Ingrese su Nombre" readonly required />');
				</script>
				<!-- <input type="text" name="name" size="50" value="${form:name}" class="username"/> -->
			</div>
		</div>
		
		<div class="form-group">
			<label class="col-sm-2 control-label">${msg:form.field.message}:</label>
			<div class="col-sm-10">
				<textarea  class="form-control" name="message" tabindex="0" cols="40" rows="2" style="overflow:auto" placeholder="Escriba su mensaje aquí" required >${form:message}</textarea>
				<!-- <textarea name="message" tabindex="0" cols="40" rows="5" style="border:1px solid #878787; overflow:auto">${form:message}</textarea> -->
			</div>
		</div>
		
			${if:showcaptcha}
				<div class="form-grou">
					<img src="captcha.php"/>			
					<input type="text" name="captcha" size="50" maxlength="15" value="" class="username " required />
				</div>
			${endif:showcaptcha}
			
			<!-- botones enviar y cerrar -->
			<p class="text-center">
				<button id="sndmessagelnk" type="submit" class="btn btn-success active">${msg:mailthread.perform}</button>
				
				<a href="javascript:window.close();" class="btn btn-danger active" role="button" title="${msg:leavemessage.close}" style="text-decoration:none">
					${msg:leavemessage.close}
				</a>
			</p>
			<!-- fin botones enviar y cerrar -->
	</form>			
			
			
		</div><!-- fin panel-body -->
		
		<!-- panel-footer -->
		<!-- <div class="panel-footer"> -->
			<!-- <p class="text-center">-->
			<!-- <a href="javascript:document.leaveMessageForm.submit();" class="btn btn-success active" role="button" id="sndmessagelnk" style="text-decoration:none;margin-right:30px">${msg:mailthread.perform}</a>-->
			<!-- original <a href="javascript:document.leaveMessageForm.submit();" class="but" id="sndmessagelnk">${msg:mailthread.perform}</a> -->
			
			<!-- boton cerrar -->
			<!-- <a href="javascript:window.close();" title="${msg:leavemessage.close}"><img src="${tplroot}/images/buttons/back.gif" width="25" height="25" border="0" alt="" /></a> -->
			<!-- <a href="javascript:window.close();" class="btn btn-danger active" role="button" title="${msg:leavemessage.close}" style="text-decoration:none">${msg:leavemessage.close}</a>-->
			<!-- fin boton cerrar -->
			<!-- </p>
		</div>
		<!-- fin panel-footer -->
	</div><!-- fin panel warning -->
	
	




	<!-- Creditos Mibew -->
	<div style="margin-top:20px">
		<p class="text-center text-muted">
			<small>Estimado usuario, para el reporte de sugerencias, quejas, reclamos o reconocimientos, ingrese a:<br><a href="http://calidadonline.unitecnologica.edu.co/CalidadOnline/sugerenciareclamo" target="_blank">Calidad Online</a></small>
		</p>
		<p class="text-center">
			<span id="poweredByTD" align="center" class="copyr">
				${msg:chat.window.poweredby} <a id="poweredByLink" href="http://mibew.org" title="Mibew Community" target="_blank">mibew.org</a>
			</span>
		</p>
	</div>

</body>
</html>
