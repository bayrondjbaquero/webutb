<!DOCTYPE html>
<html lang="es">
<head>
<title>${msg:chat.window.title.agent}</title>
<link rel="shortcut icon" href="${mibewroot}/images/favicon.ico" type="image/x-icon">
<link rel="stylesheet" type="text/css" href="${tplroot}/chat.css">

<!-- Bootstrap css -->
<meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1, user-scalable=0 ">
<link rel="stylesheet" type="text/css" href="${mibewroot}/bootstrap-3.0.3/css/bootstrap.min.css" >

<!-- jQuery (necessary for Bootstrap's JavaScript plugins) -->
<!-- <script type="text/javascript" language="javascript" src="http://code.jquery.com/jquery.min.js"></script> -->
<script type="text/javascript" language="javascript" src="${mibewroot}/js/jquery-1.10.2.min.js"></script>
<!-- Include all compiled plugins (below), or include individual files as needed -->
<script type="text/javascript" language="javascript" src="${mibewroot}/bootstrap-3.0.3/js/bootstrap.min.js"></script>
<!-- fin Bootstrap css -->

<script type="text/javascript" language="javascript" src="${mibewroot}/js/${jsver}/common.js"></script>
<script type="text/javascript" language="javascript" src="${mibewroot}/js/${jsver}/brws.js"></script>
<script type="text/javascript" language="javascript"><!--
var threadParams = { servl:"${mibewroot}/thread.php",wroot:"${mibewroot}",frequency:${page:frequency},${if:user}user:"true",${endif:user}threadid:${page:ct.chatThreadId},token:${page:ct.token},cssfile:"${tplroot}/chat.css",ignorectrl:${page:ignorectrl} };
//-->
</script>
<script type="text/javascript" language="javascript" src="${mibewroot}/js/${jsver}/chat.js"></script>


<style type="text/css">

#header{
	height:50px;
	background:url(${tplroot}/images/bg_domain.gif) repeat-x top;
	background-color:#5AD66B;
	width:99.6%;
	margin:0px 0px 20px 0px;
}
#header .mmimg{
	background:url(${tplroot}/images/quadrat.gif) bottom left no-repeat;
}
.but{
	font-family:Verdana !important;
	font-size:11px;
	background:url(${tplroot}/images/wmchat.png) top left no-repeat;
	background-position:0px -25px;
	display:block;
	text-align:center;
	padding-top:2px;
	color:white;
	width:186px;
	height:18px;
	text-decoration:none;
}
.tplimage {
	background: transparent url(${tplroot}/images/wmchat.png) no-repeat scroll 0px 0px;
	width: 25px; height: 25px;
	-moz-background-clip: -moz-initial; 
	-moz-background-origin: -moz-initial; 
	-moz-background-inline-policy: -moz-initial;
}
.irefresh { background-position:-72px 0px; }
.iclose { background-position:-24px 0px; }
.iexec { background-position:-48px 0px; }
.ihistory, .ichangeuser { background-position:-96px 0px; }
.isend { background-position:-120px 0px; }
.issl { background-position:-144px 0px; }
.isound { background-position:-168px 0px; }
.inosound { background-position:-192px 0px; }
.iemail { background-position:0px 0px; }
.iclosewin { background-position:-187px -27px; width: 15px; height: 15px; }
.tplimageloc {
	background: transparent url(${mibewroot}${url:image.chat.sprite}) no-repeat scroll 0px 0px;
	-moz-background-clip: -moz-initial; 
	-moz-background-origin: -moz-initial; 
	-moz-background-inline-policy: -moz-initial;
}
.ilog { background-position: 0px 0px;width: 20px; height: 80px; }
.imessage { background-position: 0px -82px;width: 20px; height: 85px; }


.text-center {
	text-align: center;
}

.titulo{
	padding-left:0px;
	padding-top: 5px;
	font-weight: bold;
}


</style>


</head>



<body bgcolor="#FFFFFF" text="#000000" link="#C28400" vlink="#C28400" alink="#C28400" style="margin:0px 15px 10px 15px">



<!-- header -->

	<div class="row">	
				<div class="col-xs-6 wrap-logo">
				<div class="pull-right">
		    	${if:ct.company.chatLogoURL}
		    		${if:mibewHost}
		            	<a onclick="window.open('${page:mibewHost}');return false;" href="${page:mibewHost}">
			            	<img src="${page:ct.company.chatLogoURL}" border="0" alt=""/>
			            </a>
			        ${else:mibewHost}
		            	<img src="${page:ct.company.chatLogoURL}"  border="0" alt=""/>
			        ${endif:mibewHost}
			    ${else:ct.company.chatLogoURL}
	    			${if:mibewHost}
	        	    	<a onclick="window.open('${page:mibewHost}');return false;" href="${page:mibewHost}">
	        	    		<img src="${mibewroot}/images/mibewlogo.gif"  border="0" alt=""/>
	        	    	</a>
				    ${else:mibewHost}
				    	<img src="${mibewroot}/images/mibewlogo.gif"  border="0" alt=""/>
				    ${endif:mibewHost}
		        ${endif:ct.company.chatLogoURL}
		        </div>
		      </div>
		      <div class="col-xs-6 titulo" >
		      	<div class="pull-left" >
		        		Asesoría Virtual de
		        		<br />
		        		Bienestar Universitario
		        </div>
		      </div>
		
	</div>
	
	
<!--
	<div class="container">
				<span style="font-size:16px;font-weight:bold;color:#525252">${page:chat.title}</span>
	</div>
-->

<!-- fin header -->



<!-- chateando con -->

	<div class="panel panel-success">
		<div class="panel-heading">
			<div class="panel-title">
				<p class="text-center">
				<span class="glyphicon glyphicon-comment"></span>
				${if:agent}
					${if:historyParams}
						${msg:chat.window.chatting_with}
						<a href="${page:historyParamsLink}" target="_blank" title="${msg:page.analysis.userhistory.title}" onclick="this.newWindow = window.open('${page:historyParamsLink}', 'UserHistory', 'toolbar=0,scrollbars=0,location=0,statusbar=1,menubar=0,width=703,height=380,resizable=1');this.newWindow.focus();this.newWindow.opener=window;return false;">
							${page:ct.user.name}
						</a>
					${else:historyParams}
						${msg:chat.window.chatting_with} <b>${page:ct.user.name}</b>
					${endif:historyParams}
				${endif:agent}
		
				<!-- boton cambiar nombre en la ventana del usuario -->
				${if:user}
					${if:canChangeName}
						&nbsp;${page:ct.user.name}
					<!--
						<div id="changename1" style="display:${page:displ1};">
							${msg:chat.client.name}
							<img src="${mibewroot}/images/free.gif" width="10" height="1" border="0" alt="" />
							<input id="uname" type="text" size="12" value="${page:ct.user.name}" class="username">
							<img src="${mibewroot}/images/free.gif" width="5" height="1" border="0" alt="" />
							<a href="javascript:void(0)" onclick="return false;" title="${msg:chat.client.changename}">
								<img class="tplimage iexec" src="${mibewroot}/images/free.gif" border="0" alt="&gt;&gt;" />
							</a>
							
						</div>
						<div id="changename2" style="display:${page:displ2};">
							<a id="unamelink" href="javascript:void(0)" onclick="return false;" title="${msg:chat.client.changename}">
								${page:ct.user.name}
							</a>
							<img src="${mibewroot}/images/free.gif" width="10" height="1" border="0" alt="" />
							<a href="javascript:void(0)" onclick="return false;" title="${msg:chat.client.changename}">
								<img class="tplimage ichangeuser" src="${mibewroot}/images/free.gif" border="0" alt="" />
							</a>
						</div>
						-->
						
					${else:canChangeName}
						<!-- ${msg:chat.client.name}&nbsp;${page:ct.user.name} -->
						&nbsp;${page:ct.user.name}
					${endif:canChangeName}

				${endif:user}
				</p>
				<!-- boton cambiar nombre en la ventana del usuario -->
				
				<!-- botones configuraciones -->
				<div class=""> <!-- solamente se muestar en Escritorios>=1200px -->
					<p class="text-center">
				${if:agent}
					<img src="${mibewroot}/images/free.gif" width="10" height="1" border="0" alt="" />
					<a class="closethread" href="javascript:void(0)" onclick="return false;" title="${msg:chat.window.close_title}">
						<img class="tplimage iclose" src="${mibewroot}/images/free.gif" border="0" alt="${msg:chat.window.close_title}"/>
					</a>
				${endif:agent}
				
				<!-- boton enviar cnversacion por correo de la ventana del usuario -->
				<!--
				${if:user}
					<a href="${page:mailLink}&amp;style=${styleid}" target="_blank" title="${msg:chat.window.toolbar.mail_history}" onclick="this.newWindow = window.open('${page:mailLink}&amp;style=${styleid}', 'ForwardMail', 'toolbar=0,scrollbars=0,location=0,statusbar=1,menubar=0,width=603,height=254,resizable=0'); if (this.newWindow != null) {this.newWindow.focus();this.newWindow.opener=window;}return false;">
						<img class="tplimage iemail" src="${mibewroot}/images/free.gif" border="0" alt="Mail&nbsp;"/>
					</a>
				${endif:user}
				-->
				<!-- fin boton enviar cnversacion por correo de la ventana del usuario -->
			
				${if:agent}
					${if:canpost}
						<a href="${page:redirectLink}&amp;style=${styleid}" title="${msg:chat.window.toolbar.redirect_user}">
							<img class="tplimage isend" src="${mibewroot}/images/free.gif" border="0" alt="Redirect&nbsp;" />
						</a>
					${endif:canpost}
					${if:historyParams}
						<a href="${page:historyParamsLink}" target="_blank" title="${msg:page.analysis.userhistory.title}" onclick="this.newWindow = window.open('${page:historyParamsLink}', 'UserHistory', 'toolbar=0,scrollbars=0,location=0,statusbar=1,menubar=0,width=720,height=480,resizable=1');this.newWindow.focus();this.newWindow.opener=window;return false;">
							<img class="tplimage ihistory" src="${mibewroot}/images/free.gif" border="0" alt="History&nbsp;"/>
						</a>
					${endif:historyParams}
				${endif:agent}
			
				<a id="togglesound" href="javascript:void(0)" onclick="return false;" title="${msg:chat.window.toolbar.toggle_sound}">
					<img id="soundimg" class="tplimage isound" src="${mibewroot}/images/free.gif" border="0" alt="Sound&nbsp;" />
				</a>
	
				<a id="refresh" href="javascript:void(0)" onclick="return false;" title="${msg:chat.window.toolbar.refresh}">
					<img class="tplimage irefresh" src="${mibewroot}/images/free.gif" border="0" alt="Refresh&nbsp;" />
				</a>
			
				${if:sslLink}
					<a href="${page:sslLink}&amp;style=${styleid}" title="SSL" >
						<img class="tplimage issl" src="${mibewroot}/images/free.gif" border="0" alt="SSL&nbsp;"/>
					</a>
				${endif:sslLink}
				
				<!-- enlace mibew.org -->
				<!--
				<img src="${mibewroot}/images/free.gif" width="20" height="1" border="0" alt="" />
				<a href="${msg:site.url}" title="${msg:company.title}" target="_blank">
					${msg:site.title}
				</a>
				-->
				<!-- fin enlace mibew.org -->			
				
				
				<!-- boton salir del chat -->
				<a class="closethread btn btn-danger btn-xs active" role="button" href="javascript:void(0)" onclick="return false;" title="${msg:chat.window.close_title}" >
					 <!-- <img class="" src="${mibewroot}/images/buttons/botonCerrar.jpg" height="22" border="0" alt="${msg:chat.window.close_title}"/> -->
					 ${msg:leavemessage.close} 
				</a>
				<!-- fin boton salir del chat -->
				
				<!-- fin botones configuraciones -->				
				
				</p>
				</div><!-- cerrar visible-lg -->
				
				
				
			</div><!-- fin panel-title -->
		</div><!-- fin panel-heading -->
		<!-- fin chateando con -->
	

	<div class="panel-body">
			

	

<!--
<img class="tplimageloc ilog" src="${mibewroot}/images/free.gif" border="0" alt="" />
<img src="${mibewroot}/images/free.gif" width="1" height="1" border="0" alt="" />
<img src="${mibewroot}/images/free.gif" width="1" height="1" border="0" alt="" />
-->
			
				<iframe id="chatwnd" width="100%" height="30%" src="${if:neediframesrc}${mibewroot}/images/blank.html${endif:neediframesrc}" frameborder="0" style="overflow:auto;">
				Sorry, your browser does not support iframes; try a browser that supports W3 standards.
				</iframe>
			
				
	</div><!-- fin panel-body -->
	
	<div class="panel-footer">
${if:canpost}
		<!--
		<img class="tplimageloc imessage" src="${mibewroot}/images/free.gif" border="0" alt="" />
		
	    ${if:isOpera95}
    	<td width="100%" height="60%" valign="top" id="msgwndtd">
    	${else:isOpera95}
	    	        	<td width="100%" height="100" valign="top" id="msgwndtd">
    	${endif:isOpera95}
    	-->
    	
    	<!-- input mensaje y boton enviar -->
    	<div class="input-group">
			<!-- <textarea id="msgwnd" class="message" tabindex="0"></textarea> -->
			<input id="msgwnd" type="text" class="form-control input-sm" placeholder="Escriba su mensaje aquí" />
			<span class="input-group-btn">
				 ${if:canpost}
					<table cellspacing="0" cellpadding="0" border="0" id="postmessage">
						<tr>
							<td>
								<a href="javascript:void(0)" onclick="return false;" class="btn btn-success btn-sm active" role="button" id="sndmessagelnk" onclick="focusOnElement('objetivo');">
									<!-- ${msg:chat.window.send_message_short,send_shortcut} -->
									${msg:leavemessage.perform}
								</a>
							</td>
						</tr>
					</table>
				 ${endif:canpost}
         </span>
			
		</div>
		<!-- fin input mensaje y boton enviar -->

${endif:canpost}


${if:agent}
	${if:canpost}
				<select id="predefined" size="1" class="answer">
				<option>${msg:chat.window.predefined.select_answer}</option>
				${page:predefinedAnswers}
				</select>

	${endif:canpost}
${endif:agent}

		<!-- avisa si se esta escribiendo -->
		<div class="">
						<div id="engineinfo" style="display:none;">
						</div>
						<div id="typingdiv" class="center-block" style="display:none;">
							${msg:typing.remote}	
						</div>
		</div>
		<!-- fin avisa si se esta escribiendo -->

	</div> <!-- fin panel-footer -->

</div><!-- fin panel-success -->


<!-- Creditos Mibew -->
	<div style="margin-top:20px">
		<p class="text-center text-muted">
			<small>Estimado usuario, para el reporte de sugerencias, quejas, reclamos o reconocimientos, ingrese a:<br><a href="http://calidadonline.unitecnologica.edu.co/CalidadOnline/sugerenciareclamo" target="_blank">Calidad Online</a></small>
		</p>
		<p class="text-center">
			<span id="poweredByTD" align="center" class="copyr">
				${msg:chat.window.poweredby} <a id="poweredByLink" href="http://mibew.org" title="Mibew Community" target="_blank">mibew.org</a>
			</span>
		</p>
	</div>
	


</body>
</html>
