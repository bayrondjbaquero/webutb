<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<title>${msg:chat.window.title.user}</title>
<link rel="shortcut icon" href="${mibewroot}/images/favicon.ico" type="image/x-icon"/>
<link rel="stylesheet" type="text/css" href="${tplroot}/chat.css" />

<!-- Bootstrap css -->
<meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1, user-scalable=0 ">
<link href="${mibewroot}/bootstrap-3.0.3/css/bootstrap.min.css" rel="stylesheet">
<!-- fin Bootstrap css -->

<!-- jQuery (necessary for Bootstrap's JavaScript plugins) -->
<!-- <script type="text/javascript" language="javascript" src="http://code.jquery.com/jquery.min.js"></script> -->
<script type="text/javascript" language="javascript" src="${mibewroot}/js/jquery-1.10.2.min.js"></script>
<!-- Include all compiled plugins (below), or include individual files as needed -->
<script type="text/javascript" language="javascript" src="${mibewroot}/bootstrap-3.0.3/js/bootstrap.min.js"></script>
<style type="text/css">
#header{
	height:50px;
	background:url(${tplroot}/images/bg_domain.gif) repeat-x top;
	background-color:#5AD66B;
	width:99.6%;
	margin:0px 0px 20px 0px;
}
#header .mmimg{
	background:url(${tplroot}/images/quadrat.gif) bottom left no-repeat;
}
</style>
</head>
<body bgcolor="#FFFFFF" text="#000000" link="#C28400" vlink="#C28400" alink="#C28400" style="margin:0px">

	<!-- header -->
	<div class="row">
		<div class="col-xs-6 wrap-logo">
			<div class="pull-right">
				${if:ct.company.chatLogoURL}
					${if:mibewHost}
						<a onclick="window.open('${page:mibewHost}');return false;" href="${page:mibewHost}">
							<img src="${page:ct.company.chatLogoURL}" border="0" alt=""/>
						</a>
					${else:mibewHost}
						<img src="${page:ct.company.chatLogoURL}" border="0" alt=""/>
					${endif:mibewHost}
				${else:ct.company.chatLogoURL}
					${if:mibewHost}
						<a onclick="window.open('${page:mibewHost}');return false;" href="${page:mibewHost}">
							<img src="${mibewroot}/images/mibewlogo.gif" border="0" alt=""/>
						</a>
					${else:mibewHost}
						<img src="${mibewroot}/images/mibewlogo.gif" border="0" alt=""/>
					${endif:mibewHost}
				${endif:ct.company.chatLogoURL}
			</div>
		</div>
		<div class="col-xs-6 titulo" >
		  	<div class="pull-left" >
		  		Asesoría Virtual de
		  		<br />
		  		Bienestar Universitario
		   </div>
		</div>
		
	</div><!-- fin row -->
	<!-- fin header -->

		<table cellspacing="0" cellpadding="0" border="0" id="header" class="bg_domain">
		<tr>
			<td style="padding-left:20px;color:white;" class="mmimg" width="770">
				${msg:chat.mailthread.sent.content,email}
			</td>
			<td align="right" style="padding-right:17px;">
				<table cellspacing="0" cellpadding="0" border="0">
				<tr>
					<td><a href="javascript:window.close();" title="${msg:chat.mailthread.sent.close}"><img src="${tplroot}/images/buttons/back.gif" width="25" height="25" border="0" alt="" /></a></td>
					<td width="5"></td>
					<td class="button"><a href="javascript:window.close();" title="${msg:chat.mailthread.sent.close}">${msg:chat.mailthread.sent.close}</a></td>
				</tr>
				</table>
			</td>
		</tr>
		</table>

</body>
</html>
