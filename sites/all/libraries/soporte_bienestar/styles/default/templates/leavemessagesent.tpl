<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<title>${msg:leavemessage.sent.title}</title>
<link rel="shortcut icon" href="${mibewroot}/images/favicon.ico" type="image/x-icon"/>
<link rel="stylesheet" type="text/css" href="${tplroot}/chat.css" />

<!-- Bootstrap css -->
<meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1, user-scalable=0 ">
<link href="${mibewroot}/bootstrap-3.0.3/css/bootstrap.min.css" rel="stylesheet">
<!-- fin Bootstrap css -->

<!-- jQuery (necessary for Bootstrap's JavaScript plugins) -->
<!-- <script type="text/javascript" language="javascript" src="http://code.jquery.com/jquery.min.js"></script> -->
<script type="text/javascript" language="javascript" src="${mibewroot}/js/jquery-1.10.2.min.js"></script>
<!-- Include all compiled plugins (below), or include individual files as needed -->
<script type="text/javascript" language="javascript" src="${mibewroot}/bootstrap-3.0.3/js/bootstrap.min.js"></script>
<style type="text/css">
#header{
	height:50px;
	background:url(${tplroot}/images/bg_domain.gif) repeat-x top;
	background-color:#5AD66B;
	width:99.6%;
	margin:0px 0px 20px 0px;
}
#header .mmimg{
	background:url(${tplroot}/images/quadrat.gif) bottom left no-repeat;
}

.text-center {
	text-align: center;
}

.titulo{
	padding-left:0px;
	padding-top: 5px;
	font-weight: bold;
}
</style>
</head>
<body bgcolor="#FFFFFF" text="#000000" link="#C28400" vlink="#C28400" alink="#C28400" style="margin:0px 15px 10px 15px">
	
	<!-- header -->
	<div class="row">
		<div class="col-xs-6 wrap-logo">
			<div class="pull-right">
				${if:ct.company.chatLogoURL}
					${if:mibewHost}
						<a onclick="window.open('${page:mibewHost}');return false;" href="${page:mibewHost}">
							<img src="${page:ct.company.chatLogoURL}" border="0" alt=""/>
						</a>
					${else:mibewHost}
						<img src="${page:ct.company.chatLogoURL}" border="0" alt=""/>
					${endif:mibewHost}
				${else:ct.company.chatLogoURL}
					${if:mibewHost}
						<a onclick="window.open('${page:mibewHost}');return false;" href="${page:mibewHost}">
							<img src="${mibewroot}/images/mibewlogo.gif" border="0" alt=""/>
						</a>
					${else:mibewHost}
						<img src="${mibewroot}/images/mibewlogo.gif" border="0" alt=""/>
					${endif:mibewHost}
				${endif:ct.company.chatLogoURL}
			</div>
		</div>
		<div class="col-xs-6 titulo" >
		  	<div class="pull-left" >
		  		Asesoría Virtual de
		  		<br />
		  		Bienestar Universitario
		   </div>
		</div>
		
	</div><!-- fin row -->
	<!-- fin header -->
	
	<div class="panel panel-success">
		<div class="panel-heading">
			<h2 class="panel-title">
				${msg:leavemessage.sent.title}
			</h2><!-- fin panel-title -->
			¡${msg:leavemessage.sent.message}!
		</div><!-- fin panel-heading -->
		
		<div class="panel-body">
			<p class="text-center">
									<a href="javascript:window.close();" class="btn btn-danger btn-lg active" role="button" title="${msg:chat.mailthread.sent.close}" style="text-decoration:none">
										<!-- <img src="${tplroot}/images/buttons/back.gif" width="25" height="25" border="0" alt="" /> -->
										${msg:chat.mailthread.sent.close}
									</a>
			</p>
		</div><!-- fin panel-body -->	
	</div><!-- fin panel -->
	
	
	
		

</body>
</html>
