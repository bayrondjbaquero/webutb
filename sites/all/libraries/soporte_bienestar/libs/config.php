<?php
/*
 * Copyright 2005-2013 the original author or authors.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

/*
 *  Application path on server
 */
//echo "cargando 1<br>";
//include("dset.php");
//echo "cargando 4<br>";
require_once(dirname(__FILE__) . '/../../../../bienestar.unitecnologica.edu.co/settings.php');
$mibewroot = "/sites/all/libraries/soporte_bienestar";

/*
 *  Internal encoding
 */
$mibew_encoding = "utf-8";

/*
 *  MySQL Database parameters
 */
//echo "cargando 2<br>";

/*
$mysqlhost = "localhost";
$mysqldb = "chat_bienestar";
$mysqllogin = "root";
$mysqlpass = "12345";
*/
/*
$databases = array (
  'default' => 
  array (
    'default' => 
    array (
      'database' => 'bienestar',
      'username' => 'root',
      'password' => '12345',
      'host' => 'localhost',
      'port' => '',
      'driver' => 'mysql',
      'prefix' => '',
    ),
  ),
);
*/
$mysqlhost = $databases['default']['default']['host'];
//echo $mysqlhost."<br>";
//$mysqldb = $databases['default']['default']['database'];
$mysqldb = "bienestar_chat";
//echo $mysqldb."<br>";
$mysqllogin = $databases['default']['default']['username'];
//echo $mysqllogin."<br>";
$mysqlpass = $databases['default']['default']['password'];
//echo $mysqlpass."<br>";

$mysqlprefix = "";

$dbencoding = "utf8";
$force_charset_in_connection = true;

/*
 *  Mailbox
 */
$mibew_mailbox = "asesoriabienestar@unitecnologica.edu.co";
$mail_encoding = "utf-8";

/*
 *  Locales
 */
$home_locale = "es"; /* native name will be used in this locale */
$default_locale = "es"; /* if user does not provide known lang */

?>
