<?php if (!empty($body)): ?>

    <article id="node-<?php print $node->nid; ?>" class="<?php print $classes; ?> clearfix"<?php print $attributes; ?>> <?php print $user_picture; ?> <?php print render($title_prefix); ?>

    </div>
<?php endif; ?>        
<div class="header article-header">  
    <div class="row clearfix" style="margin-bottom: 30px;">

        <div class="col-md-12 column">
            <?php print $node->body['und'][0]['safe_value']; ?>                            
            <hr>
            <div class="row">
                <!-- Navigation Buttons -->
                <div class="col-md-2">

                    <ul class="nav nav-pills nav-stacked" id="derecho">                                                         

                        <?php
                        for ($i = 0;; $i++) {
                            $ID = $node->field_derecho_pecuniario["und"][$i]["entity"]->nid;

                            if ($ID == 0 or strcmp("0", $node->status) == 0) {
                                break;
                            }
                            if ($i == 0) {
                                ?>
                                <li class="active"><a href="#<?php print $node->field_derecho_pecuniario["und"][$i]["entity"]->nid ?>"  data-toggle="tab"><?php print $node->field_derecho_pecuniario["und"][$i]["entity"]->title; ?></a></li>
                                <?php
                            } else {
                                ?>
                                <li><a href="#<?php print $node->field_derecho_pecuniario["und"][$i]["entity"]->nid ?>"  data-toggle="tab"><?php print $node->field_derecho_pecuniario["und"][$i]["entity"]->title; ?></a></li>

                                <?php
                            }
                        }
                        ?>



                    </ul>                                    
                </div>

                <!-- Content -->
                <div class="col-md-10 padding-x linea-caja-left caja-sombra efecto-sombra-2">

                    <div class="tab-content">


                        <?php
                        for ($i = 0;; $i++) {
                            $ID = $node->field_derecho_pecuniario["und"][$i]["entity"]->nid;

                            if ($ID == 0 or strcmp("0", $node->status) == 0) {
                                break;
                            }
                            ?>
                            <?php if ($i == 0): ?>
                                <div class="tab-pane active" id="<?php print $node->field_derecho_pecuniario["und"][$i]["entity"]->nid ?>">

                                <?php else: ?>
                                    <div class="tab-pane" id="<?php print $node->field_derecho_pecuniario["und"][$i]["entity"]->nid ?>">

                                    <?php endif; ?>
                                    <?php
                                    print "<h3>" . $node->field_derecho_pecuniario["und"][$i]["entity"]->title . "</h3>";

                                    print $node->field_derecho_pecuniario["und"][$i]["entity"]->body["und"][0]["safe_value"];
                                    ?>

                                    <?php if ($node->field_derecho_pecuniario["und"][$i]["entity"]->field_programas["und"][0]["target_id"] || $node->field_derecho_pecuniario["und"][$i]["entity"]->field_programas_programaspos["und"][0]["target_id"]): ?>

                                        <table class="table table-striped table-responsive table-bordered">
                                            <tbody>
                                                <tr>
                                                    <th>Programa</th>
                                                    <th width="25%" >Precio</th>
                                                </tr>    

                                                <?php
                                                for ($j = 0;; $j++) {
                                                    $ID = $node->field_derecho_pecuniario["und"][$i]["entity"]->field_programas["und"][$j]["target_id"];

                                                    if ($ID == 0 or strcmp("0", $node->status) == 0) {
                                                        break;
                                                    }


                                                    $programa = node_load($node->field_derecho_pecuniario["und"][$i]["entity"]->field_programas["und"][$j]["target_id"]);
                                                    ?>
                                                    <tr>
                                                        <td><?php print $programa->title; ?></td>
                                                        <td><?php print $programa->field_precio["und"][0]["value"]; ?></td>            
                                                    </tr> 
                                                    <?php
                                                }
                                                ?>

                                                <?php
                                                for ($j = 0;; $j++) {
                                                    $ID = $node->field_derecho_pecuniario["und"][$i]["entity"]->field_programas_programaspos["und"][$j]["target_id"];

                                                    if ($ID == 0 or strcmp("0", $node->status) == 0) {
                                                        break;
                                                    }


                                                    $programa = node_load($node->field_derecho_pecuniario["und"][$i]["entity"]->field_programas_programaspos["und"][$j]["target_id"]);
                                                    ?>
                                                    <tr>
                                                        <td><?php print $programa->title; ?></td>
                                                        <td><?php print $programa->field_precio["und"][0]["value"]; ?></td>            
                                                    </tr> 
                                                    <?php
                                                }
                                                ?>

                                            </tbody>
                                        </table>

                                    <?php endif; ?>

                                    <?php
                                    print $node->field_derecho_pecuniario["und"][$i]["entity"]->field_otros["und"][0]["safe_value"];

                                    print $node->field_derecho_pecuniario["und"][$i]["entity"]->field_texto_inferior["und"][0]["safe_value"];
                                    ?>



                                </div>                        
                                <?php
                            }
                            ?>

                        </div>

                    </div>

                </div>

            </div>

        </div>

    </div>

</article>