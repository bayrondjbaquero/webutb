<?php
$entity = node_load($fields['nid']->raw);
if ($entity->field_cursos["und"][0]["target_id"]) :
    ?>   
    <div class="panel panel-default">

        <div class="panel-heading">
            <a class="panel-title" data-toggle="collapse" data-parent="#panel-cur" href="#<?php print $entity->nid . "ext"; ?>">
                <?php print $entity->title; ?></a>
        </div>

        <div id="<?php print $entity->nid . "ext"; ?>" class="panel-collapse collapse">

            <div class="panel-body">
                <div class="panel-group" id="panel-ext">

                    <p>La oferta de cursos en programas profesionales de la <?php print $entity->title; ?> está compuesta de la siguiente manera:</p>
                    <br />                

                    <?php
                    $cur = array();
                    $sem = array();
                    $dip = array();
                    $min = array();
                    $tal = array();
                    $cha = array();
                    $conf = array();


                    for ($i = 0;; $i++) :
                        if (!$entity->field_cursos["und"][$i]["target_id"]) :
                            break;
                        endif;
                        $programa = node_load($entity->field_cursos["und"][$i]["target_id"]);

                        $temp = $programa->field_tipo["und"][0]['value'];


                        if (strcmp($temp, "Curso") == 0) {
                            array_push($cur, $programa);
                        }
                        if (strcmp($temp, "Seminario") == 0) {
                            array_push($sem, $programa);
                        }
                        if (strcmp($temp, "Diplomado") == 0) {
                            array_push($dip, $programa);
                        }
                        if (strcmp($temp, "Minor") == 0) {
                            array_push($min, $programa);
                        }
                        if (strcmp($temp, "Taller") == 0) {
                            array_push($tal, $programa);
                        }
                        if (strcmp($temp, "Charla") == 0) {
                            array_push($cha, $programa);
                        }
                        if (strcmp($temp, "Conferencia") == 0) {
                            array_push($conf, $programa);
                        }
                        ?>                   


                    <?php endfor; ?>
                    <!--                        CADA PROGRAMA/CURSO                     -->            
                    <?php if (count($cur) != 0): ?>
                        <div class="panel panel-default">
                            <div class="panel-heading">
                                <a class="panel-title" data-parent="#panel-ext" data-toggle="collapse" href="#<?php print $entity->nid . "cur"; ?>"> Cursos </a>
                            </div>
                            <div class="panel-collapse collapse" id="<?php print $entity->nid . "cur"; ?>" style="height: auto;">
                                <div class="panel-body">
                                    <p>La oferta de Cursos de la <?php print $entity->title; ?> está compuesta de la siguiente manera:</p>
                                    <br>

                                    <!--                        CADA PROGRAMA/CURSO                     -->
                                    <div class="list-group">

                                        <?php
                                        for ($z = 0; $z < count($cur); $z++) :
                                            $pro = $cur[$z];
                                            $ID = $pro->nid;
                                            $alias = drupal_get_path_alias('node/' . $ID);
                                            ?>

                                            <a class="list-group-item" href="<?php print($alias); ?>">
                                                <?php if ($pro->field_nuevo): if (strcmp($pro->field_nuevo ["und"][0]["value"], '1') == 0): ?>
                                                        <span class="badge pull-right">Nuevo</span>    
                                                    <?php endif; ?>  <?php endif; ?><?php
                                                print($pro->title);
                                                ?>                                    
                                            </a>
                                        <?php endfor; ?>

                                    </div>
                                </div>
                            </div>
                        </div>
                    <?php endif; ?>
                    <?php if (count($sem) != 0): ?> 
                        <div class="panel panel-default">
                            <div class="panel-heading">
                                <a class="panel-title" data-parent="#panel-ext" data-toggle="collapse" href="#<?php print $entity->nid . "sem"; ?>"> Seminarios </a>
                            </div>
                            <div class="panel-collapse collapse" id="<?php print $entity->nid . "sem"; ?>" style="height: auto;">
                                <div class="panel-body">
                                    <p>La oferta de Seminarios de la  <?php print $entity->title; ?> está compuesta de la siguiente manera:</p>
                                    <br>

                                    <!--                        CADA PROGRAMA/CURSO                     -->
                                    <div class="list-group">

                                        <?php
                                        for ($z = 0; $z < count($sem); $z++) :
                                            $pro = $sem[$z];
                                            $ID = $pro->nid;
                                            $alias = drupal_get_path_alias('node/' . $ID);
                                            ?>

                                            <a class="list-group-item" href="<?php print($alias); ?>">
                                                <?php if ($pro->field_nuevo): if (strcmp($pro->field_nuevo ["und"][0]["value"], '1') == 0): ?>
                                                        <span class="badge pull-right">Nuevo</span>    
                                                    <?php endif; ?>  <?php endif; ?><?php
                                                print($pro->title);
                                                ?>                                    
                                            </a>
                                        <?php endfor; ?>

                                    </div>
                                </div>
                            </div>
                        </div>
                    <?php endif; ?>
                    <?php if (count($dip) != 0): ?>               
                        <div class="panel panel-default">
                            <div class="panel-heading">
                                <a class="panel-title" data-parent="#panel-ext" data-toggle="collapse" href="#<?php print $entity->nid . "dip"; ?>"> Diplomados </a>
                            </div>
                            <div class="panel-collapse collapse" id="<?php print $entity->nid . "dip"; ?>" style="height: auto;">
                                <div class="panel-body">
                                    <p>La oferta de diplomados de la <?php print $entity->title; ?> está compuesta de la siguiente manera:</p>
                                    <br>

                                    <!--                        CADA PROGRAMA/CURSO                     -->
                                    <div class="list-group">

                                        <?php
                                        for ($z = 0; $z < count($dip); $z++) :
                                            $pro = $dip[$z];
                                            $ID = $pro->nid;
                                            $alias = drupal_get_path_alias('node/' . $ID);
                                            ?>

                                            <a class="list-group-item" href="<?php print($alias); ?>">
                                                <?php if ($pro->field_nuevo): if (strcmp($pro->field_nuevo ["und"][0]["value"], '1') == 0): ?>
                                                        <span class="badge pull-right">Nuevo</span>    
                                                    <?php endif; ?>  <?php endif; ?><?php
                                                print($pro->title);
                                                ?>                                    
                                            </a>
                                        <?php endfor; ?>

                                    </div>
                                </div>
                            </div>
                        </div>
                    <?php endif; ?>               
                    <?php if (count($min) != 0): ?>
                        <div class="panel panel-default">
                            <div class="panel-heading">
                                <a class="panel-title" data-parent="#panel-ext" data-toggle="collapse" href="#<?php print $entity->nid . "min"; ?>"> Minors </a>
                            </div>
                            <div class="panel-collapse collapse" id="<?php print $entity->nid . "min"; ?>" style="height: auto;">
                                <div class="panel-body">
                                    <p>La oferta de posgrado en minor de la <?php print $entity->title; ?> está compuesta de la siguiente manera:</p>
                                    <br>

                                    <!--                        CADA PROGRAMA/CURSO                     -->
                                    <div class="list-group">

                                        <?php
                                        for ($z = 0; $z < count($min); $z++) :
                                            $pro = $min[$z];
                                            $ID = $pro->nid;
                                            $alias = drupal_get_path_alias('node/' . $ID);
                                            ?>

                                            <a class="list-group-item" href="<?php print($alias); ?>">
                                                <?php if ($pro->field_nuevo): if (strcmp($pro->field_nuevo ["und"][0]["value"], '1') == 0): ?>
                                                        <span class="badge pull-right">Nuevo</span>    
                                                    <?php endif; ?>  <?php endif; ?><?php
                                                print($pro->title);
                                                ?>                                    
                                            </a>
                                        <?php endfor; ?>

                                    </div>
                                </div>
                            </div>
                        </div>
                    <?php endif; ?>
                    <?php if (count($tal) != 0): ?>
                        <div class="panel panel-default">
                            <div class="panel-heading">
                                <a class="panel-title" data-parent="#panel-ext" data-toggle="collapse" href="#<?php print $entity->nid . "tal"; ?>"> Talleres</a>
                            </div>
                            <div class="panel-collapse collapse" id="<?php print $entity->nid . "tal"; ?>" style="height: auto;">
                                <div class="panel-body">
                                    <p>La oferta de posgrado en talleres de la <?php print $entity->title; ?> está compuesta de la siguiente manera:</p>
                                    <br>

                                    <!--                        CADA PROGRAMA/CURSO                     -->
                                    <div class="list-group">

                                        <?php
                                        for ($z = 0; $z < count($tal); $z++) :
                                            $pro = $tal[$z];
                                            $ID = $pro->nid;
                                            $alias = drupal_get_path_alias('node/' . $ID);
                                            ?>

                                            <a class="list-group-item" href="<?php print($alias); ?>">
                                                <?php if ($pro->field_nuevo): if (strcmp($pro->field_nuevo ["und"][0]["value"], '1') == 0): ?>
                                                        <span class="badge pull-right">Nuevo</span>    
                                                    <?php endif; ?>  <?php endif; ?><?php
                                                print($pro->title);
                                                ?>                                    
                                            </a>
                                        <?php endfor; ?>

                                    </div>
                                </div>
                            </div>
                        </div> 
                    <?php endif; ?>
                    <?php if (count($cha) != 0): ?>
                        <div class="panel panel-default">
                            <div class="panel-heading">
                                <a class="panel-title" data-parent="#panel-ext" data-toggle="collapse" href="#<?php print $entity->nid . "cha"; ?>"> Charlas </a>
                            </div>
                            <div class="panel-collapse collapse" id="<?php print $entity->nid . "cha"; ?>" style="height: auto;">
                                <div class="panel-body">
                                    <p>La oferta de charlas de la <?php print $entity->title; ?> está compuesta de la siguiente manera:</p>
                                    <br>

                                    <!--                        CADA PROGRAMA/CURSO                     -->
                                    <div class="list-group">

                                        <?php
                                        for ($z = 0; $z < count($cha); $z++) :
                                            $pro = $cha[$z];
                                            $ID = $pro->nid;
                                            $alias = drupal_get_path_alias('node/' . $ID);
                                            ?>

                                            <a class="list-group-item" href="<?php print($alias); ?>">
                                                <?php if ($pro->field_nuevo): if (strcmp($pro->field_nuevo ["und"][0]["value"], '1') == 0): ?>
                                                        <span class="badge pull-right">Nuevo</span>    
                                                    <?php endif; ?>  <?php endif; ?><?php
                                                print($pro->title);
                                                ?>                                    
                                            </a>
                                        <?php endfor; ?>

                                    </div>
                                </div>
                            </div>
                        </div> 
                    <?php endif; ?>
                    <?php if (count($conf) != 0): ?>
                        <div class="panel panel-default">
                            <div class="panel-heading">
                                <a class="panel-title" data-parent="#panel-ext" data-toggle="collapse" href="#<?php print $entity->nid . "conf"; ?>"> Conferencias </a>
                            </div>
                            <div class="panel-collapse collapse" id="<?php print $entity->nid . "conf"; ?>" style="height: auto;">
                                <div class="panel-body">
                                    <p>La oferta de conferencias de la <?php print $entity->title; ?> está compuesta de la siguiente manera:</p>
                                    <br>

                                    <!--                        CADA PROGRAMA/CURSO                     -->
                                    <div class="list-group">

                                        <?php
                                        for ($z = 0; $z < count($conf); $z++) :
                                            $pro = $conf[$z];
                                            $ID = $pro->nid;
                                            $alias = drupal_get_path_alias('node/' . $ID);
                                            ?>

                                            <a class="list-group-item" href="<?php print($alias); ?>">
                                                <?php if ($pro->field_nuevo): if (strcmp($pro->field_nuevo ["und"][0]["value"], '1') == 0): ?>
                                                        <span class="badge pull-right">Nuevo</span>    
                                                    <?php endif; ?>  <?php endif; ?><?php
                                                print($pro->title);
                                                ?>                                    
                                            </a>
                                        <?php endfor; ?>

                                    </div>
                                </div>
                            </div>
                        </div> 
                    <?php endif; ?>
                </div>
            </div>
        </div>  
    </div>
<?php endif; ?> 