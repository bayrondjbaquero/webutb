<?php if (!empty($body)): ?>

    <article id="node-<?php print $node->nid; ?>" class="<?php print $classes; ?> clearfix"<?php print $attributes; ?>> <?php print $user_picture; ?> <?php print render($title_prefix); ?>

        <div id="article-<?php print $node->nid; ?>" class="article <?php
        print $classes;
        if ($display_submitted)
            
            ?>clearfix" <?php print $attributes; ?> >
             <?php if ($display_submitted): ?>
                <div class="footer submitted">
                    <?php print $user_picture; ?>
                    <?php print '<div class="time pubdate" title="' . $datetime . '">' . t($created_date) . '</div>'; ?>
                    <div class="info-wrapper">
                        <?php
                        if ($node_category)
                            print '<span class="node-category"> <span class="category-in">' . t('In') . ' </span>' . render($node_category) . '</span>';
                        print '<span class="author">' . t('Por') . ' ' . $name . '</span>';
                        print '<span class="comment-comments">' . render($comments_count) . '</span>';
                        ?>
                    </div>
                    <?php ?>
                </div>
            <?php endif; ?>            
            <?php if ($title): ?>
                <div class="header article-header">  
                    <div class="row clearfix" style="margin-bottom: 30px;">

                        <div class="col-md-12 column">
                            <?php print $node->body['und'][0]['safe_summary']; ?>                            
                            <hr>
                            <div class="row">

                                <?php if ($node->field_mini_foto): ?>
                                    <div class="col-md-2">
                                        <?php
                                        $uriImagefromNode = $node->field_mini_foto['und'][0]['uri'];
                                        $urlImagefromNode = file_create_url($uriImagefromNode);
                                        ?>
                                        <img alt="<?php print $node->field_mini_foto['und'][0]['alt']; ?>" src="<?php print $urlImagefromNode; ?>" class="img-responsive minifoto"/>
                                    </div>
                                    <div class="col-md-10">
                                    <?php else: ?>
                                        <div class="col-md-12">
                                        <?php endif; ?>
                                        <table class="table table-striped">
                                            <tbody>
                                                <tr>
                                                    <th>Modalidad</th>
                                                    <th>Tipo</th>
                                                    <th>Publico</th>
                                                </tr>      
                                                <tr>
                                                    <td><?php print $node->field_modalidad["und"][0]['value']; ?></td>
                                                    <td><?php print $node->field_tipo["und"][0]['value']; ?></td>
                                                    <td><?php print $node->field_publico["und"][0]['value']; ?></td>
                                                </tr>
                                                <tr>
                                                    <th>Horario</th>
                                                    <th>Duración</th>
                                                    <th></th>
                                                </tr>      
                                                <tr>
                                                    <td>
                                                        <?php print $node->field_horario["und"][0]["safe_value"]; ?>
                                                    <td>
                                                        <?php print $node->field_duracion_curso["und"][0]["safe_value"]; ?>
                                                    </td>
                                                    <td></td>
                                                </tr>
                                            </tbody>
                                        </table>
                                    </div>

                                    <!-- Navigation Buttons -->
                                    <div class="col-md-2">

                                        <ul class="nav nav-pills nav-stacked" id="Cursos">                                        

                                            <?php if ($node->body["und"][0]["safe_value"]) { ?> 
                                                <li class="active"><a href="#descripcion"  data-toggle="tab">Descripción</a></li><?php } ?>

                                            <?php if (($node->field_objetivo_general["und"][0]["safe_value"]) || ($node->field_objetivo_especifico["und"][0]["safe_value"])) { ?> 
                                                <li><a href="#objetivos" data-toggle="tab">Objetivos</a></li><?php } ?>      

                                            <?php
                                            $contador = 0;
                                            for ($i = 1; $i < 11; $i++) {
                                                if (isset($node->{"field_nivel_" . $i}["und"])) {
                                                    $contador++;
                                                }
                                            }
                                            ?> 
                                            <?php if ($contador <> 0) { ?> 
                                                <li><a href="#malla" data-toggle="tab">Plan de estudios</a></li><?php } ?>    

                                            <?php if (($node->field_metodologia["und"][0]["safe_value"]) || ($node->field_dirigido["und"][0]["safe_value"]) || ($node->field_ruta_academica["und"][0]["safe_value"])) { ?> 
                                                <li><a href="#detalles" data-toggle="tab">Detalles</a></li><?php } ?>	


                                            <?php if (($node->field_acreditacion["und"][0]["safe_value"]) || ($node->field_certificacion["und"][0]["safe_value"]) || ($node->field_homologacion["und"][0]["safe_value"])) { ?> 
                                                <li><a href="#soportes" data-toggle="tab">Soportes</a></li><?php } ?>

                                            <?php if ($node->field_profesores_ct["und"][0]["entity"] || $node->field_profesores_tc["und"][0]["entity"] || $node->field_profesores_mt["und"][0]["entity"] || $node->field_profesores_invitados["und"][0]["entity"]) { ?> 
                                                <li><a href="#profesores" data-toggle="tab">Profesores</a></li><?php } ?>

                                            <?php if ($node->field_contacto_curso["und"][0]["safe_value"]) { ?> 
                                                <li><a href="#contacto" data-toggle="tab">Contacto</a></li><?php } ?>
                                            <?php
                                            if ($node->field_pensum_descargable["und"][0]["uri"]) {
                                                $url = file_create_url($node->field_pensum_descargable["und"][0]["uri"]);
                                                ?>
                                                <p class="text-center"><a target="_blank" href="<?php print $url; ?>" ><i class="fa fa-3x fa-cloud-download"></i></a></p><?php } ?>

                                        </ul>                                    
                                    </div>
                                    <!-- Content -->
                                    <div class="col-md-10  padding-x linea-caja-left caja-sombra efecto-sombra-2">

                                        <div class="tab-content">

                                            <div class="tab-pane active" id="descripcion">
                                                <?php
                                                print $node->body["und"][0]["safe_value"];
                                                ?>
                                            </div>

                                            <div class="tab-pane" id="objetivos">
                                                <?php
                                                print $node->field_objetivo_general["und"][0]["safe_value"];
                                                print $node->field_objetivo_especifico["und"][0]["safe_value"];
                                                print render($content['field_tematicas_interes']);
                                                ?>
                                            </div>

                                            <div class="tab-pane" id="malla">                                    


                                                <div class = 'col-md-12'>               

                                                    <?php
                                                    if ($contador == 1) {
                                                        print("<div class='col-md-12'>");
                                                        ?>
                                                        <div class='panel panel-primary'>
                                                            <div class='panel-heading'>
                                                                <h3 class='panel-title'>Nivel <?php print $contador; ?></h3>
                                                            </div>
                                                            <div class='panel-body'> 
                                                                <?php print $node->{"field_nivel_" . $contador}["und"][0]["safe_value"]; ?>
                                                            </div>
                                                        </div>
                                                        <?php
                                                        print("</div>");
                                                    }

                                                    if ($contador == 2 || $contador == 4 || $contador == 8 || $contador == 10) {
                                                        $cont = 0;
                                                        for ($i = 1; $i <= $contador; $i++) {
                                                            if ($cont == 0) {
                                                                print("<div class='col-md-12'>");
                                                            }
                                                            print("<div class='col-md-6'>");
                                                            ?>
                                                            <div class='panel panel-primary'>
                                                                <div class='panel-heading'>
                                                                    <h3 class='panel-title'>Nivel <?php print $i; ?></h3>
                                                                </div>
                                                                <div class='panel-body'> 
                                                                    <?php print $node->{"field_nivel_" . $i}["und"][0]["safe_value"]; ?>
                                                                </div>
                                                            </div>
                                                            <?php
                                                            print("</div>");

                                                            $cont++;
                                                            if ($cont == 2) {
                                                                print("</div>");
                                                                $cont = 0;
                                                            }
                                                        }
                                                    }





                                                    if ($contador == 3 || $contador == 6 || $contador == 9) {
                                                        $cont = 0;
                                                        for ($i = 1; $i <= $contador; $i++) {
                                                            if ($cont == 0) {
                                                                print("<div class='col-md-12'>");
                                                            }
                                                            print("<div class='col-md-4'>");
                                                            ?>
                                                            <div class='panel panel-primary'>
                                                                <div class='panel-heading'>
                                                                    <h3 class='panel-title'>Nivel <?php print $i; ?></h3>
                                                                </div>
                                                                <div class='panel-body'> 
                                                                    <?php print $node->{"field_nivel_" . $i}["und"][0]["safe_value"]; ?>
                                                                </div>
                                                            </div>
                                                            <?php
                                                            print("</div>");
                                                            $cont++;
                                                            if ($cont == 3) {
                                                                print("</div>");
                                                                $cont = 0;
                                                            }
                                                        }
                                                    }

                                                    if ($contador == 5) {
                                                        for ($i = 1; $i <= $contador - 2; $i++) {
                                                            print("<div class='col-md-4'>");
                                                            ?>
                                                            <div class='panel panel-primary'>
                                                                <div class='panel-heading'>
                                                                    <h3 class='panel-title'>Nivel <?php print $i; ?></h3>
                                                                </div>
                                                                <div class='panel-body'> 
                                                                    <?php print $node->{"field_nivel_" . $i}["und"][0]["safe_value"]; ?>
                                                                </div>
                                                            </div>
                                                            <?php
                                                            print("</div>");
                                                        }
                                                        for ($i = 4; $i <= $contador; $i++) {
                                                            print("<div class='col-md-6'>");
                                                            ?>
                                                            <div class='panel panel-primary'>
                                                                <div class='panel-heading'>
                                                                    <h3 class='panel-title'>Nivel <?php print $i; ?></h3>
                                                                </div>
                                                                <div class='panel-body'> 
                                                                    <?php print $node->{"field_nivel_" . $i}["und"][0]["safe_value"]; ?>
                                                                </div>
                                                            </div>
                                                            <?php
                                                            print("</div>");
                                                        }
                                                    }

                                                    if ($contador == 7) {
                                                        for ($i = 1; $i <= $contador - 1; $i++) {
                                                            print("<div class='col-md-4'>");
                                                            ?>
                                                            <div class='panel panel-primary'>
                                                                <div class='panel-heading'>
                                                                    <h3 class='panel-title'>Nivel <?php print $i; ?></h3>
                                                                </div>
                                                                <div class='panel-body'> 
                                                                    <?php print $node->{"field_nivel_" . $i}["und"][0]["safe_value"]; ?>
                                                                </div>
                                                            </div>
                                                            <?php
                                                            print("</div>");
                                                        }
                                                        print("<div class='col-md-12'>");
                                                        ?>
                                                        <div class='panel panel-primary'>
                                                            <div class='panel-heading'>
                                                                <h3 class='panel-title'>Nivel <?php print $contador; ?></h3>
                                                            </div>
                                                            <div class='panel-body'> 
                                                                <?php print $node->{"field_nivel_" . $contador}["und"][0]["safe_value"]; ?>
                                                            </div>
                                                        </div>
                                                        <?php
                                                        print("</div>");
                                                    }
                                                    ?>

                                                </div>


                                            </div>

                                            <div class="tab-pane" id="detalles">
                                               <?php if ($node->field_metodologia["und"][0]["safe_value"]) { ?> 
                                                <div>
                                                    <h3>Metodología</h3>
                                                    <?php print $node->field_metodologia["und"][0]["safe_value"]; ?>
                                                </div>
                                                 <?php } ?>
                                                <?php if ($node->field_dirigido["und"][0]["safe_value"]) { ?> 
                                                <div>
                                                    <h3>Dirigido a</h3>
                                                    <?php print $node->field_dirigido["und"][0]["safe_value"]; ?>
                                                </div>
                                                 <?php } ?>
                                                <?php if ($node->field_ruta_academica["und"][0]["safe_value"]) { ?> 
                                                <div>
                                                    <h3>Ruta académica</h3>
                                                    <?php print $node->field_ruta_academica["und"][0]["safe_value"]; ?>
                                                </div>
                                                 <?php } ?>
                                            </div>

                                            <div class="tab-pane" id="soportes">
                                               <?php if ($node->field_acreditacion["und"][0]["safe_value"]) { ?> 
                                                <div>
                                                    <h3>Acreditación</h3>
                                                    <?php print $node->field_acreditacion["und"][0]["safe_value"]; ?>
                                                </div>
                                                 <?php } ?>
                                                <?php if ($node->field_certificacion["und"][0]["safe_value"]) { ?> 
                                                <div>
                                                    <h3>Certificación</h3>
                                                    <?php print $node->field_certificacion["und"][0]["safe_value"]; ?>
                                                </div>
                                                 <?php } ?>
                                                <?php if ($node->field_homologacion["und"][0]["safe_value"]) { ?> 
                                                <div>
                                                    <h3>Homologación</h3>
                                                    <?php print $node->field_homologacion["und"][0]["safe_value"]; ?>
                                                </div>
                                                 <?php } ?>
                                                
                                            </div>	  

                                            <div class="tab-pane" id="profesores">
                                                <h3>Profesores de tiempo completo</h3>
                                                <div class="programa_profesor_tc">
                                                    <ul class="nav nav-tabs prog" data-tabs="tabs">                       
                                                        <?php
                                                        for ($i = 0;
                                                        ; $i++) {
                                                            $ID = $node->field_profesores["und"][$i]["entity"]->nid;
                                                            if ($ID == 0) {
                                                                break;
                                                            }
                                                            $alias = drupal_get_path_alias('node/' . $ID);
                                                            if (strcmp("0", $node->field_profesores["und"][$i]["entity"]->status) != 0) {
                                                                ?>
                                                                <div class="col-md-6 thumbnail" style="height: 270px; font-size: 13px;">      
                                                                    <a href="<?php print($alias); ?>">                                                
                                                                        <?php print($node->field_profesores["und"][$i]["entity"]->title . ", "); ?><?php print($node->field_profesores["und"][$i]["entity"]->field_nivel_academico["und"][0]["value"]); ?>   
                                                                    </a><br/><br/>
                                                                    <div class="col-md-5">
                                                                        <div style="margin-bottom: 10px;"> <?php echo theme('image_style', array('style_name' => 'thumbnail', 'path' => $node->field_profesores["und"][$i]["entity"]->field_staffs_image['und'][0]['uri'], 'attributes' => array('class' => 'img-responsive'))); ?>        </div>                            
                                                                        <div class="portada_profesor"> <?php print($node->field_profesores["und"][$i]["entity"]->field_social["und"][0]["safe_value"]); ?> </div>
                                                                    </div>

                                                                    <div class="col-md-7">
                                                                        <dl>  
                                                                            <?php if (strlen($node->field_profesores["und"][$i]["entity"]->field_enlace_al_cvlac["und"][0]['url']) > 1) { ?>
                                                                                <dt>CvLAC:</dt>
                                                                                <dd><a target="_blank" href="<?php print($node->field_profesores["und"][$i]["entity"]->field_enlace_al_cvlac["und"][0]['url']) ?>"><?php print(" " . $node->field_profesores["und"][$i]["entity"]->field_enlace_al_cvlac["und"][0]['title']); ?></a></dd>
                                                                            <?php } ?>                    
                                                                            <?php if ($node->field_profesores["und"][$i]["entity"]->field_escalafon['und'][0]["value"] <> "No escalafonado") { ?>                       
                                                                                <dt>Escalafón: </dt> 
                                                                                <dd> <?php print(" " . $node->field_profesores["und"][$i]["entity"]->field_escalafon['und'][0]["value"]); ?></dd>
                                                                            <?php } ?>
                                                                            <?php if (strlen($node->field_profesores["und"][$i]["entity"]->field_mail["und"][0]["safe_value"]) > 1) { ?>   
                                                                                <dt>Correo electrónico:</dt><dd><small><?php
                                                            print(" " . $node->field_profesores["und"][$i]["entity"]->field_mail["und"][0]["safe_value"]);
                                                                                ?></small></dd>
                                                                                    <?php } ?>
                                                                            <?php if (strlen($node->field_profesores["und"][$i]["entity"]->field_telefono["und"][0]["safe_value"]) > 1) { ?>
                                                                                <dt>Teléfono:</dt><dd><?php
                                                            print(" " . $node->field_profesores["und"][$i]["entity"]->field_telefono["und"][0]["safe_value"]);
                                                                                ?></dd>
                                                                                <?php } ?>
                                                                            <?php if (strlen($node->field_profesores["und"][$i]["entity"]->field_direccion["und"][0]["safe_value"]) > 1) { ?>
                                                                                <dt>Dirección de Oficina:</dt>
                                                                                <dd><?php print($node->field_profesores["und"][$i]["entity"]->field_direccion["und"][0]["safe_value"]); ?></dd>
                                                                            <?php } ?></dl>

                                                                    </div>
                                                                </div>  

                                                                <?php
                                                            }
                                                        }
                                                        ?>

                                                </div>                                            
                                            </div>

                                            <div class="tab-pane" id="contacto">
                                                <?php print($node->field_contacto_curso["und"][0]["safe_value"]); ?>
                                            </div>	  

                                        </div>

                                    </div>

                                </div>

                            </div>

                        </div>

                    </div>


                </div>
            <?php endif; ?>

    </article>
<?php endif; ?>