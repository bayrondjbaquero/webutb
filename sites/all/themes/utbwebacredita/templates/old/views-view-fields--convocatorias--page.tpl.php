<div class="col-md-11 col-md-offset-1 column padding-m">

    <div class="panel panel-primary">

        <div class="panel-heading">
            <h4 class="panel-title"><?php print $fields['title']->content ?></h4>
        </div>

        <div class="panel-body">

            <div class="img-responsive pull-right margen-m"> <?php print $fields['field_imagen_institucion']->content; ?> </div>

            <p>

                <?php print $fields['body']->content ?>

            </p>

        </div>

        <div class="panel-footer">

            <?php if ($fields['field_entidad']) { ?>
                <p>Organiza: <strong><?php print $fields['field_entidad']->content ?></strong> 	
                <?php
                }

                
                $ID = $fields['nid']->content;
                $alias = drupal_get_path_alias('node/' . $ID);
                
                ?>

                <a class="btn btn-primary btn-default pull-right" href="<?php print $alias;  ?>" ><span class="glyphicon glyphicon-arrow-right"></span> Ver Convocatoria</a> 
            </p> 

        </div>

    </div>

</div>

<br />

<br />