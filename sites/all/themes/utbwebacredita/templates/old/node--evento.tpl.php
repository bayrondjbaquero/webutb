<div id="node-<?php print $node->nid; ?>" class="<?php print $classes; ?> clearfix"<?php print $attributes; ?>>

    <div class="row clearfix">

        <div class="col-md-12 column">

            <table class="table table-striped">
                <tbody>
                    <tr>
                        <th>Tipo de evento</th>
                        <th>Código del evento</th>
                        <th>Fecha de inicio</th>
                    </tr>      
                    <tr>
                        <td><?php print $node->field_tipo_evento["und"][0]['taxonomy_term']->name; ?></td>
                        <td><?php print $node->field_codigo_evento["und"][0]["safe_value"]; ?></td>
                        <td><?php
                            print format_date(strtotime($content['field_fecha_inicio']['#items'][0]['value']), 'custom', 'F j, Y l');
                            ?></td>
                    </tr>
                    <tr>
                        <th>Fecha final</th>
                        <th>Hora de inicio</th>
                        <th>Hora final</th>
                    </tr>      
                    <tr>
                        <td><?php
                            print format_date(strtotime($content['field_fecha_final']['#items'][0]['value']), 'custom', 'F j, Y , l');
                            ?></td>
                        <td><?php
                            print format_date(strtotime($content['field_hora_inicio']['#items'][0]['value']), 'custom', 'H:i ');
                            ?></td>
                        <td><?php print format_date(strtotime($content['field_hora_final']['#items'][0]['value']), 'custom', 'H:i ');
                            ?></td>
                    </tr>
                    <tr>
                        <th></th>
                        <th>Lugar del evento</th>
                        <th></th>
                    </tr>      
                    <tr>
                        <td></td>
                        <td><?php print $node->field_lugar_del_evento["und"][0]["safe_value"]; ?></td>
                        <td></td>
                    </tr>
                </tbody>
            </table>
            <?php print $node->body["und"][0]["safe_value"]; ?>
        </div>

    </div>
</div>


