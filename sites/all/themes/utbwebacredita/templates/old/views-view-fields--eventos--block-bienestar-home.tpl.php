<div class="panel panel-default">
    <div class="panel-heading">
        <a class="panel-title collapsed" data-toggle="collapse" data-parent="#panel-715086" href="#<?php print $fields['nid']->content; ?>"><?php print $fields['title']->content; ?></a>
    </div>
    <div id="<?php print $fields['nid']->content; ?>" class="panel-collapse collapse">
        <div class="panel-body">
            <div class="calendar_wrapper">
                <div class="calendar_head">
       				<?php print $fields['field_fecha']->content; ?> 
            	</div><!-- fin calendar_header -->
		        <div class="calendar_body">
                    <div class="num_dia_wrapper">
			            <?php print $fields['field_fecha_1']->content; ?>
        			</div>
        			<div class="dia_wrapper">
        		        <?php print $fields['field_fecha_2']->content; ?>        	
        			</div>
        		</div><!-- fin calendar_body -->
        	</div><!-- fin calendar_wrapper -->
    		<div class="">
                <?php print $fields['body']->content; ?>  
			    <br>         
        		<?php print $fields['view_node']->content; ?>           
		    </div>
        </div><!-- fin panel-body -->
    </div><!-- fin panel-collapse -->

</div><!-- fin panel -->


