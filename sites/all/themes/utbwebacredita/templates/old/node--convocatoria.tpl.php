<div id="node-<?php print $node->nid; ?>" class="<?php print $classes; ?> clearfix"<?php print $attributes; ?>>

    <div class="row clearfix">

        <div class="col-md-12 column">

            <table class="table table-striped">
                <tbody>
                    <tr>
                        <th>
                            <?php
                            if ($node->field_tipo_de_usuario["und"][0]["value"]) {
                                print "Tipo de Usuario";
                            } else {
                                print "Tipo de Convocatoria";
                            }
                            ?></th>
                        <th>Fecha Inicio</th>
                        <th>Fecha Final</th>
                    </tr>      
                    <tr>
                        <td><?php
//                            $flag = 0;
//                            while ($flag >= 0):
//                                print $node->field_tipo_de_usuario["und"][$flag]["value"];
//                                if ($node->field_tipo_de_usuario["und"][$flag + 1]["value"]):
//                                    $flag++;
//                                    echo"<br/>";
//                                else:
//                                    $flag = -1;
//                                endif;
//                            endwhile;
                            if ($node->field_tipo_de_usuario["und"][0]["value"]) {
                                print $node->field_tipo_de_usuario["und"][0]["value"];
                            } else {
                                print $node->field_tipo_convocatoria["und"][0]["value"];
                            }
                            ?></td>
                        <td><?php
                            print format_date(strtotime($content['field_fecha_convocatoria_']['#items'][0]['value']), 'custom', 'F j, Y l');
                            ?></td>
                        <td><?php
                            print format_date(strtotime($content['field_fecha_convocatoria_cierre']['#items'][0]['value']), 'custom', 'F j, Y l');
                            ?></td>
                    </tr>  

                    <tr>
                        <th></th>
                        <th>Entidad</th>
                        <th></th>
                    </tr> 
                    <tr>
                        <td></td>
                        <td><?php print $node->field_entidad["und"][0]['taxonomy_term']->name; ?></td>
                        <td></td>
                    </tr>

                </tbody>
            </table>   

            <div class="col-md-12 margen-m">
                <div class="col-md-2">
                    <?php if ($node->field_imagen_institucion) : ?>
                        <?php
                        $uriImagefromNode = $node->field_imagen_institucion['und'][0]['uri'];
                        $urlImagefromNode = file_create_url($uriImagefromNode);
                        ?>
                        <img alt="<?php print $node->field_imagen_institucion['und'][0]['alt']; ?>" src="<?php print $urlImagefromNode; ?>" style=" min-height: 64px; " class="img-responsive"/>
                    <?php endif; ?>
                </div>
                <div class="col-md-10">
                    <?php print $node->body["und"][0]["safe_value"]; ?>
                </div>
            </div>




            <a href="<?php print $node->field_informacion_adicional_url["und"][0]["url"]; ?>"><?php print $node->field_informacion_adicional_url["und"][0]["url"]; ?></a>

            <div class="col-md-12">
                <div class="clearfix">
                    <?php if ($node->field_adjuntos_convocatoria) { ?>
                        <div class="panel panel-primary">
                            <div class="panel-heading">                    
                                <h3 class="panel-title"> <span class="glyphicon glyphicon-folder-open" style="font-size:2.5em;"></span>&nbsp;&nbsp;&nbsp; Descargas adicionales </h3> 
                            </div>
                            <div class="panel-body">
                                <?php print render($content['field_adjuntos_convocatoria']); ?>                              
                            </div>											
                        </div>
                    <?php } ?>
                </div>
            </div>


        </div>
    </div>
</div>

