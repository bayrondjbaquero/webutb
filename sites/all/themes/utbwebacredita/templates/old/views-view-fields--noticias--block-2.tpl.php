<div class="panel panel-default">

    <div class="panel-heading">
        <a class="panel-title collapsed" data-toggle="collapse" data-parent="#panel-715086" href="#<?php print $fields['nid']->content; ?>"><?php print $fields['title']->content; ?></a>
    </div>
    <div id="<?php print $fields['nid']->content; ?>" class="panel-collapse collapse">
        <div class="panel-body">
            <div class="pull-left margen-s"><?php print $fields['field_image']->content; ?></div>
            <?php
            print $fields['body']->content;
            ?>           
            <a class="btn btn-default" href="<?php print "node/".$fields['nid']->content; ?>">
                Ver más ...  
            </a>            
        </div>
    </div>

</div>