<?php
$entity = node_load($fields['nid']->raw);
$uriImagefromNode = $entity->field_filtro_imagen['und'][0]['uri'];
$urlImagefromNode = file_create_url($uriImagefromNode);
?>         
<?php
$num = strlen($entity->field_dirigido_a['und'][0]['value']);

$alias = drupal_get_path_alias('node/' . $entity->nid);
?>
<div class="col-md-6 col-lg-3 thumbnail  element-item <?php print $num; ?>" data-category="<?php print $num; ?>">

    <a href="<?php print $alias ?>" title="<?php print $entity->title; ?>">
        <p class="text-primary filtro-texto"><?php print $entity->title; ?></p>

        <img alt="<?php print $temp->field_filtro_imagen['und'][0]['alt']; ?>" src="<?php print $urlImagefromNode; ?>" class="img-responsive padding-s"/>

        <hr>
        <p style="height: 10px;" >

            <?php
            if (isset($entity->field_nuevo)):
                if (strcmp($entity->field_nuevo["und"][0]["value"], '1') == 0):
                    ?>
                    <span class="badge">Nuevo</span>    
                <?php endif; ?>  
            <?php endif; ?>

            <?php
            if (isset($entity->field_inscripciones)):
                if (strcmp($entity->field_inscripciones["und"][0]["value"], '1') == 0):
                    ?>
                    <span class="badge badge-success">Inscripciones abiertas</span>    
                <?php endif; ?>  
            <?php endif; ?>

        </p>
    </a>
</div>