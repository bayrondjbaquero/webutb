<html>
    <head>
        <title>Modo Mantenimiento</title>
        <meta charset="UTF-8">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <link rel="stylesheet" type="text/css" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/css/bootstrap.min.css">
        <link rel="stylesheet" type="text/css" href="/sites/all/themes/utbweb2016/css/mdb.min.css">
        <link rel="stylesheet" type="text/css" href="/sites/all/themes/utbweb2016/css/utbweb.css">
        <link rel="stylesheet" type="text/css" href="/sites/all/themes/utbweb2016/css/sweetalert.css">
    </head>
    <body>

        <img src="/sites/all/themes/utbweb2016/screenshot.png" class="img-responsive">

        <script src="https://code.jquery.com/jquery-1.9.1.min.js" ></script>  
        <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/js/bootstrap.min.js"></script> 
        <script src="/sites/all/themes/utbweb2016/js/mdb.min.js"></script> 
        <script src="/sites/all/themes/utbweb2016/js/utb2016.js"></script>  
        <script src="/sites/all/themes/utbweb2016/js/sweetalert.min.js"></script> 


        <script>
            jQuery(document).ready(function ($) {

                swal({
                    title: "Modo Mantenimiento  Portal Web UTB",
                    text: "Trabajamos para ofrecer una mejor experiencia en nuestra web, pronto estaremos listos.",
                    type: "error",
                    closeOnConfirm: false, //It does close the popup when I click on close button
                    closeOnCancel: false,
                    allowOutsideClick: false
                },
                        function () {
                            window.location.href = 'http://programas.unitecnologica.edu.co';
                        });

            });

           
        </script>

    </body>
</html>
