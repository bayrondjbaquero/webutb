<?php
$entity = node_load($fields['nid']->raw);
$uriImagefromNode = $entity->field_fotonoticia['und'][0]['uri'];
$urlImagefromNode = file_create_url($uriImagefromNode);
?>
<div class="card">
    <div class="card-image">
        <div class="view hm-white-slight z-depth-1">
            <img src="<?php print $urlImagefromNode; ?>" class="img-responsive">
            <div class="mask waves-effect"></div>
        </div>
    </div>
    <?php if ($entity->body): ?>
        <div class="card-content">
            <h5 class="black-text"><?php print $entity->title; ?> </h5>
            <?php print $entity->body['und'][0]['safe_value']; ?> 
        </div>
    <?php endif; ?>
    <div class="card-action-utb card-action">
        <a target="_blank" href="<?php print $entity->field_link['und'][0]['url']; ?>">
            ver más
        </a>
    </div>
</div>