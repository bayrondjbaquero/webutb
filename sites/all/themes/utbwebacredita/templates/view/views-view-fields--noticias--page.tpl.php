<?php
$entity = node_load($fields['nid']->raw);
$uriImagefromNode = $entity->field_carrousel['und'][0]['uri'];
if ($uriImagefromNode) {
    $urlImagefromNode = file_create_url($uriImagefromNode);
} else {
    $uriImagefromNode = $entity->field_image['und'][0]['uri'];
    $urlImagefromNode = file_create_url($uriImagefromNode);
}
$alias = drupal_get_path_alias('node/' . $entity->nid);
$formatted_date = format_date($entity->created, 'custom', 'd m Y');


if (!$uriImagefromNode) {
    $urlImagefromNode = "http://www.unitecnologica.edu.co/newsletter/Conectate/imagen-mailing/utb1.jpg";
}
?>


<a href="<?php print $alias; ?>">
    <div class="row hoverable">
        <div class="col-sm-4">
            <img src="<?php print $urlImagefromNode; ?>" class="img-responsive z-depth-2">
        </div>
        <div class="col-sm-8">
            <h5 class="title"><?php print $entity->title; ?></h5>
            <ul class="list-inline item-details">
                <li><i class="fa fa-clock-o"><?php print $formatted_date; ?> |</i></li>
            </ul>
            <p>
                <?php print $entity->body['und'][0]['safe_summary']; ?>
            </p>
        </div>
    </div>
</a>
