<?php
$entity = node_load($fields['nid']->raw);
$uriImagefromNode = $entity->field_carrousel['und'][0]['uri'];
$urlImagefromNode = file_create_url($uriImagefromNode);

$alias = drupal_get_path_alias('node/' . $entity->nid);
?>

<div class="col-md-3">
    <!--Stylish Card Light-->
    <div class="card stylish-card hoverable">

        <div class="view overlay hm-white-slight z-depth-1">
            <a href="/<?php print $alias; ?>"><img src="<?php print $urlImagefromNode; ?>" class="img-responsive" style="height:140px;" alt="<?php print $entity->title; ?>"></a>

        </div>

        <!--Content-->
        <div class="card-content">
            <div class="card-footer">
                <a href="/<?php print $alias; ?>"><h5><?php print $entity->title; ?></h5></a>
            </div>
        </div>

    </div>
    <!--/.Stylish Card Light-->
</div>