<?php
/**
 * @file
 * Default simple view template to display a list of rows.
 *
 * @ingroup views_templates
 */
?>   

    <!-- Carousel -->
    <div id="carousel1" class="carousel slide carousel-fade hoverable">

        <!-- Indicators -->
        <ol class="carousel-indicators">
            <?php $i = 0; ?>
            <?php
            foreach ($rows as $id => $row) {
                if ($i == 0) {
                    ?>  <li data-target="#carousel1" data-slide-to="<?php print $i;?>" class="active"> <?php } else { ?>
                        <li data-target="#carousel1" data-slide-to="<?php print $i;?>"></li>
                        <?php } $i++; ?>
                    </li>
                <?php } ?>  
        </ol>

        <!-- Wrapper for slides -->
        <div class="carousel-inner z-depth-2" role="listbox">

            <!-- slide -->

            <?php $i = 0; ?>
            <?php
            foreach ($rows as $id => $row) {
                if ($i == 0) {
                    ?> <div class="item active"> <?php } else { ?>
                        <div class="item">
                        <?php } $i++; ?>

                        
                                <?php print $row; ?>
                         
                    </div>  
                <?php } ?>     

                <!-- /.slide -->

            </div>
            <!-- /.carousel-inner -->

            <!-- Controls -->
            <a  class="left carousel-control new-control" href="#carousel1" role="button" data-slide="prev">
                <span class="fa fa fa-angle-left waves-effect waves-light"></span>
                <span class="sr-only">Previous</span>
            </a>
            <a  class="right carousel-control new-control" href="#carousel1" role="button" data-slide="next">
                <span class="fa fa fa-angle-right waves-effect waves-light"></span>
                <span class="sr-only">Previous</span>
            </a>

        </div>
        <!-- /.carousel -->


