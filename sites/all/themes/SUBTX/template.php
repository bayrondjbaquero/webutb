<?php

function SUBTX_breadcrumb($breadcrumb) {
    $home = variable_get('site_name', 'drupal');
    $sep = ' &raquo; ';
    if (count($breadcrumb) > 0) {
        $breadcrumb[0] = l(t($home), '');
        return implode($sep, $breadcrumb) . $sep;
    } elseif (drupal_get_title() !== '') {
        return l(t($home), '') . $sep;
    } else {
        return t($home);
    }
}

function SUBTX_aggregator_block_item($variables) {
    // Display the external link to the item.
    return '<a class="list-group-item" href="' . check_url($variables['item']->link) . '">' . check_plain($variables['item']->title) . "</a>\n";
}

function SUBTX_menu_link(array $variables) {
    $element = $variables['element'];
    $sub_menu = '';

    if ($element['#below']) {
        // Ad our own wrapper
        unset($element['#below']['#theme_wrappers']);
        $sub_menu = '<ul class="dropdown-menu">' . drupal_render($element['#below']) . '</ul>'; // removed flyout class in ul
        unset($element['#localized_options']['attributes']['class']); // removed flydown class
        unset($element['#localized_options']['attributes']['data-toggle']); // removed data toggler
        // Check if this element is nested within another
        if ((!empty($element['#original_link']['depth'])) && ($element['#original_link']['depth'] > 1)) {

            unset($element['#attributes']['class']); // removed flyout class
        } else {
            unset($element['#attributes']['class']); // unset flyout class
            $element['#localized_options']['html'] = TRUE;
            $element['#title'] .= ''; // removed carat spans flyout
        }
        $element['#localized_options']['attributes']['class'][] = 'dropdown-toggle';
        $element['#localized_options']['attributes']['data-toggle'] = 'dropdown';
        // Set dropdown trigger element to # to prevent inadvertent page loading with submenu click
        $element['#localized_options']['attributes']['data-target']='#'; // You could unset this too as its no longer necessary. 
    }

    $output = l($element['#title'], $element['#href'], $element['#localized_options']);


    if (isset($element['#localized_options']['html'])) {
        return '<li class="dropdown">' . $output . $sub_menu . "</li>\n";
    } else {
        if (isset($element['#localized_options']['attributes']["data-target"])) {
            return '<li class="dropdown dropdown-submenu">' . $output . $sub_menu . "</li>\n";
        } else {
            return '<li>' . $output . $sub_menu . "</li>\n";
        }
    }
}

function SUBTX_menu_tree(&$variables) {
    return '<div class="collapse navbar-collapse " id="principal"><ul  class="nav navbar-nav">' . $variables['tree'] . '</ul></div>'; // added the nav-collapse wrapper so you can hide the nav at small size
}

function bootstrap_menu_tree__menu_topmenu(&$variables) {
    return '<div class="navbar-right collapse navbar-collapse" id="auxiliar"><ul  class="nav navbar-nav">' . $variables['tree'] . '</ul></div>'; // added the nav-collapse wrapper so you can hide the nav at small size
}

function bootstrap_menu_tree__menu_menu_politicas(&$variables) {
    return '<div class="collapse navbar-collapse navbar-right" id="politicas"><ul  class="nav navbar-nav"><a data-toggle="modal" href="#myModal" class="btn btn-primary">Feedback</a>' . $variables['tree'] . '</ul></div>'; // added the nav-collapse wrapper so you can hide the nav at small size
}