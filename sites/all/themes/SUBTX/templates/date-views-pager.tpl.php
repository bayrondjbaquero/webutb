<?php
/**
 * @file
 * Template file for the example display.
 *
 * Variables available:
 * 
 * $plugin: The pager plugin object. This contains the view.
 *
 * $plugin->view
 *   The view object for this navigation.
 *
 * $nav_title
 *   The formatted title for this view. In the case of block
 *   views, it will be a link to the full view, otherwise it will
 *   be the formatted name of the year, month, day, or week.
 *
 * $prev_url
 * $next_url
 *   Urls for the previous and next calendar pages. The links are
 *   composed in the template to make it easier to change the text,
 *   add images, etc.
 *
 * $prev_options
 * $next_options
 *   Query strings and other options for the links that need to
 *   be used in the l() function, including rel=nofollow.
 */
//sites/all/modules/date/date_views/theme
?>
<?php if (!empty($pager_prefix)) print $pager_prefix; ?>
<div class="<?php if (!empty($extra_classes)) print $extra_classes; ?>">
    <div>
        <ul class="pager">
            <?php if (!empty($prev_url)) : ?>
                <li class="date-prev">
                    <!--<?php print l('&lsaquo;' . ($mini ? '' : ' ' . t('Prev', array(), array('context' => 'date_nav'))), $prev_url, $prev_options); ?>-->
                    <?php
                    $ruta_icono_mes_atras = "<i class='fa fa-arrow-circle-left'></i>";
                    print l($ruta_icono_mes_atras . ($mini ? '' : ' ' . t('Prev', array(), array('context' => 'date_nav'))), $prev_url, $prev_options);
                    ?>
                    &nbsp;</li>
            <?php endif; ?>
            <?php
            $separar = explode(",", $nav_title);
            if ($separar[1]) {
                $mes = explode(" ", $separar[1]);
                ?>
                <h3 style="display:inline-block;"><a href="/calendario"><?php print $mes[1]; ?></a></h3>                       
                <?php
            } else {
                $mes = date("Y");
                ?>
                <h3 style="display:inline-block;"><a href="/calendario"><?php print $mes; ?></a></h3>   
                <?php
            }
            ?>

            <?php if (!empty($next_url)) : ?>
                <li class="date-next">&nbsp;
                    <!--<?php print l(($mini ? '' : t('Next', array(), array('context' => 'date_nav')) . ' ') . '&rsaquo;', $next_url, $next_options); ?>-->
                    <?php
                    $ruta_icono_mes_adelante = "<i class='fa fa-arrow-circle-right'></i>";
                    print l(($mini ? '' : t('Next', array(), array('context' => 'date_nav')) . ' ') . $ruta_icono_mes_adelante, $next_url, $next_options);
                    ?>
                </li>
            <?php endif; ?>
        </ul>
    </div>

</div>