<?php if (!empty($body)): ?>

    <article id="node-<?php print $node->nid; ?>" class="<?php print $classes; ?> clearfix"<?php print $attributes; ?>> <?php print $user_picture; ?> <?php print render($title_prefix); ?>

        <div id="article-<?php print $node->nid; ?>" class="article <?php
        print $classes;
        if ($display_submitted)
            
            ?>clearfix" <?php print $attributes; ?> >

            <?php if ($title): ?>
                <div class="header article-header">

                    SNIES <?php print $node->field_prg_snies["und"][0]["safe_value"] ?>
                    <hr>
                    <div class="row clearfix" style="margin-bottom: 30px;">

                        <div class="col-md-12 column">

                            <table class="table table-striped">
                                <tbody>
                                    <tr>
                                        <th>Modalidad</th>
                                        <th>Duración</th>
                                    </tr>      
                                    <tr>
                                        <td><?php print $node->field_modalidad["und"][0]["value"]; ?></td>
                                        <td><?php print $node->field_duracion["und"][0]["safe_value"]; ?></td>
                                    </tr>
                                </tbody>
                            </table>



                            <?php
                            print $node->body['und'][0]['safe_summary'];
                            print $node->field_redes_sociales_prg["und"][0]["safe_value"];
                            ?>
                            <hr>
                            <div class="row">
                                <!-- Navigation Buttons -->
                                <div class="col-md-2">
                                    <ul class="nav nav-pills nav-stacked" id="Programas">
                                        <?php if ($node->body["und"][0]["safe_value"]) { ?> 
                                            <li class="active"><a href="#descripcion"  data-toggle="tab">Descripción</a></li><?php } ?> 
                                        <?php if ($node->field_dirigido_a["und"][0]["safe_value"]) { ?> 
                                            <li><a href="#dirigido" data-toggle="tab">Dirigido a</a></li><?php } ?>
                                        <?php if ($node->field_prg_perfil_salida["und"][0]["safe_value"]) { ?> 
                                            <li><a href="#egresados" data-toggle="tab">Perfil egresados</a></li><?php } ?>                                          

                                        <?php
                                        $edupol = 0;
                                        $contador = 0;
                                        for ($i = 1; $i < 11; $i++) {
                                            if (isset($node->{"field_prg_semestre" . $i}["und"])) {
                                                $contador++;
                                            }
                                        }
                                        ?> 
                                        <?php if ($contador <> 0) { ?> 
                                            <li><a href="#malla" data-toggle="tab">Plan de estudios</a></li><?php } ?> 
                                        <?php if (($node->field_fundamentacion_basica["und"][0]["safe_value"]) || ($node->field_fundamentacion_basica_pro["und"][0]["safe_value"]) || ($node->field_fundacion_especifica_tecno["und"][0]["safe_value"]) || ($node->field_propedeutico_profesional["und"][0]["safe_value"])) { ?>
                                            <li><a href="#malla" data-toggle="tab">Plan de estudios</a></li>
                                        <?php } ?>
                                        <?php if ($node->field_inv["und"][0]["safe_value"]) { ?> 
                                            <li><a href="#investigacion" data-toggle="tab">Investigación</a></li><?php } ?>	  
                                        <?php if ($node->field_director_prg["und"][0]["entity"]) { ?> 
                                            <li><a href="#director" data-toggle="tab">Director de programa</a></li><?php } ?>
                                        <li><a href="#profesores" data-toggle="tab">Profesores</a></li>
                                        <?php if ($node->field_contacto_prg["und"][0]["safe_value"]) { ?> 
                                            <li><a href="#contacto" data-toggle="tab">Contacto académico</a></li><?php } ?>
                                        <?php
                                        if ($node->field_pensum_descargable["und"][0]["uri"]) {
                                            $url = file_create_url($node->field_pensum_descargable["und"][0]["uri"]);
                                            ?>
                                            <p class="text-center"><a target="_blank" href="<?php print $url; ?>" ><i class="fa fa-3x fa-cloud-download"></i></a></p><?php } ?>

                                    </ul>                                    
                                </div>

                                <!-- Content -->
                                <div class="col-md-10 padding-x linea-caja-left caja-sombra efecto-sombra-2">

                                    <div class="tab-content ">

                                        <div class="tab-pane active" id="descripcion">
                                            <?php print render($content['body']); ?>
                                        </div>

                                        <div class="tab-pane" id="dirigido">
                                            <?php print($node->field_dirigido_a["und"][0]["safe_value"]); ?>
                                        </div>

                                        <div class="tab-pane" id="egresados">
                                            <?php print($node->field_prg_perfil_salida["und"][0]["safe_value"]); ?>
                                        </div>

                                        <div class="tab-pane" id="malla">                                    


                                            <div class = 'col-md-12'>              
                                                <?php
                                                if ($contador == 1) {
                                                    print("<div class='col-md-12'>");
                                                    ?>
                                                    <div class='panel panel-primary'>
                                                        <div class='panel-heading'>
                                                            <h3 class='panel-title'>Nivel <?php print $contador; ?></h3>
                                                        </div>
                                                        <div class='panel-body'> 
                                                            <?php print $node->{"field_prg_semestre" . $contador}["und"][0]["safe_value"]; ?>
                                                        </div>
                                                    </div>
                                                    <?php
                                                    print("</div>");
                                                }

                                                if ($contador == 2 || $contador == 4 || $contador == 8 || $contador == 10) {
                                                    $cont = 0;
                                                    for ($i = 1; $i <= $contador; $i++) {
                                                        if ($cont == 0) {
                                                            print("<div class='col-md-12'>");
                                                        }
                                                        print("<div class='col-md-6'>");
                                                        ?>
                                                        <div class='panel panel-primary'>
                                                            <div class='panel-heading'>
                                                                <h3 class='panel-title'>Nivel <?php print $i; ?></h3>
                                                            </div>
                                                            <div class='panel-body'> 
                                                                <?php print $node->{"field_prg_semestre" . $i}["und"][0]["safe_value"]; ?>
                                                            </div>
                                                        </div>
                                                        <?php
                                                        print("</div>");

                                                        $cont++;
                                                        if ($cont == 2) {
                                                            print("</div>");
                                                            $cont = 0;
                                                        }
                                                    }
                                                }

                                                if ($contador == 3 || $contador == 6 || $contador == 9) {
                                                    $cont = 0;
                                                    for ($i = 1; $i <= $contador; $i++) {
                                                        if ($cont == 0) {
                                                            print("<div class='col-md-12'>");
                                                        }
                                                        print("<div class='col-md-4'>");
                                                        ?>
                                                        <div class='panel panel-primary'>
                                                            <div class='panel-heading'>
                                                                <h3 class='panel-title'>Nivel <?php print $i; ?></h3>
                                                            </div>
                                                            <div class='panel-body'> 
                                                                <?php print $node->{"field_prg_semestre" . $i}["und"][0]["safe_value"]; ?>
                                                            </div>
                                                        </div>
                                                        <?php
                                                        print("</div>");
                                                        $cont++;
                                                        if ($cont == 3) {
                                                            print("</div>");
                                                            $cont = 0;
                                                        }
                                                    }
                                                }

                                                if ($contador == 5) {
                                                    for ($i = 1; $i <= $contador - 2; $i++) {
                                                        print("<div class='col-md-4'>");
                                                        ?>
                                                        <div class='panel panel-primary'>
                                                            <div class='panel-heading'>
                                                                <h3 class='panel-title'>Nivel <?php print $i; ?></h3>
                                                            </div>
                                                            <div class='panel-body'> 
                                                                <?php print $node->{"field_prg_semestre" . $i}["und"][0]["safe_value"]; ?>
                                                            </div>
                                                        </div>
                                                        <?php
                                                        print("</div>");
                                                    }
                                                    for ($i = 4; $i <= $contador; $i++) {
                                                        print("<div class='col-md-6'>");
                                                        ?>
                                                        <div class='panel panel-primary'>
                                                            <div class='panel-heading'>
                                                                <h3 class='panel-title'>Nivel <?php print $i; ?></h3>
                                                            </div>
                                                            <div class='panel-body'> 
                                                                <?php print $node->{"field_prg_semestre" . $i}["und"][0]["safe_value"]; ?>
                                                            </div>
                                                        </div>
                                                        <?php
                                                        print("</div>");
                                                    }
                                                }

                                                if ($contador == 7) {
                                                    for ($i = 1; $i <= $contador - 1; $i++) {
                                                        print("<div class='col-md-4'>");
                                                        ?>
                                                        <div class='panel panel-primary'>
                                                            <div class='panel-heading'>
                                                                <h3 class='panel-title'>Nivel <?php print $i; ?></h3>
                                                            </div>
                                                            <div class='panel-body'> 
                                                                <?php print $node->{"field_prg_semestre" . $i}["und"][0]["safe_value"]; ?>
                                                            </div>
                                                        </div>
                                                        <?php
                                                        print("</div>");
                                                    }
                                                    print("<div class='col-md-12'>");
                                                    ?>
                                                    <div class='panel panel-primary'>
                                                        <div class='panel-heading'>
                                                            <h3 class='panel-title'>Nivel <?php print $contador; ?></h3>
                                                        </div>
                                                        <div class='panel-body'> 
                                                            <?php print $node->{"field_prg_semestre" . $contador}["und"][0]["safe_value"]; ?>
                                                        </div>
                                                    </div>
                                                    <?php
                                                    print("</div>");
                                                }
                                                ?>
                                                <?php if ($node->field_fundamentacion_basica["und"][0]["safe_value"]) { ?>
                                                    <div class="panel panel-primary">
                                                        <div class="panel-heading">
                                                            <h3 class="panel-title"> Fundamentación Básica </h3>
                                                        </div>
                                                        <div class="panel-body">
                                                            <?php print $node->field_fundamentacion_basica["und"][0]["safe_value"] ?>
                                                        </div>
                                                    </div>
                                                <?php } ?>

                                                <?php if ($node->field_fundamentacion_basica_pro["und"][0]["safe_value"]) { ?>
                                                    <div class="panel panel-primary">
                                                        <div class="panel-heading">
                                                            <h3 class="panel-title"> Fundamentación Básica Profesional </h3>
                                                        </div>
                                                        <div class="panel-body">
                                                            <?php print $node->field_fundamentacion_basica_pro["und"][0]["safe_value"] ?>
                                                        </div>
                                                    </div>
                                                <?php } ?>

                                                <?php if ($node->field_fundacion_especifica_tecno["und"][0]["safe_value"]) { ?>
                                                    <div class="panel panel-primary">
                                                        <div class="panel-heading">
                                                            <h3 class="panel-title"> Fundación Específica Tecnológica </h3>
                                                        </div>
                                                        <div class="panel-body">
                                                            <?php print $node->field_fundacion_especifica_tecno["und"][0]["safe_value"] ?>
                                                        </div>
                                                    </div>
                                                <?php } ?>

                                                <?php if ($node->field_propedeutico_profesional["und"][0]["safe_value"]) { ?>
                                                    <div class="panel panel-primary">
                                                        <div class="panel-heading">
                                                            <h3 class="panel-title"> Fundación Específica Tecnológica </h3>
                                                        </div>
                                                        <div class="panel-body">
                                                            <?php print $node->field_propedeutico_profesional["und"][0]["safe_value"] ?>
                                                        </div>
                                                    </div>
                                                <?php } ?>
                                            </div>
                                        </div>

                                        <div class="tab-pane" id="investigacion">
                                            <?php print($node->field_inv["und"][0]["safe_value"]); ?>
                                        </div>

                                        <div class="tab-pane" id="director">
                                            <a href="<?php echo url("node/" . $node->field_director_prg["und"][0]["entity"]->nid);
                                            ?>">
                                                   <?php
                                                   print("<h3>");
                                                   print($node->field_director_prg["und"][0]["entity"]->title);
                                                   print(", ");
                                                   print($node->field_director_prg["und"][0]["entity"]->field_nivel_academico["und"][0]["value"]);
                                                   print("</h3>");
                                                   //@TODO: incluir redes sociales
                                                   ?>
                                            </a>
                                            <div class="row ">
                                                <div class="col-md-6">

                                                    <?php echo theme('image_style', array('style_name' => 'staff', 'path' => $node->field_director_prg["und"][0]["entity"]->field_staffs_image['und'][0]['uri'], 'attributes' => array('class' => 'img-responsive'))); ?> 
                                                    <?php print($node->field_director_prg["und"][0]["entity"]->field_social["und"][0]["safe_value"]); ?>

                                                </div>

                                                <div class="col-md-6">
                                                    <div class="pull-left">
                                                        <dl>  
                                                            <?php if (strlen($node->field_director_prg["und"][0]["entity"]->field_enlace_al_cvlac["und"][0]['url']) > 1) { ?>
                                                                <dt>CvLAC:</dt>
                                                                <dd><a target="_blank" href="<?php print($node->field_director_prg["und"][0]["entity"]->field_enlace_al_cvlac["und"][0]['url']) ?>"><?php print(" " . $node->field_director_prg["und"][0]["entity"]->field_enlace_al_cvlac["und"][0]['title']); ?></a></dd>
                                                            <?php } ?>                    
                                                            <?php if ($node->field_director_prg["und"][0]["entity"]->field_escalafon['und'][0]["value"] <> "No escalafonado") { ?>                       
                                                                <dt>Escalafón: </dt> 
                                                                <dd> <?php print(" " . $node->field_director_prg["und"][0]["entity"]->field_escalafon['und'][0]["value"]); ?></dd>
                                                            <?php } ?>
                                                            <?php if (strlen($node->field_director_prg["und"][0]["entity"]->field_mail["und"][0]["safe_value"]) > 1) { ?>   
                                                                <dt>Correo electrónico:</dt><dd><small><?php
                                                                        print(" " . $node->field_director_prg["und"][0]["entity"]->field_mail["und"][0]["safe_value"]);
                                                                        ?></small></dd>
                                                            <?php } ?>
                                                            <?php if (strlen($node->field_director_prg["und"][0]["entity"]->field_telefono["und"][0]["safe_value"]) > 1) { ?>
                                                                <dt>Teléfono:</dt><dd><?php
                                                                    print(" " . $node->field_director_prg["und"][0]["entity"]->field_telefono["und"][0]["safe_value"]);
                                                                    ?></dd>
                                                            <?php } ?>
                                                            <?php if (strlen($node->field_director_prg["und"][0]["entity"]->field_direccion["und"][0]["safe_value"]) > 1) { ?>
                                                                <dt>Dirección de Oficina:</dt>
                                                                <dd><?php print($node->field_director_prg["und"][0]["entity"]->field_direccion["und"][0]["safe_value"]); ?></dd>
                                                            <?php } ?>
                                                            <?php if (strlen($node->field_director_prg["und"][0]["entity"]->field_web_page["und"][0]['url']) > 1) { ?>
                                                                <dt>Web Personal:</dt>
                                                                <dd><a target="_blank" href="<?php print($node->field_director_prg["und"][0]["entity"]->field_web_page["und"][0]['url']) ?>"><?php print(" " . $node->field_director_prg["und"][0]["entity"]->field_web_page["und"][0]['title']); ?></a></dd>                                      
                                                            <?php } ?>
                                                        </dl>
                                                    </div>
                                                </div>
                                            </div>

                                        </div>

                                        <div class="tab-pane" id="profesores">
                                            <?php if ($node->field_profesores_tc["und"][0]["entity"]->nid) { ?>
                                                <h3>Profesores de tiempo completo</h3>
                                                <div class="programa_profesor_tc">
                                                    <ul class="nav nav-tabs prog" data-tabs="tabs">                       
                                                        <?php
                                                        for ($i = 0;
                                                        ; $i++) {
                                                            $ID = $node->field_profesores_tc["und"][$i]["entity"]->nid;
                                                            if ($ID == 0) {
                                                                break;
                                                            }
                                                            $alias = drupal_get_path_alias('node/' . $ID);
                                                            if (strcmp("0", $node->field_profesores_tc["und"][$i]["entity"]->status) != 0) {
                                                                ?>
                                                                <div class="col-md-6 thumbnail" style="height: 270px; font-size: 13px;">      
                                                                    <a href="<?php print($alias); ?>">                                                
                                                                        <?php print($node->field_profesores_tc["und"][$i]["entity"]->title . ", "); ?><?php print($node->field_profesores_tc["und"][$i]["entity"]->field_nivel_academico["und"][0]["value"]); ?>   
                                                                    </a><br/><br/>
                                                                    <div class="col-md-5">
                                                                        <div style="margin-bottom: 10px;"> <?php echo theme('image_style', array('style_name' => 'thumbnail', 'path' => $node->field_profesores_tc["und"][$i]["entity"]->field_staffs_image['und'][0]['uri'], 'attributes' => array('class' => 'img-responsive'))); ?>        </div>                            
                                                                        <div class="portada_profesor"> <?php print($node->field_profesores_tc["und"][$i]["entity"]->field_social["und"][0]["safe_value"]); ?> </div>
                                                                    </div>

                                                                    <div class="col-md-7">
                                                                        <dl>  
                                                                            <?php if (strlen($node->field_profesores_tc["und"][$i]["entity"]->field_enlace_al_cvlac["und"][0]['url']) > 1) { ?>
                                                                                <dt>CvLAC:</dt>
                                                                                <dd><a target="_blank" href="<?php print($node->field_profesores_tc["und"][$i]["entity"]->field_enlace_al_cvlac["und"][0]['url']) ?>"><?php print(" " . $node->field_profesores_tc["und"][$i]["entity"]->field_enlace_al_cvlac["und"][0]['title']); ?></a></dd>
                                                                            <?php } ?>                    
                                                                            <?php if ($node->field_profesores_tc["und"][$i]["entity"]->field_escalafon['und'][0]["value"] <> "No escalafonado") { ?>                       
                                                                                <dt>Escalafón: </dt> 
                                                                                <dd> <?php print(" " . $node->field_profesores_tc["und"][$i]["entity"]->field_escalafon['und'][0]["value"]); ?></dd>
                                                                            <?php } ?>
                                                                            <?php if (strlen($node->field_profesores_tc["und"][$i]["entity"]->field_mail["und"][0]["safe_value"]) > 1) { ?>   
                                                                                <dt>Correo electrónico:</dt><dd><small><?php
                                                                                        print(" " . $node->field_profesores_tc["und"][$i]["entity"]->field_mail["und"][0]["safe_value"]);
                                                                                        ?></small></dd>
                                                                            <?php } ?>
                                                                            <?php if (strlen($node->field_profesores_tc["und"][$i]["entity"]->field_telefono["und"][0]["safe_value"]) > 1) { ?>
                                                                                <dt>Teléfono:</dt><dd><?php
                                                                                    print(" " . $node->field_profesores_tc["und"][$i]["entity"]->field_telefono["und"][0]["safe_value"]);
                                                                                    ?></dd>
                                                                            <?php } ?>
                                                                            <?php if (strlen($node->field_profesores_tc["und"][$i]["entity"]->field_direccion["und"][0]["safe_value"]) > 1) { ?>
                                                                                <dt>Dirección de Oficina:</dt>
                                                                                <dd><?php print($node->field_profesores_tc["und"][$i]["entity"]->field_direccion["und"][0]["safe_value"]); ?></dd>
                                                                            <?php } ?></dl>

                                                                    </div>
                                                                </div>  

                                                                <?php
                                                            }
                                                        }
                                                        ?>

                                                </div>
                                            <?php } if ($node->field_profesores_ct["und"][0]["entity"]->nid) { ?>
                                                <h3>Profesores de cátedra</h3>
                                                <div class="programa_profesor_ct">
                                                    <ul class="nav nav-tabs prog" data-tabs="tabs">                       
                                                        <?php
                                                        for ($i = 0;
                                                        ; $i++) {
                                                            $ID = $node->field_profesores_ct["und"][$i]["entity"]->nid;
                                                            if ($ID == 0) {
                                                                break;
                                                            }
                                                            $alias = drupal_get_path_alias('node/' . $ID);
                                                            if (strcmp("0", $node->field_profesores_ct["und"][$i]["entity"]->status) != 0) {
                                                                ?>                                                    
                                                                <div class="col-md-6 thumbnail" style="height: 270px; font-size: 13px;">      
                                                                    <a href="<?php print($alias); ?>">                                                
                                                                        <?php print($node->field_profesores_ct["und"][$i]["entity"]->title . ", "); ?><?php print($node->field_profesores_ct["und"][$i]["entity"]->field_nivel_academico["und"][0]["value"]); ?>   
                                                                    </a><br/><br/>
                                                                    <div class="col-md-5">
                                                                        <div style="margin-bottom: 10px;"> <?php echo theme('image_style', array('style_name' => 'thumbnail', 'path' => $node->field_profesores_ct["und"][$i]["entity"]->field_staffs_image['und'][0]['uri'], 'attributes' => array('class' => 'img-responsive'))); ?>        </div>                            
                                                                        <div class="portada_profesor"> <?php print($node->field_profesores_ct["und"][$i]["entity"]->field_social["und"][0]["safe_value"]); ?> </div>
                                                                    </div>


                                                                    <div class="col-md-7">
                                                                        <dl>  
                                                                            <?php if (strlen($node->field_profesores_ct["und"][$i]["entity"]->field_enlace_al_cvlac["und"][0]['url']) > 1) { ?>
                                                                                <dt>CvLAC:</dt>
                                                                                <dd><a target="_blank" href="<?php print($node->field_profesores_ct["und"][$i]["entity"]->field_enlace_al_cvlac["und"][0]['url']) ?>"><?php print(" " . $node->field_profesores_ct["und"][$i]["entity"]->field_enlace_al_cvlac["und"][0]['title']); ?></a></dd>
                                                                            <?php } ?>                    
                                                                            <?php if ($node->field_profesores_ct["und"][$i]["entity"]->field_escalafon['und'][0]["value"] <> "No escalafonado") { ?>                       
                                                                                <dt>Escalafón: </dt> 
                                                                                <dd> <?php print(" " . $node->field_profesores_ct["und"][$i]["entity"]->field_escalafon['und'][0]["value"]); ?></dd>
                                                                            <?php } ?>
                                                                            <?php if (strlen($node->field_profesores_ct["und"][$i]["entity"]->field_mail["und"][0]["safe_value"]) > 1) { ?>   
                                                                                <dt>Correo electrónico:</dt><dd><small><?php
                                                                                        print(" " . $node->field_profesores_ct["und"][$i]["entity"]->field_mail["und"][0]["safe_value"]);
                                                                                        ?></small></dd>
                                                                            <?php } ?>
                                                                            <?php if (strlen($node->field_profesores_ct["und"][$i]["entity"]->field_telefono["und"][0]["safe_value"]) > 1) { ?>
                                                                                <dt>Teléfono:</dt><dd><?php
                                                                                    print(" " . $node->field_profesores_ct["und"][$i]["entity"]->field_telefono["und"][0]["safe_value"]);
                                                                                    ?></dd>
                                                                            <?php } ?>
                                                                            <?php if (strlen($node->field_profesores_ct["und"][$i]["entity"]->field_direccion["und"][0]["safe_value"]) > 1) { ?>
                                                                                <dt>Dirección de Oficina:</dt>
                                                                                <dd><?php print($node->field_profesores_ct["und"][$i]["entity"]->field_direccion["und"][0]["safe_value"]); ?></dd>
                                                                            <?php } ?></dl>

                                                                    </div>
                                                                </div> 
                                                                <?php
                                                            }
                                                        }
                                                        ?>
                                                    </ul>
                                                </div>
                                            <?php } if ($node->field_profesores_invitados["und"][0]["entity"]->nid) { ?>
                                                <h3>Profesores invitados</h3>
                                                <div class="programa_profesor_ct">
                                                    <ul class="nav nav-tabs prog" data-tabs="tabs">                       
                                                        <?php
                                                        for ($i = 0;
                                                        ; $i++) {
                                                            $ID = $node->field_profesores_invitados["und"][$i]["entity"]->nid;
                                                            if ($ID == 0) {
                                                                break;
                                                            }
                                                            $alias = drupal_get_path_alias('node/' . $ID);
                                                            if (strcmp("0", $node->field_profesores_invitados["und"][$i]["entity"]->status) != 0) {
                                                                ?>                                                    
                                                                <div class="col-md-6 thumbnail" style="height: 270px; font-size: 13px;">      
                                                                    <a href="<?php print($alias); ?>">                                                
                                                                        <?php print($node->field_profesores_invitados["und"][$i]["entity"]->title . ", "); ?><?php print($node->field_profesores_invitados["und"][$i]["entity"]->field_nivel_academico["und"][0]["value"]); ?>   
                                                                    </a><br/><br/>
                                                                    <div class="col-md-5">
                                                                        <div style="margin-bottom: 10px;"> <?php echo theme('image_style', array('style_name' => 'thumbnail', 'path' => $node->field_profesores_invitados["und"][$i]["entity"]->field_staffs_image['und'][0]['uri'], 'attributes' => array('class' => 'img-responsive'))); ?>        </div>                            
                                                                        <div class="portada_profesor"> <?php print($node->field_profesores_invitados["und"][$i]["entity"]->field_social["und"][0]["safe_value"]); ?> </div>
                                                                    </div>


                                                                    <div class="col-md-7">
                                                                        <dl>  
                                                                            <?php if (strlen($node->field_profesores_invitados["und"][$i]["entity"]->field_enlace_al_cvlac["und"][0]['url']) > 1) { ?>
                                                                                <dt>CvLAC:</dt>
                                                                                <dd><a target="_blank" href="<?php print($node->field_profesores_invitados["und"][$i]["entity"]->field_enlace_al_cvlac["und"][0]['url']) ?>"><?php print(" " . $node->field_profesores_invitados["und"][$i]["entity"]->field_enlace_al_cvlac["und"][0]['title']); ?></a></dd>
                                                                            <?php } ?>                    
                                                                            <?php if ($node->field_profesores_invitados["und"][$i]["entity"]->field_escalafon['und'][0]["value"] <> "No escalafonado") { ?>                       
                                                                                <dt>Escalafón: </dt> 
                                                                                <dd> <?php print(" " . $node->field_profesores_invitados["und"][$i]["entity"]->field_escalafon['und'][0]["value"]); ?></dd>
                                                                            <?php } ?>
                                                                            <?php if (strlen($node->field_profesores_invitados["und"][$i]["entity"]->field_mail["und"][0]["safe_value"]) > 1) { ?>   
                                                                                <dt>Correo electrónico:</dt><dd><small><?php
                                                                                        print(" " . $node->field_profesores_invitados["und"][$i]["entity"]->field_mail["und"][0]["safe_value"]);
                                                                                        ?></small></dd>
                                                                            <?php } ?>
                                                                            <?php if (strlen($node->field_profesores_invitados["und"][$i]["entity"]->field_telefono["und"][0]["safe_value"]) > 1) { ?>
                                                                                <dt>Teléfono:</dt><dd><?php
                                                                                    print(" " . $node->field_profesores_invitados["und"][$i]["entity"]->field_telefono["und"][0]["safe_value"]);
                                                                                    ?></dd>
                                                                            <?php } ?>
                                                                            <?php if (strlen($node->field_profesores_invitados["und"][$i]["entity"]->field_direccion["und"][0]["safe_value"]) > 1) { ?>
                                                                                <dt>Dirección de Oficina:</dt>
                                                                                <dd><?php print($node->field_profesores_invitados["und"][$i]["entity"]->field_direccion["und"][0]["safe_value"]); ?></dd>
                                                                            <?php } ?></dl>

                                                                    </div>
                                                                </div> 
                                                                <?php
                                                            }
                                                        }
                                                        ?>
                                                    </ul>
                                                </div>
                                            <?php } if ($node->field_profesores_mt["und"][0]["entity"]->nid) { ?>
                                                <h3>Profesores de tiempo parcial</h3>
                                                <div class="programa_profesor_ct">
                                                    <ul class="nav nav-tabs prog" data-tabs="tabs">                       
                                                        <?php
                                                        for ($i = 0;
                                                        ; $i++) {
                                                            $ID = $node->field_profesores_mt["und"][$i]["entity"]->nid;
                                                            if ($ID == 0) {
                                                                break;
                                                            }
                                                            $alias = drupal_get_path_alias('node/' . $ID);
                                                            if (strcmp("0", $node->field_profesores_mt["und"][$i]["entity"]->status) != 0) {
                                                                ?>                                                    
                                                                <div class="col-md-6 thumbnail" style="height: 270px; font-size: 13px;">      
                                                                    <a href="<?php print($alias); ?>">                                                
                                                                        <?php print($node->field_profesores_mt["und"][$i]["entity"]->title . ", "); ?><?php print($node->field_profesores_mt["und"][$i]["entity"]->field_nivel_academico["und"][0]["value"]); ?>   
                                                                    </a><br/><br/>
                                                                    <div class="col-md-5">
                                                                        <div style="margin-bottom: 10px;"> <?php echo theme('image_style', array('style_name' => 'thumbnail', 'path' => $node->field_profesores_mt["und"][$i]["entity"]->field_staffs_image['und'][0]['uri'], 'attributes' => array('class' => 'img-responsive'))); ?>        </div>                            
                                                                        <div class="portada_profesor"> <?php print($node->field_profesores_mt["und"][$i]["entity"]->field_social["und"][0]["safe_value"]); ?> </div>
                                                                    </div>


                                                                    <div class="col-md-7">
                                                                        <dl>  
                                                                            <?php if (strlen($node->field_profesores_mt["und"][$i]["entity"]->field_enlace_al_cvlac["und"][0]['url']) > 1) { ?>
                                                                                <dt>CvLAC:</dt>
                                                                                <dd><a target="_blank" href="<?php print($node->field_profesores_mt["und"][$i]["entity"]->field_enlace_al_cvlac["und"][0]['url']) ?>"><?php print(" " . $node->field_profesores_mt["und"][$i]["entity"]->field_enlace_al_cvlac["und"][0]['title']); ?></a></dd>
                                                                            <?php } ?>                    
                                                                            <?php if ($node->field_profesores_mt["und"][$i]["entity"]->field_escalafon['und'][0]["value"] <> "No escalafonado") { ?>                       
                                                                                <dt>Escalafón: </dt> 
                                                                                <dd> <?php print(" " . $node->field_profesores_mt["und"][$i]["entity"]->field_escalafon['und'][0]["value"]); ?></dd>
                                                                            <?php } ?>
                                                                            <?php if (strlen($node->field_profesores_mt["und"][$i]["entity"]->field_mail["und"][0]["safe_value"]) > 1) { ?>   
                                                                                <dt>Correo electrónico:</dt><dd><small><?php
                                                                                        print(" " . $node->field_profesores_mt["und"][$i]["entity"]->field_mail["und"][0]["safe_value"]);
                                                                                        ?></small></dd>
                                                                            <?php } ?>
                                                                            <?php if (strlen($node->field_profesores_mt["und"][$i]["entity"]->field_telefono["und"][0]["safe_value"]) > 1) { ?>
                                                                                <dt>Teléfono:</dt><dd><?php
                                                                                    print(" " . $node->field_profesores_mt["und"][$i]["entity"]->field_telefono["und"][0]["safe_value"]);
                                                                                    ?></dd>
                                                                            <?php } ?>
                                                                            <?php if (strlen($node->field_profesores_mt["und"][$i]["entity"]->field_direccion["und"][0]["safe_value"]) > 1) { ?>
                                                                                <dt>Dirección de Oficina:</dt>
                                                                                <dd><?php print($node->field_profesores_mt["und"][$i]["entity"]->field_direccion["und"][0]["safe_value"]); ?></dd>
                                                                            <?php } ?></dl>

                                                                    </div>
                                                                </div> 
                                                                <?php
                                                            }
                                                        }
                                                        ?>
                                                    </ul>
                                                </div>

                                            <?php } ?>


                                        </div>

                                        <div class="tab-pane" id="contacto">
                                            <?php print($node->field_contacto_prg["und"][0]["safe_value"]); ?>
                                        </div>	  

                                    </div>

                                </div>

                            </div>

                        </div>

                    </div>



                </div>


            </div>
            <?php //var_dump($node->field_director_prg["und"][0]["entity"]);                  ?>
        <?php endif; ?>

        <?php if ($links = render($content['links'])): ?>
            <div class="menu node-links clearfix"><?php print $links; ?></div>
        <?php endif; ?>

        <?php //var_dump($node->field_director_prg["und"][0]["entity"]->field_nivel_academico["und"][0]);                           ?>




    <?php endif; ?>
</article>