<div id="article-<?php print $node->nid; ?>" class="article <?php
print $classes;
if ($display_submitted)

?>clearfix" <?php print $attributes; ?> >
    <div class="row col-md-12 ">  
        
    
  <!-- Modal -->
  <div class="modal fade" id="myModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
    <div class="modal-dialog">
      <div class="modal-content" style="width: 802.222222328186px;">
        <div class="modal-header">
          <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
          <h4 class="modal-title"> <?php
            print("<h1><small> ");
            print($node->title);
            print(", ");
            print($node->field_nivel_academico["und"][0]["value"]);
            print("</small></h1> ");
            //@TODO: incluir redes sociales
            ?></h4>
        </div>
        <div class="modal-body">
          <?php echo theme('image_style', array('style_name' => 'large', 'path' => $node->field_staffs_image['und'][0]['uri'], 'class' => 'thumbnail')); ?> 
        </div>
        <div class="modal-footer">
          <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
        </div>
      </div><!-- /.modal-content -->
    </div><!-- /.modal-dialog -->
  </div><!-- /.modal -->
        
        
        <a href="<?php echo url("node/" . $node->nid); ?>">
            <?php
            print("<h1><small> ");
            print($node->title);
            print(", ");
            print($node->field_nivel_academico["und"][0]["value"]);
            print("</small></h1> ");
            //@TODO: incluir redes sociales
            ?>
        </a>
        <br/>
        <div class="row">
            <div class="col-md-6">

                <a href="#myModal" class="imagen" role="button" data-toggle="modal"> <?php echo theme('image_style', array('style_name' => 'staff', 'path' => $node->field_staffs_image['und'][0]['uri'], 'class' => 'imagen', 'attributes' => array('class' => 'img-responsive'))); ?> </a>                 
                <div style="margin-top:10px"><?php print($node->field_social["und"][0]["safe_value"]); ?></div>
            </div>

            <div class="col-md-6 centrado">
                <div class="pull-left">
                    <dl>  
                        <?php if (strlen($node->field_enlace_al_cvlac["und"][0]['url']) > 1) { ?>
                        <dt><span class="glyphicon glyphicon-user"></span>CvLAC:</dt>
                        <dd><a target="_blank" href="<?php print($node->field_enlace_al_cvlac["und"][0]['url']) ?>"><?php print(" " . $node->field_enlace_al_cvlac["und"][0]['title']); ?></a></dd>
                        <?php } ?>                    
                        <?php if ($node->field_escalafon['und'][0]["value"] <> "No escalafonado") { ?>                       
                        <dt><span class="glyphicon glyphicon-lock"></span>Escalafón: </dt> 
                        <dd> <?php print(" " . $node->field_escalafon['und'][0]["value"]); ?></dd>
                        <?php } ?>
                        <?php if (strlen($node->field_mail["und"][0]["safe_value"]) > 1) { ?>   
                        <dt><span class="glyphicon glyphicon-envelope"></span>Correo electrónico:</dt><dd><?php
                            print(" " . $node->field_mail["und"][0]["safe_value"]);
                            ?></dd>
                        <?php } ?>
                        <?php if (strlen($node->field_telefono["und"][0]["safe_value"]) > 1) { ?>
                        <dt><span class="glyphicon glyphicon-earphone"></span>Teléfono:</dt><dd><?php
                            print(" " . $node->field_telefono["und"][0]["safe_value"]);
                            ?></dd>
                        <?php } ?>
                        <?php if (strlen($node->field_direccion["und"][0]["safe_value"]) > 1) { ?>
                        <dt><span class="glyphicon glyphicon-map-marker"></span>Dirección de Oficina:</dt>
                        <dd><?php print($node->field_direccion["und"][0]["safe_value"]); ?></dd>
                        <?php } ?>
                        <?php if (strlen($node->field_web_page["und"][0]['url']) > 1) { ?>
                        <dt><span class="glyphicon glyphicon-link"></span>Web Personal:</dt>
                        <dd><a target="_blank" href="<?php print($node->field_web_page["und"][0]['url']) ?>"><?php print(" " . $node->field_web_page["und"][0]['title']); ?></a></dd>                                      
                        <?php } ?>
<!--
                        <dt>Temáticas de Interés:</dt>-->
                        <dd><span class="glyphicon glyphicon-saved"></span><?php
                            print render($content['field_tematicas_interes']);                            
                            ?></dd>                                      

                    </dl>
                </div>
            </div>
        </div>
        <br/>
        <hr>
        <br/>        
        <div class="row"
             <div class="col-md-12">
                <?php print($node->body["und"][0]["safe_value"]); ?>
                <?php
                if (isset($node->field_articulos_publicados['und'][0]["safe_value"])) {
                    print("<hr><br/>");
                    print("<h4>Artículos publicados</h4>");
                    print($node->field_articulos_publicados['und'][0]["safe_value"]);
                }
                ?>
                <?php
                if (isset($node->field_participacion_en_proyectos['und'][0]["safe_value"])) {
                    print("<hr><br/>");
                    print("<h4>Participación en proyectos</h4>");
                    print($node->field_participacion_en_proyectos['und'][0]["safe_value"]);
                }
                ?>
            </div>
        </div>
    </div>