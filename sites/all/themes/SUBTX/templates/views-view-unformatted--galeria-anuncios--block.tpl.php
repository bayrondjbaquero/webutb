<?php
/**
 * @file
 * Default simple view template to display a list of rows.
 *
 * @ingroup views_templates
 */
?>   


<div id="carousel-anuncio" class="carousel slide" data-ride="carousel">

    <div class="carousel-inner">
        <?php $i = 0; ?>
        <?php
        foreach ($rows as $id => $row) {
            if ($i == 0) {
                ?> <div class="item active"> <?php } else { ?>
                    <div class="item">
                    <?php } $i++; ?>

                    <div<?php
                    if ($classes_array[$id]) {
                        print ' class="' . $classes_array[$id] . '"';
                    }
                    ?>>
                            <?php print $row; ?>
                    </div>  
                </div>  
            <?php } ?>        

        </div>

        <div class="text-center">

            <a class="" href="javascript:void(0)" 
               data-slide="prev" data-target="#carousel-anuncio">
                <span class="glyphicon glyphicon-chevron-left"></span>
            </a>
                <li href="javascript:void(0)" class="indicators active" data-target="#carousel-anuncio" data-slide-to="0" ></li>
                <li href="javascript:void(0)" class="indicators" data-target="#carousel-anuncio" data-slide-to="1" ></li>
                <li href="javascript:void(0)" class="indicators" data-target="#carousel-anuncio" data-slide-to="2" ></li>
            
            <a class="" href="javascript:void(0)" 
               data-slide="next" data-target="#carousel-anuncio">
                <span class="glyphicon glyphicon-chevron-right"></span>
            </a>

        </div>
    </div>

