<?php
$entity = node_load($fields['nid']->raw);
if ($entity->field_programas["und"][1]["target_id"]) :
    ?>                             
    <div class="panel panel-default">

        <div class="panel-heading">
            <a class="panel-title" data-toggle="collapse" data-parent="#panel-pregrado" href="#<?php print $entity->nid . "pre"; ?>">
                <?php print $entity->title; ?></a>
        </div>

        <div id="<?php print $entity->nid . "pre"; ?>" class="panel-collapse collapse">

            <div class="panel-body">

                <p>La oferta de pregrado en programas profesionales de la <?php print $entity->title; ?> está compuesta de la siguiente manera:</p>
                <br />
                <div class="list-group">                  
                    <?php
                    $list_program = array();

                    for ($i = 0;; $i++) :
                        $ID = $entity->field_programas["und"][$i]["target_id"];
                        if ($ID == 0 or strcmp("0", $entity->status) == 0) :
                            break;
                        endif;
                        $program = node_load($entity->field_programas["und"][$i]["target_id"]);
                        array_push($list_program, $program);
                    endfor;
                    $titles = array();

                    for ($x = 0; $x < count($list_program); $x++) :
                        $titles[$x] = $list_program[$x]->title;
                    endfor;
                    asort($titles);
                    foreach ($titles as $key => $val):

                        $ID = $list_program[$key]->nid;
                        $alias = drupal_get_path_alias('node/' . $ID);
                        ?>

                        <a class="list-group-item" href="<?php print($alias); ?>">
                            <?php if ($list_program[$key]->field_nuevo):
                                if (strcmp($list_program[$key]->field_nuevo ["und"][0]["value"], '1') == 0): ?>
                                    <span class="badge pull-right">Nuevo</span>    
                                <?php endif; ?>  
                            <?php endif; ?><?php print $list_program[$key]->title; ?>                                    
                        </a>

                        <?php
                    endforeach;
                    ?>    
                </div>						

            </div>
        </div>
    </div>
<?php endif; ?> 