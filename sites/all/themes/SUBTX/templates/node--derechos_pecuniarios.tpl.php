<?php if (!empty($body)): ?>

    <?php print $node->body["und"][0]["safe_value"]; ?>

    <?php if ($node->field_programas["und"][0]["entity"]) : ?>
        <table class="table table-striped table-responsive table-bordered">
            <tbody>
                <tr>
                    <th>Programa</th>
                    <th>Precio</th>
                </tr>      
                <?php
                for ($i = 0;; $i++):
                    if (!$node->field_programas["und"][$i]["entity"]) :
                        break;
                    endif;
                    ?>  
                    <tr>
                        <td><?php print $node->field_programas["und"][$i]["entity"]->title; ?></td>
                        <td><?php print $node->field_programas["und"][$i]["entity"]->field_precio["und"][0]["value"]; ?></td>            
                    </tr>       
                <?php endfor; ?>        
            </tbody>
        </table>
    <?php endif; ?>


    <?php if ($node->field_cursos["und"][0]["entity"]) : ?>
        <table class="table table-striped table-responsive table-bordered">
            <tbody>
                <tr>
                    <th>Curso</th>
                    <th>Precio</th>
                </tr>      
                <?php
                for ($i = 0;; $i++):
                    if (!$node->field_cursos["und"][$i]["entity"]) :
                        break;
                    endif;
                    ?>  
                    <tr>
                        <td><?php print $node->field_cursos["und"][$i]["entity"]->title; ?></td>
                        <td><?php print $node->field_cursos["und"][$i]["entity"]->field_precio["und"][0]["value"]; ?></td>            
                    </tr>       
                <?php endfor; ?>        
            </tbody>
        </table>
    <?php endif; ?>


    <?php if ($node->field_programas_programaspos["und"][0]["entity"]) : ?>
        <table class="table table-striped table-responsive table-bordered">
            <tbody>
                <tr>
                    <th>Programa</th>
                    <th>Precio</th>
                </tr>      
                <?php
                for ($i = 0;; $i++):
                    if (!$node->field_programas_programaspos["und"][$i]["entity"]) :
                        break;
                    endif;
                    ?>  
                    <tr>
                        <td><?php print $node->field_programas_programaspos["und"][$i]["entity"]->title; ?></td>
                        <td><?php print $node->field_programas_programaspos["und"][$i]["entity"]->field_precio["und"][0]["value"]; ?></td>            
                    </tr>       
                <?php endfor; ?>        
            </tbody>
        </table>
    <?php endif; ?>


    <?php print $node->field_otros["und"][0]["safe_value"]; ?>

    <?php print $node->field_texto_inferior["und"][0]["safe_value"]; ?>

<?php endif; ?>