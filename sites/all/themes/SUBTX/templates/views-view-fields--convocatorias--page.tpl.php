<div class="col-md-11 col-md-offset-1 column">

    <div class="panel panel-primary">

        <div class="panel-heading">
            <h4 class="panel-title"><?php print  $fields['title']->content?></h4>
        </div>

        <div class="panel-body">

            <div class="img-responsive pull-right margen-m"> <?php print $fields['field_imagen_institucion']->content; ?> </div>

            <p>

              <?php print  $fields['body']->content?>

            </p>


        </div>

        <div class="panel-footer">

            <p>Organiza: <strong><?php  print  $fields['field_entidad']->content ?></strong> 							
            <a class="btn btn-primary btn-default pull-right" href="<?php print "node/".$fields['nid']->content; ?>" ><span class="glyphicon glyphicon-arrow-right"></span> Ver Convocatoria</a> 
            </p> 

        </div>

    </div>

</div>