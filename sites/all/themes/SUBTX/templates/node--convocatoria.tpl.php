<div id="node-<?php print $node->nid; ?>" class="<?php print $classes; ?> clearfix"<?php print $attributes; ?>>
   
    <div class="row clearfix">

        <div class="col-md-12 column">

            <table class="table table-striped">
                <tbody>
                    <tr>
                        <th>
                            <?php
                            if ($node->field_tipo_de_usuario["und"][0]["value"]) {
                                print "Tipo de Usuario";
                            } else {
                                print "Tipo de Convocatoria";
                            }
                            ?></th>
                        <th>Fecha Inicio</th>
                        <th>Fecha Final</th>
                    </tr>      
                    <tr>
                        <td><?php
//                            $flag = 0;
//                            while ($flag >= 0):
//                                print $node->field_tipo_de_usuario["und"][$flag]["value"];
//                                if ($node->field_tipo_de_usuario["und"][$flag + 1]["value"]):
//                                    $flag++;
//                                    echo"<br/>";
//                                else:
//                                    $flag = -1;
//                                endif;
//                            endwhile;
                            if ($node->field_tipo_de_usuario["und"][0]["value"]) {
                                print $node->field_tipo_de_usuario["und"][0]["value"];
                            } else {
                                print $node->field_tipo_convocatoria["und"][0]["value"];
                            }
                            ?></td>
                        <td><?php
                            print format_date(strtotime($content['field_fecha_convocatoria_']['#items'][0]['value']), 'custom', 'F j, Y l');
                            ?></td>
                        <td><?php
                            print format_date(strtotime($content['field_fecha_convocatoria_cierre']['#items'][0]['value']), 'custom', 'F j, Y l');
                            ?></td>
                    </tr>  

                    <tr>
                        <th></th>
                        <th>Entidad</th>
                        <th></th>
                    </tr> 
                    <tr>
                        <td></td>
                        <td><?php print $node->field_entidad["und"][0]['taxonomy_term']->name; ?></td>
                        <td></td>
                    </tr>

                </tbody>
            </table>   
            <?php if ($node->field_imagen_institucion) { ?>
            <div class="pull-left margen-s">
            <?php echo theme('image_style', array('style_name' => 'medium', 'path' => $node->field_imagen_institucion['und'][0]['uri'], 'attributes' => array('class' => 'img-responsive'))); ?> 
            </div> 
            <?php } ?>
            <?php print $node->body["und"][0]["safe_value"]; ?>
            <a href="<?php print $node->field_informacion_adicional_url["und"][0]["url"]; ?>"><?php print $node->field_informacion_adicional_url["und"][0]["url"]; ?></a>

<?php if ($node->field_adjuntos_convocatoria) { ?>
                <div class="panel panel-primary">
                    <div class="panel-heading">                    
                        <h3 class="panel-title"> <span class="glyphicon glyphicon-folder-open" style="font-size:2.5em;"></span>&nbsp;&nbsp;&nbsp; Descargas adicionales </h3> 
                    </div>
                    <div class="panel-body">
                <?php print render($content['field_adjuntos_convocatoria']); ?>                              
                    </div>											
                </div>
<?php } ?>
        </div>

    </div>
</div>


