<?php
$entity = node_load($fields['nid']->raw);
$uriImagefromNode = $entity->field_image['und'][0]['uri'];
$urlImagefromNode = file_create_url($uriImagefromNode);

?>
<a href="<?php print $entity->field_link['und'][0]['url']; ?>"><img alt="<?php print $entity->field_image['und'][0]['alt']; ?>" src="<?php  print $urlImagefromNode;?>" style=" min-height: 64px; " class="img-responsive"/></a>
<?php if($entity->title):?>
<?php if (strcmp($entity->title , "<none>") <> 0): ?>
<div class="captions-galeria padding-s opaco text-center">
    <h5><strong><?php print $entity->title; ?></strong></h5>
</div>

<?php endif;?>
<?php endif;?>