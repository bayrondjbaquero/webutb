<div id="node-<?php print $node->nid; ?>" class="<?php print $classes; ?> clearfix"<?php print $attributes; ?>>

    <div class="row clearfix">

        <div class="col-md-12 column">

            <?php print($node->body["und"][0]['safe_summary']); ?>

            <div class="row" style="margin-bottom: 30px;">

                <!-- Navigation Buttons -->
                <div class="col-md-2">

                    <ul class="nav nav-pills nav-stacked" id="Programas">

                        <?php if ($node->body["und"][0]["safe_value"]) { ?> 
                            <li class="active"><a href="#descripcion"  data-toggle="tab">Descripción</a></li><?php } ?> 

                        <?php if ($node->field_director["und"][0]["entity"]) { ?> 
                            <li ><a href="#director" data-toggle="tab">Director</a></li><?php } ?>                       

                        <?php if ($node->field_cuerpo_docente["und"][0]["entity"]) { ?> 
                            <li ><a href="#profesor" data-toggle="tab">Profesores</a></li><?php } ?>       

                        <?php if ($node->field_contacto_curso["und"][0]["safe_value"]) { ?>    
                            <li><a href="#contacto" data-toggle="tab">Contacto</a></li><?php } ?> 

                    </ul>

                </div>
                <!-- Content --> 
                <div class="col-md-10">

                    <div class="tab-content">

                        <div class="tab-pane active" id="descripcion">
                            <?php print render($content['body']); ?>
                        </div>

                        <div class="tab-pane " id="director">
                            <a href="<?php echo url("node/" . $node->field_director["und"][0]["entity"]->nid);
                            ?>">
                                   <?php
                                   print("<h3>");
                                   print($node->field_director["und"][0]["entity"]->title);
                                   print(", ");
                                   print($node->field_director["und"][0]["entity"]->field_nivel_academico["und"][0]["value"]);
                                   print("</h3>");
                                   //@TODO: incluir redes sociales
                                   ?>
                            </a>
                            <div class="row ">
                                <div class="col-md-6">
                                    <div>
                                        <?php echo theme('image_style', array('style_name' => 'staff', 'path' => $node->field_director["und"][0]["entity"]->field_staffs_image['und'][0]['uri'], 'attributes' => array('class' => 'img-responsive'))); ?> 
                                        <?php print($node->field_director["und"][0]["entity"]->field_social["und"][0]["safe_value"]); ?>
                                    </div>
                                </div>

                                <div class="col-md-6">
                                    <div class="pull-left">
                                        <dl>  
                                            <?php if (strlen($node->field_director["und"][0]["entity"]->field_enlace_al_cvlac["und"][0]['url']) > 1) { ?>
                                                <dt>CvLAC:</dt>
                                                <dd><a target="_blank" href="<?php print($node->field_director["und"][0]["entity"]->field_enlace_al_cvlac["und"][0]['url']) ?>"><?php print(" " . $node->field_director["und"][0]["entity"]->field_enlace_al_cvlac["und"][0]['title']); ?></a></dd>
                                            <?php } ?>                    
                                            <?php if ($node->field_director["und"][0]["entity"]->field_escalafon['und'][0]["value"] <> "No escalafonado") { ?>                       
                                                <dt>Escalafón: </dt> 
                                                <dd> <?php print(" " . $node->field_director["und"][0]["entity"]->field_escalafon['und'][0]["value"]); ?></dd>
                                            <?php } ?>
                                            <?php if (strlen($node->field_director["und"][0]["entity"]->field_mail["und"][0]["safe_value"]) > 1) { ?>   
                                                <dt>Correo electrónico:</dt><dd><small><?php
                                                        print(" " . $node->field_director["und"][0]["entity"]->field_mail["und"][0]["safe_value"]);
                                                        ?></small></dd>
                                            <?php } ?>
                                            <?php if (strlen($node->field_director["und"][0]["entity"]->field_telefono["und"][0]["safe_value"]) > 1) { ?>
                                                <dt>Teléfono:</dt><dd><?php
                                                    print(" " . $node->field_director["und"][0]["entity"]->field_telefono["und"][0]["safe_value"]);
                                                    ?></dd>
                                            <?php } ?>
                                            <?php if (strlen($node->field_director["und"][0]["entity"]->field_direccion["und"][0]["safe_value"]) > 1) { ?>
                                                <dt>Dirección de Oficina:</dt>
                                                <dd><?php print($node->field_director["und"][0]["entity"]->field_direccion["und"][0]["safe_value"]); ?></dd>
                                            <?php } ?>
                                            <?php if (strlen($node->field_director["und"][0]["entity"]->field_web_page["und"][0]['url']) > 1) { ?>
                                                <dt>Web Personal:</dt>
                                                <dd><a target="_blank" href="<?php print($node->field_director["und"][0]["entity"]->field_web_page["und"][0]['url']) ?>"><?php print(" " . $node->field_director["und"][0]["entity"]->field_web_page["und"][0]['title']); ?></a></dd>                                      
                                            <?php } ?>
                                        </dl>
                                    </div>
                                </div>
                            </div>

                        </div>

                        <div class="tab-pane" id="profesor">
                            <h3>Profesores</h3>

                            <?php if ($node->field_profesores_tc["und"][0]["entity"]->nid) { ?>
                                <h3>Profesores de tiempo completo</h3>
                                <div class="programa_profesor_tc">
                                    <ul class="nav nav-tabs prog" data-tabs="tabs">                       
                                        <?php
                                        for ($i = 0;
                                        ; $i++) {
                                            $ID = $node->field_profesores_tc["und"][$i]["entity"]->nid;
                                            if ($ID == 0) {
                                                break;
                                            }
                                            $alias = drupal_get_path_alias('node/' . $ID);
                                            if (strcmp("0", $node->field_profesores_tc["und"][$i]["entity"]->status) != 0) {
                                                ?>
                                                <div class="col-md-6 thumbnail" style="height: 270px; font-size: 13px;">      
                                                    <a href="<?php print($alias); ?>">                                                
                                                        <?php print($node->field_profesores_tc["und"][$i]["entity"]->title . ", "); ?><?php print($node->field_profesores_tc["und"][$i]["entity"]->field_nivel_academico["und"][0]["value"]); ?>   
                                                    </a><br/><br/>
                                                    <div class="col-md-5">
                                                        <div style="margin-bottom: 10px;"> <?php echo theme('image_style', array('style_name' => 'thumbnail', 'path' => $node->field_profesores_tc["und"][$i]["entity"]->field_staffs_image['und'][0]['uri'], 'attributes' => array('class' => 'img-responsive'))); ?>        </div>                            
                                                        <div class="portada_profesor"> <?php print($node->field_profesores_tc["und"][$i]["entity"]->field_social["und"][0]["safe_value"]); ?> </div>
                                                    </div>

                                                    <div class="col-md-7">
                                                        <dl>  
                                                            <?php if (strlen($node->field_profesores_tc["und"][$i]["entity"]->field_enlace_al_cvlac["und"][0]['url']) > 1) { ?>
                                                                <dt>CvLAC:</dt>
                                                                <dd><a target="_blank" href="<?php print($node->field_profesores_tc["und"][$i]["entity"]->field_enlace_al_cvlac["und"][0]['url']) ?>"><?php print(" " . $node->field_profesores_tc["und"][$i]["entity"]->field_enlace_al_cvlac["und"][0]['title']); ?></a></dd>
                                                            <?php } ?>                    
                                                            <?php if ($node->field_profesores_tc["und"][$i]["entity"]->field_escalafon['und'][0]["value"] <> "No escalafonado") { ?>                       
                                                                <dt>Escalafón: </dt> 
                                                                <dd> <?php print(" " . $node->field_profesores_tc["und"][$i]["entity"]->field_escalafon['und'][0]["value"]); ?></dd>
                                                            <?php } ?>
                                                            <?php if (strlen($node->field_profesores_tc["und"][$i]["entity"]->field_mail["und"][0]["safe_value"]) > 1) { ?>   
                                                                <dt>Correo electrónico:</dt><dd><small><?php
                                                                        print(" " . $node->field_profesores_tc["und"][$i]["entity"]->field_mail["und"][0]["safe_value"]);
                                                                        ?></small></dd>
                                                            <?php } ?>
                                                            <?php if (strlen($node->field_profesores_tc["und"][$i]["entity"]->field_telefono["und"][0]["safe_value"]) > 1) { ?>
                                                                <dt>Teléfono:</dt><dd><?php
                                                                    print(" " . $node->field_profesores_tc["und"][$i]["entity"]->field_telefono["und"][0]["safe_value"]);
                                                                    ?></dd>
                                                            <?php } ?>
                                                            <?php if (strlen($node->field_profesores_tc["und"][$i]["entity"]->field_direccion["und"][0]["safe_value"]) > 1) { ?>
                                                                <dt>Dirección de Oficina:</dt>
                                                                <dd><?php print($node->field_profesores_tc["und"][$i]["entity"]->field_direccion["und"][0]["safe_value"]); ?></dd>
                                                            <?php } ?></dl>

                                                    </div>
                                                </div>  

                                                <?php
                                            }
                                        }
                                        ?>

                                </div>
                            <?php } if ($node->field_profesores_ct["und"][0]["entity"]->nid) { ?>
                                <h3>Profesores de cátedra</h3>
                                <div class="programa_profesor_ct">
                                    <ul class="nav nav-tabs prog" data-tabs="tabs">                       
                                        <?php
                                        for ($i = 0;
                                        ; $i++) {
                                            $ID = $node->field_profesores_ct["und"][$i]["entity"]->nid;
                                            if ($ID == 0) {
                                                break;
                                            }
                                            $alias = drupal_get_path_alias('node/' . $ID);
                                            if (strcmp("0", $node->field_profesores_ct["und"][$i]["entity"]->status) != 0) {
                                                ?>                                                    
                                                <div class="col-md-6 thumbnail" style="height: 270px; font-size: 13px;">      
                                                    <a href="<?php print($alias); ?>">                                                
                                                        <?php print($node->field_profesores_ct["und"][$i]["entity"]->title . ", "); ?><?php print($node->field_profesores_ct["und"][$i]["entity"]->field_nivel_academico["und"][0]["value"]); ?>   
                                                    </a><br/><br/>
                                                    <div class="col-md-5">
                                                        <div style="margin-bottom: 10px;"> <?php echo theme('image_style', array('style_name' => 'thumbnail', 'path' => $node->field_profesores_ct["und"][$i]["entity"]->field_staffs_image['und'][0]['uri'], 'attributes' => array('class' => 'img-responsive'))); ?>        </div>                            
                                                        <div class="portada_profesor"> <?php print($node->field_profesores_ct["und"][$i]["entity"]->field_social["und"][0]["safe_value"]); ?> </div>
                                                    </div>


                                                    <div class="col-md-7">
                                                        <dl>  
                                                            <?php if (strlen($node->field_profesores_ct["und"][$i]["entity"]->field_enlace_al_cvlac["und"][0]['url']) > 1) { ?>
                                                                <dt>CvLAC:</dt>
                                                                <dd><a target="_blank" href="<?php print($node->field_profesores_ct["und"][$i]["entity"]->field_enlace_al_cvlac["und"][0]['url']) ?>"><?php print(" " . $node->field_profesores_ct["und"][$i]["entity"]->field_enlace_al_cvlac["und"][0]['title']); ?></a></dd>
                                                            <?php } ?>                    
                                                            <?php if ($node->field_profesores_ct["und"][$i]["entity"]->field_escalafon['und'][0]["value"] <> "No escalafonado") { ?>                       
                                                                <dt>Escalafón: </dt> 
                                                                <dd> <?php print(" " . $node->field_profesores_ct["und"][$i]["entity"]->field_escalafon['und'][0]["value"]); ?></dd>
                                                            <?php } ?>
                                                            <?php if (strlen($node->field_profesores_ct["und"][$i]["entity"]->field_mail["und"][0]["safe_value"]) > 1) { ?>   
                                                                <dt>Correo electrónico:</dt><dd><small><?php
                                                                        print(" " . $node->field_profesores_ct["und"][$i]["entity"]->field_mail["und"][0]["safe_value"]);
                                                                        ?></small></dd>
                                                            <?php } ?>
                                                            <?php if (strlen($node->field_profesores_ct["und"][$i]["entity"]->field_telefono["und"][0]["safe_value"]) > 1) { ?>
                                                                <dt>Teléfono:</dt><dd><?php
                                                                    print(" " . $node->field_profesores_ct["und"][$i]["entity"]->field_telefono["und"][0]["safe_value"]);
                                                                    ?></dd>
                                                            <?php } ?>
                                                            <?php if (strlen($node->field_profesores_ct["und"][$i]["entity"]->field_direccion["und"][0]["safe_value"]) > 1) { ?>
                                                                <dt>Dirección de Oficina:</dt>
                                                                <dd><?php print($node->field_profesores_ct["und"][$i]["entity"]->field_direccion["und"][0]["safe_value"]); ?></dd>
                                                            <?php } ?></dl>

                                                    </div>
                                                </div> 
                                                <?php
                                            }
                                        }
                                        ?>
                                    </ul>
                                </div>
                            <?php } if ($node->field_profesores_invitados["und"][0]["entity"]->nid) { ?>
                                <h3>Profesores invitados</h3>
                                <div class="programa_profesor_ct">
                                    <ul class="nav nav-tabs prog" data-tabs="tabs">                       
                                        <?php
                                        for ($i = 0;
                                        ; $i++) {
                                            $ID = $node->field_profesores_invitados["und"][$i]["entity"]->nid;
                                            if ($ID == 0) {
                                                break;
                                            }
                                            $alias = drupal_get_path_alias('node/' . $ID);
                                            if (strcmp("0", $node->field_profesores_invitados["und"][$i]["entity"]->status) != 0) {
                                                ?>                                                    
                                                <div class="col-md-6 thumbnail" style="height: 270px; font-size: 13px;">      
                                                    <a href="<?php print($alias); ?>">                                                
                                                        <?php print($node->field_profesores_invitados["und"][$i]["entity"]->title . ", "); ?><?php print($node->field_profesores_invitados["und"][$i]["entity"]->field_nivel_academico["und"][0]["value"]); ?>   
                                                    </a><br/><br/>
                                                    <div class="col-md-5">
                                                        <div style="margin-bottom: 10px;"> <?php echo theme('image_style', array('style_name' => 'thumbnail', 'path' => $node->field_profesores_invitados["und"][$i]["entity"]->field_staffs_image['und'][0]['uri'], 'attributes' => array('class' => 'img-responsive'))); ?>        </div>                            
                                                        <div class="portada_profesor"> <?php print($node->field_profesores_invitados["und"][$i]["entity"]->field_social["und"][0]["safe_value"]); ?> </div>
                                                    </div>


                                                    <div class="col-md-7">
                                                        <dl>  
                                                            <?php if (strlen($node->field_profesores_invitados["und"][$i]["entity"]->field_enlace_al_cvlac["und"][0]['url']) > 1) { ?>
                                                                <dt>CvLAC:</dt>
                                                                <dd><a target="_blank" href="<?php print($node->field_profesores_invitados["und"][$i]["entity"]->field_enlace_al_cvlac["und"][0]['url']) ?>"><?php print(" " . $node->field_profesores_invitados["und"][$i]["entity"]->field_enlace_al_cvlac["und"][0]['title']); ?></a></dd>
                                                            <?php } ?>                    
                                                            <?php if ($node->field_profesores_invitados["und"][$i]["entity"]->field_escalafon['und'][0]["value"] <> "No escalafonado") { ?>                       
                                                                <dt>Escalafón: </dt> 
                                                                <dd> <?php print(" " . $node->field_profesores_invitados["und"][$i]["entity"]->field_escalafon['und'][0]["value"]); ?></dd>
                                                            <?php } ?>
                                                            <?php if (strlen($node->field_profesores_invitados["und"][$i]["entity"]->field_mail["und"][0]["safe_value"]) > 1) { ?>   
                                                                <dt>Correo electrónico:</dt><dd><small><?php
                                                                        print(" " . $node->field_profesores_invitados["und"][$i]["entity"]->field_mail["und"][0]["safe_value"]);
                                                                        ?></small></dd>
                                                            <?php } ?>
                                                            <?php if (strlen($node->field_profesores_invitados["und"][$i]["entity"]->field_telefono["und"][0]["safe_value"]) > 1) { ?>
                                                                <dt>Teléfono:</dt><dd><?php
                                                                    print(" " . $node->field_profesores_invitados["und"][$i]["entity"]->field_telefono["und"][0]["safe_value"]);
                                                                    ?></dd>
                                                            <?php } ?>
                                                            <?php if (strlen($node->field_profesores_invitados["und"][$i]["entity"]->field_direccion["und"][0]["safe_value"]) > 1) { ?>
                                                                <dt>Dirección de Oficina:</dt>
                                                                <dd><?php print($node->field_profesores_invitados["und"][$i]["entity"]->field_direccion["und"][0]["safe_value"]); ?></dd>
                                                            <?php } ?></dl>

                                                    </div>
                                                </div> 
                                                <?php
                                            }
                                        }
                                        ?>
                                    </ul>
                                </div>
                            <?php } if ($node->field_profesores_mt["und"][0]["entity"]->nid) { ?>
                                <h3>Profesores de tiempo parcial</h3>
                                <div class="programa_profesor_ct">
                                    <ul class="nav nav-tabs prog" data-tabs="tabs">                       
                                        <?php
                                        for ($i = 0;
                                        ; $i++) {
                                            $ID = $node->field_profesores_mt["und"][$i]["entity"]->nid;
                                            if ($ID == 0) {
                                                break;
                                            }
                                            $alias = drupal_get_path_alias('node/' . $ID);
                                            if (strcmp("0", $node->field_profesores_mt["und"][$i]["entity"]->status) != 0) {
                                                ?>                                                    
                                                <div class="col-md-6 thumbnail" style="height: 270px; font-size: 13px;">      
                                                    <a href="<?php print($alias); ?>">                                                
                                                        <?php print($node->field_profesores_mt["und"][$i]["entity"]->title . ", "); ?><?php print($node->field_profesores_mt["und"][$i]["entity"]->field_nivel_academico["und"][0]["value"]); ?>   
                                                    </a><br/><br/>
                                                    <div class="col-md-5">
                                                        <div style="margin-bottom: 10px;"> <?php echo theme('image_style', array('style_name' => 'thumbnail', 'path' => $node->field_profesores_mt["und"][$i]["entity"]->field_staffs_image['und'][0]['uri'], 'attributes' => array('class' => 'img-responsive'))); ?>        </div>                            
                                                        <div class="portada_profesor"> <?php print($node->field_profesores_mt["und"][$i]["entity"]->field_social["und"][0]["safe_value"]); ?> </div>
                                                    </div>


                                                    <div class="col-md-7">
                                                        <dl>  
                                                            <?php if (strlen($node->field_profesores_mt["und"][$i]["entity"]->field_enlace_al_cvlac["und"][0]['url']) > 1) { ?>
                                                                <dt>CvLAC:</dt>
                                                                <dd><a target="_blank" href="<?php print($node->field_profesores_mt["und"][$i]["entity"]->field_enlace_al_cvlac["und"][0]['url']) ?>"><?php print(" " . $node->field_profesores_mt["und"][$i]["entity"]->field_enlace_al_cvlac["und"][0]['title']); ?></a></dd>
                                                            <?php } ?>                    
                                                            <?php if ($node->field_profesores_mt["und"][$i]["entity"]->field_escalafon['und'][0]["value"] <> "No escalafonado") { ?>                       
                                                                <dt>Escalafón: </dt> 
                                                                <dd> <?php print(" " . $node->field_profesores_mt["und"][$i]["entity"]->field_escalafon['und'][0]["value"]); ?></dd>
                                                            <?php } ?>
                                                            <?php if (strlen($node->field_profesores_mt["und"][$i]["entity"]->field_mail["und"][0]["safe_value"]) > 1) { ?>   
                                                                <dt>Correo electrónico:</dt><dd><small><?php
                                                                        print(" " . $node->field_profesores_mt["und"][$i]["entity"]->field_mail["und"][0]["safe_value"]);
                                                                        ?></small></dd>
                                                            <?php } ?>
                                                            <?php if (strlen($node->field_profesores_mt["und"][$i]["entity"]->field_telefono["und"][0]["safe_value"]) > 1) { ?>
                                                                <dt>Teléfono:</dt><dd><?php
                                                                    print(" " . $node->field_profesores_mt["und"][$i]["entity"]->field_telefono["und"][0]["safe_value"]);
                                                                    ?></dd>
                                                            <?php } ?>
                                                            <?php if (strlen($node->field_profesores_mt["und"][$i]["entity"]->field_direccion["und"][0]["safe_value"]) > 1) { ?>
                                                                <dt>Dirección de Oficina:</dt>
                                                                <dd><?php print($node->field_profesores_mt["und"][$i]["entity"]->field_direccion["und"][0]["safe_value"]); ?></dd>
                                                            <?php } ?></dl>

                                                    </div>
                                                </div> 
                                                <?php
                                            }
                                        }
                                        ?>
                                    </ul>
                                </div>

                            <?php } ?>



                        </div>

                        <div class="tab-pane" id="contacto">

                            <?php print($node->field_contacto_curso["und"][0]["safe_value"]); ?>

                        </div>	  

                    </div>

                </div>

            </div>

        </div>
        <?php if (($node->field_libre_curso) || ($node->field_libre_seminario) || ($node->field_libre_diplomado) || ($node->field_libre_minor) || ($node->field_libre_taller) || ($node->field_libre_charla) || ($node->field_libre_conferencia) || ($node->field_libre_homologable)) { ?>
            <div class="col-md-12 column thumbnail">
                <h3>Cursos Libres</h3>

                <div class="col-md-2">

                    <ul class="nav nav-pills nav-stacked" id="Programas">
                        <?php if ($node->field_libre_curso["und"][0]["entity"]) { ?> 
                            <li class="active"><a href="#libre_curso"  data-toggle="tab">Cursos</a></li><?php } ?> 
                        <?php if ($node->field_libre_seminario["und"][0]["entity"]) { ?> 
                            <li><a href="#libre_seminario"  data-toggle="tab">Seminarios</a></li><?php } ?> 
                        <?php if ($node->field_libre_diplomado["und"][0]["entity"]) { ?> 
                            <li><a href="#libre_diplomado"  data-toggle="tab">Diplomados</a></li><?php } ?> 
                        <?php if ($node->field_libre_minor["und"][0]["entity"]) { ?> 
                            <li><a href="#libre_minor"  data-toggle="tab">Minors</a></li><?php } ?> 
                        <?php if ($node->field_libre_taller["und"][0]["entity"]) { ?> 
                            <li><a href="#libre_taller"  data-toggle="tab">Talleres</a></li><?php } ?> 
                        <?php if ($node->field_libre_charla["und"][0]["entity"]) { ?> 
                            <li><a href="#libre_charla"  data-toggle="tab">Charlas</a></li><?php } ?> 
                        <?php if ($node->field_libre_conferencia["und"][0]["entity"]) { ?> 
                            <li><a href="#libre_conferencia"  data-toggle="tab">Conferencias</a></li><?php } ?>
                        <?php if ($node->field_libre_homologable["und"][0]["entity"]) { ?> 
                            <li><a href="#libre_homologable"  data-toggle="tab">Homologables</a></li><?php } ?> 
                    </ul> 

                </div>
                <div class="col-md-10">
                    <div class="tab-content">
                        <div class="tab-pane active" id="libre_curso">
                            <h3>Cursos</h3> 
                            <ul class="nav list-group" data-tabs="tabs">                       
                                <?php
                                for ($i = 0;; $i++) {
                                    $ID = $node->field_libre_curso["und"][$i]["entity"]->nid;

                                    if ($ID == 0 or strcmp("0", $node->status) == 0) {
                                        break;
                                    }
                                    if ($node->field_libre_curso["und"][$i]["entity"]->status > 0) {
                                        $alias = drupal_get_path_alias('node/' . $ID);
                                        ?>
                                        <li class="list-group-item" >
                                            <a href="<?php print($alias); ?>">
                                                <div class=""> <?php if ($node->field_libre_curso["und"][$i]["entity"]->field_nuevo): if (strcmp($node->field_libre_curso["und"][$i]["entity"]->field_nuevo ["und"][0]["value"], '1') == 0): ?>
                                                            <span class="badge pull-right">Nuevo</span>    
                                                        <?php endif; ?>  <?php endif; ?>
                                                    <?php print($node->field_libre_curso["und"][$i]["entity"]->title); ?> 
                                                </div>
                                            </a>
                                        </li>

                                        <?php
                                    }
                                }
                                ?>
                            </ul>
                        </div>
                        <div class="tab-pane" id="libre_seminario">
                            <h3>Seminarios</h3> 
                            <ul class="nav list-group" data-tabs="tabs">                       
                                <?php
                                for ($i = 0;; $i++) {
                                    $ID = $node->field_libre_seminario["und"][$i]["entity"]->nid;

                                    if ($ID == 0 or strcmp("0", $node->status) == 0) {
                                        break;
                                    }
                                    if ($node->field_libre_seminario["und"][$i]["entity"]->status > 0) {
                                        $alias = drupal_get_path_alias('node/' . $ID);
                                        ?>
                                        <li class="list-group-item" >
                                            <a href="<?php print($alias); ?>">
                                                <div class=""> <?php if ($node->field_libre_seminario["und"][$i]["entity"]->field_nuevo): if (strcmp($node->field_libre_seminario["und"][$i]["entity"]->field_nuevo ["und"][0]["value"], '1') == 0): ?>
                                                            <span class="badge pull-right">Nuevo</span>    
                                                        <?php endif; ?>  <?php endif; ?>
                                                    <?php print($node->field_libre_seminario["und"][$i]["entity"]->title); ?> 
                                                </div>
                                            </a>
                                        </li>

                                        <?php
                                    }
                                }
                                ?>
                            </ul>
                        </div>
                        <div class="tab-pane" id="libre_diplomado">
                            <h3>Diplomados</h3> 
                            <ul class="nav list-group" data-tabs="tabs">                       
                                <?php
                                for ($i = 0;; $i++) {
                                    $ID = $node->field_libre_diplomado["und"][$i]["entity"]->nid;

                                    if ($ID == 0 or strcmp("0", $node->status) == 0) {
                                        break;
                                    }
                                    if ($node->field_libre_diplomado["und"][$i]["entity"]->status > 0) {
                                        $alias = drupal_get_path_alias('node/' . $ID);
                                        ?>
                                        <li class="list-group-item" >
                                            <a href="<?php print($alias); ?>">
                                                <div class=""> <?php if ($node->field_libre_diplomado["und"][$i]["entity"]->field_nuevo): if (strcmp($node->field_libre_diplomado["und"][$i]["entity"]->field_nuevo ["und"][0]["value"], '1') == 0): ?>
                                                            <span class="badge pull-right">Nuevo</span>    
                                                        <?php endif; ?>  <?php endif; ?>
                                                    <?php print($node->field_libre_diplomado["und"][$i]["entity"]->title); ?> 
                                                </div>
                                            </a>
                                        </li>

                                        <?php
                                    }
                                }
                                ?>
                            </ul>
                        </div>
                        <div class="tab-pane" id="libre_minor">
                            <h3>Minors</h3> 
                            <ul class="nav list-group" data-tabs="tabs">                       
                                <?php
                                for ($i = 0;; $i++) {
                                    $ID = $node->field_libre_minor["und"][$i]["entity"]->nid;

                                    if ($ID == 0 or strcmp("0", $node->status) == 0) {
                                        break;
                                    }
                                    if ($node->field_libre_minor["und"][$i]["entity"]->status > 0) {
                                        $alias = drupal_get_path_alias('node/' . $ID);
                                        ?>
                                        <li class="list-group-item" >
                                            <a href="<?php print($alias); ?>">
                                                <div class=""> <?php if ($node->field_libre_minor["und"][$i]["entity"]->field_nuevo): if (strcmp($node->field_libre_minor["und"][$i]["entity"]->field_nuevo ["und"][0]["value"], '1') == 0): ?>
                                                            <span class="badge pull-right">Nuevo</span>    
                                                        <?php endif; ?>  <?php endif; ?>
                                                    <?php print($node->field_libre_minor["und"][$i]["entity"]->title); ?> 
                                                </div>
                                            </a>
                                        </li>

                                        <?php
                                    }
                                }
                                ?>
                            </ul>
                        </div>
                        <div class="tab-pane" id="libre_taller">
                            <h3>Talleres</h3> 
                            <ul class="nav list-group" data-tabs="tabs">                       
                                <?php
                                for ($i = 0;; $i++) {
                                    $ID = $node->field_libre_taller["und"][$i]["entity"]->nid;

                                    if ($ID == 0 or strcmp("0", $node->status) == 0) {
                                        break;
                                    }
                                    if ($node->field_libre_taller["und"][$i]["entity"]->status > 0) {
                                        $alias = drupal_get_path_alias('node/' . $ID);
                                        ?>
                                        <li class="list-group-item" >
                                            <a href="<?php print($alias); ?>">
                                                <div class=""> <?php if ($node->field_libre_taller["und"][$i]["entity"]->field_nuevo): if (strcmp($node->field_libre_taller["und"][$i]["entity"]->field_nuevo ["und"][0]["value"], '1') == 0): ?>
                                                            <span class="badge pull-right">Nuevo</span>    
                                                        <?php endif; ?>  <?php endif; ?>
                                                    <?php print($node->field_libre_taller["und"][$i]["entity"]->title); ?> 
                                                </div>
                                            </a>
                                        </li>

                                        <?php
                                    }
                                }
                                ?>
                            </ul>
                        </div>
                        <div class="tab-pane" id="libre_charla">
                            <h3>Charlas</h3> 
                            <ul class="nav list-group" data-tabs="tabs">                       
                                <?php
                                for ($i = 0;; $i++) {
                                    $ID = $node->field_libre_charla["und"][$i]["entity"]->nid;

                                    if ($ID == 0 or strcmp("0", $node->status) == 0) {
                                        break;
                                    }
                                    if ($node->field_libre_charla["und"][$i]["entity"]->status > 0) {
                                        $alias = drupal_get_path_alias('node/' . $ID);
                                        ?>
                                        <li class="list-group-item" >
                                            <a href="<?php print($alias); ?>">
                                                <div class=""> <?php if ($node->field_libre_charla["und"][$i]["entity"]->field_nuevo): if (strcmp($node->field_libre_charla["und"][$i]["entity"]->field_nuevo ["und"][0]["value"], '1') == 0): ?>
                                                            <span class="badge pull-right">Nuevo</span>    
                                                        <?php endif; ?>  <?php endif; ?>
                                                    <?php print($node->field_libre_charla["und"][$i]["entity"]->title); ?> 
                                                </div>
                                            </a>
                                        </li>

                                        <?php
                                    }
                                }
                                ?>
                            </ul>
                        </div>
                        <div class="tab-pane" id="libre_conferencia">
                            <h3>Conferencias</h3> 
                            <ul class="nav list-group" data-tabs="tabs">                       
                                <?php
                                for ($i = 0;; $i++) {
                                    $ID = $node->field_libre_conferencia["und"][$i]["entity"]->nid;

                                    if ($ID == 0 or strcmp("0", $node->status) == 0) {
                                        break;
                                    }
                                    if ($node->field_libre_conferencia["und"][$i]["entity"]->status > 0) {
                                        $alias = drupal_get_path_alias('node/' . $ID);
                                        ?>
                                        <li class="list-group-item" >
                                            <a href="<?php print($alias); ?>">
                                                <div class=""> <?php if ($node->field_libre_conferencia["und"][$i]["entity"]->field_nuevo): if (strcmp($node->field_libre_conferencia["und"][$i]["entity"]->field_nuevo ["und"][0]["value"], '1') == 0): ?>
                                                            <span class="badge pull-right">Nuevo</span>    
                                                        <?php endif; ?>  <?php endif; ?>
                                                    <?php print($node->field_libre_conferencia["und"][$i]["entity"]->title); ?> 
                                                </div>
                                            </a>
                                        </li>

                                        <?php
                                    }
                                }
                                ?>
                            </ul>
                        </div>
                        <div class="tab-pane" id="libre_homologable">
                            <h3>Homologables</h3> 
                            <ul class="nav list-group" data-tabs="tabs">                       
                                <?php
                                for ($i = 0;; $i++) {
                                    $ID = $node->field_libre_homologable["und"][$i]["entity"]->nid;

                                    if ($ID == 0 or strcmp("0", $node->status) == 0) {
                                        break;
                                    }
                                    if ($node->field_libre_homologable["und"][$i]["entity"]->status > 0) {
                                        $alias = drupal_get_path_alias('node/' . $ID);
                                        ?>
                                        <li class="list-group-item" >
                                            <a href="<?php print($alias); ?>">
                                                <div class=""> <?php if ($node->field_libre_homologable["und"][$i]["entity"]->field_nuevo): if (strcmp($node->field_libre_homologable["und"][$i]["entity"]->field_nuevo ["und"][0]["value"], '1') == 0): ?>
                                                            <span class="badge pull-right">Nuevo</span>    
                                                        <?php endif; ?>  <?php endif; ?>
                                                    <?php print($node->field_libre_homologable["und"][$i]["entity"]->title); ?> 
                                                </div>
                                            </a>
                                        </li>

                                        <?php
                                    }
                                }
                                ?>
                            </ul>
                        </div>


                    </div>
                </div>

            </div>
        <?php } ?>
        <?php if (($node->field_empresarial_curso) || ($node->field_empresarial_seminario) || ($node->field_empresarial_diplomado) || ($node->field_empresarial_minor) || ($node->field_empresarial_taller) || ($node->field_empresarial_charla) || ($node->field_empresarial_conferencia) || ($node->field_empresarial_homologable)) { ?>
            <div class="col-md-12 column thumbnail">
                <h3>Cursos Empresariales</h3>

                <div class="col-md-2">

                    <ul class="nav nav-pills nav-stacked" id="Programas">
                        <?php if ($node->field_empresarial_curso["und"][0]["entity"]) { ?> 
                            <li class="active"><a href="#empresarial_curso"  data-toggle="tab">Cursos</a></li><?php } ?> 
                        <?php if ($node->field_empresarial_seminario["und"][0]["entity"]) { ?> 
                            <li><a href="#empresarial_seminario"  data-toggle="tab">Seminarios</a></li><?php } ?> 
                        <?php if ($node->field_empresarial_diplomado["und"][0]["entity"]) { ?> 
                            <li><a href="#empresarial_diplomado"  data-toggle="tab">Diplomados</a></li><?php } ?> 
                        <?php if ($node->field_empresarial_minor["und"][0]["entity"]) { ?> 
                            <li><a href="#empresarial_minor"  data-toggle="tab">Minors</a></li><?php } ?> 
                        <?php if ($node->field_empresarial_taller["und"][0]["entity"]) { ?> 
                            <li><a href="#empresarial_taller"  data-toggle="tab">Talleres</a></li><?php } ?> 
                        <?php if ($node->field_empresarial_charla["und"][0]["entity"]) { ?> 
                            <li><a href="#empresarial_charla"  data-toggle="tab">Charlas</a></li><?php } ?> 
                        <?php if ($node->field_empresarial_conferencia["und"][0]["entity"]) { ?> 
                            <li><a href="#empresarial_conferencia"  data-toggle="tab">Conferencias</a></li><?php } ?> 
                        <?php if ($node->field_empresarial_homologable["und"][0]["entity"]) { ?> 
                            <li><a href="#empresarial_homologable"  data-toggle="tab">Homologables</a></li><?php } ?> 
                    </ul> 

                </div>
                <div class="col-md-10">
                    <div class="tab-content">
                        <div class="tab-pane active" id="empresarial_curso">
                            <h3>Cursos</h3> 
                            <ul class="nav list-group" data-tabs="tabs">                       
                                <?php
                                for ($i = 0;; $i++) {
                                    $ID = $node->field_empresarial_curso["und"][$i]["entity"]->nid;

                                    if ($ID == 0 or strcmp("0", $node->status) == 0) {
                                        break;
                                    }
                                    if ($node->field_empresarial_curso["und"][$i]["entity"]->status > 0) {
                                        $alias = drupal_get_path_alias('node/' . $ID);
                                        ?>
                                        <li class="list-group-item" >
                                            <a href="<?php print($alias); ?>">
                                                <div class=""> <?php if ($node->field_empresarial_curso["und"][$i]["entity"]->field_nuevo): if (strcmp($node->field_empresarial_curso["und"][$i]["entity"]->field_nuevo ["und"][0]["value"], '1') == 0): ?>
                                                            <span class="badge pull-right">Nuevo</span>    
                                                        <?php endif; ?>  <?php endif; ?>
                                                    <?php print($node->field_empresarial_curso["und"][$i]["entity"]->title); ?> 
                                                </div>
                                            </a>
                                        </li>

                                        <?php
                                    }
                                }
                                ?>
                            </ul>
                        </div>
                        <div class="tab-pane" id="empresarial_seminario">
                            <h3>Seminarios</h3> 
                            <ul class="nav list-group" data-tabs="tabs">                       
                                <?php
                                for ($i = 0;; $i++) {
                                    $ID = $node->field_empresarial_seminario["und"][$i]["entity"]->nid;

                                    if ($ID == 0 or strcmp("0", $node->status) == 0) {
                                        break;
                                    }
                                    if ($node->field_empresarial_seminario["und"][$i]["entity"]->status > 0) {
                                        $alias = drupal_get_path_alias('node/' . $ID);
                                        ?>
                                        <li class="list-group-item" >
                                            <a href="<?php print($alias); ?>">
                                                <div class=""> <?php if ($node->field_empresarial_seminario["und"][$i]["entity"]->field_nuevo): if (strcmp($node->field_empresarial_seminario["und"][$i]["entity"]->field_nuevo ["und"][0]["value"], '1') == 0): ?>
                                                            <span class="badge pull-right">Nuevo</span>    
                                                        <?php endif; ?>  <?php endif; ?>
                                                    <?php print($node->field_empresarial_seminario["und"][$i]["entity"]->title); ?> 
                                                </div>
                                            </a>
                                        </li>

                                        <?php
                                    }
                                }
                                ?>
                            </ul>
                        </div>
                        <div class="tab-pane" id="empresarial_diplomado">
                            <h3>Diplomados</h3> 
                            <ul class="nav list-group" data-tabs="tabs">                       
                                <?php
                                for ($i = 0;; $i++) {
                                    $ID = $node->field_empresarial_diplomado["und"][$i]["entity"]->nid;

                                    if ($ID == 0 or strcmp("0", $node->status) == 0) {
                                        break;
                                    }
                                    if ($node->field_empresarial_diplomado["und"][$i]["entity"]->status > 0) {
                                        $alias = drupal_get_path_alias('node/' . $ID);
                                        ?>
                                        <li class="list-group-item" >
                                            <a href="<?php print($alias); ?>">
                                                <div class=""> <?php if ($node->field_empresarial_diplomado["und"][$i]["entity"]->field_nuevo): if (strcmp($node->field_empresarial_diplomado["und"][$i]["entity"]->field_nuevo ["und"][0]["value"], '1') == 0): ?>
                                                            <span class="badge pull-right">Nuevo</span>    
                                                        <?php endif; ?>  <?php endif; ?>
                                                    <?php print($node->field_empresarial_diplomado["und"][$i]["entity"]->title); ?> 
                                                </div>
                                            </a>
                                        </li>

                                        <?php
                                    }
                                }
                                ?>
                            </ul>
                        </div>
                        <div class="tab-pane" id="empresarial_minor">
                            <h3>Minors</h3> 
                            <ul class="nav list-group" data-tabs="tabs">                       
                                <?php
                                for ($i = 0;; $i++) {
                                    $ID = $node->field_empresarial_minor["und"][$i]["entity"]->nid;

                                    if ($ID == 0 or strcmp("0", $node->status) == 0) {
                                        break;
                                    }
                                    if ($node->field_empresarial_minor["und"][$i]["entity"]->status > 0) {
                                        $alias = drupal_get_path_alias('node/' . $ID);
                                        ?>
                                        <li class="list-group-item" >
                                            <a href="<?php print($alias); ?>">
                                                <div class=""> <?php if ($node->field_empresarial_minor["und"][$i]["entity"]->field_nuevo): if (strcmp($node->field_empresarial_minor["und"][$i]["entity"]->field_nuevo ["und"][0]["value"], '1') == 0): ?>
                                                            <span class="badge pull-right">Nuevo</span>    
                                                        <?php endif; ?>  <?php endif; ?>
                                                    <?php print($node->field_empresarial_minor["und"][$i]["entity"]->title); ?> 
                                                </div>
                                            </a>
                                        </li>

                                        <?php
                                    }
                                }
                                ?>
                            </ul>
                        </div>
                        <div class="tab-pane" id="empresarial_taller">
                            <h3>Talleres</h3> 
                            <ul class="nav list-group" data-tabs="tabs">                       
                                <?php
                                for ($i = 0;; $i++) {
                                    $ID = $node->field_empresarial_taller["und"][$i]["entity"]->nid;

                                    if ($ID == 0 or strcmp("0", $node->status) == 0) {
                                        break;
                                    }
                                    if ($node->field_empresarial_taller["und"][$i]["entity"]->status > 0) {
                                        $alias = drupal_get_path_alias('node/' . $ID);
                                        ?>
                                        <li class="list-group-item" >
                                            <a href="<?php print($alias); ?>">
                                                <div class=""> <?php if ($node->field_empresarial_taller["und"][$i]["entity"]->field_nuevo): if (strcmp($node->field_empresarial_taller["und"][$i]["entity"]->field_nuevo ["und"][0]["value"], '1') == 0): ?>
                                                            <span class="badge pull-right">Nuevo</span>    
                                                        <?php endif; ?>  <?php endif; ?>
                                                    <?php print($node->field_empresarial_taller["und"][$i]["entity"]->title); ?> 
                                                </div>
                                            </a>
                                        </li>

                                        <?php
                                    }
                                }
                                ?>
                            </ul>
                        </div>
                        <div class="tab-pane" id="empresarial_charla">
                            <h3>Charlas</h3> 
                            <ul class="nav list-group" data-tabs="tabs">                       
                                <?php
                                for ($i = 0;; $i++) {
                                    $ID = $node->field_empresarial_charla["und"][$i]["entity"]->nid;

                                    if ($ID == 0 or strcmp("0", $node->status) == 0) {
                                        break;
                                    }
                                    if ($node->field_empresarial_charla["und"][$i]["entity"]->status > 0) {
                                        $alias = drupal_get_path_alias('node/' . $ID);
                                        ?>
                                        <li class="list-group-item" >
                                            <a href="<?php print($alias); ?>">
                                                <div class=""> <?php if ($node->field_empresarial_charla["und"][$i]["entity"]->field_nuevo): if (strcmp($node->field_empresarial_charla["und"][$i]["entity"]->field_nuevo ["und"][0]["value"], '1') == 0): ?>
                                                            <span class="badge pull-right">Nuevo</span>    
                                                        <?php endif; ?>  <?php endif; ?>
                                                    <?php print($node->field_empresarial_charla["und"][$i]["entity"]->title); ?> 
                                                </div>
                                            </a>
                                        </li>

                                        <?php
                                    }
                                }
                                ?>
                            </ul>
                        </div>
                        <div class="tab-pane" id="empresarial_conferencia">
                            <h3>Conferencias</h3> 
                            <ul class="nav list-group" data-tabs="tabs">                       
                                <?php
                                for ($i = 0;; $i++) {
                                    $ID = $node->field_empresarial_conferencia["und"][$i]["entity"]->nid;

                                    if ($ID == 0 or strcmp("0", $node->status) == 0) {
                                        break;
                                    }
                                    if ($node->field_empresarial_conferencia["und"][$i]["entity"]->status > 0) {
                                        $alias = drupal_get_path_alias('node/' . $ID);
                                        ?>
                                        <li class="list-group-item" >
                                            <a href="<?php print($alias); ?>">
                                                <div class=""> <?php if ($node->field_empresarial_conferencia["und"][$i]["entity"]->field_nuevo): if (strcmp($node->field_empresarial_conferencia["und"][$i]["entity"]->field_nuevo ["und"][0]["value"], '1') == 0): ?>
                                                            <span class="badge pull-right">Nuevo</span>    
                                                        <?php endif; ?>  <?php endif; ?>
                                                    <?php print($node->field_empresarial_conferencia["und"][$i]["entity"]->title); ?> 
                                                </div>
                                            </a>
                                        </li>

                                        <?php
                                    }
                                }
                                ?>
                            </ul>
                        </div>
                        <div class="tab-pane" id="empresarial_homologable">
                            <h3>Homologables</h3> 
                            <ul class="nav list-group" data-tabs="tabs">                       
                                <?php
                                for ($i = 0;; $i++) {
                                    $ID = $node->field_empresarial_homologable["und"][$i]["entity"]->nid;

                                    if ($ID == 0 or strcmp("0", $node->status) == 0) {
                                        break;
                                    }
                                    if ($node->field_empresarial_homologable["und"][$i]["entity"]->status > 0) {
                                        $alias = drupal_get_path_alias('node/' . $ID);
                                        ?>
                                        <li class="list-group-item" >
                                            <a href="<?php print($alias); ?>">
                                                <div class=""> <?php if ($node->field_empresarial_homologable["und"][$i]["entity"]->field_nuevo): if (strcmp($node->field_empresarial_homologable["und"][$i]["entity"]->field_nuevo ["und"][0]["value"], '1') == 0): ?>
                                                            <span class="badge pull-right">Nuevo</span>    
                                                        <?php endif; ?>  <?php endif; ?>
                                                    <?php print($node->field_empresarial_homologable["und"][$i]["entity"]->title); ?> 
                                                </div>
                                            </a>
                                        </li>

                                        <?php
                                    }
                                }
                                ?>
                            </ul>
                        </div>


                    </div>
                </div>

            </div>
        <?php } ?>
        <?php if (($node->field_ninos_curso) || ($node->field_ninos_seminario) || ($node->field_ninos_diplomado) || ($node->field_ninos_minor) || ($node->field_ninos_taller) || ($node->field_ninos_charla) || ($node->field_ninos_conferencia) || ($node->field_ninos_homologable)) { ?>
            <div class="col-md-12 column thumbnail">
                <h3>Cursos para Niños</h3>

                <div class="col-md-2">

                    <ul class="nav nav-pills nav-stacked" id="Programas">
                        <?php if ($node->field_ninos_curso["und"][0]["entity"]) { ?> 
                            <li class="active"><a href="#ninos_curso"  data-toggle="tab">Cursos</a></li><?php } ?> 
                        <?php if ($node->field_ninos_seminario["und"][0]["entity"]) { ?> 
                            <li><a href="#ninos_seminario"  data-toggle="tab">Seminarios</a></li><?php } ?> 
                        <?php if ($node->field_ninos_diplomado["und"][0]["entity"]) { ?> 
                            <li><a href="#ninos_diplomado"  data-toggle="tab">Diplomados</a></li><?php } ?> 
                        <?php if ($node->field_ninos_minor["und"][0]["entity"]) { ?> 
                            <li><a href="#ninos_minor"  data-toggle="tab">Minors</a></li><?php } ?> 
                        <?php if ($node->field_ninos_taller["und"][0]["entity"]) { ?> 
                            <li><a href="#ninos_taller"  data-toggle="tab">Talleres</a></li><?php } ?> 
                        <?php if ($node->field_ninos_charla["und"][0]["entity"]) { ?> 
                            <li><a href="#ninos_charla"  data-toggle="tab">Charlas</a></li><?php } ?> 
                        <?php if ($node->field_ninos_conferencia["und"][0]["entity"]) { ?> 
                            <li><a href="#ninos_conferencia"  data-toggle="tab">Conferencias</a></li><?php } ?> 
                        <?php if ($node->field_ninos_homologable["und"][0]["entity"]) { ?> 
                            <li><a href="#ninos_homologable"  data-toggle="tab">Homologables</a></li><?php } ?>      
                    </ul> 

                </div>
                <div class="col-md-10">
                    <div class="tab-content">
                        <div class="tab-pane active" id="ninos_curso">
                            <h3>Cursos</h3> 
                            <ul class="nav list-group" data-tabs="tabs">                       
                                <?php
                                for ($i = 0;; $i++) {
                                    $ID = $node->field_ninos_curso["und"][$i]["entity"]->nid;

                                    if ($ID == 0 or strcmp("0", $node->status) == 0) {
                                        break;
                                    }
                                    if ($node->field_ninos_curso["und"][$i]["entity"]->status > 0) {
                                        $alias = drupal_get_path_alias('node/' . $ID);
                                        ?>
                                        <li class="list-group-item" >
                                            <a href="<?php print($alias); ?>">
                                                <div class=""> <?php if ($node->field_ninos_curso["und"][$i]["entity"]->field_nuevo): if (strcmp($node->field_ninos_curso["und"][$i]["entity"]->field_nuevo ["und"][0]["value"], '1') == 0): ?>
                                                            <span class="badge pull-right">Nuevo</span>    
                                                        <?php endif; ?>  <?php endif; ?>
                                                    <?php print($node->field_ninos_curso["und"][$i]["entity"]->title); ?> 
                                                </div>
                                            </a>
                                        </li>

                                        <?php
                                    }
                                }
                                ?>
                            </ul>
                        </div>
                        <div class="tab-pane" id="ninos_seminario">
                            <h3>Seminarios</h3> 
                            <ul class="nav list-group" data-tabs="tabs">                       
                                <?php
                                for ($i = 0;; $i++) {
                                    $ID = $node->field_ninos_seminario["und"][$i]["entity"]->nid;

                                    if ($ID == 0 or strcmp("0", $node->status) == 0) {
                                        break;
                                    }
                                    if ($node->field_ninos_seminario["und"][$i]["entity"]->status > 0) {
                                        $alias = drupal_get_path_alias('node/' . $ID);
                                        ?>
                                        <li class="list-group-item" >
                                            <a href="<?php print($alias); ?>">
                                                <div class=""> <?php if ($node->field_ninos_seminario["und"][$i]["entity"]->field_nuevo): if (strcmp($node->field_ninos_seminario["und"][$i]["entity"]->field_nuevo ["und"][0]["value"], '1') == 0): ?>
                                                            <span class="badge pull-right">Nuevo</span>    
                                                        <?php endif; ?>  <?php endif; ?>
                                                    <?php print($node->field_ninos_seminario["und"][$i]["entity"]->title); ?> 
                                                </div>
                                            </a>
                                        </li>

                                        <?php
                                    }
                                }
                                ?>
                            </ul>
                        </div>
                        <div class="tab-pane" id="ninos_diplomado">
                            <h3>Diplomados</h3> 
                            <ul class="nav list-group" data-tabs="tabs">                       
                                <?php
                                for ($i = 0;; $i++) {
                                    $ID = $node->field_ninos_diplomado["und"][$i]["entity"]->nid;

                                    if ($ID == 0 or strcmp("0", $node->status) == 0) {
                                        break;
                                    }
                                    if ($node->field_ninos_diplomado["und"][$i]["entity"]->status > 0) {
                                        $alias = drupal_get_path_alias('node/' . $ID);
                                        ?>
                                        <li class="list-group-item" >
                                            <a href="<?php print($alias); ?>">
                                                <div class=""> <?php if ($node->field_ninos_diplomado["und"][$i]["entity"]->field_nuevo): if (strcmp($node->field_ninos_diplomado["und"][$i]["entity"]->field_nuevo ["und"][0]["value"], '1') == 0): ?>
                                                            <span class="badge pull-right">Nuevo</span>    
                                                        <?php endif; ?>  <?php endif; ?>
                                                    <?php print($node->field_ninos_diplomado["und"][$i]["entity"]->title); ?> 
                                                </div>
                                            </a>
                                        </li>

                                        <?php
                                    }
                                }
                                ?>
                            </ul>
                        </div>
                        <div class="tab-pane" id="ninos_minor">
                            <h3>Minors</h3> 
                            <ul class="nav list-group" data-tabs="tabs">                       
                                <?php
                                for ($i = 0;; $i++) {
                                    $ID = $node->field_ninos_minor["und"][$i]["entity"]->nid;

                                    if ($ID == 0 or strcmp("0", $node->status) == 0) {
                                        break;
                                    }
                                    if ($node->field_ninos_minor["und"][$i]["entity"]->status > 0) {
                                        $alias = drupal_get_path_alias('node/' . $ID);
                                        ?>
                                        <li class="list-group-item" >
                                            <a href="<?php print($alias); ?>">
                                                <div class=""> <?php if ($node->field_ninos_minor["und"][$i]["entity"]->field_nuevo): if (strcmp($node->field_ninos_minor["und"][$i]["entity"]->field_nuevo ["und"][0]["value"], '1') == 0): ?>
                                                            <span class="badge pull-right">Nuevo</span>    
                                                        <?php endif; ?>  <?php endif; ?>
                                                    <?php print($node->field_ninos_minor["und"][$i]["entity"]->title); ?> 
                                                </div>
                                            </a>
                                        </li>

                                        <?php
                                    }
                                }
                                ?>
                            </ul>
                        </div>
                        <div class="tab-pane" id="ninos_taller">
                            <h3>Talleres</h3> 
                            <ul class="nav list-group" data-tabs="tabs">                       
                                <?php
                                for ($i = 0;; $i++) {
                                    $ID = $node->field_ninos_taller["und"][$i]["entity"]->nid;

                                    if ($ID == 0 or strcmp("0", $node->status) == 0) {
                                        break;
                                    }
                                    if ($node->field_ninos_taller["und"][$i]["entity"]->status > 0) {
                                        $alias = drupal_get_path_alias('node/' . $ID);
                                        ?>
                                        <li class="list-group-item" >
                                            <a href="<?php print($alias); ?>">
                                                <div class=""> <?php if ($node->field_ninos_taller["und"][$i]["entity"]->field_nuevo): if (strcmp($node->field_ninos_taller["und"][$i]["entity"]->field_nuevo ["und"][0]["value"], '1') == 0): ?>
                                                            <span class="badge pull-right">Nuevo</span>    
                                                        <?php endif; ?>  <?php endif; ?>
                                                    <?php print($node->field_ninos_taller["und"][$i]["entity"]->title); ?> 
                                                </div>
                                            </a>
                                        </li>

                                        <?php
                                    }
                                }
                                ?>
                            </ul>
                        </div>
                        <div class="tab-pane" id="ninos_charla">
                            <h3>Charlas</h3> 
                            <ul class="nav list-group" data-tabs="tabs">                       
                                <?php
                                for ($i = 0;; $i++) {
                                    $ID = $node->field_ninos_charla["und"][$i]["entity"]->nid;

                                    if ($ID == 0 or strcmp("0", $node->status) == 0) {
                                        break;
                                    }
                                    if ($node->field_ninos_charla["und"][$i]["entity"]->status > 0) {
                                        $alias = drupal_get_path_alias('node/' . $ID);
                                        ?>
                                        <li class="list-group-item" >
                                            <a href="<?php print($alias); ?>">
                                                <div class=""> <?php if ($node->field_ninos_charla["und"][$i]["entity"]->field_nuevo): if (strcmp($node->field_ninos_charla["und"][$i]["entity"]->field_nuevo ["und"][0]["value"], '1') == 0): ?>
                                                            <span class="badge pull-right">Nuevo</span>    
                                                        <?php endif; ?>  <?php endif; ?>
                                                    <?php print($node->field_ninos_charla["und"][$i]["entity"]->title); ?> 
                                                </div>
                                            </a>
                                        </li>

                                        <?php
                                    }
                                }
                                ?>
                            </ul>
                        </div>
                        <div class="tab-pane" id="ninos_conferencia">
                            <h3>Conferencias</h3> 
                            <ul class="nav list-group" data-tabs="tabs">                       
                                <?php
                                for ($i = 0;; $i++) {
                                    $ID = $node->field_ninos_conferencia["und"][$i]["entity"]->nid;

                                    if ($ID == 0 or strcmp("0", $node->status) == 0) {
                                        break;
                                    }
                                    if ($node->field_ninos_conferencia["und"][$i]["entity"]->status > 0) {
                                        $alias = drupal_get_path_alias('node/' . $ID);
                                        ?>
                                        <li class="list-group-item" >
                                            <a href="<?php print($alias); ?>">
                                                <div class=""> <?php if ($node->field_ninos_conferencia["und"][$i]["entity"]->field_nuevo): if (strcmp($node->field_ninos_conferencia["und"][$i]["entity"]->field_nuevo ["und"][0]["value"], '1') == 0): ?>
                                                            <span class="badge pull-right">Nuevo</span>    
                                                        <?php endif; ?>  <?php endif; ?>
                                                    <?php print($node->field_ninos_conferencia["und"][$i]["entity"]->title); ?> 
                                                </div>
                                            </a>
                                        </li>

                                        <?php
                                    }
                                }
                                ?>
                            </ul>
                        </div>
                        <div class="tab-pane" id="ninos_homologable">
                            <h3>Homologables</h3> 
                            <ul class="nav list-group" data-tabs="tabs">                       
                                <?php
                                for ($i = 0;; $i++) {
                                    $ID = $node->field_ninos_homologable["und"][$i]["entity"]->nid;

                                    if ($ID == 0 or strcmp("0", $node->status) == 0) {
                                        break;
                                    }
                                    if ($node->field_ninos_homologable["und"][$i]["entity"]->status > 0) {
                                        $alias = drupal_get_path_alias('node/' . $ID);
                                        ?>
                                        <li class="list-group-item" >
                                            <a href="<?php print($alias); ?>">
                                                <div class=""> <?php if ($node->field_ninos_homologable["und"][$i]["entity"]->field_nuevo): if (strcmp($node->field_ninos_homologable["und"][$i]["entity"]->field_nuevo ["und"][0]["value"], '1') == 0): ?>
                                                            <span class="badge pull-right">Nuevo</span>    
                                                        <?php endif; ?>  <?php endif; ?>
                                                    <?php print($node->field_ninos_homologable["und"][$i]["entity"]->title); ?> 
                                                </div>
                                            </a>
                                        </li>

                                        <?php
                                    }
                                }
                                ?>
                            </ul>
                        </div>


                    </div>
                </div>

            </div>
        <?php } ?>
        <?php if (($node->field_academia_curso) || ($node->field_academia_seminario) || ($node->field_academia_diplomado) || ($node->field_academia_minor) || ($node->field_academia_taller) || ($node->field_academia_charla) || ($node->field_academia_conferencia) || ($node->field_academia_homologable)) { ?>
            <div class="col-md-12 column thumbnail">
                <h3>Cursos de la Academia</h3>

                <div class="col-md-2">

                    <ul class="nav nav-pills nav-stacked" id="Programas">
                        <?php if ($node->field_academia_curso["und"][0]["entity"]) { ?> 
                            <li class="active"><a href="#academia_curso"  data-toggle="tab">Cursos</a></li><?php } ?> 
                        <?php if ($node->field_academia_seminario["und"][0]["entity"]) { ?> 
                            <li><a href="#academia_seminario"  data-toggle="tab">Seminarios</a></li><?php } ?> 
                        <?php if ($node->field_academia_diplomado["und"][0]["entity"]) { ?> 
                            <li><a href="#academia_diplomado"  data-toggle="tab">Diplomados</a></li><?php } ?> 
                        <?php if ($node->field_academia_minor["und"][0]["entity"]) { ?> 
                            <li><a href="#academia_minor"  data-toggle="tab">Minors</a></li><?php } ?> 
                        <?php if ($node->field_academia_taller["und"][0]["entity"]) { ?> 
                            <li><a href="#academia_taller"  data-toggle="tab">Talleres</a></li><?php } ?> 
                        <?php if ($node->field_academia_charla["und"][0]["entity"]) { ?> 
                            <li><a href="#academia_charla"  data-toggle="tab">Charlas</a></li><?php } ?> 
                        <?php if ($node->field_academia_conferencia["und"][0]["entity"]) { ?> 
                            <li><a href="#academia_conferencia"  data-toggle="tab">Conferencias</a></li><?php } ?> 
                        <?php if ($node->field_academia_homologable["und"][0]["entity"]) { ?> 
                            <li><a href="#academia_homologable"  data-toggle="tab">Homologables</a></li><?php } ?> 
                    </ul> 

                </div>
                <div class="col-md-10">
                    <div class="tab-content">
                        <div class="tab-pane active" id="academia_curso">
                            <h3>Cursos</h3> 
                            <ul class="nav list-group" data-tabs="tabs">                       
                                <?php
                                for ($i = 0;; $i++) {
                                    $ID = $node->field_academia_curso["und"][$i]["entity"]->nid;

                                    if ($ID == 0 or strcmp("0", $node->status) == 0) {
                                        break;
                                    }
                                    if ($node->field_academia_curso["und"][$i]["entity"]->status > 0) {
                                        $alias = drupal_get_path_alias('node/' . $ID);
                                        ?>
                                        <li class="list-group-item" >
                                            <a href="<?php print($alias); ?>">
                                                <div class=""> <?php if ($node->field_academia_curso["und"][$i]["entity"]->field_nuevo): if (strcmp($node->field_academia_curso["und"][$i]["entity"]->field_nuevo ["und"][0]["value"], '1') == 0): ?>
                                                            <span class="badge pull-right">Nuevo</span>    
                                                        <?php endif; ?>  <?php endif; ?>
                                                    <?php print($node->field_academia_curso["und"][$i]["entity"]->title); ?> 
                                                </div>
                                            </a>
                                        </li>

                                        <?php
                                    }
                                }
                                ?>
                            </ul>
                        </div>
                        <div class="tab-pane" id="academia_seminario">
                            <h3>Seminarios</h3> 
                            <ul class="nav list-group" data-tabs="tabs">                       
                                <?php
                                for ($i = 0;; $i++) {
                                    $ID = $node->field_academia_seminario["und"][$i]["entity"]->nid;

                                    if ($ID == 0 or strcmp("0", $node->status) == 0) {
                                        break;
                                    }
                                    if ($node->field_academia_seminario["und"][$i]["entity"]->status > 0) {
                                        $alias = drupal_get_path_alias('node/' . $ID);
                                        ?>
                                        <li class="list-group-item" >
                                            <a href="<?php print($alias); ?>">
                                                <div class=""> <?php if ($node->field_academia_seminario["und"][$i]["entity"]->field_nuevo): if (strcmp($node->field_academia_seminario["und"][$i]["entity"]->field_nuevo ["und"][0]["value"], '1') == 0): ?>
                                                            <span class="badge pull-right">Nuevo</span>    
                                                        <?php endif; ?>  <?php endif; ?>
                                                    <?php print($node->field_academia_seminario["und"][$i]["entity"]->title); ?> 
                                                </div>
                                            </a>
                                        </li>

                                        <?php
                                    }
                                }
                                ?>
                            </ul>
                        </div>
                        <div class="tab-pane" id="academia_diplomado">
                            <h3>Diplomados</h3> 
                            <ul class="nav list-group" data-tabs="tabs">                       
                                <?php
                                for ($i = 0;; $i++) {
                                    $ID = $node->field_academia_diplomado["und"][$i]["entity"]->nid;

                                    if ($ID == 0 or strcmp("0", $node->status) == 0) {
                                        break;
                                    }
                                    if ($node->field_academia_diplomado["und"][$i]["entity"]->status > 0) {
                                        $alias = drupal_get_path_alias('node/' . $ID);
                                        ?>
                                        <li class="list-group-item" >
                                            <a href="<?php print($alias); ?>">
                                                <div class=""> <?php if ($node->field_academia_diplomado["und"][$i]["entity"]->field_nuevo): if (strcmp($node->field_academia_diplomado["und"][$i]["entity"]->field_nuevo ["und"][0]["value"], '1') == 0): ?>
                                                            <span class="badge pull-right">Nuevo</span>    
                                                        <?php endif; ?>  <?php endif; ?>
                                                    <?php print($node->field_academia_diplomado["und"][$i]["entity"]->title); ?> 
                                                </div>
                                            </a>
                                        </li>

                                        <?php
                                    }
                                }
                                ?>
                            </ul>
                        </div>
                        <div class="tab-pane" id="academia_minor">
                            <h3>Minors</h3> 
                            <ul class="nav list-group" data-tabs="tabs">                       
                                <?php
                                for ($i = 0;; $i++) {
                                    $ID = $node->field_academia_minor["und"][$i]["entity"]->nid;

                                    if ($ID == 0 or strcmp("0", $node->status) == 0) {
                                        break;
                                    }
                                    if ($node->field_academia_minor["und"][$i]["entity"]->status > 0) {
                                        $alias = drupal_get_path_alias('node/' . $ID);
                                        ?>
                                        <li class="list-group-item" >
                                            <a href="<?php print($alias); ?>">
                                                <div class=""> <?php if ($node->field_academia_minor["und"][$i]["entity"]->field_nuevo): if (strcmp($node->field_academia_minor["und"][$i]["entity"]->field_nuevo ["und"][0]["value"], '1') == 0): ?>
                                                            <span class="badge pull-right">Nuevo</span>    
                                                        <?php endif; ?>  <?php endif; ?>
                                                    <?php print($node->field_academia_minor["und"][$i]["entity"]->title); ?> 
                                                </div>
                                            </a>
                                        </li>

                                        <?php
                                    }
                                }
                                ?>
                            </ul>
                        </div>
                        <div class="tab-pane" id="academia_taller">
                            <h3>Talleres</h3> 
                            <ul class="nav list-group" data-tabs="tabs">                       
                                <?php
                                for ($i = 0;; $i++) {
                                    $ID = $node->field_academia_taller["und"][$i]["entity"]->nid;

                                    if ($ID == 0 or strcmp("0", $node->status) == 0) {
                                        break;
                                    }
                                    if ($node->field_academia_taller["und"][$i]["entity"]->status > 0) {
                                        $alias = drupal_get_path_alias('node/' . $ID);
                                        ?>
                                        <li class="list-group-item" >
                                            <a href="<?php print($alias); ?>">
                                                <div class=""> <?php if ($node->field_academia_taller["und"][$i]["entity"]->field_nuevo): if (strcmp($node->field_academia_taller["und"][$i]["entity"]->field_nuevo ["und"][0]["value"], '1') == 0): ?>
                                                            <span class="badge pull-right">Nuevo</span>    
                                                        <?php endif; ?>  <?php endif; ?>
                                                    <?php print($node->field_academia_taller["und"][$i]["entity"]->title); ?> 
                                                </div>
                                            </a>
                                        </li>

                                        <?php
                                    }
                                }
                                ?>
                            </ul>
                        </div>
                        <div class="tab-pane" id="academia_charla">
                            <h3>Charlas</h3> 
                            <ul class="nav list-group" data-tabs="tabs">                       
                                <?php
                                for ($i = 0;; $i++) {
                                    $ID = $node->field_academia_charla["und"][$i]["entity"]->nid;

                                    if ($ID == 0 or strcmp("0", $node->status) == 0) {
                                        break;
                                    }
                                    if ($node->field_academia_charla["und"][$i]["entity"]->status > 0) {
                                        $alias = drupal_get_path_alias('node/' . $ID);
                                        ?>
                                        <li class="list-group-item" >
                                            <a href="<?php print($alias); ?>">
                                                <div class=""> <?php if ($node->field_academia_charla["und"][$i]["entity"]->field_nuevo): if (strcmp($node->field_academia_charla["und"][$i]["entity"]->field_nuevo ["und"][0]["value"], '1') == 0): ?>
                                                            <span class="badge pull-right">Nuevo</span>    
                                                        <?php endif; ?>  <?php endif; ?>
                                                    <?php print($node->field_academia_charla["und"][$i]["entity"]->title); ?> 
                                                </div>
                                            </a>
                                        </li>

                                        <?php
                                    }
                                }
                                ?>
                            </ul>
                        </div>
                        <div class="tab-pane" id="academia_conferencia">
                            <h3>Conferencias</h3> 
                            <ul class="nav list-group" data-tabs="tabs">                       
                                <?php
                                for ($i = 0;; $i++) {
                                    $ID = $node->field_academia_conferencia["und"][$i]["entity"]->nid;

                                    if ($ID == 0 or strcmp("0", $node->status) == 0) {
                                        break;
                                    }
                                    if ($node->field_academia_conferencia["und"][$i]["entity"]->status > 0) {
                                        $alias = drupal_get_path_alias('node/' . $ID);
                                        ?>
                                        <li class="list-group-item" >
                                            <a href="<?php print($alias); ?>">
                                                <div class=""> <?php if ($node->field_academia_conferencia["und"][$i]["entity"]->field_nuevo): if (strcmp($node->field_academia_conferencia["und"][$i]["entity"]->field_nuevo ["und"][0]["value"], '1') == 0): ?>
                                                            <span class="badge pull-right">Nuevo</span>    
                                                        <?php endif; ?>  <?php endif; ?>
                                                    <?php print($node->field_academia_conferencia["und"][$i]["entity"]->title); ?> 
                                                </div>
                                            </a>
                                        </li>

                                        <?php
                                    }
                                }
                                ?>
                            </ul>
                        </div>
                        <div class="tab-pane" id="academia_homologable">
                            <h3>Homologables</h3> 
                            <ul class="nav list-group" data-tabs="tabs">                       
                                <?php
                                for ($i = 0;; $i++) {
                                    $ID = $node->field_academia_homologable["und"][$i]["entity"]->nid;

                                    if ($ID == 0 or strcmp("0", $node->status) == 0) {
                                        break;
                                    }
                                    if ($node->field_academia_homologable["und"][$i]["entity"]->status > 0) {
                                        $alias = drupal_get_path_alias('node/' . $ID);
                                        ?>
                                        <li class="list-group-item" >
                                            <a href="<?php print($alias); ?>">
                                                <div class=""> <?php if ($node->field_academia_homologable["und"][$i]["entity"]->field_nuevo): if (strcmp($node->field_academia_homologable["und"][$i]["entity"]->field_nuevo ["und"][0]["value"], '1') == 0): ?>
                                                            <span class="badge pull-right">Nuevo</span>    
                                                        <?php endif; ?>  <?php endif; ?>
                                                    <?php print($node->field_academia_homologable["und"][$i]["entity"]->title); ?> 
                                                </div>
                                            </a>
                                        </li>

                                        <?php
                                    }
                                }
                                ?>
                            </ul>
                        </div>


                    </div>
                </div>

            </div>
        <?php } ?>

        <?php if ($node->field_area["und"][0]["entity"]) { ?>

            <div class="col-md-12">
                <h3>Areas</h3> 
                <div class="col-md-2">
                    <ul class="nav nav-pills nav-stacked" id="Areas">                   
                        <?php
                        $i = 0;
                        for ($i = 0;; $i++) {
                            $ID = $node->field_area["und"][$i]["entity"]->nid;
                            if ($ID == 0 or strcmp("0", $node->status) == 0) {
                                break;
                            }
                            if ($node->field_area["und"][$i]["entity"]->status > 0) {
                                ?>
                                <li <?php
                                if ($i == 0) {
                                    print "class='active'";
                                };
                                ?>><a href="#<?php print $node->field_area["und"][$i]["entity"]->nid ?>"  data-toggle="tab"><?php print $node->field_area["und"][$i]["entity"]->title ?></a></li>
                                    <?php
                                }
                            }
                            ?>
                    </ul>
                </div>

                <div class="col-md-10">
                    <div class="tab-content">
                        <?php
                        for ($i = 0;; $i++) {
                            $ID = $node->field_area["und"][$i]["entity"]->nid;
                            if ($ID == 0 or strcmp("0", $node->status) == 0) {
                                break;
                            }
                            ?>
                            <div class="tab-pane<?php
                         if ($i == 0) {
                             print " active";
                         };
                            ?>" id="<?php print $ID; ?>">

                                <?php
                                for ($t = 0;; $t++) {
                                    $temp = $node->field_area["und"][$i]["entity"]->field_cursos_resumidos["und"][$t]["safe_value"];
                                    if (!isset($temp)) {
                                        break;
                                    }
                                    print $node->field_area["und"][$i]["entity"]->field_cursos_resumidos["und"][$t]["safe_value"] . "<br>";
                                }
                                ?>

                            </div>  
    <?php } ?>
                    </div>
                </div>
<?php } ?>



        </div>
    </div>

</div>
