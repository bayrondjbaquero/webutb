<div class="panel panel-brand2">

    <div class="panel-heading">

        <a class="panel-title" data-toggle="collapse" data-parent="#panel-ver-extension" href="#Cursos">
            Clic para ver Programas de Centro de Educación Permanente<span class="caret"></span>
        </a>
        <i class="fa fa-book fa-5x pull-right"></i>

        <div class="padding-x">
            Realizando uno de nuestros cursos, diplomados o seminarios, podrás actualizar tus conocimientos y prepárate para competir en el mercado laboral.
            Diseñamos programas empresariales a la medida de las necesidades de su organización.    
        </div>

    </div>

    <div id="Cursos" class="panel-collapse collapse bg-brand2">

        <div class="panel-body">	

            <div class="col-lg-12 thumbnail text-muted padding-x">

                <div class="panel-group" id="panel-cur">

                    <?php foreach ($rows as $id => $row): ?>

                        <?php print $row; ?>

                    <?php endforeach; ?>

                </div>				

            </div>	

        </div>

    </div>

</div>	