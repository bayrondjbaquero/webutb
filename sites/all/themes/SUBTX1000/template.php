<?php

function SUBTX1000_breadcrumb($breadcrumb) {
    $home = variable_get('site_name', 'drupal');
    $sep = ' &raquo; ';
    if (count($breadcrumb) > 0) {
        $breadcrumb[0] = l(t($home), '');
        return implode($sep, $breadcrumb) . $sep;
    } elseif (drupal_get_title() !== '') {
        return l(t($home), '') . $sep;
    } else {
        return t($home);
    }
}

function SUBTX1000_menu_link(array $variables) {
    $element = $variables['element'];
    $sub_menu = '';

    if ($element['#below']) {
        // Ad our own wrapper
        unset($element['#below']['#theme_wrappers']);
        $sub_menu = '<ul class="dropdown-menu">' . drupal_render($element['#below']) . '</ul>'; // removed flyout class in ul
        unset($element['#localized_options']['attributes']['class']); // removed flydown class
        unset($element['#localized_options']['attributes']['data-toggle']); // removed data toggler
        // Check if this element is nested within another
        if ((!empty($element['#original_link']['depth'])) && ($element['#original_link']['depth'] > 1)) {

            unset($element['#attributes']['class']); // removed flyout class
        } else {
            unset($element['#attributes']['class']); // unset flyout class
            $element['#localized_options']['html'] = TRUE;
            $element['#title'] .= ''; // removed carat spans flyout
        }
        $element['#localized_options']['attributes']['class'][] = 'dropdown-toggle';
        $element['#localized_options']['attributes']['data-toggle'] = 'dropdown';
        // Set dropdown trigger element to # to prevent inadvertent page loading with submenu click
        $element['#localized_options']['attributes']['data-target'] = '#'; // You could unset this too as its no longer necessary.
    }

    $output = l($element['#title'], $element['#href'], $element['#localized_options']);


    if (isset($element['#localized_options']['html'])) {
        return '<li class="dropdown">' . $output . $sub_menu . "</li>\n";
    } else {
        if (isset($element['#localized_options']['attributes']["data-target"])) {
            return '<li class="dropdown dropdown-submenu">' . $output . $sub_menu . "</li>\n";
        } else {
            return '<li>' . $output . $sub_menu . "</li>\n";
        }
    }
}

function SUBTX1000_aggregator_block_item($variables) {
    // Display the external link to the item.
    return '<a class="list-group-item" href="' . check_url($variables['item']->link) . '">' . check_plain($variables['item']->title) . "</a>\n";
}

function SUBTX1000_menu_tree(&$variables) {
    return '<div class="collapse navbar-collapse " id="principal"><ul  class="nav navbar-nav">' . $variables['tree'] . '</ul></div>'; // added the nav-collapse wrapper so you can hide the nav at small size
}

//function bootstrap_menu_tree__menu_topmenu(&$variables) {
//    return '<div class="navbar-right collapse navbar-collapse" id="auxiliar"><ul  class="nav navbar-nav">' . $variables['tree'] . '</ul></div>'; // added the nav-collapse wrapper so you can hide the nav at small size
//}

//function bootstrap_menu_tree__menu_menu_politicas(&$variables) {
//    return '<div class="collapse navbar-collapse navbar-right" id="politicas"><ul  class="nav navbar-nav"><a data-toggle="modal" href="#myModal" class="btn btn-primary">Reportar Error</a>' . $variables['tree'] . '</ul></div>'; // added the nav-collapse wrapper so you can hide the nav at small size
//}

function SUBTX1000_preprocess_page(&$vars) {
  $vars['show_title'] = TRUE; // so that showtitle is true by default
  if (isset($vars['node'])) {
    if ($vars['node']->type == 'staffs') { // set to false when the content type is event
      $vars['show_title'] = FALSE;
    }
    if ($vars['node']->type == 'proyecto') { // set to false when the content type is proyecto
      $vars['show_title'] = FALSE;
    }
  }

  /* check current multisite */
  $conf_path_array = explode('/', conf_path());
  $multisite_name = array_pop($conf_path_array);
  switch ($multisite_name) {
    case 'emprendimiento.unitecnologica.edu.co':
        //print ($multisite);
        drupal_add_css(drupal_get_path('theme', 'SUBTX1000') . '/css/materialize.min.css');
        drupal_add_css("https://fonts.googleapis.com/icon?family=Material+Icons", 'external');
        drupal_add_css(drupal_get_path('theme', 'SUBTX1000') . '/css/fix_material_conflict.css');
        drupal_add_css(drupal_get_path('theme', 'SUBTX1000') . '/css/elpatio.css');
        //see: https://api.drupal.org/api/drupal/includes%21common.inc/function/drupal_add_js/7
        //drupal_add_js(drupal_get_path('theme', 'SUBTX1000') . '/js/materialize.min.js', 'file');
        drupal_add_js(drupal_get_path('theme', 'SUBTX1000') . '/js/materialize.js', 'file');
        drupal_add_js(drupal_get_path('theme', 'SUBTX1000') . '/js/velocity.min.js', 'file');
        drupal_add_js(drupal_get_path('theme', 'SUBTX1000') . '/js/velocity.ui.min.js', 'file');
        drupal_add_js(drupal_get_path('theme', 'SUBTX1000') . '/js/elpatio.js', 'file');
    break;
  }
  /* end check current multisite */
}

/**
 * Implements hook_element_info_alter().
 */
function SUBTX1000_element_info_alter(&$elements) {
  foreach ($elements as &$element) {
    // Process all elements.
    $element['#process'][] = '_bootstrap_process_element';
    // Process input elements.
    if (!empty($element['#input'])) {
      $element['#process'][] = '_custom_bootstrap_process_input'; // we provide our own custom function
    }
    // Process core's fieldset element.
    if (!empty($element['#type']) && $element['#type'] === 'fieldset') {
      $element['#theme_wrappers'] = array('bootstrap_panel');
    }
    if (!empty($element['#theme']) && $element['#theme'] === 'fieldset') {
      $element['#theme'] = 'bootstrap_panel';
    }
    // Replace #process function.
    if (!empty($element['#process']) && ($key = array_search('form_process_fieldset', $element['#process'])) !== FALSE) {
      $element['#process'][$key] = '_bootstrap_process_fieldset';
    }
    // Replace #pre_render function.
    if (!empty($element['#pre_render']) && ($key = array_search('form_pre_render_fieldset', $element['#pre_render'])) !== FALSE) {
      $element['#pre_render'][$key] = '_bootstrap_pre_render_fieldset';
    }
    // Replace #theme_wrappers function.
    if (!empty($element['#theme_wrappers']) && ($key = array_search('fieldset', $element['#theme_wrappers'])) !== FALSE) {
      $element['#theme_wrappers'][$key] = 'bootstrap_panel';
    }
  }
}



/**
 * Process input elements.
 */
function _custom_bootstrap_process_input(&$element, &$form_state) {
  // Only add the "form-control" class for specific element input types.
  $types = array(
    // Core.
    'select',
    'webform_email',  // added email fields 

  );
  if (!empty($element['#type']) && (in_array($element['#type'], $types) || ($element['#type'] === 'file' && empty($element['#managed_file'])))) {
    $element['#attributes']['class'][] = 'form-control';
  }
  return $element;
}
