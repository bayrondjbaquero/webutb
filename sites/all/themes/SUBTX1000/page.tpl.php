<div id="menuauxiliar">
    <a name="start"></a> <section>
        <nav class="inverse" role="navigation">
            <div class="navbar-header">
                <button type="button" style=" border-color: white; " class="navbar-toggle" data-toggle="collapse" data-target="#auxiliar">
                    <span class="sr-only" >Toggle navigation</span>
                    <span class="icon-bar" style=" background-color: white; " ></span>
                    <span class="icon-bar" style=" background-color: white; " ></span>
                    <span class="icon-bar" style=" background-color: white; " ></span>
                </button>
                <a class="navbar-brand" href="http://www.unitecnologica.edu.co"><img style="border-top-width: 0px;width: 80px;margin-top: -7px;"  src="http://web.unitecnologica.edu.co/sites/web.unitecnologica.edu.co/files/HomeUTB.png" /></a>
            </div>
            <div class="navbar-collapse">

                <?php print render($page['menu_auxiliar']); ?>
            </div>
        </nav>
    </section>
</div>

<div class="container">

    <div class="row clearfix">
        <div class="col-lg-8 col-md-12 col-sm-12 col-xs-12">
            <br/>
            <?php if ($logo): ?>
                <div class="col-lg-5 col-md-6 col-sm-6 col-xs-12">
                    <a href="<?php print $front_page; ?>" title="<?php print t('Home'); ?>" rel="home"> <img class="img-responsive" src="<?php print $logo; ?>" alt="<?php print $site_name; ?>" role="presentation" /> </a>
                </div>
            <?php endif; ?>

            <?php $home_utb = "Universidad Tecnológica de Bolívar"; ?>

            <div class="col-lg-7 col-md-6 col-sm-6 col-xs-12">
                <hgroup class="Titulo_UTB">
                    <h1><?php print $home_utb; ?></h1>

                    <?php if ($site_slogan): ?>
                        <h2><?php print $site_slogan; ?></h2>
                    <?php endif; ?>
                    <?php if ($site_name != $home_utb): ?>
                        <?php if ($site_name): ?>
                            <h3><?php print $site_name; ?></h3>
                        <?php endif; ?>
                    <?php endif; ?>
                </hgroup>
            </div>

        </div>

        <?php if ($page['encabezado_principal-2']): ?>
            <div class="col-lg-4 col-md-12 col-sm-12 col-xs-12">
                <section>
                    <?php print render($page['encabezado_principal-2']); ?>
                </section>
            </div>
        <?php endif; ?>
    </div>


    <?php if (($page['encabezado_auxiliar-1']) || ($page['encabezado_auxiliar-2'])): ?>
        <div class="row clearfix">
            <?php if ($page['encabezado_auxiliar-1']): ?>
                <div class="col-md-6 column">
                    <section>
                        <?php print render($page['encabezado_auxiliar-1']); ?>
                    </section>
                </div>
            <?php endif; ?>
            <?php if ($page['encabezado_auxiliar-2']): ?>
                <div class="col-md-6 column">
                    <section>
                        <?php print render($page['encabezado_auxiliar-2']); ?>
                    </section>
                </div>
            <?php endif; ?>
        </div>
    <?php endif; ?>

    <?php if ($page['jumbo_1']): ?>
    </div>
    <?php print render($page['jumbo_1']) ?>
    <div class="container">
    <?php endif; ?>
</div>
<div class="clearfix">
    <div class="col-md-12 column" style="padding-right: 0px;padding-left: 0px;">

        <section>
            <?php if (!empty($page['main_menu'])): ?>


                <nav class="navbar-default" role="navigation">
                    <div class="container main-menu-wrapper">
                        <div class="navbar-header">
                            <button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#principal">
                                <span class="sr-only">Toggle navigation</span>
                                <span class="icon-bar"></span>
                                <span class="icon-bar"></span>
                                <span class="icon-bar"></span>
                            </button>
                            <a class="navbar-brand" href="/"> <span class="glyphicon glyphicon-home" style="z-index: 1;"></span></a>
                        </div>



                        <?php print render($page['main_menu']); ?>



                    </div>
                </nav>


            <?php endif; ?>
        </section>

    </div>
</div>

<?php if ($messages): ?>
    <div class="container">
        <div class="row clearfix">
            <div class="row clearfix">
                <div class="col-md-12 column">
                    <section>
                        <?php print render($messages); ?>
                    </section>
                </div>
            </div>
        </div>
    </div>
<?php endif; ?>

<?php if (!empty($page['menu_secundario'])): ?>
    <div class="row clearfix">
        <div class="row clearfix">
            <div class="col-md-12 column">
                <section>
                    <?php if (!empty($page['menu_secundario'])): ?>
                        <?php print render($page['menu_secundario']); ?>
                    <?php endif; ?>
                </section>
            </div>
        </div>
    </div>
<?php endif; ?>


<div class="container">
    <?php if (($page['galeria'])): ?>
        <div class="row clearfix">
            <div class="col-md-12 column">
                <section>
                    <?php print render($page['galeria']); ?>
                </section>
            </div>
        </div>
    <?php endif; ?>


    <?php if (($page['galeria_prog'])): ?>
    </div>

    <div class="clearfix">
        <div class="col-md-12 column" style="padding-right: 0px;padding-left: 0px;">
            <section>
                <?php print render($page['galeria_prog']); ?>
            </section>
        </div>
    </div>


    <div class="container">

    <?php endif; ?>

    <?php if (($page['especial-completa-1'])): ?>
        <div class="row clearfix">
            <div class="col-md-12 column">
                <?php print render($page['especial-completa-1']); ?>
            </div>
        </div>
    <?php endif; ?>

    <?php if ($page['jumbo_2']): ?>
    </div>
    <?php print render($page['jumbo_2']) ?>
    <div class="container">
    <?php endif; ?>

    <?php if (($page['anuncios_destacados']) || ($page['noticias_destacadas'])): ?>
        <div class="row clearfix">
            <section>
                <?php if ($page['anuncios_destacados']): ?>
                    <div class="col-md-8 column">
                        <?php print render($page['anuncios_destacados']); ?>
                    </div>
                <?php endif; ?>
                <?php if ($page['noticias_destacadas']): ?>
                    <div class="col-md-4 column">
                        <?php print render($page['noticias_destacadas']); ?>
                    </div>
                <?php endif; ?>
            </section>
        </div>
    <?php endif; ?>


    <?php if (($page['complemento_portada_superior-1']) || ($page['complemento_portada_superior-2']) || ($page['complemento_portada_superior-3'])): ?>
        <div class="row clearfix">
            <?php if (($page['complemento_portada_superior-1'])): ?>
                <div class="col-md-4 column">
                    <?php print render($page['complemento_portada_superior-1']); ?></div>
            <?php endif; ?>
            <?php if (($page['complemento_portada_superior-2'])): ?>
                <div class="col-md-4 column">
                    <?php print render($page['complemento_portada_superior-2']); ?></div>
            <?php endif; ?>
            <?php if (($page['complemento_portada_superior-3'])): ?>
                <div class="col-md-4 column">
                    <?php print render($page['complemento_portada_superior-3']); ?></div>
            <?php endif; ?>
        </div>
    <?php endif; ?>

    <?php if (($page['migas_pan']) || ($breadcrumb)): ?>
        <div class="row clearfix">
            <section>
                <?php if (($page['migas_pan'])): ?>
                    <?php print render($page['migas_pan']); ?>
                <?php endif; ?>
                <div>
                    <?php if ($breadcrumb): ?>
                        <div id="breadcrumb"><?php // print $breadcrumb;                    ?></div>
                    <?php endif; ?>
                </div>
            </section>
        </div>
    <?php endif; ?>

    <?php if (($page['destacados-1']) || ($page['destacados-2']) || ($page['destacados-3']) || ($page['destacados-4']) || ($page['destacados-5']) || ($page['destacados-6'])): ?>
        <div class="row clearfix">
            <section>
                <?php if ($page['destacados-1']): ?>
                    <div class="col-md-2">
                        <?php print render($page['destacados-1']); ?>
                    </div>
                <?php endif; ?>
                <?php if ($page['destacados-2']): ?>
                    <div class="col-md-2">
                        <?php print render($page['destacados-2']); ?>
                    </div>
                <?php endif; ?>
                <?php if ($page['destacados-3']): ?>
                    <div class="col-md-2">
                        <?php print render($page['destacados-3']); ?>
                    </div>
                <?php endif; ?>
                <?php if ($page['destacados-4']): ?>
                    <div class="col-md-2">
                        <?php print render($page['destacados-4']); ?>
                    </div>
                <?php endif; ?>
                <?php if ($page['destacados-5']): ?>
                    <div class="col-md-2">
                        <?php print render($page['destacados-5']); ?>
                    </div>
                <?php endif; ?>
                <?php if ($page['destacados-6']): ?>
                    <div class="col-md-2">
                        <?php print render($page['destacados-6']); ?>
                    </div>
                <?php endif; ?>
            </section>
        </div>
    <?php endif; ?>

    <?php if (($page['especial-1']) || ($page['especial-2'])): ?>
        <div class="row clearfix">
            <section>
                <?php if ($page['especial-1']): ?>
                    <div class="col-md-8 column">
                        <?php print render($page['especial-1']); ?>
                    </div>
                <?php endif; ?>
                <?php if ($page['especial-2']): ?>
                    <div class="col-md-4 column">
                        <?php print render($page['especial-2']); ?>
                    </div>
                <?php endif; ?>
            </section>
        </div>
    <?php endif; ?>

    <?php if (($page['conceptos-1']) || ($page['conceptos-2']) || ($page['conceptos-3']) || ($page['conceptos-4'])): ?>
        <div class="row clearfix">
            <section>
                <?php if ($page['conceptos-1']): ?>
                    <div class="col-md-3 column">
                        <?php print render($page['conceptos-1']); ?>
                    </div>
                <?php endif; ?>
                <?php if ($page['conceptos-2']): ?>
                    <div class="col-md-3 column">
                        <?php print render($page['conceptos-2']); ?>
                    </div>
                <?php endif; ?>

                <?php if ($page['conceptos-3']): ?>
                    <div class="col-md-3 column">
                        <?php print render($page['conceptos-3']); ?>
                    </div>
                <?php endif; ?>
                <?php if ($page['conceptos-4']): ?>
                    <div class="col-md-3 column">
                        <?php print render($page['conceptos-4']); ?>
                    </div>
                <?php endif; ?>
            </section>
        </div>
    <?php endif; ?>

    <?php if (($page['especial-completa-2'])): ?>
        <div class="row clearfix">
            <div class="col-md-12 column">
                <?php print render($page['especial-completa-2']); ?>
            </div>
        </div>
    <?php endif; ?>

    <?php if (($page['informacion-1']) || ($page['informacion-2'])): ?>
        <div class="row clearfix">
            <section>
                <?php if ($page['informacion-1']): ?>
                    <div class="col-md-6 column">
                        <?php print render($page['informacion-1']); ?>
                    </div>
                <?php endif; ?>
                <?php if ($page['informacion-2']): ?>
                    <div class="col-md-6 column">
                        <?php print render($page['informacion-2']); ?>
                    </div>
                <?php endif; ?>
            </section>
        </div>
    <?php endif; ?>

    <?php if (($page['complemento_portada_medio-1']) || ($page['complemento_portada_medio-2']) || ($page['complemento_portada_medio-3'])): ?>
        <div class="row clearfix">
            <?php if (($page['complemento_portada_medio-1'])): ?>
                <div class="col-md-4 column">
                    <?php print render($page['complemento_portada_medio-1']); ?></div>
            <?php endif; ?>

            <?php if (($page['complemento_portada_medio-2'])): ?>
                <div class="col-md-4 column">
                    <?php print render($page['complemento_portada_medio-2']); ?></div>
            <?php endif; ?>

            <?php if (($page['complemento_portada_medio-3'])): ?>
                <div class="col-md-4 column">
                    <?php print render($page['complemento_portada_medio-3']); ?></div>
            <?php endif; ?>
        </div>
    <?php endif; ?>

    <?php if ($page['jumbo_3']): ?>
    </div>
    <?php print render($page['jumbo_3']) ?>
    <div class="container">
    <?php endif; ?>

    <?php if ($tabs): ?>
        <div class="tabs">
            <?php print render($tabs); ?>
        </div>
    <?php endif; ?>


    <?php if (($page['left_sidebar']) || ($page['informacion_prioritaria']) || ($page['informacion_complementaria']) || ($page['content']) || ($page['right_sidebar'])): ?>
        <div class="row clearfix">
            <?php if ($page['left_sidebar']): ?>
                <aside>
                    <div class="col-md-3 column">
                        <?php print render($page['left_sidebar']); ?>
                    </div>
                </aside>
            <?php endif; ?>

            <?php
            $class_content = "col-md-12";
            if ($page['left_sidebar'] && $page['right_sidebar']):
                $class_content = "col-md-6 column";
            endif;
            if ($page['left_sidebar'] && !$page['right_sidebar']):
                $class_content = "col-md-9 column";
            endif;
            if (!$page['left_sidebar'] && $page['right_sidebar']):
                $class_content = "col-md-9 column";
            endif;
            if (!$page['left_sidebar'] && !$page['right_sidebar']):
                $class_content = "col-md-12 column";
            endif;
            ?>
            <div class="<?php echo $class_content; ?>">
                <?php if ($page['highlighted']): ?>
                    <aside>
                        <div class="col-md-12 column">
                            <?php print render($page['highlighted']); ?>
                        </div>
                    </aside>
                <?php endif; ?>

                <?php if ($page['help']): ?>
                    <aside>
                        <div class="col-md-12 column">
                            <?php print render($page['help']); ?>
                            <?php print $messages; ?>
                        </div>
                    </aside>
                <?php endif; ?>

                <?php if ($page['informacion_prioritaria']): ?>
                    <aside>
                        <div class="col-md-12 column">
                            <?php print render($page['informacion_prioritaria']); ?>
                        </div>
                    </aside>
                <?php endif; ?>
                <?php if (($page['noticias-1']) || ($page['noticias-2']) || ($page['noticias-3'])): ?>
                    <div class="panel panel-default jumbo-gallery">
                        <div class="panel-heading">
                            <a class="panel-title collapsed" data-parent="#nuevas" data-toggle="collapse" href="#nuevo"> Noticias otros portales UTB <span class="caret"></span> </a>
                        </div>
                        <div class="panel-collapse collapse" id="nuevo" style="height: 199px;">
                            <div class="panel-body">
                                <p>Últimas noticias :</p>
                                <br>
                                <!--                        CADA PROGRAMA/CURSO                     -->
                                <div class="list-group">

                                    <div class="row clearfix">
                                        <section>

                                            <?php if ($page['noticias-1']): ?>
                                                <div class="col-md-4 column">
                                                    <?php print render($page['noticias-1']); ?>
                                                </div>
                                            <?php endif; ?>

                                            <?php if ($page['noticias-2']): ?>
                                                <div class="col-md-4 column">
                                                    <?php print render($page['noticias-2']); ?>
                                                </div>
                                            <?php endif; ?>


                                            <?php if ($page['noticias-3']): ?>
                                                <div class="col-md-4 column">
                                                    <?php print render($page['noticias-3']); ?>
                                                </div>
                                            <?php endif; ?>
                                        </section>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>

                <?php endif; ?>
                <?php if (!drupal_is_front_page()) : ?>
                    <?php if ($page['content']): ?>
                        <article>
                            <div class="col-md-12 column">


                                <?php
                                //if $show_title var is TRUE, print the title, otherwise, not
                                if ($show_title != FALSE):
                                    ?>
                                    <br>
                                    <h2 class="page-title">
                                        <?php if ($title): ?>

                                            <?php
                                            if (isset($node->field_nuevo)):
                                                if (strcmp($node->field_nuevo["und"][0]["value"], '1') == 0):
                                                    ?>
                                                    <span class="badge pull-right">Nuevo</span>
                                                <?php endif; ?>
                                            <?php endif; ?>
                                            <?php
                                            print $title;
                                            ?>

                                        <?php endif; ?>



                                        <?php
                                        if (isset($node->field_inscripciones)):
                                            if (strcmp($node->field_inscripciones["und"][0]["value"], '1') == 0):
                                                ?>
                                                <span class="badge pull-right badge-success">Inscripciones abiertas</span>
                                            <?php endif; ?>  <?php endif; ?>



                                        <?php
                                        if (isset($node->field_preinscripciones)):
                                            if (strcmp($node->field_preinscripciones["und"][0]["value"], '1') == 0):
                                                ?>
                                                <span class="badge pull-right badge-info">Preinscripciones</span>
                                            <?php endif; ?>  <?php endif; ?>
                                    </h2>
                                    <?php
                                endif; //end if $show_title var is TRUE, print the title, otherwise, not
                                ?>

                                <?php if (isset($node->field_prg_snies)): ?>
                                    SNIES <?php print $node->field_prg_snies["und"][0]["value"] ?><br>
                                     <?php print $node->field_registro_calificado["und"][0]["value"] ?><br>
                                    <?php print $node->field_acreditacion_men["und"][0]["value"] ?>
                                    <hr>
                                <?php endif; ?>


                                <?php if (isset($node->field_carrousel)): ?>
                                    <?php if ($node->field_carrousel): ?>
                                        <div id="carousel-general" class="carousel slide" data-ride="carousel">

                                            <div class="carousel-inner">
                                                <?php
                                                $val = 0;
                                                for ($i = 0;
                                                ; $i++) :
                                                    if (!$node->field_carrousel['und'][$i]['uri']):
                                                        break;
                                                    endif;
                                                    if ($i == 0) :
                                                        ?> <div class="item active"> <?php else: ?>
                                                            <div class="item">
                                                            <?php endif; ?>

                                                            <?php
                                                            $val++;
                                                            $uriImagefromNode = $node->field_carrousel['und'][$i]['uri'];
                                                            $urlImagefromNode = file_create_url($uriImagefromNode);
                                                            ?>
                                                            <img src="<?php print $urlImagefromNode; ?>" style=" min-height: 150px;  width: 100%;"   class="img-responsive"/>
                                                            <?php if ($node->field_carrousel['und'][$i]['alt']): ?>
                                                                <div class="captions-galeria opaco">
                                                                    <p class="small"><?php print $node->field_carrousel['und'][$i]['alt']; ?> </p>
                                                                </div>
                                                            <?php endif; ?>
                                                        </div>
                                                    <?php endfor; ?>

                                                </div>
                                                <?php if ($val > 1) { ?>
                                                    <div class="text-center">

                                                        <a class="" href="javascript:void(0)"
                                                           data-slide="prev" data-target="#carousel-general">
                                                            <span class="glyphicon glyphicon-chevron-left"></span>
                                                        </a>


                                                        <?php
                                                        for ($x = 0; $x < $val; $x++) {
                                                            if ($x == 0) {
                                                                ?>  <li href="javascript:void(0)" class="indicators active" data-target="#carousel-general" data-slide-to="<?php print $x; ?>" > <?php } else { ?>
                                                                <li href="javascript:void(0)" class="indicators" data-target="#carousel-general" data-slide-to="<?php print $x; ?>" >
                                                                <?php } $i++; ?>
                                                            </li>
                                                        <?php } ?>

                                                        <a class="" href="javascript:void(0)"
                                                           data-slide="next" data-target="#carousel-general">
                                                            <span class="glyphicon glyphicon-chevron-right"></span>
                                                        </a>

                                                    </div>
                                                <?php } ?>
                                            </div>
                                        <?php endif; ?>
                                    <?php endif; ?>


                                    <?php print render($page['content']); ?>
                                </div>
                        </article>
                    <?php endif; ?>
                <?php endif; ?>

                <?php if ($page['informacion_complementaria']): ?>
                    <aside>
                        <div class="col-md-12 column">
                            <?php print render($page['informacion_complementaria']); ?>
                        </div>
                    </aside>
                <?php endif; ?>
            </div>
            <?php if ($page['right_sidebar']): ?>
                <div class="col-md-3 column z-depth-1">
                    <aside>
                        <?php print render($page['right_sidebar']); ?>
                    </aside>
                </div>

            <?php endif; ?>
        </div>
    <?php endif; ?>




    <?php if ($page['jumbo_4']): ?>
    </div>
    <?php print render($page['jumbo_4']) ?>
    <div class="container">
    <?php endif; ?>

    <?php if (($page['novedades-1']) || ($page['novedades-2']) || ($page['novedades-3']) || ($page['novedades-4'])): ?>
        <div class="row clearfix">
            <section>

                <?php if ($page['novedades-1']): ?>
                    <div class="col-md-3 column">
                        <?php print render($page['novedades-1']); ?>
                    </div>
                <?php endif; ?>

                <?php if ($page['novedades-2']): ?>
                    <div class="col-md-3 column">
                        <?php print render($page['novedades-2']); ?>
                    </div>
                <?php endif; ?>


                <?php if ($page['novedades-3']): ?>
                    <div class="col-md-3 column">
                        <?php print render($page['novedades-3']); ?>
                    </div>
                <?php endif; ?>
                <?php if ($page['novedades-4']): ?>
                    <div class="col-md-3 column">
                        <?php print render($page['novedades-4']); ?>
                    </div>
                <?php endif; ?>
            </section>
        </div>
    <?php endif; ?>

    <?php if (($page['resaltados-1']) || ($page['resaltados-2']) || ($page['resaltados-3']) || ($page['resaltados-4']) || ($page['resaltados-5']) || ($page['resaltados-6'])): ?>
        <div class="row clearfix">
            <section>
                <?php if ($page['resaltados-1']): ?>
                    <div class="col-md-2">
                        <?php print render($page['resaltados-1']); ?>
                    </div>
                <?php endif; ?>

                <?php if ($page['resaltados-2']): ?>
                    <div class="col-md-2">
                        <?php print render($page['resaltados-2']); ?>
                    </div>
                <?php endif; ?>

                <?php if ($page['resaltados-3']): ?>
                    <div class="col-md-2">
                        <?php print render($page['resaltados-3']); ?>
                    </div>
                <?php endif; ?>

                <?php if ($page['resaltados-4']): ?>
                    <div class="col-md-2">
                        <?php print render($page['resaltados-4']); ?>
                    </div>
                <?php endif; ?>

                <?php if ($page['resaltados-5']): ?>
                    <div class="col-md-2">
                        <?php print render($page['resaltados-5']); ?>
                    </div>
                <?php endif; ?>

                <?php if ($page['resaltados-6']): ?>
                    <div class="col-md-2">
                        <?php print render($page['resaltados-6']); ?>
                    </div>
                <?php endif; ?>
            </section>
        </div>
    <?php endif; ?>

    <?php if (($page['notas-1']) || ($page['notas-2'])): ?>
        <div class="row clearfix">
            <section>
                <?php if ($page['notas-1']): ?>
                    <div class="col-md-6 column">
                        <?php print render($page['notas-1']); ?>
                    </div>
                <?php endif; ?>
                <?php if ($page['notas-2']): ?>
                    <div class="col-md-6 column">
                        <?php print render($page['notas-2']); ?>
                    </div>
                <?php endif; ?>
            </section>
        </div>
    <?php endif; ?>

    <?php if (($page['catalogo-1']) || ($page['catalogo-2']) || ($page['catalogo-3']) || ($page['catalogo-4']) || ($page['catalogo-5'])): ?>
        <div class="row clearfix">
            <?php if ($page['catalogo-1']): ?>
                <div class="col-md-2 col-md-offset-1">
                    <?php print render($page['catalogo-1']); ?>
                </div>
            <?php endif; ?>
            <?php if ($page['catalogo-2']): ?>
                <div class="col-md-2">
                    <?php print render($page['catalogo-2']); ?>
                </div>
            <?php endif; ?>

            <?php if ($page['catalogo-3']): ?>
                <div class="col-md-2">
                    <?php print render($page['catalogo-3']); ?>
                </div>
            <?php endif; ?>

            <?php if ($page['catalogo-4']): ?>
                <div class="col-md-2">
                    <?php print render($page['catalogo-4']); ?>
                </div>
            <?php endif; ?>

            <?php if ($page['catalogo-5']): ?>
                <div class="col-md-2">
                    <?php print render($page['catalogo-5']); ?>
                </div>
            <?php endif; ?>
        </div>
    <?php endif; ?>

    <?php if (($page['especial-completa-3'])): ?>
        <div class="row clearfix">
            <div class="col-md-12 column">
                <?php print render($page['especial-completa-3']); ?>
            </div>
        </div>
    <?php endif; ?>

    <?php if (($page['inferior_especial-1']) || ($page['inferior_especial-2'])): ?>
        <div class="row clearfix">
            <?php if ($page['inferior_especial-1']): ?>
                <div class="col-md-4 column">
                    <?php print render($page['inferior_especial-1']); ?>
                </div>
            <?php endif; ?>

            <?php if ($page['inferior_especial-2']): ?>
                <div class="col-md-8 column">
                    <?php print render($page['inferior_especial-2']); ?>
                </div>
            <?php endif; ?>
        </div>
    <?php endif; ?>

    <?php if ($page['jumbo_5']): ?>
    </div>
    <?php print render($page['jumbo_5']) ?>
    <div class="container">
    <?php endif; ?>

    <?php if (($page['opciones-1']) || ($page['opciones-2']) || ($page['opciones-3']) || ($page['opciones-4'])): ?>
        <div class="row clearfix">
            <?php if ($page['opciones-1']): ?>
                <div class="col-md-3 column">
                    <?php print render($page['opciones-1']); ?>
                </div>
            <?php endif; ?>

            <?php if ($page['opciones-2']): ?>
                <div class="col-md-3 column">
                    <?php print render($page['opciones-2']); ?>
                </div>
            <?php endif; ?>

            <?php if ($page['opciones-3']): ?>
                <div class="col-md-3 column">
                    <?php print render($page['opciones-3']); ?>
                </div>
            <?php endif; ?>

            <?php if ($page['opciones-4']): ?>
                <div class="col-md-3 column">
                    <?php print render($page['opciones-4']); ?>
                </div>
            <?php endif; ?>
        </div>
    <?php endif; ?>

    <?php if (($page['complemento-1']) || ($page['complemento-2'])): ?>
        <div class="row clearfix">
            <?php if ($page['complemento-1']): ?>
                <div class="col-md-6 column">
                    <?php print render($page['complemento-1']); ?>
                </div>
            <?php endif; ?>
            <?php if ($page['complemento-2']): ?>
                <div class="col-md-6 column">
                    <?php print render($page['complemento-2']); ?>
                </div>
            <?php endif; ?>
        </div>
    <?php endif; ?>

    <?php if (($page['especial-completa-4'])): ?>
        <div class="row clearfix">
            <div class="col-md-12 column">
                <?php print render($page['especial-completa-4']); ?>
            </div>
        </div>
    <?php endif; ?>
    <?php if (($page['especial-completa-5'])): ?>
        <div class="row clearfix">
            <div class="col-md-12 column">
                <?php print render($page['especial-completa-5']); ?>
            </div>
        </div>
    <?php endif; ?>
    <br>

    <?php if (($page['publi-1']) || ($page['publi-2']) || ($page['publi-3']) || ($page['publi-4'])): ?>
        <div class="row clearfix">
            <?php if ($page['publi-1']): ?>
                <div class="col-md-3 column">
                    <?php print render($page['publi-1']); ?>
                </div>
            <?php endif; ?>

            <?php if ($page['publi-2']): ?>
                <div class="col-md-3 column">
                    <?php print render($page['publi-2']); ?>
                </div>
            <?php endif; ?>

            <?php if ($page['publi-3']): ?>
                <div class="col-md-3 column">
                    <?php print render($page['publi-3']); ?>
                </div>
            <?php endif; ?>

            <?php if ($page['publi-4']): ?>
                <div class="col-md-3 column">
                    <?php print render($page['publi-4']); ?>
                </div>
            <?php endif; ?>
        </div>
    <?php endif; ?>

    <?php if (($page['complemento_portada_inferior-1']) || ($page['complemento_portada_inferior-2']) || ($page['complemento_portada_inferior-3'])): ?>
        <div class="row clearfix">
            <?php if (($page['complemento_portada_inferior-1'])): ?>
                <div class="col-md-4 column">
                    <?php print render($page['complemento_portada_inferior-1']); ?>
                </div>
            <?php endif; ?>
            <?php if (($page['complemento_portada_inferior-2'])): ?>
                <div class="col-md-4 column">
                    <?php print render($page['complemento_portada_inferior-2']); ?>
                </div>
            <?php endif; ?>
            <?php if (($page['complemento_portada_inferior-3'])): ?>
                <div class="col-md-4 column">
                    <?php print render($page['complemento_portada_inferior-3']); ?>
                </div>
            <?php endif; ?>
        </div>
    <?php endif; ?>

    <?php if ($page['social']): ?>
        <div class="row clearfix text-right">
            <p class="small"><strong>¡Comparte este contenido en tus redes sociales!</strong></p>
            <?php print render($page['social']) ?>
        </div>
    <?php endif; ?>

</div>

<?php if ($page['jumbo_6']): ?>
    <?php print render($page['jumbo_6']) ?>
<?php endif; ?>
<!--<a data-toggle="modal" href="#myModal" style="background-color:black;" class="btn btn-primary">Reporta Error</a>-->
<footer id="footer" >
    <div>
        <section>
            <nav class="inverse" role="navigation">
                <div class="container-fluid">
                    <div class="navbar-header" >
                        <button type="button" style=" border-color: white; " class="navbar-toggle" data-toggle="collapse" data-target="#politicas">
                            <span class="sr-only" >Toggle navigation</span>
                            <span class="icon-bar" style=" background-color: white; " ></span>
                            <span class="icon-bar" style=" background-color: white; " ></span>
                            <span class="icon-bar" style=" background-color: white; " ></span>
                        </button>
                        <a class="navbar-brand" href="#start"><span class="glyphicon glyphicon-arrow-up" style=" color: white; "></span></a>

                    </div>


                    <?php print render($page['menu_politicas']); ?>


                </div>
            </nav>
        </section>
    </div>

    <?php if ($page['jumbo_7']): ?>
        <?php print render($page['jumbo_7']) ?>
    <?php endif; ?>

    <?php if (($page['footer_left']) || ($page['footer_center']) || ($page['footer_right']) || ($page['footer_completo'])): ?>
        <div class="clearfix bg-primary padding-m">
            <div class="container">
                <?php if ($page['footer_left']): ?>
                    <div class="col-md-4 column bg-primary padding-m ">
                        <?php print render($page['footer_left']); ?>
                    </div>
                <?php endif; ?>
                <?php if ($page['footer_center']): ?>
                    <div class="col-md-4 column bg-primary padding-m ">
                        <?php print render($page['footer_center']); ?>
                    </div>
                <?php endif; ?>
                <?php if ($page['footer_right']): ?>
                    <div class="col-md-4 column bg-primary padding-m ">
                        <?php print render($page['footer_right']); ?>
                    </div>
                <?php endif; ?>

                <?php if ($page['footer_completo']): ?>
                    <div class="col-md-12 column bg-primary padding-m ">
                        <?php print render($page['footer_completo']) ?>
                    </div>
                <?php endif; ?>

            </div>
        </div>
    <?php endif; ?>

    <?php if ($page['jumbo_8']): ?>
        <?php print render($page['jumbo_8']) ?>
    <?php endif; ?>

    <?php if (($page['copyright'])): ?>
        <div class="clearfix text-muted text-center small padding-s jumbo-inverted">
            <div class="col-md-12 column">
                <?php print render($page['copyright']); ?>
            </div>
        </div>
    <?php endif; ?>

</footer>
