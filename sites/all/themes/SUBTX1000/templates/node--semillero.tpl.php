<?php if (!empty($body)): ?>

    <article id="node-<?php print $node->nid; ?>" class="<?php print $classes; ?> clearfix"<?php print $attributes; ?>> <?php print $user_picture; ?> <?php print render($title_prefix); ?>

        <div id="article-<?php print $node->nid; ?>" class="article <?php
        print $classes;
        if ($display_submitted)
            
            ?>clearfix" <?php print $attributes; ?> >
             <?php if ($display_submitted): ?>
                <div class="footer submitted">
                    <?php print $user_picture; ?>
                    <?php print '<div class="time pubdate" title="' . $datetime . '">' . t($created_date) . '</div>'; ?>
                    <div class="info-wrapper">
                        <?php
                        if ($node_category)
                            print '<span class="node-category"> <span class="category-in">' . t('In') . ' </span>' . render($node_category) . '</span>';
                        print '<span class="author">' . t('Por') . ' ' . $name . '</span>';
                        print '<span class="comment-comments">' . render($comments_count) . '</span>';
                        ?>
                    </div>
                    <?php ?>
                </div>
            <?php endif; ?>
            <?php if ($title): ?>
                <div class="header article-header">

                    <div class="row clearfix" style="margin-bottom: 30px;">

                        <div class="col-md-12 column">
                            <?php if ($node->field_imagen_semillero): ?>
                                <?php
                                $uriImagefromNode = $node->field_imagen_semillero['und'][0]['uri'];
                                $urlImagefromNode = file_create_url($uriImagefromNode);
                                ?>           
                                <img src="<?php print $urlImagefromNode; ?>" style=" min-height: 150px; " class="img-responsive"/>
                            <?php endif; ?>                    
                            <hr/>


                            <div class="row">

                                <!-- Navigation Buttons field_redes_sociales_semillero -->
                                <div class="col-md-2">
                                    <ul class="nav nav-pills nav-stacked" id="Programas">
                                        <?php if ($node->body) { ?> 
                                            <li><a href="#descripcion" data-toggle="tab">Descripción</a></li><?php } ?>
                                        <?php if ($node->field_lineas_semillero["und"][0]["safe_value"]) { ?> 
                                            <li><a href="#lineas" data-toggle="tab">Lineas</a></li><?php } ?>
                                        <?php if ($node->field_direccion["und"][0]["entity"]) { ?> 
                                            <li><a href="#director" data-toggle="tab">Director Grupo</a></li><?php } ?>
                                        <?php if ($node->field_estudiantes["und"][0]["entity"]->nid) { ?>
                                            <li><a href="#estudiantes" data-toggle="tab">Estudiantes</a></li><?php } ?>	  
                                        <?php if ($node->field_investigadores_apoyo["und"][0]["entity"]->nid) { ?>
                                            <li><a href="#investigadores" data-toggle="tab">Investigadores</a></li><?php } ?>	  
                                        <?php if ($node->field_proyectos_semillero["und"][0]["entity"]->nid) { ?> 
                                            <li><a href="#proyectos" data-toggle="tab">Proyectos</a></li><?php } ?>  
                                        <?php if ($node->field_contacto["und"][0]["entity"]->nid) { ?>
                                            <li><a href="#contacto" data-toggle="tab">Contacto</a></li><?php } ?>	
                                    </ul>                                    
                                </div>

                                <!-- Content -->
                                <div class="col-md-10">
                                    <div class="tab-content">                                        
                                        <div class="tab-pane active" id="descripcion">
                                            <?php print render($content['body']); ?>
                                            <?php print($node->field_sitio_web_semillero["und"][0]["url"]); ?>
                                        </div>

                                        <div class="tab-pane " id="lineas">
                                            <?php print($node->field_lineas_semillero["und"][0]["safe_value"]); ?>
                                        </div>

                                        <div class="tab-pane" id="director">

                                            <a href="<?php echo url("node/" . $node->field_direccion["und"][0]["entity"]->nid);
                                            ?>">
                                                   <?php
                                                   print("<h3>");
                                                   print($node->field_direccion["und"][0]["entity"]->title);
                                                   print("</h3>");
                                                   //@TODO: incluir redes sociales
                                                   ?>
                                            </a>
                                            <div class="row ">
                                                <div class="col-md-6">
                                                    <div class="pull-right">
                                                        <?php echo theme('image_style', array('style_name' => 'staff', 'path' => $node->field_direccion["und"][0]["entity"]->field_foto['und'][0]['uri'], 'attributes' => array('class' => 'img-responsive'))); ?> 
                                                    </div>
                                                </div>

                                                <div class="col-md-6">
                                                    <div class="pull-left">
                                                        <dl>  
                                                            <?php if (strlen($node->field_direccion["und"][0]["entity"]->field_url_perfil["und"][0]['url']) > 1) { ?>
                                                                <dt>Perfil Investigador::</dt>
                                                                <dd><a class="btn btn-default" target="_blank" href="<?php print($node->field_direccion["und"][0]["entity"]->field_url_perfil["und"][0]['url']) ?>">Información adicional</a></dd>
                                                            <?php } ?> 
                                                            <?php if (strlen($node->field_direccion["und"][0]["entity"]->field_email["und"][0]["safe_value"]) > 1) { ?>   
                                                                <dt>Correo electrónico:</dt><dd><?php
                                                                    print(" " . $node->field_direccion["und"][0]["entity"]->field_email["und"][0]["safe_value"]);
                                                                    ?></dd>
                                                            <?php } ?>
                                                            <?php if (strlen($node->field_direccion["und"][0]["entity"]->field_telefono["und"][0]["safe_value"]) > 1) { ?>
                                                                <dt>Teléfono:</dt><dd><?php
                                                                    print(" " . $node->field_direccion["und"][0]["entity"]->field_telefono["und"][0]["safe_value"]);
                                                                    ?></dd>
                                                            <?php } ?>
                                                            <?php if (strlen($node->field_direccion["und"][0]["entity"]->field_direccion_oficina["und"][0]["safe_value"]) > 1) { ?>
                                                                <dt>Dirección de Oficina:</dt>
                                                                <dd><?php print($node->field_direccion["und"][0]["entity"]->field_direccion_oficina["und"][0]["safe_value"]); ?></dd>
                                                            <?php } ?>
                                                            <?php if (strlen($node->field_direccion["und"][0]["entity"]->field_tipo_de_investigador["und"][0]["safe_value"]) > 1) { ?>
                                                                <dt>Tipo de Investigador:</dt>
                                                                <dd><?php print $node->field_direccion["und"][0]["entity"]->field_tipo_de_investigador["und"][0]["safe_value"]; ?></dd>                                      
                                                            <?php } ?>
                                                        </dl>
                                                    </div>
                                                </div>
                                            </div>
                                            <h5>Contacto</h5><br/>
                                            <?php print $node->field_email_contacto["und"][0]["safe_value"]; ?>
                                        </div>

                                        <div class="tab-pane" id="estudiantes">


                                            <h3>Estudiantes</h3>
                                            <div class="programa_profesor_tc">
                                                <ul class="nav nav-tabs prog" data-tabs="tabs">                       
                                                    <?php
                                                    for ($i = 0;
                                                    ; $i++) {
                                                        $ID = $node->field_estudiantes["und"][$i]["entity"]->nid;
                                                        if ($ID == 0) {
                                                            break;
                                                        }
                                                        if (strcmp("0", $node->field_estudiantes["und"][$i]["entity"]->status) != 0) {
                                                            ?>
                                                            <div class="col-md-6 thumbnail" style="height: 270px; font-size: 13px;">    
                                                                <a href="<?php echo url("node/" . $node->field_estudiantes["und"][$i]["entity"]->nid); ?>">                                             
                                                                    <?php print($node->field_estudiantes["und"][$i]["entity"]->title); ?>  </a>
                                                                <br/><br/>
                                                                <div class="col-md-5">
                                                                    <?php if ($node->field_estudiantes["und"][$i]["entity"]->field_foto['und'][0]['uri']) { ?>                       
                                                                        <div style="margin-bottom: 10px;"> <?php echo theme('image_style', array('style_name' => 'thumbnail', 'path' => $node->field_estudiantes["und"][$i]["entity"]->field_foto['und'][0]['uri'], 'attributes' => array('class' => 'img-responsive'))); ?>        </div>                            
                                                                    <?php } ?>
                                                                    <div class="portada_profesor"> <a class="btn btn-default" target="_blank" href="<?php print $node->field_estudiantes["und"][$i]["entity"]->field_url_perfil["und"][0]["url"] ?>">Información adicional</a> </div>
                                                                </div>

                                                                <div class="col-md-7">
                                                                    <dl>                                                                                             
                                                                        <?php if ($node->field_estudiantes["und"][$i]["entity"]->field_tipo_de_investigador["und"][0]["value"]) { ?>                       
                                                                            <dt>Tipo de Investigador: </dt> 
                                                                            <dd> <?php print($node->field_estudiantes["und"][$i]["entity"]->field_tipo_de_investigador["und"][0]["value"]); ?> </dd>
                                                                        <?php } ?>
                                                                        <?php if (strlen($node->field_estudiantes["und"][$i]["entity"]->field_email["und"][0]["safe_value"]) > 1) { ?>   
                                                                            <dt>Correo electrónico:</dt><dd><small><?php
                                                                                    print(" " . $node->field_estudiantes["und"][$i]["entity"]->field_email["und"][0]["safe_value"]);
                                                                                    ?></small></dd>
                                                                        <?php } ?>
                                                                        <?php if (strlen($node->field_estudiantes["und"][$i]["entity"]->field_telefono["und"][0]["safe_value"]) > 1) { ?>
                                                                            <dt>Teléfono:</dt><dd><?php
                                                                                print(" " . $node->field_estudiantes["und"][$i]["entity"]->field_telefono["und"][0]["safe_value"]);
                                                                                ?></dd>
                                                                        <?php } ?>
                                                                        <?php if (strlen($node->field_estudiantes["und"][$i]["entity"]->field_direccion_oficina["und"][0]["safe_value"]) > 1) { ?>
                                                                            <dt>Dirección de Oficina:</dt>
                                                                            <dd><?php print($node->field_estudiantes["und"][$i]["entity"]->field_direccion_oficina["und"][0]["safe_value"]); ?></dd>
                                                                        <?php } ?></dl>

                                                                </div>
                                                            </div>  

                                                            <?php
                                                        }
                                                    }
                                                    ?>

                                            </div>



                                        </div>

                                        <div class="tab-pane" id="investigadores">


                                            <h3>Investigador de Apoyo</h3>
                                            <div class="programa_profesor_tc">
                                                <ul class="nav nav-tabs prog" data-tabs="tabs">                       
                                                    <?php
                                                    for ($i = 0;
                                                    ; $i++) {
                                                        $ID = $node->field_investigadores_apoyo["und"][$i]["entity"]->nid;
                                                        if ($ID == 0) {
                                                            break;
                                                        }
                                                        if (strcmp("0", $node->field_investigadores_apoyo["und"][$i]["entity"]->status) != 0) {
                                                            ?>
                                                            <div class="col-md-6 thumbnail" style="height: 270px; font-size: 13px;">    
                                                                <a href="<?php echo url("node/" . $node->field_investigadores_apoyo["und"][$i]["entity"]->nid); ?>">                                             
                                                                    <?php print($node->field_investigadores_apoyo["und"][$i]["entity"]->title); ?>  </a>
                                                                <br/><br/>
                                                                <div class="col-md-5">
                                                                    <?php if ($node->field_investigadores_apoyo["und"][$i]["entity"]->field_foto['und'][0]['uri']) { ?>                       
                                                                        <div style="margin-bottom: 10px;"> <?php echo theme('image_style', array('style_name' => 'thumbnail', 'path' => $node->field_investigadores_apoyo["und"][$i]["entity"]->field_foto['und'][0]['uri'], 'attributes' => array('class' => 'img-responsive'))); ?>        </div>                            
                                                                    <?php } ?>
                                                                    <div class="portada_profesor"> <a class="btn btn-default" target="_blank" href="<?php print $node->field_investigadores_apoyo["und"][$i]["entity"]->field_url_perfil["und"][0]["url"] ?>">Información adicional</a> </div>
                                                                </div>

                                                                <div class="col-md-7">
                                                                    <dl>                                                                                             
                                                                        <?php if ($node->field_investigadores_apoyo["und"][$i]["entity"]->field_tipo_de_investigador["und"][0]["value"]) { ?>                       
                                                                            <dt>Tipo de Investigador: </dt> 
                                                                            <dd> <?php print($node->field_investigadores_apoyo["und"][$i]["entity"]->field_tipo_de_investigador["und"][0]["value"]); ?> </dd>
                                                                        <?php } ?>
                                                                        <?php if (strlen($node->field_investigadores_apoyo["und"][$i]["entity"]->field_email["und"][0]["safe_value"]) > 1) { ?>   
                                                                            <dt>Correo electrónico:</dt><dd><small><?php
                                                                                    print(" " . $node->field_investigadores_apoyo["und"][$i]["entity"]->field_email["und"][0]["safe_value"]);
                                                                                    ?></small></dd>
                                                                        <?php } ?>
                                                                        <?php if (strlen($node->field_investigadores_apoyo["und"][$i]["entity"]->field_telefono["und"][0]["safe_value"]) > 1) { ?>
                                                                            <dt>Teléfono:</dt><dd><?php
                                                                                print(" " . $node->field_investigadores_apoyo["und"][$i]["entity"]->field_telefono["und"][0]["safe_value"]);
                                                                                ?></dd>
                                                                        <?php } ?>
                                                                        <?php if (strlen($node->field_investigadores_apoyo["und"][$i]["entity"]->field_direccion_oficina["und"][0]["safe_value"]) > 1) { ?>
                                                                            <dt>Dirección de Oficina:</dt>
                                                                            <dd><?php print($node->field_investigadores_apoyo["und"][$i]["entity"]->field_direccion_oficina["und"][0]["safe_value"]); ?></dd>
                                                                        <?php } ?></dl>

                                                                </div>
                                                            </div>  

                                                            <?php
                                                        }
                                                    }
                                                    ?>

                                            </div>



                                        </div>

                                        <div class="tab-pane" id="proyectos">

                                            <h3>Proyectos</h3> 
                                            <ul class="nav list-group" data-tabs="tabs">                       
                                                <?php
                                                for ($i = 0;; $i++) {
                                                    $ID = $node->field_proyectos_semillero["und"][$i]["entity"]->nid;

                                                    if ($ID == 0 or strcmp("0", $node->status) == 0) {
                                                        break;
                                                    }
                                                    if ($node->field_proyectos_semillero["und"][$i]["entity"]->status > 0) {
                                                        $alias = drupal_get_path_alias('node/' . $ID);
                                                        ?>
                                                        <li class="list-group-item" >
                                                            <a href="<?php print($alias); ?>">
                                                                <div class=""> <?php print($node->field_proyectos_semillero["und"][$i]["entity"]->title); ?> 
                                                                </div>
                                                            </a>
                                                        </li>

                                                        <?php
                                                    }
                                                }
                                                ?>

                                        </div>

                                        <div class="tab-pane" id="contacto">    

                                            <div class="tab-pane" id="director">

                                                <a href="<?php echo url("node/" . $node->field_contacto["und"][0]["entity"]->nid);
                                                ?>">
                                                       <?php
                                                       print("<h3>");
                                                       print($node->field_contacto["und"][0]["entity"]->title);
                                                       print("</h3>");
                                                       //@TODO: incluir redes sociales
                                                       ?>
                                                </a>
                                                <div class="row ">
                                                    <div class="col-md-6">
                                                        <div class="pull-right">
                                                            <?php echo theme('image_style', array('style_name' => 'staff', 'path' => $node->field_contacto["und"][0]["entity"]->field_foto['und'][0]['uri'], 'attributes' => array('class' => 'img-responsive'))); ?> 
                                                        </div>
                                                    </div>

                                                    <div class="col-md-6">
                                                        <div class="pull-left">
                                                            <dl>  
                                                                <?php if (strlen($node->field_contacto["und"][0]["entity"]->field_url_perfil["und"][0]['url']) > 1) { ?>
                                                                    <dt>Perfil Investigador::</dt>
                                                                    <dd><a class="btn btn-default" target="_blank" href="<?php print($node->field_contacto["und"][0]["entity"]->field_url_perfil["und"][0]['url']) ?>">Información adicional</a></dd>
                                                                <?php } ?> 
                                                                <?php if (strlen($node->field_contacto["und"][0]["entity"]->field_email["und"][0]["safe_value"]) > 1) { ?>   
                                                                    <dt>Correo electrónico:</dt><dd><?php
                                                                        print(" " . $node->field_contacto["und"][0]["entity"]->field_email["und"][0]["safe_value"]);
                                                                        ?></dd>
                                                                <?php } ?>
                                                                <?php if (strlen($node->field_contacto["und"][0]["entity"]->field_telefono["und"][0]["safe_value"]) > 1) { ?>
                                                                    <dt>Teléfono:</dt><dd><?php
                                                                        print(" " . $node->field_contacto["und"][0]["entity"]->field_telefono["und"][0]["safe_value"]);
                                                                        ?></dd>
                                                                <?php } ?>
                                                                <?php if (strlen($node->field_contacto["und"][0]["entity"]->field_contacto_oficina["und"][0]["safe_value"]) > 1) { ?>
                                                                    <dt>Dirección de Oficina:</dt>
                                                                    <dd><?php print($node->field_contacto["und"][0]["entity"]->field_contacto_oficina["und"][0]["safe_value"]); ?></dd>
                                                                <?php } ?>
                                                                <?php if (strlen($node->field_contacto["und"][0]["entity"]->field_tipo_de_investigador["und"][0]["safe_value"]) > 1) { ?>
                                                                    <dt>Tipo de Investigador:</dt>
                                                                    <dd><?php print $node->field_contacto["und"][0]["entity"]->field_tipo_de_investigador["und"][0]["safe_value"]; ?></dd>                                      
                                                                <?php } ?>
                                                            </dl>
                                                        </div>
                                                    </div>
                                                </div>
                                                <h5>Contacto</h5><br/>
                                                <?php print $node->field_email_contacto["und"][0]["safe_value"]; ?>
                                            </div>



                                        </div>

                                    </div>

                                </div>

                            </div>

                        </div>

                    </div>






                </div>


            </div>
            <?php //var_dump($node->field_direccion["und"][0]["entity"]);         ?>
        <?php endif; ?>

        <?php if ($links = render($content['links'])): ?>
            <div class="menu node-links clearfix"><?php print $links; ?></div>
        <?php endif; ?>

        <?php //var_dump($node->field_direccion["und"][0]["entity"]->field_nivel_academico["und"][0]);                  ?>




    <?php endif; ?>
</article>