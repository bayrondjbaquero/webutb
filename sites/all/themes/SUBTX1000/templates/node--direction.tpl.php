<div id="node-<?php print $node->nid; ?>" class="<?php print $classes; ?> clearfix"<?php print $attributes; ?>>

    <div class="row clearfix">

        <div class="col-md-12 column">

            <?php print($node->body["und"][0]['safe_summary']); ?>

            <div class="row" style="margin-bottom: 30px;">

                <!-- Navigation Buttons -->
                <div class="col-md-2">
                    <ul class="nav nav-pills nav-stacked" id="Programas">

                        <?php if ($node->body["und"][0]["safe_value"]) { ?> 
                            <li class="active"><a href="#descripcion"  data-toggle="tab">Descripción</a></li><?php } ?> 

                        <?php if ($node->field_director["und"][0]["entity"]) { ?> 
                            <li ><a href="#director" data-toggle="tab">Director</a></li><?php } ?>

                        <?php if ($node->field_jefatura["und"][0]["entity"]) { ?> 
                            <li ><a href="#jefatura" data-toggle="tab">Jefatura</a></li><?php } ?>

                        <?php if ($node->field_coordinacion["und"][0]["entity"]) { ?> 
                            <li ><a href="#coordinacion" data-toggle="tab">Coordinación</a></li><?php } ?>                           

                        <?php if ($node->field_equipo_humano["und"][0]["entity"]) { ?> 
                            <li ><a href="#equipo_humano" data-toggle="tab">Equipo humano</a></li><?php } ?>     

                        <?php if ($node->field_contacto["und"][0]["safe_value"]) { ?>    
                            <li><a href="#contacto" data-toggle="tab">Contacto</a></li><?php } ?> 

                    </ul>

                </div>

                <!-- Content -->
                <div class="col-md-10">

                    <div class="tab-content">

                        <div class="tab-pane active" id="descripcion">
                            <?php print render($content['body']); ?>
                        </div>

                        <div class="tab-pane " id="director">
                            <a href="<?php echo url("node/" . $node->field_director["und"][0]["entity"]->nid);
                            ?>">
                                   <?php
                                   print("<h3>");
                                   print($node->field_director["und"][0]["entity"]->title);
                                   print(", ");
                                   print($node->field_director["und"][0]["entity"]->field_nivel_academico["und"][0]["value"]);
                                   print("</h3>");
                                   //@TODO: incluir redes sociales
                                   ?>
                            </a>
                            <div class="row ">
                                <div class="col-md-6">
                                    <div>
                                        <?php echo theme('image_style', array('style_name' => 'staff', 'path' => $node->field_director["und"][0]["entity"]->field_staffs_image['und'][0]['uri'], 'attributes' => array('class' => 'img-responsive'))); ?> 
                                        <?php print($node->field_director["und"][0]["entity"]->field_social["und"][0]["safe_value"]); ?>
                                    </div>
                                </div>

                                <div class="col-md-6">
                                    <div class="pull-left">
                                        <dl>  
                                            <?php if (strlen($node->field_director["und"][0]["entity"]->field_enlace_al_cvlac["und"][0]['url']) > 1) { ?>
                                                <dt>CvLAC:</dt>
                                                <dd><a target="_blank" href="<?php print($node->field_director["und"][0]["entity"]->field_enlace_al_cvlac["und"][0]['url']) ?>"><?php print(" " . $node->field_director["und"][0]["entity"]->field_enlace_al_cvlac["und"][0]['title']); ?></a></dd>
                                            <?php } ?>                    
                                            <?php if ($node->field_director["und"][0]["entity"]->field_escalafon['und'][0]["value"] <> "No escalafonado") { ?>                       
                                                <dt>Escalafón: </dt> 
                                                <dd> <?php print(" " . $node->field_director["und"][0]["entity"]->field_escalafon['und'][0]["value"]); ?></dd>
                                            <?php } ?>
                                            <?php if (strlen($node->field_director["und"][0]["entity"]->field_mail["und"][0]["safe_value"]) > 1) { ?>   
                                                <dt>Correo electrónico:</dt><dd><small><?php
                                                        print(" " . $node->field_director["und"][0]["entity"]->field_mail["und"][0]["safe_value"]);
                                                        ?></small></dd>
                                            <?php } ?>
                                            <?php if (strlen($node->field_director["und"][0]["entity"]->field_telefono["und"][0]["safe_value"]) > 1) { ?>
                                                <dt>Teléfono:</dt><dd><?php
                                                    print(" " . $node->field_director["und"][0]["entity"]->field_telefono["und"][0]["safe_value"]);
                                                    ?></dd>
                                            <?php } ?>
                                            <?php if (strlen($node->field_director["und"][0]["entity"]->field_direccion["und"][0]["safe_value"]) > 1) { ?>
                                                <dt>Dirección de Oficina:</dt>
                                                <dd><?php print($node->field_director["und"][0]["entity"]->field_direccion["und"][0]["safe_value"]); ?></dd>
                                            <?php } ?>
                                            <?php if (strlen($node->field_director["und"][0]["entity"]->field_web_page["und"][0]['url']) > 1) { ?>
                                                <dt>Web Personal:</dt>
                                                <dd><a target="_blank" href="<?php print($node->field_director["und"][0]["entity"]->field_web_page["und"][0]['url']) ?>"><?php print(" " . $node->field_director["und"][0]["entity"]->field_web_page["und"][0]['title']); ?></a></dd>                                      
                                            <?php } ?>
                                        </dl>
                                    </div>
                                </div>
                            </div>

                        </div>


                        <div class="tab-pane" id="jefatura">

                            <h3>Jefatura</h3> 
                            <ul class="nav list-group" data-tabs="tabs">                       
                                <?php
                                for ($i = 0;; $i++) {
                                    $ID = $node->field_jefatura["und"][$i]["entity"]->nid;

                                    if ($ID == 0 or strcmp("0", $node->status) == 0) {
                                        break;
                                    }
                                    if ($node->field_jefatura["und"][$i]["entity"]->status > 0) {
                                        $alias = drupal_get_path_alias('node/' . $ID);
                                        ?>
                                        <li class="list-group-item" >
                                            <a href="<?php print($alias); ?>">
                                                <div> 
                                                    <?php print($node->field_jefatura["und"][$i]["entity"]->title); ?> 
                                                </div>
                                            </a>
                                        </li>

                                        <?php
                                    }
                                }
                                ?>
                            </ul>

                        </div>

                        <div class="tab-pane" id="coordinacion">

                            <h3>Jefatura</h3> 
                            <ul class="nav list-group" data-tabs="tabs">                       
                                <?php
                                for ($i = 0;; $i++) {
                                    $ID = $node->field_coordinacion["und"][$i]["entity"]->nid;

                                    if ($ID == 0 or strcmp("0", $node->status) == 0) {
                                        break;
                                    }
                                    if ($node->field_coordinacion["und"][$i]["entity"]->status > 0) {
                                        $alias = drupal_get_path_alias('node/' . $ID);
                                        ?>
                                        <li class="list-group-item" >
                                            <a href="<?php print($alias); ?>">
                                                <div> 
                                                    <?php print($node->field_coordinacion["und"][$i]["entity"]->title); ?> 
                                                </div>
                                            </a>
                                        </li>

                                        <?php
                                    }
                                }
                                ?>
                            </ul>

                        </div>                       

                        <div class="tab-pane" id="equipo_humano">
                            <?php if ($node->field_equipo_humano["und"][0]["entity"]->nid) { ?>
                                <h3>Profesores de tiempo completo</h3>
                                <div class="programa_profesor_tc">
                                    <ul class="nav nav-tabs prog" data-tabs="tabs">                       
                                        <?php
                                        for ($i = 0;
                                        ; $i++) {
                                            $ID = $node->field_equipo_humano["und"][$i]["entity"]->nid;
                                            if ($ID == 0) {
                                                break;
                                            }
                                            $alias = drupal_get_path_alias('node/' . $ID);
                                            if (strcmp("0", $node->field_equipo_humano["und"][$i]["entity"]->status) != 0) {
                                                ?>
                                                <div class="col-md-6 thumbnail" style="height: 270px; font-size: 13px;">      
                                                    <a href="<?php print($alias); ?>">                                                
                                                        <?php print($node->field_equipo_humano["und"][$i]["entity"]->title . ", "); ?><?php print($node->field_equipo_humano["und"][$i]["entity"]->field_nivel_academico["und"][0]["value"]); ?>   
                                                    </a><br/><br/>
                                                    <div class="col-md-5">
                                                        <div style="margin-bottom: 10px;"> <?php echo theme('image_style', array('style_name' => 'thumbnail', 'path' => $node->field_equipo_humano["und"][$i]["entity"]->field_staffs_image['und'][0]['uri'], 'attributes' => array('class' => 'img-responsive'))); ?>        </div>                            
                                                        <div class="portada_profesor"> <?php print($node->field_equipo_humano["und"][$i]["entity"]->field_social["und"][0]["safe_value"]); ?> </div>
                                                    </div>

                                                    <div class="col-md-7">
                                                        <dl>  
                                                            <?php if (strlen($node->field_equipo_humano["und"][$i]["entity"]->field_enlace_al_cvlac["und"][0]['url']) > 1) { ?>
                                                                <dt>CvLAC:</dt>
                                                                <dd><a target="_blank" href="<?php print($node->field_equipo_humano["und"][$i]["entity"]->field_enlace_al_cvlac["und"][0]['url']) ?>"><?php print(" " . $node->field_equipo_humano["und"][$i]["entity"]->field_enlace_al_cvlac["und"][0]['title']); ?></a></dd>
                                                            <?php } ?>                    
                                                            <?php if ($node->field_equipo_humano["und"][$i]["entity"]->field_escalafon['und'][0]["value"] <> "No escalafonado") { ?>                       
                                                                <dt>Escalafón: </dt> 
                                                                <dd> <?php print(" " . $node->field_equipo_humano["und"][$i]["entity"]->field_escalafon['und'][0]["value"]); ?></dd>
                                                            <?php } ?>
                                                            <?php if (strlen($node->field_equipo_humano["und"][$i]["entity"]->field_mail["und"][0]["safe_value"]) > 1) { ?>   
                                                                <dt>Correo electrónico:</dt><dd><small><?php
                                                                        print(" " . $node->field_equipo_humano["und"][$i]["entity"]->field_mail["und"][0]["safe_value"]);
                                                                        ?></small></dd>
                                                            <?php } ?>
                                                            <?php if (strlen($node->field_equipo_humano["und"][$i]["entity"]->field_telefono["und"][0]["safe_value"]) > 1) { ?>
                                                                <dt>Teléfono:</dt><dd><?php
                                                                    print(" " . $node->field_equipo_humano["und"][$i]["entity"]->field_telefono["und"][0]["safe_value"]);
                                                                    ?></dd>
                                                            <?php } ?>
                                                            <?php if (strlen($node->field_equipo_humano["und"][$i]["entity"]->field_direccion["und"][0]["safe_value"]) > 1) { ?>
                                                                <dt>Dirección de Oficina:</dt>
                                                                <dd><?php print($node->field_equipo_humano["und"][$i]["entity"]->field_direccion["und"][0]["safe_value"]); ?></dd>
                                                            <?php } ?></dl>

                                                    </div>
                                                </div>  

                                                <?php
                                            }
                                        }
                                    }
                                    ?>

                            </div>
                        </div>
                                                
                        <div class="tab-pane" id="contacto">
                            <?php print($node->field_contacto["und"][0]["safe_value"]); ?>
                        </div>

                    </div>	  

                </div>

            </div>

        </div>

    </div>

</div>

</div>