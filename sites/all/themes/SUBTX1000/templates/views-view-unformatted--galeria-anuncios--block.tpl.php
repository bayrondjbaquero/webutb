<?php
/**
 * @file
 * Default simple view template to display a list of rows.
 *
 * @ingroup views_templates
 */
?>   


<div id="carousel-anuncio" class="carousel slide" data-ride="carousel">

    <div class="carousel-inner">
        <?php $i = 0; ?>
        <?php
        foreach ($rows as $id => $row) {
            if ($i == 0) {
                ?> <div class="item active"> <?php } else { ?>
                    <div class="item">
                    <?php } $i++; ?>

                    <div<?php
                    if ($classes_array[$id]) {
                        print ' class="' . $classes_array[$id] . '"';
                    }
                    ?>>
                            <?php print $row; ?>
                    </div>  
                </div>  
            <?php } ?>        

        </div>

        <div class="text-center">

            <a class="" href="javascript:void(0)" 
               data-slide="prev" data-target="#carousel-anuncio">
                <span class="glyphicon glyphicon-chevron-left"></span>
            </a>
            
            
             <?php
            foreach ($rows as $id => $row) {
                if ($i == 0) {
                    ?>  <li href="javascript:void(0)" class="indicators active" data-target="#carousel-anuncio" data-slide-to="<?php print $i; ?>" > <?php } else { ?>
                        <li href="javascript:void(0)" class="indicators" data-target="#carousel-anuncio" data-slide-to="<?php print $i;?>" >
                        <?php } $i++; ?>
                        </li>
                <?php } ?>  
            
            <a class="" href="javascript:void(0)" 
               data-slide="next" data-target="#carousel-anuncio">
                <span class="glyphicon glyphicon-chevron-right"></span>
            </a>

        </div>
    </div>

