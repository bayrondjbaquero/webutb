<?php
$entity = node_load($fields['nid']->raw);
$uriImagefromNode = $entity->field_carrousel["und"][0]['uri'];
if ($uriImagefromNode) {
    $urlImagefromNode = file_create_url($uriImagefromNode);
} else {
    $uriImagefromNode = $entity->field_image["und"][0]['uri'];
    $urlImagefromNode = file_create_url($uriImagefromNode);
}
$alias = drupal_get_path_alias('node/' . $entity->nid);
$formatted_date = format_date($entity->created, 'custom', 'F Y');


$titulo = substr($entity->title, 0, 50);

$dia = format_date($entity->created, 'custom', 'j');

if (!$uriImagefromNode) {
    $urlImagefromNode = "http://www.unitecnologica.edu.co/newsletter/Conectate/imagen-mailing/utb1.jpg";
}
?>

<div class="col-md-3">
    <div class=" thumbnail padding-s"  style="height: 260px; width:296px ;" >
        <div><h5><?php print $titulo." ..."; ?></h5></div>
        <div  style="max-height: 150px; min-height: 150px;" ><a href="<?php print $alias; ?>"><img style="height: 150px; width:250px ;" class="img-rounded img-responsive center-block" src="<?php print $urlImagefromNode; ?>" /></a></div>
        <hr>
        <div><p><em><strong><?php print $dia . " de " . $formatted_date; ?></em> | <a href="<?php print $alias; ?>"> Ver más </a></p></div>
        <hr>
    </div>
</div>