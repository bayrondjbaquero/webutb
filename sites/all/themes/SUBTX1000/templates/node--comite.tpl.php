<?php if (!empty($body)): ?>

    <article id="node-<?php print $node->nid; ?>" class="<?php print $classes; ?> clearfix"<?php print $attributes; ?>> <?php print $user_picture; ?> <?php print render($title_prefix); ?>

    </div>
<?php endif; ?>        
<div class="header article-header">  
    <div class="row clearfix" style="margin-bottom: 30px;">

        <div class="col-md-12 column">
            <?php print $node->body['und'][0]['safe_value']; ?>                            
            <hr>
            <div class="row">
                <!-- Navigation Buttons -->
                <div class="col-md-2">

                    <ul class="nav nav-pills nav-stacked" id="Comite">                                        

                        <li class="active"><a href="#superior"  data-toggle="tab">Consejo Superior</a></li>

                        <li><a href="#academico" data-toggle="tab">Consejo Academico</a></li>   

                        <li><a href="#administrativo" data-toggle="tab">Consejo Administrativo</a></li>	

                    </ul>      

                </div>

                <!-- Content -->
                <div class="col-md-10 padding-x linea-caja-left caja-sombra efecto-sombra-2">

                    <div class="tab-content">

                        <div class="tab-pane active" id="superior">
                            <?php print $node->field_contenido_superior['und'][0]['safe_value']; ?>     
                            <hr>
                            <?php
                            for ($i = 0;; $i++) {
                                $ID = $node->field_superior["und"][$i]["entity"]->nid;

                                if ($ID == 0 or strcmp("0", $node->status) == 0) {
                                    break;
                                }

                                if (isset($node->field_superior["und"][$i]["entity"]->field_codigo_programas["und"][0]["value"])) {
                                    db_set_active('programas');
                                    $integrante = node_load($node->field_superior["und"][$i]["entity"]->field_codigo_programas["und"][0]["value"]);
                                    $uriImagefromNode = $integrante->field_staffs_image["und"][0]["uri"];
                                    $semi_url = str_replace("public://", "", $uriImagefromNode);
                                    $URL = "http://programas.unitecnologica.edu.co/sites/programas.unitecnologica.edu.co/files/" . $semi_url;
                                    db_set_active();
                                }
                                if ($node->field_superior["und"][$i]["entity"]->field_staffs_image["und"][0]["uri"]) {

                                    $uriImagefromNode = $node->field_superior["und"][$i]["entity"]->field_staffs_image["und"][0]["uri"];
                                    $urlImagefromNode = file_create_url($uriImagefromNode);
                                    $URL = file_create_url($uriImagefromNode);
                                }
                                ?>

                                <div class="col-md-6 col-xs-12 col-lg-3">

                                    <div class="thumbnail" style="min-height: 350px; ">
                                        <img class="margen-m img-responsive "  src="<?php
                                        print $URL;
                                        $URL = null;
                                        ?>">

                                        <dl>
                                            <dt>Nombre: </dt>
                                            <?php if (isset($node->field_superior["und"][$i]["entity"]->field_codigo_programas["und"][0]["value"])) { ?>
                                                <dd><a href="<?php print "http://programas.unitecnologica.edu.co/node/" . $integrante->nid;
                                        $integrante = null; ?>"><?php print $node->field_superior["und"][$i]["entity"]->title; ?></a></dd>
                                            <?php } else { ?>
                                                <dd><?php print $node->field_superior["und"][$i]["entity"]->title; ?></dd>
    <?php } ?>
                                            <dt><span class="glyphicon glyphicon-lock"></span> Cargo: </dt>
                                            	
                                            <dt><i class="fa fa-building-o"></i> Entidad: </dt>
                                            <dd><?php print $node->field_superior["und"][$i]["entity"]->field_representacion["und"][0]["value"]; ?></dd>	
                                        </dl>
                                    </div>	

                                </div>

                                <?php
                            }
                            ?>
                        </div>

                        <div class="tab-pane" id="academico">

                            <?php print $node->field_contenido_academico['und'][0]['safe_value']; ?>     
                            <hr>
                            <?php
                            for ($i = 0;; $i++) {
                                $ID = $node->field_academico["und"][$i]["entity"]->nid;
                                if ($ID == 0 or strcmp("0", $node->status) == 0) {
                                    break;
                                }

                                if (isset($node->field_academico["und"][$i]["entity"]->field_codigo_programas["und"][0]["value"])) {
                                    db_set_active('programas');
                                    $integrante = node_load($node->field_academico["und"][$i]["entity"]->field_codigo_programas["und"][0]["value"]);
                                    $uriImagefromNode = $integrante->field_staffs_image["und"][0]["uri"];
                                    $semi_url = str_replace("public://", "", $uriImagefromNode);
                                    $URL = "http://programas.unitecnologica.edu.co/sites/programas.unitecnologica.edu.co/files/" . $semi_url;
                                    db_set_active();
                                }
                                if ($node->field_academico["und"][$i]["entity"]->field_staffs_image["und"][0]["uri"]) {

                                    $uriImagefromNode = $node->field_academico["und"][$i]["entity"]->field_staffs_image["und"][0]["uri"];
                                    $urlImagefromNode = file_create_url($uriImagefromNode);
                                    $URL = file_create_url($uriImagefromNode);
                                }
                                ?>

                                <div class="col-md-6 col-xs-12 col-lg-3">

                                    <div class="thumbnail" style="min-height: 350px; ">
                                        <img class="margen-m img-responsive "  src="<?php
                                        print $URL;
                                        $URL = null;
                                        ?>">

                                        <dl>
                                            <dt>Nombre: </dt>
                                            <?php if (isset($node->field_academico["und"][$i]["entity"]->field_codigo_programas["und"][0]["value"])) { ?>
                                                <dd><a href="<?php print "http://programas.unitecnologica.edu.co/node/" . $integrante->nid;
                                                $integrante = null; ?>"><?php print $node->field_academico["und"][$i]["entity"]->title; ?></a></dd>
                                            <?php } else { ?>
                                                <dd><?php print $node->field_academico["und"][$i]["entity"]->title; ?></dd>
    <?php } ?>
                                            <dt><span class="glyphicon glyphicon-lock"></span> Cargo: </dt>
                                            <dt><i class="fa fa-building-o"></i> Entidad: </dt>
                                            <dd><?php print $node->field_academico["und"][$i]["entity"]->field_representacion["und"][0]["value"]; ?></dd>	
                                        </dl>
                                    </div>	

                                </div>

                                <?php
                            }
                            ?>

                        </div>	  

                        <div class="tab-pane" id="administrativo">      

                            <?php print $node->field_contenido_administrativo['und'][0]['safe_value']; ?>     
                            <hr>
                            <?php
                            for ($i = 0;; $i++) {
                                $ID = $node->field_administrativo["und"][$i]["entity"]->nid;
                                if ($ID == 0 or strcmp("0", $node->status) == 0) {
                                    break;
                                }

                                if (isset($node->field_administrativo["und"][$i]["entity"]->field_codigo_programas["und"][0]["value"])) {
                                    db_set_active('programas');
                                    $integrante = node_load($node->field_administrativo["und"][$i]["entity"]->field_codigo_programas["und"][0]["value"]);
                                    $uriImagefromNode = $integrante->field_staffs_image["und"][0]["uri"];
                                    $semi_url = str_replace("public://", "", $uriImagefromNode);
                                    $URL = "http://programas.unitecnologica.edu.co/sites/programas.unitecnologica.edu.co/files/" . $semi_url;
                                    db_set_active();
                                }
                                if ($node->field_administrativo["und"][$i]["entity"]->field_staffs_image["und"][0]["uri"]) {

                                    $uriImagefromNode = $node->field_administrativo["und"][$i]["entity"]->field_staffs_image["und"][0]["uri"];
                                    $urlImagefromNode = file_create_url($uriImagefromNode);
                                    $URL = file_create_url($uriImagefromNode);
                                }
                                ?>

                                <div class="col-md-6 col-xs-12 col-lg-3">

                                    <div class="thumbnail" style="min-height: 350px; ">
                                        <img class="margen-m img-responsive "  src="<?php
                                             print $URL;
                                             $URL = null;
                                             ?>">

                                        <dl>
                                            <dt>Nombre: </dt>
                                            <?php if (isset($node->field_administrativo["und"][$i]["entity"]->field_codigo_programas["und"][0]["value"])) { ?>
                                                <dd><a href="<?php print "http://programas.unitecnologica.edu.co/node/" . $integrante->nid;
                                        $integrante = null; ?>"><?php print $node->field_administrativo["und"][$i]["entity"]->title; ?></a></dd>
    <?php } else { ?>
                                                <dd><?php print $node->field_administrativo["und"][$i]["entity"]->title; ?></dd>
    <?php } ?>
                                            <dt><span class="glyphicon glyphicon-lock"></span> Cargo: </dt>
                                            <dt><i class="fa fa-building-o"></i> Entidad: </dt>
                                            <dd><?php print $node->field_administrativo["und"][$i]["entity"]->field_representacion["und"][0]["value"]; ?></dd>	
                                        </dl>
                                    </div>	

                                </div>

    <?php
}
?>

                        </div>

                    </div>

                </div>

            </div>

        </div>

    </div>

</div>

</article>