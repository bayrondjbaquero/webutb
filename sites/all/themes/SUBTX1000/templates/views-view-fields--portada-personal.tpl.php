 <div class="seccion_noticia">
			<div class="titulo_noticia">
				<?php print $fields['title']->content; ?>
			</div>
			<div class="postcard-left">
				<div class="span5">
					<?php print $fields['field_staffs_image']->content; ?>
				</div>
				<div class="span7 resumen_noticia">
					<?php 
						print $fields['body']->content; 
					?>
					<!--
					<?php if ($fields['view_node']): ?>
						
					<?php endif; ?>
					-->
					<div class="link_leer_mas_noticias">
						<?php print $fields['view_node']->content; ?>
					</div>
				</div>
				
			</div>
        </div>
