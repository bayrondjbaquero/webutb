<?php
$pasos = array();
for ($i = 0;; $i++) :
    $ID = $node->field_pasos["und"][$i]["entity"]->nid;


    if ($ID == 0 or strcmp("0", $node->status) == 0) :
        break;
    endif;

    if ($node->field_pasos["und"][$i]["entity"]->status > 0) :
        $paso = $node->field_pasos["und"][$i]["entity"];
        array_push($pasos, $paso);

    endif;
endfor;
?>
<div class="clearfix"><?php print $node->body["und"][0]["safe_value"]; ?></div>
<br>
<!--Numeros paso a paso-->

<?php if (strcmp($node->field_estilo["und"][0]["value"], "Numero") == 0): ?>

    <div class="col-md-12 jumbo">

        <!-- Nav tabs -->

        <ul class="nav nav-pills" role="tablist">
            <?php for ($i = 0; $i < count($pasos); $i++): ?>
                <?php if ($i == 0): ?>
                    <li class="active">
                    <?php else: ?>            
                    <li>
                    <?php endif; ?>
                    <a href="#paso<?php print $node->nid.$i; ?>" role="tab" data-toggle="pill"><?php print $i + 1; ?></a>
                </li>
            <?php endfor; ?>

        </ul>

        <!-- Tab panes -->
        <div class="tab-content padding-x linea-caja-top">
            <?php for ($z = 0; $z < count($pasos); $z++): ?>
                <?php if ($z == 0): ?>
                    <div class="tab-pane active" id="paso<?php print $node->nid.$z; ?>">
                    <?php else: ?>      
                        <div class="tab-pane" id="paso<?php print $node->nid.$z; ?>">   
                        <?php endif; ?>

                        <?php print $pasos[$z]->body["und"][0]["safe_value"]; ?>

                    </div>
                <?php endfor; ?>

            </div>                

        </div>
    <?php endif; ?>

    <!--    Texto Paso a Paso-->
    <?php if (strcmp($node->field_estilo["und"][0]["value"], "Texto") == 0): ?>       

        <div class="col-md-12 jumbo">

            <!-- Nav tabs -->
            <ul class="nav nav-tabs" role="tablist">

                <?php for ($i = 0; $i < count($pasos); $i++): ?>
                    <?php if ($i == 0): ?>
                        <li class="active" >
                        <?php else: ?>            
                        <li>
                        <?php endif; ?>
                        <a href="#paso<?php print $node->nid.$i; ?>" role="tab" data-toggle="tab"><?php print $pasos[$i]->field_titulo_pestana["und"][0]["value"]; ?></a>
                    </li>
                <?php endfor; ?>


            </ul>

            <!-- Tab panes -->
            <div class="tab-content padding-x linea-caja-bottom">

                <?php for ($z = 0; $z < count($pasos); $z++): ?>
                    <?php if ($z == 0): ?>
                        <div class="tab-pane active" id="paso<?php print $node->nid.$z; ?>">
                        <?php else: ?>      
                            <div class="tab-pane" id="paso<?php print $node->nid.$z; ?>">   
                            <?php endif; ?>

                            <?php print $pasos[$z]->body["und"][0]["safe_value"]; ?>

                        </div>
                    <?php endfor; ?>
                </div>
            </div>
        <?php endif; ?>


        <!--    Pasos Paso a Paso-->
        <?php if (strcmp($node->field_estilo["und"][0]["value"], "Pasos") == 0): ?>

            <div class="col-md-12 jumbo">

                <div class="">

                    <!-- Nav tabs -->

                    <ul class="nav nav-pills" role="tablist">
                        <?php for ($i = 0; $i < count($pasos); $i++): ?>
                            <?php if ($i == 0): ?>
                                <li class="active" >
                                <?php else: ?>            
                                <li>
                                <?php
                                endif;
                                $a = $i + 1;
                                ?>
                                <a href="#paso<?php print $node->nid.$i; ?>" role="tab" data-toggle="pill"><?php print "Paso " . $a; ?></a>
                            </li>
                        <?php endfor; ?>

                    </ul>

                </div>

                <!-- Tab panes -->
                <div class="tab-content padding-x linea-caja-top">

                    <?php for ($z = 0; $z < count($pasos); $z++): ?>
                        <?php if ($z == 0): ?>
                            <div class="tab-pane fade in active" id="paso<?php print $node->nid.$z; ?>">
                            <?php else: ?>      
                                <div class="tab-pane fade" id="paso<?php print $node->nid.$z; ?>">   
                                <?php endif; ?>

                                <?php print $pasos[$z]->body["und"][0]["safe_value"]; ?>

                            </div>
                        <?php endfor; ?>


                    </div>                

                </div>

            <?php endif; ?>


            <?php if (strcmp($node->field_estilo["und"][0]["value"], "Icono") == 0): ?>


                <div class="col-md-12 jumbo">

                    <!-- Nav tabs -->
                    <ul class="nav nav-pills" role="tablist">

                        <?php for ($i = 0; $i < count($pasos); $i++): ?>
                            <?php if ($i == 0): ?>
                                <li class="active" >
                                <?php else: ?>            
                                <li>
                                <?php
                                endif;
                                $a = $i + 1;
                                ?>
                                <a href="#paso<?php print $node->nid.$i; ?>" role="tab" data-toggle="pill"><?php print "<span class='fa fa-4x " . $pasos[$i]->field_icono["und"][0]["safe_value"] . "'></span>"; ?></a>
                            </li>
                        <?php endfor; ?>

                    </ul>

                    <!-- Tab panes -->
                    <div class="tab-content padding-x linea-caja-top">
                        <?php for ($z = 0; $z < count($pasos); $z++): ?>
                            <?php if ($z == 0): ?>
                                <div class="tab-pane active"  id="paso<?php print $node->nid.$z; ?>">
                                <?php else: ?>      
                                    <div class="tab-pane" id="paso<?php print $node->nid.$z; ?>">   
                                    <?php endif; ?>

                                    <?php print $pasos[$z]->body["und"][0]["safe_value"]; ?>

                                </div>
                            <?php endfor; ?>


                        </div>

                    </div>

                <?php endif; ?>


                <?php if (strcmp($node->field_estilo["und"][0]["value"], "Combinado") == 0): ?>
                    <div class="col-md-12 jumbo">

                        <!-- Nav tabs -->

                        <ul class="nav nav-pills text-center" role="tablist">

                            <?php for ($i = 0; $i < count($pasos); $i++): ?>
                                <?php if ($i == 0): ?>
                                    <li class="active" >
                                    <?php else: ?>            
                                    <li>
                                    <?php
                                    endif;
                                    $a = $i + 1;
                                    ?>
                                    <a href="#paso<?php print $node->nid.$i; ?>" role="tab" data-toggle="pill"><?php print "<span class='fa fa-4x " . $pasos[$i]->field_icono["und"][0]["safe_value"] . "'></span>"; ?> <br />  <span class="small"><?php print $pasos[$i]->field_titulo_pestana["und"][0]["value"]; ?></span></a>
                                </li>
                            <?php endfor; ?>   




                        </ul>


                        <!-- Tab panes -->
                        <div class="tab-content padding-x linea-caja-top">

                            <?php for ($z = 0; $z < count($pasos); $z++): ?>
                                <?php if ($z == 0): ?>
                                    <div class="tab-pane active" id="paso<?php print $node->nid.$z; ?>">
                                    <?php else: ?>      
                                        <div class="tab-pane" id="paso<?php print $node->nid.$z; ?>">   
                                        <?php endif; ?>

                                        <?php print $pasos[$z]->body["und"][0]["safe_value"]; ?>

                                    </div>
                                <?php endfor; ?>

                            </div>                

                        </div>
                    <?php endif; ?>      

                    <br>
                    <div class="clearfix"><?php print $node->field_pie["und"][0]["safe_value"]; ?></div>