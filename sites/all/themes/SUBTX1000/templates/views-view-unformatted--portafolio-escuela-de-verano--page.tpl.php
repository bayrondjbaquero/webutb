<?php
/**
 * @file
 * Default simple view template to display a list of rows.
 *
 * @ingroup views_templates
 */
?>   

<div class="col-md-12">
    <hr>

     <div id="filters" class="jumbo-ternera img-rounded padding-x">
        <div class="button-group" data-filter-group="Escuela_Verano">
            <div class="row">        
                <div class="col-md-1"><button class="btn margen-m btn-success3d btn-lg" data-filter="*">Todos</button></div>
                <div class="col-md-11">
                    <button class="btn margen-m btn-default3d" data-filter=".34"> ESTUDIANTES DE PREGRADO Y POSGRADO </button>
                    <button class="btn margen-m btn-default3d" data-filter=".15"> PUBLICO GENERAL </button>
                    <button class="btn margen-m btn-default3d" data-filter=".31"> NIÑOS, ADOLESCENTES Y JÓVENES </button>
                    <button class="btn margen-m btn-default3d" data-filter=".47"> PROFESORES Y PROFESIONALES EN CIENCIAS BÁSICAS </button>
                </div>
            </div>
        </div>
    </div>
    <hr>
    <div class="isotope">   

        <?php
        foreach ($rows as $id => $row) {
            print $row;
        }
        ?>   

    </div>
</div>