<div id="node-<?php print $node->nid; ?>" class="<?php print $classes; ?> clearfix"<?php print $attributes; ?>>
   
    <div class="row clearfix">

        <div class="col-md-12 column">
            <?php print render($content['body']); ?>
            <?php print render($content['field_videos']); ?>  
            <?php print render($content['field_tags']); ?>  
            <?php print render($content['field_video_testimonio']); ?>  
        </div>
    </div>
</div>