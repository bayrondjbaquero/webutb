<div class="col-md-12">
    <?php
    print $node->body["und"][0]["safe_value"];
    print "<hr>";
    $node_type = $node->field_machine_name_tp["und"][0]["value"]; // can find this on the node type's "edit" screen in the Drupal admin section.
    $result = db_query("SELECT nid, title FROM {node} WHERE type = :type", array(
        ':type' => $node_type,
    ));
    $nodos = array();
    $campo = $node->field_nombre_campo_tc["und"][0]["value"]
    ?>


    <div id="filters" class="jumbo-ternera img-rounded padding-x">
        <div class="button-group" data-filter-group="<?php print $node_type; ?>">
            <div class="row">        
                <div class="col-md-1"><button class="btn margen-m btn-success3d btn-lg" data-filter="*">Todos</button></div>
                <div class="col-md-11">
                    <?php
                    while ($row = $result->fetchAssoc()) {
                        $entity = node_load($row['nid']);
                        if ($entity->{'field_' . $campo}["und"][0]["target_id"]) {
                            ?>
                            <button class="btn margen-m btn-default3d" data-filter="<?php print "." . $entity->nid ?>"><?php print $entity->title ?></button>
                            <?php
                        }
                        array_push($nodos, $entity);
                    }
                    ?>
                </div>
            </div>
        </div>
    </div>
    <hr>
    <div class="isotope row">   
        <?php
        for ($i = 0; $i < count($nodos); $i++) {
            for ($z = 0;; $z++) {
                $ID = $nodos[$i]->{'field_' . $campo}["und"][$z]["target_id"];
                if ($ID == 0) {
                    break;
                }
                ?>
                <div class="col-md-6 col-lg-3 thumbnail  element-item <?php print $nodos[$i]->nid ?>" data-category="<?php print $nodos[$i]->nid ?>">
                    <?php
                    $temp = node_load($nodos[$i]->{'field_' . $campo}["und"][$z]["target_id"]);
                    $ID = $nodos[$i]->{'field_' . $campo}["und"][$z]["target_id"];
                    $alias = drupal_get_path_alias('node/' . $ID);
                    ?>
                    <a href="<?php print $alias ?>" title="<?php print $temp->title; ?>">
                        <p class="text-primary filtro-texto"><?php print $temp->title; ?></p>

                        <?php
                        $uriImagefromNode = $temp->field_filtro_imagen['und'][0]['uri'];
                        $urlImagefromNode = file_create_url($uriImagefromNode);
                        ?>
                        <img alt="<?php print $temp->field_filtro_imagen['und'][0]['alt']; ?>" src="<?php print $urlImagefromNode; ?>" class="img-responsive padding-s"/>

                        <hr>
                        <p style="height: 10px;" >
                            <?php
                            if (isset($temp->field_nuevo)):
                                if (strcmp($temp->field_nuevo["und"][0]["value"], '1') == 0):
                                    ?>
                                    <span class="badge">Nuevo</span>    
                                <?php endif; ?>  <?php endif; ?>

                            <?php
                            if (isset($temp->field_inscripciones)):
                                if (strcmp($temp->field_inscripciones["und"][0]["value"], '1') == 0):
                                    ?>
                                    <span class="badge badge-success">Inscripciones abiertas</span>    
                                <?php endif; ?>  <?php endif; ?>



                            <?php
                            if (isset($temp->field_preinscripciones)):
                                if (strcmp($temp->field_preinscripciones["und"][0]["value"], '1') == 0):
                                    ?>
                                    <span class="badge badge-info">Preinscripciones</span>    
                                <?php endif; ?>  <?php endif; ?>
                        </p>
                    </a>
                </div>
                <?php
            }
        }
        ?>
    </div>
</div>