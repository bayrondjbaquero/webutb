<?php
/**
 * @file
 * Default simple view template to display a list of rows.
 *
 * @ingroup views_templates
 */
?>

<div id="carousel-quote" class="carousel slide"  data-ride="carousel">

    <div class="carousel-inner">

        <?php $i = 0; ?>
        <?php
        foreach ($rows as $id => $row) {
            if ($i == 0) {
                ?> <div class="item active"> <?php } else { ?>
                    <div class="item">
                    <?php } $i++; ?>

                    <div<?php
                    if ($classes_array[$id]) {
                        print ' class="' . $classes_array[$id] . '"';
                    }
                    ?>>
                            <?php print $row; ?>
                    </div>  
                </div>  
            <?php } ?>


        </div> 


        <div class="col-md-2 col-md-offset-5">

                <a class="left pull-left" href="javascript:void(0)" 
       data-slide="prev" data-target="#carousel-quote">
                <span class="glyphicon glyphicon-chevron-left"></span>
            </a> 							

                <a class="right pull-right" href="javascript:void(0)" 
       data-slide="next" data-target="#carousel-quote">
                <span class="glyphicon glyphicon-chevron-right"></span>
            </a>

        </div>   

    </div>