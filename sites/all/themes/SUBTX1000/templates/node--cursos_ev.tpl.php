<div id="node-<?php print $node->nid; ?>" class="<?php print $classes; ?> clearfix"<?php print $attributes; ?>>

    <div class="row clearfix">

        <div class="col-md-12 column">

            <?php print $node->body['und'][0]['safe_summary']; ?>                            
            <hr>
            <div class="row">
                 <div class="col-md-12">
                    
                    <?php print $node->field_objetivo["und"][0]["safe_value"]; ?>

                </div>

                <div class="row clearfix">

                    <div class="col-md-12 column" style="padding: 20px;">

                        <!-- Navigation Buttons -->
                        <div class="col-md-12">

                            <ul class="nav nav-tabs" id="Cursos">                                        

                                <?php if ($node->body["und"][0]["safe_value"]) { ?> 
                                    <li class="active"><a href="#descripcion"  data-toggle="tab">Descripción</a></li><?php } ?>

                                <?php if ($node->field_contenido["und"][0]["safe_value"]) { ?> 
                                    <li><a href="#malla" data-toggle="tab">Contenido</a></li><?php } ?>                          	

                                <?php if ($node->field_docentes["und"][0]["safe_value"]) { ?> 
                                    <li><a href="#profesores" data-toggle="tab">Profesores</a></li><?php } ?> 
                                <?php if ($node->field_informes["und"][0]["safe_value"]) { ?> 
                                    <li><a href="#contacto" data-toggle="tab">Contacto</a></li><?php } ?>

                            </ul>                                    
                        </div>

                        <div class="col-md-12  padding-x linea-caja-left caja-sombra efecto-sombra-2">

                            <div class="tab-content">


                                <div class="tab-pane active" id="descripcion">
                                    <?php print $node->body["und"][0]["safe_value"]; ?>
                                </div>
                                
                                <div class="tab-pane" id="malla">
                                    <?php print $node->field_contenido["und"][0]["safe_value"]; ?>
                                </div>

                                <div class="tab-pane" id="profesores">
                                    <?php print $node->field_docentes["und"][0]["safe_value"]; ?>
                                </div>

                                <div class="tab-pane" id="contacto">
                                    <?php print $node->field_informes["und"][0]["safe_value"]; ?>
                                </div>


                            </div>
                            
                        </div>


                    </div>

                </div>


            </div>

        </div>

    </div>

</div>


