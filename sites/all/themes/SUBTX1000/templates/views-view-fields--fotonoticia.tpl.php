<?php
$entity = node_load($fields['nid']->raw);
$uriImagefromNode = $entity->field_fotonoticia['und'][0]['uri'];
$urlImagefromNode = file_create_url($uriImagefromNode); 
?>
<a target="_blank" href="<?php print $entity->field_link['und'][0]['url']; ?>"><img alt="<?php print $entity->field_imagen['und'][0]['alt']; ?>" src="<?php  print $urlImagefromNode;?>" class="img-responsive img-rounded"/></a>
<?php if($entity->body):?>
 <div class="margen-s">
    <?php print $entity->body['und'][0]['safe_value']; ?> 
</div>
<?php endif;?>