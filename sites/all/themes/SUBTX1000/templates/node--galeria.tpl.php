<style> 
    #lightbox .modal-content {
        display: inline-block;
        text-align: center;   
    }

    #lightbox .close {
        opacity: 1;
        color: rgb(255, 255, 255);
        background-color: rgb(25, 25, 25);
        padding: 5px 8px;
        border-radius: 30px;
        border: 2px solid rgb(255, 255, 255);
        position: absolute;
        top: -15px;
        right: -55px;

        z-index:1032;
    }
</style>

<div id="node-<?php print $node->nid; ?>" class="<?php print $classes; ?> clearfix"<?php print $attributes; ?>>

    <div>
        <?php print $node->body['und'][0]['safe_value']; ?>     
    </div>

    <div class="row clearfix">
        <div class="col-md-12 grid">
            <?php
            for ($i = 0;; $i++) {
                if (isset($node->field_imagen['und'][$i]['uri'])) {
                    ?>

                    <div class="grid-item" style="width: 30%; padding: 2px;">

                        <?php
                        $uriImagefromNode = $node->field_imagen['und'][$i]['uri'];
                        $urlImagefromNode = file_create_url($uriImagefromNode);
                        ?>
                        <a href="#" data-toggle="modal" data-target="#lightbox">
                            <img alt="<?php print $node->title; ?>" src="<?php print $urlImagefromNode; ?>" class="img-responsive"/>
                        </a>
                    </div>

                    <?php
                } else {
                    break;
                }
            }
            ?>

        </div>        
    </div>
</div>


<div id="lightbox" class="modal fade" tabindex="-1" role="dialog" aria-labelledby="myLargeModalLabel" aria-hidden="true">
    <div class="modal-dialog">
        <button type="button" class="close hidden" data-dismiss="modal" aria-hidden="true">×</button>
        <div class="modal-content">
            <div class="modal-body">
                <img src="" alt="" />
            </div>
        </div>
    </div>
</div>




<script>
    jQuery(document).ready(function ($) {
        var $lightbox = $('#lightbox');

        $('[data-target="#lightbox"]').on('click', function (event) {
            var $img = $(this).find('img'),
                    src = $img.attr('src'),
                    alt = $img.attr('alt'),
                    css = {
                        'maxWidth': $(window).width() - 100,
                        'maxHeight': $(window).height() - 100
                    };

            $lightbox.find('.close').addClass('hidden');
            $lightbox.find('img').attr('src', src);
            $lightbox.find('img').attr('alt', alt);
            $lightbox.find('img').css(css);
        });

        $lightbox.on('shown.bs.modal', function (e) {
            var $img = $lightbox.find('img');

            $lightbox.find('.modal-dialog').css({'width': $img.width()});
            $lightbox.find('.close').removeClass('hidden');
        });
    });
</script>