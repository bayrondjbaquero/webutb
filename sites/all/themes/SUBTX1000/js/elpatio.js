jQuery(document).ready(function ($) {
    /*** effects for the proyect views in the Elpatio website ***/
    //$('div.animation-delay').animate({'opacity':'1'},2000);
    //see: http://jsfiddle.net/tcloninger/e5qad/

    //$('select').material_select();

    //$('#edit-submitted-area-del-proyecto-de-interes').material_select();

    $(window).scroll( function(){
      /* Check the location of each desired element */
      $('div.slideupin-animation').each( function(i){
        var bottom_of_object = $(this).offset().top + $(this).outerHeight();
        var bottom_of_window = $(window).scrollTop() + $(window).height();

        /* If the object is completely visible in the window, fade it it */
        if( bottom_of_window > (bottom_of_object-50) ){//-50 for execute the effect before
          var retardo = $(this).data('velocity-delay');
          retardo = retardo * 500;

          $(this).velocity("transition.slideUpIn").delay(retardo);
          $(this).removeClass('slideupin-animation');
        }
      });//end function for div.slideupin-animation CSS class

      $('div.slidedownin-animation').each( function(i){
        var bottom_of_object = $(this).offset().top + $(this).outerHeight();
        var bottom_of_window = $(window).scrollTop() + $(window).height();

        /* If the object is completely visible in the window, fade it it */
        if( bottom_of_window > (bottom_of_object-50) ){//-50 for execute the effect before
          var retardo = $(this).data('velocity-delay');
          if (!retardo){
            retardo = 750;
          }else{
            retardo = retardo * 500;
          }
          $(this).velocity("transition.slideDownIn").delay(retardo);
          $(this).removeClass('slidedownin-animation');
        }
      });//end function for div.slidedownin-animation CSS class


      $('div.expandin-animation').each( function(i){
        var bottom_of_object = $(this).offset().top + $(this).outerHeight();
        var bottom_of_window = $(window).scrollTop() + $(window).height();

        //If the object is completely visible in the window, fade it it
        if( bottom_of_window > (bottom_of_object-50) ){//-50 for execute the effect before
          var retardo = $(this).data('velocity-delay');
          if (!retardo){
            retardo = 50;
          }else{
            retardo = retardo * 500;
          }
          $(this).velocity("transition.expandIn").delay(retardo);
          $(this).removeClass('expandin-animation');
        }
      });//end function for div.expanin-animation CSS class

      /*** velocity effects for the logo and text below parallax image ***/
      $('.velocity-effect').each( function(i){
        var bottom_of_object = $(this).offset().top + $(this).outerHeight();
        var bottom_of_window = $(window).scrollTop() + $(window).height();

        /* If the object is completely visible in the window, fade it it */
        if( bottom_of_window > bottom_of_object ){
          var transition = $(this).data('velocity-transition');
          var duration = $(this).data('velocity-duration');
          if (!duration){
            duration = 750; // 750 is default
          }
          $(this).velocity("transition."+transition, duration,{ backwards: true });
          $(this).removeClass('velocity-effect');
        }
      });
      /*** end velocity effects for the logo and text below parallax image ***/

    });//end scroll function

    /*** end effects for the proyect views in the Elpatio website ***/


  /***** slider home ElPatio *******/
  /*
  var slider_interval = $('.slider').data('slider-interval');
  if(!slider_interval){
    slider_interval = 5000; //default
  }
  var home_slider = $('.slider').slider({indicators: true, interval: slider_interval});
  $('.slider #prev-button a').click(function(){
    $('.slider').slider('prev');
    //reset the slider transition
    $('.slider').slider('start');
      //var pos = home_slider.find('.active').index();
      //home_slider.trigger('sliderPrev');

      // for restart slider time transition
      //home_slider.trigger('sliderStart'); //or $('.slider').slider('start');
  });//end click prev-button

  $('.slider #next-button a').click(function(){
    $('.slider').slider('next');
    //reset the slider transition
    $('.slider').slider('start');
    //home_slider.trigger('sliderNext');
    // for restart slider time transition
    //home_slider.trigger('sliderStart'); //or $('.slider').slider('start');
  });//end click next-button
**********/
    /***** end slider home ElPatio *******/

$('.slider').each(function (index){

  /*** adjust indicators ***/
  var display_indicators = 'true';
  if($(this).data('slider-indicators')){
    display_indicators = $(this).data('slider-indicators');
  }
  if(display_indicators == 'true'){
    display_indicators = false;
    //alert(index + '-> verdadero');
  }else{
    display_indicators = true;
    //alert(index + '-> falso');
  }
  /*** end adjust indicators ***/

  /*** adjust interval transition ***/
  var slider_interval = 5000; //default
  if($(this).data('slider-interval')){
    slider_interval = $('.slider').data('slider-interval');
  }
  /*** end adjust interval transition ***/

  $(this).slider({indicators: display_indicators, interval: slider_interval});
  var btn_prev = $(this).find('#prev-button a');
  btn_prev.click(function(){
    $(this).slider('prev');
    $(this).slider('start');//reset the slider transition
  });

  var btn_next = $(this).find('#next-button a');
  btn_next.click(function(){
    $(this).slider('next');
    $(this).slider('start');//reset the slider transition
  });

});

  /*
  $('.slider_empresas').each(function( index ) {
      $(this).slider({indicators: true});
  });
*/

  // add parallax effects
  $('.parallax').parallax();

  /** add taget=_blank and rel=nofollolw attributes to file fields (papers)
  * in content type Proyecto
  * */
  $(".node-proyecto .field-name-field-papers a[href^='http://']").attr("target","_blank").attr("rel", "nofollow");



 });// end document

/** wave-effect in mouseenter in proyectos views grid images **/
var ink, d, x, y;
$(document).on('mouseenter', '.logo_proyecto_grid a', function(e){
  if($(this).find('.ink').length === 0){
    $(this).prepend("<span class='ink'></span>");
  }

  ink = $(this).find('.ink');
  ink.removeClass('animate');

  if(!ink.height() && !ink.width()){
    d = Math.max($(this).outerWidth(), $(this).outerHeight());
    ink.css({height: d, width: d});
  }

  x = e.pageX - $(this).offset().left - ink.width()/2;
  y = e.pageY - $(this).offset().top - ink.height()/2;

  ink.css({top: y+'px', left: x+'px'}).addClass('animate');

});
/**  end wave-effect in mouseenter in proyectos views grid images **/

/*
jQuery.noConflict();
jQuery(document).ready(function ($) {

});
*/
