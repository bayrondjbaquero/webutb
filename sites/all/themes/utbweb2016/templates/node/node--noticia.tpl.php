<!--Post-->
<div class="row">
    <div class="col-md-12 blog-column" style="padding-top: 25px;">



        <div class="view overlay hm-blue-slight z-depth-2">
            <?php if ($node->field_carrousel['und'][0]['uri']) { ?>

                <?php
                $uriImagefromNode = $node->field_carrousel['und'][0]['uri'];
                $urlImagefromNode = file_create_url($uriImagefromNode);
                ?>
                <a>
                    <img src="<?php print $urlImagefromNode; ?>" class="img-responsive">
                </a>
            <?php } else { ?>
                <a>
                    <img src="http://www.unitecnologica.edu.co/sites/web.unitecnologica.edu.co/files/img-2076_0.jpg" class="img-responsive">
                </a>            
            <?php } ?>
        </div>



        <div class="card-panel bl-panel hoverable">
            <div class="text-center">
                <h3 class="black-text"><?php print $node->title; ?></h3>
                <h5>Publicado el <?php print $formatted_date = format_date($node->created, 'custom', 'j M Y'); ?></h5>
            </div>
            <hr>
            <p class="text-left"><?php print $node->body["und"][0]["value"]; ?></p>
        </div>
    </div>
</div>
<!--/.Post-->
