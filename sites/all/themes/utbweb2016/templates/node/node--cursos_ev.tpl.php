
<div class="row">
    <div class="col-md-12 blog-column" style="padding-top: 25px;">



        <div class="view overlay hm-blue-slight z-depth-2">
            <?php if ($node->field_carrousel['und'][0]['uri']) { ?>

                <?php
                $uriImagefromNode = $node->field_carrousel['und'][0]['uri'];
                $urlImagefromNode = file_create_url($uriImagefromNode);
                ?>
                <a>
                    <img src="<?php print $urlImagefromNode; ?>" class="img-responsive">
                </a>
            <?php } else { ?>
                <a>
                    <img src="http://www.unitecnologica.edu.co/sites/web.unitecnologica.edu.co/files/img-2076_0.jpg" class="img-responsive">
                </a>
            <?php } ?>
        </div>

        <div class="card-panel bl-panel hoverable">
            <div class="text-center">
                <h3  class="blue-text text-darken-2"><?php print $node->title; ?></h3>
            </div>
            <hr>  
            <?php if ($content['body']): ?>
                <h4 class="blue-text text-darken-2">Descripción</h4>
                <p class="text-left"><?php print $node->body["und"][0]["value"]; ?></p>
                <hr>   
            <?php endif; ?>
            <?php if ($content['field_objetivo']): ?>
                <h4 class="blue-text text-darken-2">Objetivo</h4>
                <p class="text-left"><?php print $node->field_objetivo["und"][0]["safe_value"]; ?></p>
                <hr>      
            <?php endif; ?>
            <?php if ($content['field_profesores']): ?>        
                <h4 class="blue-text text-darken-2">Profesores</h4>
                <p class="text-left"><?php print $node->field_profesores["und"][0]["value"]; ?></p>
                <hr>    
            <?php endif; ?>

            <?php if ($content['field_docentes']): ?>        
                <h4 class="blue-text text-darken-2">Docentes</h4>
                <p class="text-left"><?php print $node->field_docentes["und"][0]["value"]; ?></p>
                <hr>    
            <?php endif; ?>
            <?php if ($content['field_detalles']): ?>
                <h4 class="blue-text text-darken-2">Detalles</h4>
                <p class="text-left"><?php print $node->field_detalles["und"][0]["value"]; ?></p>
                <hr>
            <?php endif; ?>
            <?php if ($content['field_adjuntos_beca']) { ?>
                <div class="panel panel-primary">
                    <div class="panel-heading">                    
                        <h3 class="panel-title"> <span class="glyphicon glyphicon-folder-open" style="font-size:2.5em;"></span>&nbsp;&nbsp;&nbsp; Descargas adicionales </h3> 
                    </div>
                    <div class="panel-body">
                        <?php print render($content['field_adjuntos_beca']); ?>                              
                    </div>											
                </div>
            <?php } ?>
        </div>
    </div>
</div>
<!--/.Post-->
