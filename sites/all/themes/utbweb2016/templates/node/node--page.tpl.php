<?php if (!drupal_is_front_page()) : ?>

    <article>
        <div class="col-md-12 column">


            <br>
            <h2 class="text-center">
                <?php if ($title): ?>

                    <?php
                    print $title;
                    ?>

                <?php endif; ?>

            </h2>
            <?php if ($page['content']): ?>
                <?php print render($page['content']); ?>
            <?php endif; ?>
        </div>

    </article>

<?php endif; ?>

<?php print $node->body["und"][0]["value"]; ?>