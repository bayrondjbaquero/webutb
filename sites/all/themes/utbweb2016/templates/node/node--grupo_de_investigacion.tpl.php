<?php if (!empty($body)): ?>

    <article id="node-<?php print $node->nid; ?>" class="<?php print $classes; ?> clearfix"<?php print $attributes; ?>> <?php print $user_picture; ?> <?php print render($title_prefix); ?>

        <div id="article-<?php print $node->nid; ?>" class="article <?php
        print $classes;
        if ($display_submitted)
            
            ?>clearfix" <?php print $attributes; ?> >
             <?php if ($display_submitted): ?>
                <div class="footer submitted">
                    <?php print $user_picture; ?>
                    <?php print '<div class="time pubdate" title="' . $datetime . '">' . t($created_date) . '</div>'; ?>
                    <div class="info-wrapper">
                        <?php
                        if ($node_category)
                            print '<span class="node-category"> <span class="category-in">' . t('In') . ' </span>' . render($node_category) . '</span>';
                        print '<span class="author">' . t('Por') . ' ' . $name . '</span>';
                        print '<span class="comment-comments">' . render($comments_count) . '</span>';
                        ?>
                    </div>
                    <?php ?>
                </div>
            <?php endif; ?>
            <?php if ($title): ?>
                <div class="header article-header">

                    <h3 class="text-center"><?php print $node->title; ?></h3>

                    <div class="row clearfix" style="margin-bottom: 30px;">

                        <div class="col-md-12 column">

                            <?php if ($node->field_codigo_colciencias["und"][0]["safe_value"] || $node->field_url_colciencias["und"][0]['url'] > 1 || $node->field_categoria_grupo["und"][0]["safe_value"] || $node->field_convocatoria_grupo["und"][0]["safe_value"]) { ?> 
                                <div class="col-md-12">
                                    <table class="table table-striped">
                                        <tbody>
                                            <tr>
                                                <th>Codigo Colciencias</th>
                                                <th>Categoria del Grupo</th>
                                            </tr>      
                                            <tr>
                                                <td><?php print$node->field_codigo_colciencias["und"][0]["safe_value"]; ?></td>
                                                <td><?php print $node->field_categoria_grupo["und"][0]["safe_value"]; ?></td>
                                            </tr>
                                            <tr>                                            
                                                <th>Convocatoria del Grupo</th>
                                                <th>Enlace Colciencias</th>
                                            </tr> 
                                            <tr>                                            
                                                <td><?php print $node->field_convocatoria_grupo["und"][0]["safe_value"]; ?></td>
                                                <td><a class="btn btn-default" target="_blank" href="<?php print($node->field_url_colciencias["und"][0]['url']); ?>">Link a Colciencias</a></td>
                                            </tr>
                                        </tbody>
                                    </table>

                                <?php } ?> 
                            </div>
                            <hr/>

                            <div class="row">

                                <!-- Navigation grupos Buttons -->
                                <div class="col-md-2">
                                    <ul class="nav nav-pills nav-stacked" id="Programas">
                                        <?php if ($node->body["und"][0]["safe_value"]) : ?> 
                                            <li class="active"><a class="btn" href="#descripcion" data-toggle="tab">Descripción</a></li><?php endif; ?>
                                        <?php if ($node->field_director_de_grupo["und"][0]["entity"] || $node->field_email_contacto["und"][0]["safe_value"]) : ?> 
                                            <li><a class="btn" href="#director" data-toggle="tab">Director Grupo</a></li><?php endif; ?>
                                        <?php if ($node->field_semilleros["und"][0]["entity"]) : ?> 
                                            <li><a class="btn" href="#semilleros" data-toggle="tab">Semilleros</a></li><?php endif; ?>                                       
                                        <?php if ($node->field_teaminvestigadores["und"][0]["entity"]->nid) : ?>
                                            <li><a class="btn" href="#investigadores" data-toggle="tab">Investigadores</a></li><?php endif; ?>	  
                                        <?php if ($node->field_lineas_investigacion["und"][0]["safe_value"] || $node->field_lineas_publicacion["und"][0]["safe_value"]) : ?> 
                                            <li><a class="btn" href="#detalles" data-toggle="tab">Detalles del grupo</a></li><?php endif; ?>
<?php if ($node->field_seminarios_ide["und"][0]["safe_value"]) : ?>
    <li><a class="btn" href="#semillerosid" data-toggle="tab">Seminarios IDE</a></li>
<?php endif; ?>                                       
                                    </ul>                                    
                                </div>

                                <!-- Content -->
                                <div class="col-md-10">
                                    <div class="card darken-2">
                                        <div class="card-content">

                                            <div class="tab-content">


                                                <div class="tab-pane active" id="descripcion">                                            
                                                    <?php print render($content['body']); ?>
                                                </div>

                                                <div class="tab-pane" id="director">

                                                    <a href="<?php echo url("node/" . $node->field_director_de_grupo["und"][0]["entity"]->nid);
                                                    ?>">
                                                           <?php
                                                           print("<h5>");
                                                           print($node->field_director_de_grupo["und"][0]["entity"]->title);
                                                           print("</h5>");
                                                           //@TODO: incluir redes sociales
                                                           ?>
                                                    </a>
                                                    <div class="row ">
                                                        <div class="col-md-6">
                                                            <div class="pull-right">
                                                                <?php echo theme('image_style', array('style_name' => 'staff', 'path' => $node->field_director_de_grupo["und"][0]["entity"]->field_foto['und'][0]['uri'], 'attributes' => array('class' => 'img-responsive'))); ?> 
                                                            </div>
                                                        </div>

                                                        <div class="col-md-6">
                                                            <div class="pull-left">
                                                                <dl>  
                                                                    <?php if (strlen($node->field_director_de_grupo["und"][0]["entity"]->field_url_perfil["und"][0]['url']) > 1) { ?>
                                                                        <dt>Perfil Investigador:</dt>
                                                                        <dd><a class="btn btn-default" target="_blank" href="<?php print($node->field_director_de_grupo["und"][0]["entity"]->field_url_perfil["und"][0]['url']) ?>">Información adicional</a></dd>
                                                                    <?php } ?> 
                                                                    <?php if (strlen($node->field_director_de_grupo["und"][0]["entity"]->field_email["und"][0]["safe_value"]) > 1) { ?>   
                                                                        <dt>Correo electrónico:</dt><dd><?php
                                                                            print(" " . $node->field_director_de_grupo["und"][0]["entity"]->field_email["und"][0]["safe_value"]);
                                                                            ?></dd>
                                                                    <?php } ?>
                                                                    <?php if (strlen($node->field_director_de_grupo["und"][0]["entity"]->field_telefono["und"][0]["safe_value"]) > 1) { ?>
                                                                        <dt>Teléfono:</dt><dd><?php
                                                                            print(" " . $node->field_director_de_grupo["und"][0]["entity"]->field_telefono["und"][0]["safe_value"]);
                                                                            ?></dd>
                                                                    <?php } ?>
                                                                    <?php if (strlen($node->field_director_de_grupo["und"][0]["entity"]->field_direccion_oficina["und"][0]["safe_value"]) > 1) { ?>
                                                                        <dt>Dirección de Oficina:</dt>
                                                                        <dd><?php print($node->field_director_de_grupo["und"][0]["entity"]->field_direccion_oficina["und"][0]["safe_value"]); ?></dd>
                                                                    <?php } ?>
                                                                    <?php if (strlen($node->field_director_de_grupo["und"][0]["entity"]->field_tipo_de_investigador["und"][0]["safe_value"]) > 1) { ?>
                                                                        <dt>Tipo de Investigador:</dt>
                                                                        <dd><?php print $node->field_director_de_grupo["und"][0]["entity"]->field_tipo_de_investigador["und"][0]["safe_value"]; ?></dd>                                      
                                                                    <?php } ?>
                                                                </dl>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <h5>Contacto</h5><br/>
                                                    <?php print $node->field_email_contacto["und"][0]["safe_value"]; ?>
                                                </div>
<style>
			.text-dark{
				font-weight: 600;
			}
			.bg-success{
				background-color: #23c36c;
				color: white;
			}
			ul.tbs{
				text-align: center;
			}
#semillerosid .nav li a {font-size: 20px;}
h4.text-dark{ font-size: 17px;}
			.tbs li{
				float: none;
				display: inline-flex;
			}
		</style>
<div class="tab-pane" id="semillerosid">
    <h4>Seminarios IDE </h4>
<ul class="nav nav-tabs tbs">
			<li class="active">
				<a href="#2018" data-toggle="tab">2018</a>
			</li>
			<li>
				<a href="#2017" data-toggle="tab">2017</a>
			</li>
			<li>
				<a href="#2016" data-toggle="tab">2016</a>
			</li>
			<li>
				<a href="#2015" data-toggle="tab">2015</a>
			</li>
			<li>
				<a href="#2014" data-toggle="tab">2014</a>
			</li>
			<li>
				<a href="#2013" data-toggle="tab">2013</a>
			</li>
			<li>
				<a href="#2012" data-toggle="tab">2012</a>
			</li>
		</ul>
		<div class="tab-content">
			<div id="2018" class="tab-pane fade in active">
				<h4 class="text-regular text-dark">Primer Periodo</h4>
				<table class="table table-bordered">
					<tr class="bg-primary">
						<td>Actividad</td>
						<td style="width: 230px;">Ponente(s)</td>
						<td style="width: 245px;">Afiliación</td>
						<td style="width: 80px;">Fecha</td>
					</tr>
					
					<tr>
						<td >Cambio tecnológico y crecimiento económico
</td>
						<td >Roberto Fortich Mesa
</td>
						<td>Universidad Tecnológica de Bolívar
</td>
						<td>14-feb
</td>
					</tr>
					
					<tr>
						<td >Llevando políticas de movilidad de una ciudad a otra
</td>
						<td >Diego Silva Ardila
</td>
						<td>Universidad del Rosario
</td>
						<td>07-mar
</td>
					</tr>
					
					<tr>
						<td >Flujos de capital, recursos naturales y enfermedad holandesa: El caso colombiano
</td>
						<td >Alejandro Torres
</td>
						<td>EAFIT
</td>
						<td>21-mar
</td>
					</tr>
					<tr>
						<td >Violencia contra la mujer en el mercado laboral de Perú
</td>
						<td >Laura Rueda Sierra
</td>
						<td>Universidad Tecnológica de Bolívar
</td>
						<td>04-abr
</td>
					</tr>
					<tr>
						<td >Sesgos de confirmación en modelos de aprendizaje para la toma de decisiones
</td>
						<td >Jorge Castro
</td>
						<td>Universidad de los Andes
</td>
						<td>09-may
</td>
					</tr>
				</table>
				<h4 class="text-regular text-dark">Segundo Periodo</h4>
				<table class="table table-bordered">
					<tr class="bg-success">
						<td>Actividad</td>
						<td style="width: 230px;">Ponente(s)</td>
						<td style="width: 245px;">Afiliación</td>
						<td style="width: 80px;">Fecha</td>
					</tr>
					<tr>
						<td >La subregión del Canal del Dique
</td>
						<td >Luis Fernando López <br>
José Saenz Zapata
</td>
						<td>Cámara de Comercio de Cartagena
</td>
						<td>12-sep
</td>
					</tr>
					<tr>
						<td >El transporte acuático de pasajeros en Cartagena
</td>
						<td >Wilmer Iriarte Restrepo <br>
Jairo Cabrera Tovar <br>
Javier Campillo Jiménez
</td>
						<td>WEIRAICC Consultoría <br>
Universidad Tecnológica de Bolívar <br>
Universidad Tecnológica de Bolívar
</td>
						<td>19-sep
</td>
					</tr>
					<tr>
						<td >Percepción del mototaxismo en cuatro municipios de Bolívar
</td>
						<td >Francisco Maza Ávila <br>
Rosario Blanco Bello
</td>
						<td>Universidad de Cartagena
</td>
						<td>17-oct
</td>
					</tr>
					
					
					
					
				</table>
			</div>
			<div id="2017" class="tab-pane fade">
				<h4 class="text-regular text-dark">Primer Periodo</h4>
				<table class="table table-bordered">
					<tr class="bg-primary">
						<td>Actividad</td>
						<td style="width: 230px;">Ponente(s)</td>
						<td style="width: 245px;">Afiliación</td>
						<td style="width: 80px;">Fecha</td>
					</tr>
					
					<tr>
						<td >Resultados Encuesta de Percepción Ciudadana 2016
</td>
						<td >María Claudia Peñas Arana
</td>
						<td>Cartagena Cómo Vamos
</td>
						<td>22-feb
</td>
					</tr>
					<tr>
						<td >¿Parques de papel? Áreas protegidas y deforestación en Colombia
</td>
						<td >Leonardo Bonilla Mejía
</td>
						<td>Banco de la República
</td>
						<td>08-mar
</td>
					</tr>
					<tr>
						<td >Salud y el uso de Internet: Un estudio de la relación médico-paciente
</td>
						<td >Carolina Barrios Laborda <br>
Dayana Pinzón Callejas
</td>
						<td>Universidad Tecnológica de Bolívar
</td>
						<td>22-mar
</td>
					</tr>
					<tr>
						<td >Conversatorio: Kenneth Arrow y su aporte a las ciencias sociales
</td>
						<td >Julio Romero Prieto <br>
Haroldo Calvo Stevenson <br>
Pablo Abitbol Piñeiro <br>
Daniel Toro González
</td>
						<td>Banco de la República <br>
Universidad Tecnológica de Bolívar <br>
Universidad Tecnológica de Bolívar <br>
Universidad Tecnológica de Bolívar 
</td>
						<td>05-abr
</td>
					</tr>
					<tr>
						<td >El desarrollo local en los Montes de María: Retos en el postconflicto
</td>
						<td >Jorge Alvis Arrieta
</td>
						<td>Universidad Tecnológica de Bolívar
</td>
						<td>26-abr
</td>
					</tr>
					<tr>
						<td >Integraciones empresariales en el mercado de bebidas isotónicas en Colombia
</td>
						<td >Juan Pablo Herrera Saavedra
</td>
						<td>Superintendencia de Industria y Comercio
</td>
						<td>10-may
</td>
					</tr>

					
				</table>
				<h4 class="text-regular text-dark">Segundo Periodo</h4>
				<table class="table table-bordered">
					<tr class="bg-success">
						<td>Actividad</td>
						<td style="width: 230px;">Ponente(s)</td>
						<td style="width: 245px;">Afiliación</td>
						<td style="width: 80px;">Fecha</td>
					</tr>
					<tr>
						<td >Mortalidad y desnutrición en La Guajira
</td>
						<td >Lucas Hahn de Castro
</td>
						<td>Banco de la República
</td>
						<td>23-ago
</td>
					</tr>
					<tr>
						<td >Cartagena frente al cambio climático: Propuestas de ciudad
</td>
						<td >Mauro Maza <br>
Milton Guerrero
</td>
						<td>Universidad Tecnológica de Bolívar
</td>
						<td>06-sep
</td>
					</tr>
					<tr>
						<td >La revoulción de la comida: Hacia un sistema alimentario sano, justo y sostenible
</td>
						<td >Pablo Abitbol Piñeiro <br>
Liliana Ujueta
</td>
						<td>Universidad Tecnológica de Bolívar
</td>
						<td>20-sep
</td>
					</tr>
					<tr>
						<td >Retos de desarrollo económico para la paz
</td>
						<td >Gonzalo Hernández
</td>
						<td>Pontificia Universidad Javeriana
</td>
						<td>18-oct
</td>
					</tr>
					<tr>
						<td >La segregación residencial de la población afrocolombiana en Cartagena
</td>
						<td >Aarón Espinosa Espinosa
</td>
						<td >Universidad Tecnológica de Bolívar
</td>
						<td>22-nov
</td>
					</tr>
				</table>
			</div>
			<div id="2016" class="tab-pane fade">
				<h4 class="text-regular text-dark">Primer Periodo</h4>
				<table class="table table-bordered">
					<tr class="bg-primary">
						<td>Actividad</td>
						<td style="width: 230px;">Ponente(s)</td>
						<td style="width: 230px;">Afiliación</td>
						<td style="width: 80px;">Fecha</td>
					</tr>
					<tr>
						<td >Progreso social en Cartagena, 2009 - 2014
</td>
						<td >María Claudia Peñas Arana
</td>
						<td>Cartagena Cómo Vamos
</td>
						<td>17-feb
</td>
					</tr>
					<tr>
						<td >La participación en la vida cultural en Cartagena, 2008 - 2013
</td>
						<td >Aarón Espinosa Espinosa <br>
Daniel Toro González
</td>
						<td>Universidad Tecnológica de Bolívar 

</td>
						<td>09-mar
</td>
					</tr>
					<tr>
						<td >Encadenamientos regionales en Colombia, 2004 - 2012
</td>
						<td >Lucas Hahn de Castro
</td>
						<td>Banco de la República
</td>
						<td>06-abr
</td>
					</tr>
					<tr>
						<td >Calidad de vida laboral en Colombia
</td>
						<td >Luis Armando Galvis Aponte
</td>
						<td>Banco de la República
</td>
						<td>20-abr
</td>
					</tr>
					<tr>
						<td >Encuesta Mundial de Valores
</td>
						<td >Andrés Casas Casas
</td>
						<td>Corpovisionarios
</td>
						<td>11-may
</td>
					</tr>
					
					

					
				</table>
				<h4 class="text-regular text-dark">Segundo Periodo</h4>
				<table class="table table-bordered">
					<tr class="bg-success">
						<td>Actividad</td>
						<td style="width: 230px;">Ponente(s)</td>
						<td style="width: 245px;">Afiliación</td>
						<td style="width: 80px;">Fecha</td>
					</tr>
					
					<tr>
						<td >Construcción de paz como aprendizaje social
</td>
						<td >Pablo Abitbol Piñeiro
</td>
						<td>Universidad Tecnológica de Bolívar
</td>
						<td>17-ago
</td>
					</tr>
					<tr>
						<td >Conversatorio: Cartagena vista por visitantes, siglos XVII al XIX
</td>
						<td >Jaime Bernal Villegas <br>
Alberto Gómez Gutiérrez
</td>
						<td>Universidad Tecnológica de Bolívar <br>
Pontificia Universidad Javeriana
</td>
						<td>07-sep
</td>
					</tr>
					<tr>
						<td >La mortalidad en la Guerra de los Mil Días, 1899 - 1902
</td>
						<td >Julio Romero Prieto
</td>
						<td >Banco de la República
</td>
						<td>05-oct
</td>
					</tr>
					
				</table>
			</div>
			<div id="2015" class="tab-pane fade">
				<h4 class="text-regular text-dark">Primer Periodo</h4>
				<table class="table table-bordered">
					<tr class="bg-primary">
						<td>Actividad</td>
						<td style="width: 230px;">Ponente(s)</td>
						<td style="width: 245px;">Afiliación</td>
						<td style="width: 80px;">Fecha</td>
					</tr>
					<tr>
						<td >Modelo gravitacional del comercio exterior
</td>
						<td >Luis Miguel Bolívar Caro
</td>
						<td>Universidad Tecnológica de Bolívar
</td>
						<td>11-feb
</td>
					</tr>
					<tr>
						<td >Introducción a los métodos históricos en demografía
</td>
						<td >Julio Romero Prieto
</td>
						<td>Banco de la República
</td>
						<td>04-mar
</td>
					</tr>
					<tr>
						<td >El puerto de Cartagena: Antecedentes, situación y perspectivas
</td>
						<td >Luis Eduardo Jiménez
</td>
						<td>Sociedad Portuaria de Cartagena
</td>
						<td>25-mar
</td>
					</tr>
					<tr>
						<td >Capacidades de innovación en el departamento de Bolívar
</td>
						<td >Luis Fernando lópez <br> Dairo Novoa
</td>
						<td>Cámara de Comercio de Cartagena <br> Cámara de Comercio de Cartagena

 </td>
						<td>06-may
</td>
					</tr>
					
					
				</table>
				<h4 class="text-regular text-dark">Segundo Periodo</h4>
				<table class="table table-bordered">
					<tr class="bg-success">
						<td>Actividad</td>
						<td style="width: 230px;">Ponente(s)</td>
						<td style="width: 245px;">Afiliación</td>
						<td style="width: 80px;">Fecha</td>
					</tr>
					<tr>
						<td >Estructura de la industria cervecera en América Latina
</td>
						<td >Daniel Toro González
</td>
						<td>Universidad Tecnológica de Bolívar
</td>
						<td>26-ago
</td>
					</tr>
					<tr>
						<td >La historia de Cartagena en una cáscara de nuez
</td>
						<td >Haroldo Calvo Stevenson
</td>
						<td>Universidad Tecnológica de Bolívar
</td>
						<td>09-sep
</td>
					</tr>
					<tr>
						<td >Calidad de vida y participación política
</td>
						<td >Maristella Madero <br> Rosaura Arrieta

</td>
						<td>Fundación social <br> Universidad de Cartagena

</td>
						<td>30-sep
</td>
					</tr>
					
					

					<tr>
						<td >Necesidades de inversión y escenarios fiscales en Cartagena
</td>
						<td >Jaime Bonet Morón
</td>
						<td>Banco de la República
</td>
						<td>21-oct
</td>
					</tr>
					<tr>
						<td >Población y desarrollo en el Caribe colombiano en el siglo XX
</td>
						<td >Julio Romero Prieto
</td>
						<td >Banco de la República
</td>
						<td>04-nov
</td>
					</tr>
				</table>
			</div>
			<div id="2014" class="tab-pane fade">
				<h4 class="text-regular text-dark">Primer Periodo</h4>
				<table class="table table-bordered">
					<tr class="bg-primary">
						<td>Actividad</td>
						<td style="width: 230px;">Ponente(s)</td>
						<td style="width: 245px;">Afiliación</td>
						<td style="width: 80px;">Fecha</td>
					</tr>
					<tr>
						<td >Pobreza y desarrollo rural en Cartagena de Indias
</td>
						<td >Aarón Espinosa Espinosa <br> Jorge Alvis Arrieta

</td>
						<td>Universidad Tecnológica de Bolívar
</td>
						<td>19-feb
</td>
					</tr>
					<tr>
						<td >Crónicas de la tragedia invernal, 2010 - 2011
</td>
						<td >Andrés Sánchez Jabba
</td>
						<td>Banco de la República
</td>
						<td>12-mar
</td>
					</tr>
					<tr>
						<td >Mercado de aseguramiento y acceso a servicios de salud en Colombia
</td>
						<td >Sandra Rodríguez Acosta
</td>
						<td>Universidad del Norte
</td>
						<td>26-mar
</td>
					</tr>
					<tr>
						<td >Aspectos regionales de la movilidad social y la igualdad de oportunidades en Colombia
</td>
						<td >Luis Armando Galvis Aponte
</td>
						<td>Banco de la República
</td>
						<td>23-abr
</td>
					</tr>
					
					
					
				</table>
				<h4 class="text-regular text-dark">Segundo Periodo</h4>
				<table class="table table-bordered">
					<tr class="bg-success">
						<td>Actividad</td>
						<td style="width: 230px;">Ponente(s)</td>
						<td style="width: 245px;">Afiliación</td>
						<td style="width: 80px;">Fecha</td>
					</tr>
					<tr>
						<td >La salud en Colombia: Más cobertura, menos acceso
</td>
						<td >Jhorland Ayala García
</td>
						<td>Banco de la República
</td>
						<td>03-sep
</td>
					</tr>
					<tr>
						<td >Evaluación de la calidad de vida en Cartagena, 2013
</td>
						<td >María Claudia Peñas Arana
</td>
						<td>Cartagena Cómo Vamos
</td>
						<td>01-oct
</td>
					</tr>
					
					<tr>
						<td >Modelo para mejorar la seguridad urbana en Cartagena
</td>
						<td >Luis Ignacio Morales Eckardt
</td>
						<td>Universidad Tecnológica de Bolívar
</td>
						<td>22-oct
</td>
					</tr>
					
					<tr>
						<td >Cambios recientes en las principales causas de mortalidad en Colombia
</td>
						<td >Karina Acosta Ordoñez
</td>
						<td>Banco de la República
</td>
						<td>05-nov
</td>
					</tr>
					<tr>
						<td >Apuntes para la historia genética de la población colombiana
</td>
						<td >Jaime Bernal Villegas
</td>
						<td >Universidad Tecnológica de Bolívar
</td>
						<td>19-nov
</td>
					</tr>
				</table>
			</div>
			<div id="2013" class="tab-pane fade">
				<h4 class="text-regular text-dark">Primer Periodo</h4>
				<table class="table table-bordered">
					<tr class="bg-primary">
						<td>Actividad</td>
						<td style="width: 230px;">Ponente(s)</td>
						<td style="width: 245px;">Afiliación</td>
						<td style="width: 80px;">Fecha</td>
					</tr>
					<tr>
						<td >Ciudad, modernidad y representación:  Cartagena de Indias, 1850-1950</td>
						<td >Javier Ortiz Cassiani</td>
						<td>El Colegio de México</td>
						<td>13-mar</td>
					</tr>
					<tr>
						<td >Planeación del desarrollo local: Adaptación diversa y aprendizaje social</td>
						<td >Juan Camilo Oliveros</td>
						<td>Universidad Tecnológica de Bolívar</td>
						<td>10-abr</td>
					</tr>
					<tr>
						<td >Cartagena: Entre el progreso industrial y el rezago social</td>
						<td >Karina Acosta Ordoñez</td>
						<td>Banco de la República</td>
						<td>24-abr</td>
					</tr>
					<tr>
						<td >¿El triunfo de Bogotá?: Desempeño reciente de la ciudad capital</td>
						<td >Luis Armando Galvis Aponte</td>
						<td>Banco de la República</td>
						<td>15-may</td>
					</tr>
					
					
					
				</table>
				<h4 class="text-regular text-dark">Segundo Periodo</h4>
				<table class="table table-bordered">
					<tr class="bg-success">
						<td>Actividad</td>
						<td style="width: 230px;">Ponente(s)</td>
						<td style="width: 245px;">Afiliación</td>
						<td style="width: 80px;">Fecha</td>
					</tr>
					<tr>
						<td >El bilingüismo en Colombia</td>
						<td >Andrés Sánchez Jabba</td>
						<td>Banco de la República</td>
						<td>4-sep</td>
					</tr>
					<tr>
						<td >Estimación de la demanda por transporte público en Colombia</td>
						<td >Daniel Toro González</td>
						<td>Universidad Tecnológica de Bolívar</td>
						<td>25-sep</td>
					</tr>
					<tr>
						<td >Impuestos locales para el desarrollo local</td>
						<td >Jaime Bonet Morón</td>
						<td>Banco de la República</td>
						<td>16-oct</td>
					</tr>
					
					
				</table>
			</div>
			<div id="2012" class="tab-pane fade">
				<h4 class="text-regular text-dark">Primer Periodo</h4>
				<table class="table table-bordered">
					<tr class="bg-primary">
						<td>Actividad</td>
						<td style="width: 230px;">Ponente(s)</td>
						<td style="width: 245px;">Afiliación</td>
						<td style="width: 80px;">Fecha</td>
					</tr>
					<tr>
						<td >El Canal del Dique: ¿Un error histórico?</td>
						<td >José Vicente Mogollón Vélez</td>
						<td>Ex-Ministro de Ambiente</td>
						<td>28-mar</td>
					</tr>
					<tr>
						<td >Propuestas para repensar el problema de la cooperación y el free-riding en la economía</td>
						<td >Juan Camilo Cárdenas</td>
						<td>Universidad de los Andes</td>
						<td>4-may</td>
					</tr>
				</table>
				<h4 class="text-regular text-dark">Segundo Periodo</h4>
				<table class="table table-bordered">
					<tr class="bg-success">
						<td>Actividad</td>
						<td style="width: 230px;">Ponente(s)</td>
						<td style="width: 245px;">Afiliación</td>
						<td style="width: 80px;">Fecha</td>
					</tr>
					<tr>
						<td >La infraestructura aeroportuaria del Caribe colombiano</td>
						<td >Andrea Otero Cortés</td>
						<td>Banco de la República</td>
						<td>12-sep</td>
					</tr>
					<tr>
						<td >La noción de justicia en Adam Smith y Amartya Sen</td>
						<td >Augusto Aleán Pico</td>
						<td>Universidad Tecnológica de Bolívar</td>
						<td>3-oct</td>
					</tr>
					<tr>
						<td >Estimación de demanda de bienes no homogéneos</td>
						<td >Daniel Toro González</td>
						<td>Universidad Tecnológica de Bolívar</td>
						<td>24-oct</td>
					</tr>
					<tr>
						<td >La planeación del cambio climático en Cartagena: Institucionalizando enfoques alternativos</td>
						<td >Jorge Campos Pérez</td>
						<td >Universidad de Manchester</td>
						<td>21-nov</td>
					</tr>
				</table>
			</div>
		</div>
</div>

                                                <div class="tab-pane" id="semilleros">
                                                    <h3>Semilleros </h3> 
                                                    <ul class="nav list-group" data-tabs="tabs">     
                                                        <?php
                                                        for ($i = 0;; $i++):
                                                            $ID = $node->field_semilleros["und"][$i]["entity"]->nid;
                                                            if ($ID == 0 or strcmp("0", $node->status) == 0) :
                                                                break;
                                                            endif;
                                                            if ($node->field_semilleros["und"][$i]["entity"]->status > 0) :
                                                                $alias = drupal_get_path_alias('node/' . $ID);
                                                                ?>
                                                                <li class="list-group-item" >
                                                                    <a href="<?php print($alias); ?>" ><?php print $node->field_semilleros["und"][$i]["entity"]->title; ?></a>
                                                                </li>
                                                                <?php
                                                            endif;
                                                        endfor;
                                                        ?>
                                                    </ul>
                                                </div>

                                                <div class="tab-pane" id="investigadores">


                                                    <h3>Investigadores</h3>
                                                    <div class="programa_profesor_tc">
                                                            <?php
                                                            for ($i = 0;
                                                            ; $i++) {
                                                                $ID = $node->field_teaminvestigadores["und"][$i]["entity"]->nid;
                                                                if ($ID == 0) {
                                                                    break;
                                                                }
                                                                if (strcmp("0", $node->field_teaminvestigadores["und"][$i]["entity"]->status) != 0) {
                                                                    ?>
                                                                    <a href="<?php echo url("node/" . $node->field_teaminvestigadores["und"][$i]["entity"]->nid); ?>">                                             
                                                                        <?php print($node->field_teaminvestigadores["und"][$i]["entity"]->title); ?>  </a>
                                                                    <br/><br/>                                                                        


                                                                    <?php
                                                                }
                                                            }
                                                            ?>

                                                    </div>
                                               
                                            </div> 
                                            <div class="tab-pane" id="detalles">    
                                                <h5>Líneas de Investigación</h5>
                                                <?php print $node->field_lineas_investigacion["und"][0]["safe_value"]; ?>

                                                <?php print render($content['field_lineas_publicacion']); ?>
                                            </div>

                                        </div>

                                    </div>

                                </div>

                            </div>

                        </div>

                    </div>

                </div>






            </div>


        </div>
        <?php //var_dump($node->field_director_de_grupo["und"][0]["entity"]);         ?>
    <?php endif; ?>

    <?php if ($links = render($content['links'])): ?>
        <div class="menu node-links clearfix"><?php print $links; ?></div>
    <?php endif; ?>

    <?php //var_dump($node->field_director_de_grupo["und"][0]["entity"]->field_nivel_academico["und"][0]);                   ?>




<?php endif; ?>
</article>

