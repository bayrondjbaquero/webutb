<?php ?>
<div class = "row">
    <div class = "col-md-12">
        <div class = "card">
            <div class = "card-content">
                <h4 style="color:#2B3988; "><?php print $node->title; ?></h4>
                <hr/>
                <p>
                    <strong style="color:#318055; ">Investigador Principal:</strong> <?php print $node->field_investigador_principal["und"][0]["value"]; ?>
                </p>
                <p>
                    <strong style="color:#318055; ">Correo de Investigador</strong> <a style="color:#2B3988; " href="mailto:<?php print $node->field_correo_de_investigador["und"][0]["email"]; ?>"><?php print $node->field_correo_de_investigador["und"][0]["email"]; ?></a>
                </p>
                <p>
                    <strong style="color:#318055; ">Facultad:</strong> <?php print $node->field_facultad["und"][0]["value"]; ?>
                </p>
                <p>
                    <strong style="color:#318055; ">Resumen:</strong><?php print $node->body["und"][0]["safe_value"]; ?>
                </p>
            </div>           
        </div>
    </div>
</div>