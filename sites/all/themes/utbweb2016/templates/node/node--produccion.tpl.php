</div>
<?php
$capitulos = array();
for ($i = 0;; $i++) :
    $ID = $node->field_capitulo_producciones["und"][$i]["entity"]->nid;

    if ($ID == 0 or strcmp("0", $node->status) == 0) :
        break;
    endif;

    if ($node->field_capitulo_producciones["und"][$i]["entity"]->status > 0) :
        $capitulo = $node->field_capitulo_producciones["und"][$i]["entity"];
        array_push($capitulos, $capitulo);

    endif;
endfor;
?>
<style>
    .centrodemedios{
        margin-top: 0px;
        margin-bottom: 0px;
    }

    .container {
        padding-right: 0px;
        padding-left: 0px;
        margin-right: 0px;
        margin-left: 0px;
        width: 100%;
    }

    @media (min-width: 768px){
        .container2 {
            width: 750px;
        }
    }

    @media (min-width: 992px){
        .container2 {
            width: 970px;
        }
    }

    @media (min-width: 1200px){    
        .container2 {
            width: 1170px;
        }
    }

    hgroup{display:none !important;}

    .col-lg-5{display:none !important;}

    #menuauxiliar{display:none !important;}

    .region-jumbo-6{display:none !important;}

    #menupoliticas{display:none !important;}

    .inst-strong{display:none !important;}

    .container2 {
        padding-right: 15px;
        padding-left: 15px;
        margin-right: auto;
        margin-left: auto;
    }

    .stylish-card .card-content {
        padding: 0px 15px !important;
    }
    .card-footer p {
        text-align: left;
    }
</style>
<A name="video"></A>

<?php 

$term = taxonomy_term_load($node->field_categoria_produccion['und'][0]['tid']);
$category_name = $term->name; 

?>

<div style="background:black;">
    <div class="container2">

        <div  id="playvideo" class="embed-responsive embed-responsive-16by9">
            <iframe class="embed-responsive-item" src="http://www.youtube.com/embed/<?php print $capitulos[count($capitulos)-1]->field_youtube['und'][0]['video_id']; ?>"></iframe>
        </div>

    </div>	
</div>

<div class="card-panel bl-panel hoverable">
    <div class="row">
        <div class="col-md-4">

            <?php if ($node->field_carrousel['und'][0]['uri']) { ?>

                <?php
                $uriImagefromNode = $node->field_carrousel['und'][0]['uri'];
                $urlImagefromNode = file_create_url($uriImagefromNode);
                ?>
                <img class="img-responsive"  src="<?php print $urlImagefromNode; ?>">

            <?php } else { ?>

                <img class=" img-responsive" src="http://www.unitecnologica.edu.co/sites/web.unitecnologica.edu.co/files/img-2076_0.jpg">

            <?php } ?>   

        </div>
        <div class="col-md-8">
            <h4 class="section-title st-blue" style="margin-bottom:10px;"><?php print $node->title; ?><small><?php print $category_name; ?></small></h4>
            <p><?php print $node->body["und"][0]["safe_value"] ?></p>
        </div>
    </div>

    <hr>
    <div class="row">

        <?php
        $bandera = 0;
        for ($z = 0; $z < count($capitulos); $z++):
            $uriImagefromNodeCap = $capitulos[$z]->field_carrousel['und'][0]['uri'];
            $urlImagefromNodeCap = file_create_url($uriImagefromNodeCap);
            if ($bandera == 0) {
                ?>
            <?php } $bandera++;
            ?>

            <div class="col-md-2">
                <!--Stylish Card Light-->
                <div class="card stylish-card hoverable">

                    <div class="view overlay hm-white-slight z-depth-1">
                        <h2 class="centrodemedios"><a id="scroll-top" linkattr="https://www.youtube.com/embed/<?php print $capitulos[$z]->field_youtube['und'][0]['video_id']; ?>" href="#video" class="black-text waves-effect"><img src="<?php print $urlImagefromNodeCap; ?>" class="img-responsive" style="height:140px;" alt="<?php print $capitulos[$z]->title; ?>"></a></h2>

                    </div>

                    <!--Content-->
                    <div class="card-content">
                        <div class="card-footer">
                            <h2 class="centrodemedios"><a id="scroll-top" linkattr="https://www.youtube.com/embed/<?php print $capitulos[$z]->field_youtube['und'][0]['video_id']; ?>" href="#video" class="black-text waves-effect"><p><?php print $capitulos[$z]->title; ?></p></a></h2>
                        </div>
                    </div>

                </div>
                <!--/.Stylish Card Light-->
            </div>

            <?php if ($bandera == 4) { ?>

                <?php
                $bandera = 0;
            } endfor;
        ?>


    </div>

</div>





<!-- SCRIPTS -->

<!-- JQuery -->
<script type="text/javascript" src="http://www.unitecnologica.edu.co/mailings/mdbUTB/js/jquery.min.js"></script>

<!-- Bootstrap core JavaScript -->
<script type="text/javascript" src="http://www.unitecnologica.edu.co/mailings/mdbUTB/js/bootstrap.min.js"></script>

<!-- Material Design Bootstrap -->
<script type="text/javascript" src="http://mdbootstrap.com/live/_products/Personal-Blog/js/mdb.min.js"></script>

<script type="text/javascript">
    $(document).ready(function () {
        $(".centrodemedios").click(function () {
            $("#playvideo").html(play($(this).find("a").attr("linkattr")));
            return false;
        });

        $('#scroll-top p').click(function () {
            $('body,html').animate({
                scrollTop: 0
            }, 800);
        });
        
        
        $('#scroll-top img').click(function () {
            $('body,html').animate({
                scrollTop: 0
            }, 800);
        });
    });
    function play(id) {
        var html = '';
        html += '<iframe class="embed-responsive-item" src="' + id + '" frameborder="0" ></iframe>';
        return html;
    }
    ;


</script>











<div class="container">