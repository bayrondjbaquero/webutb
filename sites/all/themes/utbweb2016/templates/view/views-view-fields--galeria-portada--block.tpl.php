<?php
$entity = node_load($fields['nid']->raw);
$uriImagefromNode = $entity->field_imagen['und'][0]['uri'];
$urlImagefromNode = file_create_url($uriImagefromNode);
?>
<div class="view overlay">
    <a href="<?php print $entity->field_link['und'][0]['url']; ?>" target="_blank">
        <img  alt="<?php print $entity->field_imagen['und'][0]['alt']; ?>" src="<?php print $urlImagefromNode; ?>" class="img-responsive" alt="slide3">
        <div class="mask waves-effect waves-light"></div>
    </a>
</div>