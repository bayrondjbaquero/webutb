<div class="col-md-12">
  <h3 class="text-center">Portafolio Escuela de Verano</h3>

    <hr>

        <button class="btn mer-pos text-white" data-filter="*">Todos</button>
        <button class="btn inst-strong text-white" data-filter=".item34"> Estudiantes de Pregrado y Posgrado </button>
        <button class="btn inst-strong text-white" data-filter=".item15"> Público General </button>
        <button class="btn inst-strong text-white" data-filter=".item31"> Niños, Adolescentes y Jóvenes </button>
        <button class="btn inst-strong text-white" data-filter=".item47"> Profesores y Profesionales</button>

    <hr>
</div>

<div class="containerbox">
  <?php
  foreach ($rows as $id => $row) {
      print $row;
  }
  ?>
</div>
