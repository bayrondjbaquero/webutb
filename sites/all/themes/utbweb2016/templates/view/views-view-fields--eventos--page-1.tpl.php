<?php
$entity = node_load($fields['nid']->raw);
$uriImagefromNode = $entity->field_image['und'][0]['uri'];
if ($uriImagefromNode) {
    $urlImagefromNode = file_create_url($uriImagefromNode);
}

setlocale(LC_TIME, "spanish");
setlocale(LC_TIME, "es_ES");
$date_test = $entity->field_fecha_evento["und"][0]["value"];
$time = date("h:i a", strtotime($date_test));

$format = "D M"; //or something else that date() accepts as a format
$date = strftime("%A %d", strtotime($date_test));

?>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
<div class="col-md-4">

    <!--Card-->
    <div class="card card-min hoverable">
        <!--Image-->
        <div class="card-image waves-effect waves-block waves-light view overlay hm-white-slight">
            <!--Discount label-->
            <h5 class="card-label"> <span class="label rgba-blueUTB-strong text-capitalize"><?php print utf8_encode($date); ?></span></h5>

            <a href=""><img class="img-responsive"  style="max-height: 200px !important;" src="<?php print $urlImagefromNode; ?>">
                <div class="mask"> </div>
            </a>
        </div>
        <!--/.Image-->

        <!--Card content: Name and price-->
        <div class="card-content" style="font-size: 15px; line-height: 1.5;">
            <a href=""><h5 class="product-title"><?php print $entity->title; ?></h5></a>
            <p>
                <?php print $entity->body["und"][0]["value"];  ?>
                <hr/>
                <strong>Lugar:</strong> <?php print $entity->field_lugar["und"][0]["value"]; ?><br/>
                <strong>Hora:</strong> <?php print $time; ?><br/>
                <strong>Organiza:</strong> <?php print $entity->field_organiza["und"][0]["value"]; ?><br/>
            </p>
        </div>
        <!--/.Card content: Name and price-->

    </div>
    <!--/.Card-->
</div>
