<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

 $entity = node_load($fields['nid']->raw);
 $uriImagefromNode = $entity->field_image['und'][0]['uri'];
 if ($uriImagefromNode) {
     $urlImagefromNode = file_create_url($uriImagefromNode);
 }

$alias = drupal_get_path_alias('node/' . $fields['nid']->raw);

?>
<a href="<?php print $alias; ?>"><img class="img-responsive"  style="max-height: 200px !important;" src="<?php print $urlImagefromNode; ?>">
    <div class="mask"> </div>
</a>
<h5 class="black-text"><?php print $fields['title']->content; ?></h5>
<p><?php print $fields['body']->content; ?></p>
<a href = "<?php print $alias; ?>" class = "btn btn-primary pull-right">Ver más</a>
