<?php
$entity = node_load($fields['nid']->raw);
$uriImagefromNode = $entity->field_image['und'][0]['uri'];
if ($uriImagefromNode) {
    $urlImagefromNode = file_create_url($uriImagefromNode);
}

setlocale(LC_ALL,”es_ES”);

$date_test = $entity->field_fecha["und"][0]["value"];
$time = date("h:i a", strtotime($date_test));

$format = "d M"; //or something else that date() accepts as a format
$date = date_format(date_create($date_test), $format);
?>


<?php 

$dates = explode(" ", $date);

?>
<div class="row row-striped row-striped-color">
    <div class="col-md-12">
        <div class="head-event"> <span class="badge badge-color"><?php print $dates[0]; ?></span><h4 class="display-4"> <?php print $dates[1]; ?></h4></div>
    </div>
    <div class="col-md-12">
        <h4 style="padding-bottom: 15px; padding-top: 10px;"><strong><?php print $entity->title; ?></strong></h4>
        <ul class="list-inline">
            <li class="list-inline-item"><i class="fa fa-clock-o" aria-hidden="true"></i> <?php print $time; ?></li>
            <li class="list-inline-item"><i class="fa fa-location-arrow" aria-hidden="true"></i> <?php print $entity->field_lugar["und"][0]["value"]; ?></li>
        <li class="list-inline-item"><i class="fa fa-calendar-o" aria-hidden="true"></i> <?php print $entity->field_organizador["und"][0]["value"]; ?></li>           
        </ul>
        <p>  <?php print $entity->body["und"][0]["value"]; ?></p>
    </div>
</div>

<!--<div class="row row-striped row-striped-color-reverse">
    <div class="col-md-12">
        <div class="head-event"> <span class="badge badge-color">23</span><h4 class="display-4-reverse"> OCT</h4></div>
    </div>
    <div class="col-md-12">
        <h3 class="text-uppercase"><strong>Operations Meeting</strong></h3>
        <ul class="list-inline">
            <li class="list-inline-item"><i class="fa fa-calendar-o" aria-hidden="true"></i> Friday</li>
            <li class="list-inline-item"><i class="fa fa-clock-o" aria-hidden="true"></i> 2:30 PM - 4:00 PM</li>
            <li class="list-inline-item"><i class="fa fa-location-arrow" aria-hidden="true"></i> Room 4019</li>
        </ul>
        <p>Lorem ipsum dolsit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua.</p>
    </div>
</div>-->