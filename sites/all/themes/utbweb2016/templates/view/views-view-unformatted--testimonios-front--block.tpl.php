<div id="quote-carousel" class="carousel slide" data-ride="carousel" >
          <!-- Bottom Carousel Indicators -->
          <ol class="carousel-indicators">

  <?php $i = 0; ?>
            <?php
            foreach ($rows as $id => $row) {
                if ($i == 0) {
                    ?> <li data-target="#quote-carousel" data-slide-to="<?php print $i; ?>" class="active"> <?php } else { ?>
                        <li data-target="#quote-carousel" data-slide-to="<?php print $i; ?>"></li>
                        <?php } $i++; ?>

                      </li>
                <?php } ?>

          </ol>


          <div class="carousel-inner">


            <?php $i = 0; ?>
            <?php
            foreach ($rows as $id => $row) {
                if ($i == 0) {
                    ?> <div class="item active"> <?php } else { ?>
                        <div class="item">
                        <?php } $i++; ?>

                        <div<?php
                        if ($classes_array[$id]) {
                            print ' class="' . $classes_array[$id] . '"';
                        }
                        ?>>
                                <?php print $row; ?>
                        </div>
                    </div>
                <?php } ?>

            </div>

          </div>

          <!-- Carousel Buttons Next/Prev -->
          <a data-slide="prev" href="#quote-carousel" class="left carousel-control"><i class="fa fa-chevron-left"></i></a>
          <a data-slide="next" href="#quote-carousel" class="right carousel-control"><i class="fa fa-chevron-right"></i></a>
