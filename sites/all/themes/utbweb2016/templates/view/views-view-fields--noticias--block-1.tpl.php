<?php
$entity = node_load($fields['nid']->raw);
$formatted_date = format_date($entity->created, 'custom', 'd m Y');
 ?>

    <!--Second row-->
    <a href="<?php print "node/".$fields['nid']->content; ?>">
        <div class="row">
            <div class="col-md-5">
                <img src="http://www.unitecnologica.edu.co/newsletter/Conectate/imagen-mailing/utb1.jpg" class="img-responsive z-depth-2">
            </div>
            <div class="col-md-7">
                <h5 class="text-center" style="    font-size: 1.2rem;   margin: .82rem 0 .656rem;    padding: 5px;"><?php print $fields['title']->content; ?></h5>
                <ul class="list-inline item-details text-center">
                    <li><i class="fa fa-clock-o"> <?php print $formatted_date; ?></i></li>
                </ul>
            </div>
        </div>
    </a>
    <!--/.Second row-->
