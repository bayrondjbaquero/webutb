<?php

$ent = node_load($fields['nid']->raw);
if ($ent->field_foto_persona['und'][0]['uri']) {
    $uriImagefromNode = $ent->field_foto_persona['und'][0]['uri'];
    $urlImagefromNode = file_create_url($uriImagefromNode);
}
?>

<blockquote>
  <div class="row">
    <div class="col-sm-3 text-center">
        <?php if ($ent->field_foto_persona['und'][0]['uri']) { ?>
      <img class="img-circle" src="<?php print $urlImagefromNode; ?>" style="width: 100px;height:100px;">
      <?php } ?>
      </div>
    <div class="col-sm-9">
      <p><?php print $fields['body']->content; ?></p>
      <small><?php print $ent->field_tipo_persona_testimonio["und"][0]["value"]."<br>"; ?><?php print $ent->field_nombre_persona['und'][0]['safe_value']; ?></small>
    </div>
  </div>
</blockquote>
