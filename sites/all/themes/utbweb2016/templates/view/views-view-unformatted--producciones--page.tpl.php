<?php
/**
 * @file
 * Default simple view template to display a list of rows.
 *
 * @ingroup views_templates
 */
?>

<style>
    .centrodemedios{
        margin-top: 0px;
        margin-bottom: 0px;
    }

    .container {
        padding-right: 0px;
        padding-left: 0px;
        margin-right: 0px;
        margin-left: 0px;
        width: 100%;
    }

    @media (min-width: 768px){
        .container2 {
            width: 750px;
        }
    }

    @media (min-width: 992px){
        .container2 {
            width: 970px;
        }
    }

    @media (min-width: 1200px){    
        .container2 {
            width: 1170px;
        }
    }

    hgroup{display:none !important;}

    .col-lg-5{display:none !important;}

    #menuauxiliar{display:none !important;}

    .region-jumbo-6{display:none !important;}

    #menupoliticas{display:none !important;}

    .inst-strong{display:none !important;}

    .container2 {
        padding-right: 15px;
        padding-left: 15px;
        margin-right: auto;
        margin-left: auto;
    }
    .stylish-card .card-content h5{
        text-align: center;
        color: black;
        font-weight: 400;
    }

    .stylish-card .card-content {
        padding: 0px 15px !important;
    }
    .card-footer p {
        text-align: left;
    }
</style>




<h3>Producciones</h3>




<hr>
<div class="row">
    <?php foreach ($rows as $id => $row): ?>

        <?php print $row; ?>

    <?php endforeach; ?>

</div>
