<?php
$entity = node_load($fields['nid']->raw);

if ($entity->field_image['und'][0]['uri']) {

    $uriImagefromNode = $entity->field_image['und'][0]['uri'];
    $urlImagefromNode = file_create_url($uriImagefromNode);
} else {

    $urlImagefromNode = "https://mdbootstrap.com/images/reg/reg%20(55).jpg";
}
?>

<!-- Card -->
<div class="col-sm-6 item-card">
    <!--Image Card-->
    <div class="card hoverable">
        <div id="card-egre-inst">
            <div class="card-image">
                <div class="view overlay hm-white-slight z-depth-1">
                    <img src="<?php print $urlImagefromNode; ?>" class="img-responsive" alt="">
                    <a href="<?php print "node/" . $fields['nid']->raw; ?>">
                        <div class="mask waves-effect"></div>
                    </a>
                </div>
            </div>
            <div class="card-content">
                <h5><?php print $entity->title; ?></h5>
                <p>
                    <?php print $entity->body['und'][0]['safe_summary']; ?>
                </p>
            </div>

        </div>
        <!--Image Card-->

    </div>
</div>
<!-- /.Card -->
