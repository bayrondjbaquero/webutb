<div id="node-<?php print $node->nid; ?>" class="<?php print $classes; ?> clearfix"<?php print $attributes; ?>>

    <div class="row clearfix">

        <div class="col-md-12 column">

            <?php print $node->body['und'][0]['safe_summary']; ?>

            <hr>

            <div class="row" style="margin-bottom: 30px;">

                <!-- Navigation Buttons -->
                <div class="col-md-2">
                    <ul class="nav nav-pills nav-stacked" id="Programas">
                        <?php if ($node->body["und"][0]["safe_value"]) { ?> 
                            <li class="active"><a href="#descripcion"  data-toggle="tab">Descripción</a></li><?php } ?>
                        <?php if ($node->field_decano["und"][0]["entity"]) { ?> 
                            <li><a href="#director" data-toggle="tab">Decano</a></li><?php } ?>
                        <?php if ($node->field_programas["und"][1]["entity"]->nid) { ?> 
                            <li class=""><a href="#pregrado" data-toggle="tab">Pregrado</a></li>   <?php } ?> 
                        <?php if ($node->field_programas_virtual["und"][0]["entity"]->nid) { ?> 
                            <li class=""><a href="#virtual" data-toggle="tab">Programas Virtuales</a></li>   <?php } ?>     
                        <?php if ($node->field_programas_programaspos["und"][0]["entity"]->nid) { ?>
                            <li class=""><a href="#posgrados" data-toggle="tab">Posgrados</a></li><?php } ?> 
                        <?php if ($node->field_cursos["und"][0]["entity"]->nid) { ?>
                            <li class=""><a href="#cursos" data-toggle="tab">Educación Permanente</a></li><?php } ?> 
                        <?php if ($node->field_investigacion["und"][0]["safe_value"]) { ?>           
                            <li><a href="#investigacion" data-toggle="tab">Investigación</a></li><?php } ?>
                        <?php if ($node->field_laboratorios["und"][0]["safe_value"]) { ?>           
                            <li><a href="#laboratorios" data-toggle="tab">Laboratorios</a></li><?php } ?>
                        <li><a href="#profesores" data-toggle="tab">Profesores</a></li>
                        <?php if ($node->field_contacto["und"][0]["safe_value"]) { ?>    
                            <li><a href="#contacto" data-toggle="tab">Contacto</a></li><?php } ?> 
                    </ul>

                </div>

                <!-- Content -->
                <div class="col-md-10 padding-x linea-caja-left caja-sombra efecto-sombra-2">

                    <div class="tab-content">

                        <div class="tab-pane active" id="descripcion">
                            <?php print render($content['body']); ?>
                        </div>

                        <div class="tab-pane" id="director">
                            <a href="<?php echo url("node/" . $node->field_decano["und"][0]["entity"]->nid);
                            ?>">
                                   <?php
                                   print("<h3>");
                                   print($node->field_decano["und"][0]["entity"]->title);
                                   print(", ");
                                   print($node->field_decano["und"][0]["entity"]->field_nivel_academico["und"][0]["value"]);
                                   print("</h3>");
                                   //@TODO: incluir redes sociales
                                   ?>
                            </a>
                            <div class="row ">
                                <div class="col-md-6">
                                    <div>
                                        <?php echo theme('image_style', array('style_name' => 'staff', 'path' => $node->field_decano["und"][0]["entity"]->field_staffs_image['und'][0]['uri'], 'attributes' => array('class' => 'img-responsive'))); ?> 
                                        <?php print($node->field_decano["und"][0]["entity"]->field_social["und"][0]["safe_value"]); ?>
                                    </div>
                                </div>

                                <div class="col-md-6">
                                    <div class="pull-left">
                                        <dl>  
                                            <?php if (strlen($node->field_decano["und"][0]["entity"]->field_enlace_al_cvlac["und"][0]['url']) > 1) { ?>
                                                <dt>CvLAC:</dt>
                                                <dd><a target="_blank" href="<?php print($node->field_decano["und"][0]["entity"]->field_enlace_al_cvlac["und"][0]['url']) ?>"><?php print(" " . $node->field_decano["und"][0]["entity"]->field_enlace_al_cvlac["und"][0]['title']); ?></a></dd>
                                            <?php } ?>                    
                                            <?php if ($node->field_decano["und"][0]["entity"]->field_escalafon['und'][0]["value"] <> "No escalafonado") { ?>                       
                                                <dt>Escalafón: </dt> 
                                                <dd> <?php print(" " . $node->field_decano["und"][0]["entity"]->field_escalafon['und'][0]["value"]); ?></dd>
                                            <?php } ?>
                                            <?php if (strlen($node->field_decano["und"][0]["entity"]->field_mail["und"][0]["safe_value"]) > 1) { ?>   
                                                <dt>Correo electrónico:</dt><dd><small><?php
                                                        print(" " . $node->field_decano["und"][0]["entity"]->field_mail["und"][0]["safe_value"]);
                                                        ?></small></dd>
                                            <?php } ?>
                                            <?php if (strlen($node->field_decano["und"][0]["entity"]->field_telefono["und"][0]["safe_value"]) > 1) { ?>
                                                <dt>Teléfono:</dt><dd><?php
                                                    print(" " . $node->field_decano["und"][0]["entity"]->field_telefono["und"][0]["safe_value"]);
                                                    ?></dd>
                                            <?php } ?>
                                            <?php if (strlen($node->field_decano["und"][0]["entity"]->field_direccion["und"][0]["safe_value"]) > 1) { ?>
                                                <dt>Dirección de Oficina:</dt>
                                                <dd><?php print($node->field_decano["und"][0]["entity"]->field_direccion["und"][0]["safe_value"]); ?></dd>
                                            <?php } ?>
                                            <?php if (strlen($node->field_decano["und"][0]["entity"]->field_web_page["und"][0]['url']) > 1) { ?>
                                                <dt>Web Personal:</dt>
                                                <dd><a target="_blank" href="<?php print($node->field_decano["und"][0]["entity"]->field_web_page["und"][0]['url']) ?>"><?php print(" " . $node->field_decano["und"][0]["entity"]->field_web_page["und"][0]['title']); ?></a></dd>                                      
                                            <?php } ?>
                                        </dl>
                                    </div>
                                </div>
                            </div>

                        </div>

                        <div class="tab-pane" id="pregrado">

                            <h3>Programas</h3> 
                            <ul class="nav list-group" data-tabs="tabs">                       
                                <?php
                                for ($i = 0;
                                ; $i++) {
                                    $ID = $node->field_programas["und"][$i]["entity"]->nid;

                                    if ($ID == 0 or strcmp("0", $node->status) == 0) {
                                        break;
                                    }
                                    if ($node->field_programas["und"][$i]["entity"]->status > 0) {
                                        $alias = drupal_get_path_alias('node/' . $ID);
                                        ?>
                                        <li class="list-group-item" >
                                            <a href="<?php print($alias); ?>">
                                                <div class=""> <?php if ($node->field_programas["und"][$i]["entity"]->field_nuevo): if (strcmp($node->field_programas["und"][$i]["entity"]->field_nuevo ["und"][0]["value"], '1') == 0): ?>
                                                            <span class="badge pull-right badge-primary">Nuevo</span>    
                                                        <?php endif; ?>  <?php endif; ?>
                                                    <?php print($node->field_programas["und"][$i]["entity"]->title); ?> 
                                                </div>
                                            </a>
                                        </li>

                                        <?php
                                    }
                                }
                                ?>
                            </ul>

                        </div>

                        <div class="tab-pane" id="virtual">

                            <h3>Programas Virtuales</h3> 
                            <ul class="nav list-group" data-tabs="tabs">                       
                                <?php
                                for ($i = 0;
                                ; $i++) {
                                    $ID = $node->field_programas_virtual["und"][$i]["entity"]->nid;

                                    if ($ID == 0 or strcmp("0", $node->status) == 0) {
                                        break;
                                    }
                                    if ($node->field_programas_virtual["und"][$i]["entity"]->status > 0) {
                                        $alias = drupal_get_path_alias('node/' . $ID);
                                        ?>
                                        <li class="list-group-item" >
                                            <a href="<?php print($alias); ?>">
                                                <div class=""> <?php if ($node->field_programas_virtual["und"][$i]["entity"]->field_nuevo): if (strcmp($node->field_programas_virtual["und"][$i]["entity"]->field_nuevo ["und"][0]["value"], '1') == 0): ?>
                                                            <span class="badge pull-right badge-primary">Nuevo</span>    
                                                        <?php endif; ?>  <?php endif; ?>
                                                    <?php print($node->field_programas_virtual["und"][$i]["entity"]->title); ?> 
                                                </div>
                                            </a>
                                        </li>

                                        <?php
                                    }
                                }
                                ?>
                            </ul>

                        </div>

                        <div class="tab-pane" id="posgrados">

                            <?php
                            if ($node->field_programas_programaspos["und"][0]["target_id"]) :
                                ?>   
                                <div class="panel-group" id="panel-posg">

                                    <?php
                                    $esp_tea = array();
                                    $esp_teo = array();
                                    $esp_pro = array();
                                    $mae = array();
                                    $doc = array();


                                    for ($i = 0;
                                    ; $i++) :
                                        if (!$node->field_programas_programaspos["und"][$i]["target_id"]) :
                                            break;
                                        endif;
                                        $programa = node_load($node->field_programas_programaspos["und"][$i]["target_id"]);

                                        $temp = $programa->field_nivel["und"][0]['value'];

                                        if (strcmp($temp, "Especialización Técnica") == 0) {
                                            array_push($esp_tea, $programa);
                                        }
                                        if (strcmp($temp, "Especialización Tecnólogica") == 0) {
                                            array_push($esp_teo, $programa);
                                        }
                                        if (strcmp($temp, "Especialización Profesional") == 0) {
                                            array_push($esp_pro, $programa);
                                        }
                                        if (strcmp($temp, "Maestría") == 0) {
                                            array_push($mae, $programa);
                                        }
                                        if (strcmp($temp, "Doctorado") == 0) {
                                            array_push($doc, $programa);
                                        }
                                        ?>                   


                                    <?php endfor; ?>
                                    <!--                        CADA PROGRAMA/CURSO                     -->            

                                    <?php if (count($esp_tea) != 0): ?>
                                        <div class="panel panel-default">
                                            <div class="panel-heading">
                                                <a class="panel-title" data-parent="#panel-posg" data-toggle="collapse" href="#<?php print $node->nid . "esp_tea"; ?>"> Especializaciones Técnicas </a>
                                            </div>
                                            <div class="panel-collapse collapse" id="<?php print $node->nid . "esp_tea"; ?>" style="height: auto;">
                                                <div class="panel-body">
                                                    <p>La oferta de posgrado en especializaciones de <?php print $node->title; ?> está compuesta de la siguiente manera:</p>
                                                    <br>

                                                    <!--                        CADA PROGRAMA/CURSO                     -->
                                                    <div class="list-group">

                                                        <?php
                                                        for ($z = 0; $z < count($esp_tea); $z++) :
                                                            $pro = $esp_tea[$z];
                                                            $ID = $pro->nid;
                                                            $alias = drupal_get_path_alias('node/' . $ID);
                                                            ?>

                                                            <a class="list-group-item" href="<?php print($alias); ?>">
                                                                <?php if ($pro->field_nuevo): if (strcmp($pro->field_nuevo ["und"][0]["value"], '1') == 0): ?>
                                                                        <span class="badge pull-right badge-primary">Nuevo</span>    
                                                                    <?php endif; ?>  <?php endif; ?>

                                                                <?php if ($pro->field_modalidad): if (strcmp($pro->field_modalidad["und"][0]["value"], 'Virtual') == 0): ?>
                                                                        <span class="badge pull-right badge-brand">Virtual</span>    
                                                                    <?php endif; ?>  <?php endif; ?>   

                                                                <?php
                                                                print($pro->title);
                                                                ?>                                    
                                                            </a>
                                                        <?php endfor; ?>

                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    <?php endif; ?>
                                    <?php if (count($esp_teo) != 0): ?>
                                        <div class="panel panel-default">
                                            <div class="panel-heading">
                                                <a class="panel-title" data-parent="#panel-posg" data-toggle="collapse" href="#<?php print $node->nid . "esp_teo"; ?>"> Especializaciones Tecnólogicas </a>
                                            </div>
                                            <div class="panel-collapse collapse" id="<?php print $node->nid . "esp_teo"; ?>" style="height: auto;">
                                                <div class="panel-body">
                                                    <p>La oferta de posgrado en especializaciones de <?php print $node->title; ?> está compuesta de la siguiente manera:</p>
                                                    <br>

                                                    <!--                        CADA PROGRAMA/CURSO                     -->
                                                    <div class="list-group">

                                                        <?php
                                                        for ($z = 0; $z < count($esp_teo); $z++) :
                                                            $pro = $esp_teo[$z];
                                                            $ID = $pro->nid;
                                                            $alias = drupal_get_path_alias('node/' . $ID);
                                                            ?>

                                                            <a class="list-group-item" href="<?php print($alias); ?>">
                                                                <?php if ($pro->field_nuevo): if (strcmp($pro->field_nuevo ["und"][0]["value"], '1') == 0): ?>
                                                                        <span class="badge pull-right badge-primary">Nuevo</span>    
                                                                    <?php endif; ?>  <?php endif; ?>

                                                                <?php if ($pro->field_modalidad): if (strcmp($pro->field_modalidad["und"][0]["value"], 'Virtual') == 0): ?>
                                                                        <span class="badge pull-right badge-brand">Virtual</span>    
                                                                    <?php endif; ?>  <?php endif; ?>

                                                                <?php
                                                                print($pro->title);
                                                                ?>                                    
                                                            </a>
                                                        <?php endfor; ?>

                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    <?php endif; ?>
                                    <?php if (count($esp_pro) != 0): ?>               
                                        <div class="panel panel-default">
                                            <div class="panel-heading">
                                                <a class="panel-title" data-parent="#panel-posg" data-toggle="collapse" href="#<?php print $node->nid . "esp_pro"; ?>"> Especializaciones Profesionales </a>
                                            </div>
                                            <div class="panel-collapse collapse" id="<?php print $node->nid . "esp_pro"; ?>" style="height: auto;">
                                                <div class="panel-body">
                                                    <p>La oferta de posgrado en especializaciones de <?php print $node->title; ?> está compuesta de la siguiente manera:</p>
                                                    <br>

                                                    <!--                        CADA PROGRAMA/CURSO                     -->
                                                    <div class="list-group">

                                                        <?php
                                                        for ($z = 0; $z < count($esp_pro); $z++) :
                                                            $pro = $esp_pro[$z];
                                                            $ID = $pro->nid;
                                                            $alias = drupal_get_path_alias('node/' . $ID);
                                                            ?>

                                                            <a class="list-group-item" href="<?php print($alias); ?>">
                                                                <?php if ($pro->field_nuevo): if (strcmp($pro->field_nuevo ["und"][0]["value"], '1') == 0): ?>
                                                                        <span class="badge pull-right badge-primary">Nuevo</span>    
                                                                    <?php endif; ?>  <?php endif; ?>

                                                                <?php if ($pro->field_modalidad): if (strcmp($pro->field_modalidad["und"][0]["value"], 'Virtual') == 0): ?>
                                                                        <span class="badge pull-right badge-brand">Virtual</span>    
                                                                    <?php endif; ?>  <?php endif; ?>

                                                                <?php
                                                                print($pro->title);
                                                                ?>                                    
                                                            </a>
                                                        <?php endfor; ?>

                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    <?php endif; ?>               
                                    <?php if (count($mae) != 0): ?>
                                        <div class="panel panel-default">
                                            <div class="panel-heading">
                                                <a class="panel-title" data-parent="#panel-posg" data-toggle="collapse" href="#<?php print $node->nid . "mae"; ?>"> Maestrías </a>
                                            </div>
                                            <div class="panel-collapse collapse" id="<?php print $node->nid . "mae"; ?>" style="height: auto;">
                                                <div class="panel-body">
                                                    <p>La oferta de posgrado en maestrías de <?php print $node->title; ?> está compuesta de la siguiente manera:</p>
                                                    <br>

                                                    <!--                        CADA PROGRAMA/CURSO                     -->
                                                    <div class="list-group">

                                                        <?php
                                                        for ($z = 0; $z < count($mae); $z++) :
                                                            $pro = $mae[$z];
                                                            $ID = $pro->nid;
                                                            $alias = drupal_get_path_alias('node/' . $ID);
                                                            ?>

                                                            <a class="list-group-item" href="<?php print($alias); ?>">
                                                                <?php if ($pro->field_nuevo): if (strcmp($pro->field_nuevo ["und"][0]["value"], '1') == 0): ?>
                                                                        <span class="badge pull-right badge-primary">Nuevo</span>    
                                                                    <?php endif; ?>  <?php endif; ?>

                                                                <?php if ($pro->field_modalidad): if (strcmp($pro->field_modalidad["und"][0]["value"], 'Virtual') == 0): ?>
                                                                        <span class="badge pull-right badge-brand">Virtual</span>    
                                                                    <?php endif; ?>  <?php endif; ?>

                                                                <?php
                                                                print($pro->title);
                                                                ?>                                    
                                                            </a>
                                                        <?php endfor; ?>

                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    <?php endif; ?>
                                    <?php if (count($doc) != 0): ?>
                                        <div class="panel panel-default">
                                            <div class="panel-heading">
                                                <a class="panel-title" data-parent="#panel-posg" data-toggle="collapse" href="#<?php print $node->nid . "doc"; ?>"> Doctorados </a>
                                            </div>
                                            <div class="panel-collapse collapse" id="<?php print $node->nid . "doc"; ?>" style="height: auto;">
                                                <div class="panel-body">
                                                    <p>La oferta de posgrado en doctorado de <?php print $node->title; ?> está compuesta de la siguiente manera:</p>
                                                    <br>

                                                    <!--                        CADA PROGRAMA/CURSO                     -->
                                                    <div class="list-group">

                                                        <?php
                                                        for ($z = 0; $z < count($doc); $z++) :
                                                            $pro = $doc[$z];
                                                            $ID = $pro->nid;
                                                            $alias = drupal_get_path_alias('node/' . $ID);
                                                            ?>

                                                            <a class="list-group-item" href="<?php print($alias); ?>">
                                                                <?php if ($pro->field_nuevo): if (strcmp($pro->field_nuevo ["und"][0]["value"], '1') == 0): ?>
                                                                        <span class="badge pull-right badge-primary">Nuevo</span>    
                                                                    <?php endif; ?>  <?php endif; ?>

                                                                <?php if ($pro->field_modalidad): if (strcmp($pro->field_modalidad["und"][0]["value"], 'Virtual') == 0): ?>
                                                                        <span class="badge pull-right badge-brand">Virtual</span>    
                                                                    <?php endif; ?>  <?php endif; ?>

                                                                <?php
                                                                print($pro->title);
                                                                ?>                                    
                                                            </a>
                                                        <?php endfor; ?>

                                                    </div>
                                                </div>
                                            </div>
                                        </div> 
                                    <?php endif; ?>
                                </div>

                            <?php endif; ?> 

                        </div>

                        <div class="tab-pane" id="cursos">

                            <?php
                            if ($node->field_cursos["und"][0]["target_id"]) :
                                ?>  
                                <div class="panel-group" id="panel-ext">                    

                                    <?php
                                    $cur = array();
                                    $sem = array();
                                    $dip = array();
                                    $min = array();
                                    $tal = array();
                                    $cha = array();
                                    $conf = array();


                                    for ($i = 0;
                                    ; $i++) :
                                        if (!$node->field_cursos["und"][$i]["target_id"]) :
                                            break;
                                        endif;
                                        $programa = node_load($node->field_cursos["und"][$i]["target_id"]);

                                        $temp = $programa->field_tipo["und"][0]['value'];


                                        if (strcmp($temp, "Curso") == 0) {
                                            array_push($cur, $programa);
                                        }
                                        if (strcmp($temp, "Seminario") == 0) {
                                            array_push($sem, $programa);
                                        }
                                        if (strcmp($temp, "Diplomado") == 0) {
                                            array_push($dip, $programa);
                                        }
                                        if (strcmp($temp, "Minor") == 0) {
                                            array_push($min, $programa);
                                        }
                                        if (strcmp($temp, "Taller") == 0) {
                                            array_push($tal, $programa);
                                        }
                                        if (strcmp($temp, "Charla") == 0) {
                                            array_push($cha, $programa);
                                        }
                                        if (strcmp($temp, "Conferencia") == 0) {
                                            array_push($conf, $programa);
                                        }
                                        ?>                   


                                    <?php endfor; ?>
                                    <!--                        CADA PROGRAMA/CURSO                     -->            
                                    <?php if (count($cur) != 0): ?>
                                        <div class="panel panel-default">
                                            <div class="panel-heading">
                                                <a class="panel-title" data-parent="#panel-ext" data-toggle="collapse" href="#<?php print $node->nid . "cur"; ?>"> Cursos </a>
                                            </div>
                                            <div class="panel-collapse collapse" id="<?php print $node->nid . "cur"; ?>" style="height: auto;">
                                                <div class="panel-body">
                                                    <p>La oferta de Cursos de <?php print $node->title; ?> está compuesta de la siguiente manera:</p>
                                                    <br>

                                                    <!--                        CADA PROGRAMA/CURSO                     -->
                                                    <div class="list-group">

                                                        <?php
                                                        for ($z = 0; $z < count($cur); $z++) :
                                                            $pro = $cur[$z];
                                                            $ID = $pro->nid;
                                                            $alias = drupal_get_path_alias('node/' . $ID);
                                                            ?>

                                                            <a class="list-group-item" href="<?php print($alias); ?>">
                                                                <?php if ($pro->field_nuevo): if (strcmp($pro->field_nuevo ["und"][0]["value"], '1') == 0): ?>
                                                                        <span class="badge pull-right badge-primary">Nuevo</span>    
                                                                    <?php endif; ?>  <?php endif; ?>

                                                                <?php if ($pro->field_modalidad): if (strcmp($pro->field_modalidad["und"][0]["value"], 'Virtual') == 0): ?>
                                                                        <span class="badge pull-right badge-brand">Virtual</span>    
                                                                    <?php endif; ?>  <?php endif; ?>

                                                                <?php
                                                                print($pro->title);
                                                                ?>                                    
                                                            </a>
                                                        <?php endfor; ?>

                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    <?php endif; ?>
                                    <?php if (count($sem) != 0): ?> 
                                        <div class="panel panel-default">
                                            <div class="panel-heading">
                                                <a class="panel-title" data-parent="#panel-ext" data-toggle="collapse" href="#<?php print $node->nid . "sem"; ?>"> Seminarios </a>
                                            </div>
                                            <div class="panel-collapse collapse" id="<?php print $node->nid . "sem"; ?>" style="height: auto;">
                                                <div class="panel-body">
                                                    <p>La oferta de Seminarios de <?php print $node->title; ?> está compuesta de la siguiente manera:</p>
                                                    <br>

                                                    <!--                        CADA PROGRAMA/CURSO                     -->
                                                    <div class="list-group">

                                                        <?php
                                                        for ($z = 0; $z < count($sem); $z++) :
                                                            $pro = $sem[$z];
                                                            $ID = $pro->nid;
                                                            $alias = drupal_get_path_alias('node/' . $ID);
                                                            ?>

                                                            <a class="list-group-item" href="<?php print($alias); ?>">
                                                                <?php if ($pro->field_nuevo): if (strcmp($pro->field_nuevo ["und"][0]["value"], '1') == 0): ?>
                                                                        <span class="badge pull-right badge-primary">Nuevo</span>    
                                                                    <?php endif; ?>  <?php endif; ?>

                                                                <?php if ($pro->field_modalidad): if (strcmp($pro->field_modalidad["und"][0]["value"], 'Virtual') == 0): ?>
                                                                        <span class="badge pull-right badge-brand">Virtual</span>    
                                                                    <?php endif; ?>  <?php endif; ?>

                                                                <?php
                                                                print($pro->title);
                                                                ?>                                    
                                                            </a>
                                                        <?php endfor; ?>

                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    <?php endif; ?>
                                    <?php if (count($dip) != 0): ?>               
                                        <div class="panel panel-default">
                                            <div class="panel-heading">
                                                <a class="panel-title" data-parent="#panel-ext" data-toggle="collapse" href="#<?php print $node->nid . "dip"; ?>"> Diplomados </a>
                                            </div>
                                            <div class="panel-collapse collapse" id="<?php print $node->nid . "dip"; ?>" style="height: auto;">
                                                <div class="panel-body">
                                                    <p>La oferta de diplomados de <?php print $node->title; ?> está compuesta de la siguiente manera:</p>
                                                    <br>

                                                    <!--                        CADA PROGRAMA/CURSO                     -->
                                                    <div class="list-group">

                                                        <?php
                                                        for ($z = 0; $z < count($dip); $z++) :
                                                            $pro = $dip[$z];
                                                            $ID = $pro->nid;
                                                            $alias = drupal_get_path_alias('node/' . $ID);
                                                            ?>

                                                            <a class="list-group-item" href="<?php print($alias); ?>">
                                                                <?php if ($pro->field_nuevo): if (strcmp($pro->field_nuevo ["und"][0]["value"], '1') == 0): ?>
                                                                        <span class="badge pull-right badge-primary">Nuevo</span>    
                                                                    <?php endif; ?>  <?php endif; ?>

                                                                <?php if ($pro->field_modalidad): if (strcmp($pro->field_modalidad["und"][0]["value"], 'Virtual') == 0): ?>
                                                                        <span class="badge pull-right badge-brand">Virtual</span>    
                                                                    <?php endif; ?>  <?php endif; ?>

                                                                <?php
                                                                print($pro->title);
                                                                ?>                                    
                                                            </a>
                                                        <?php endfor; ?>

                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    <?php endif; ?>               
                                    <?php if (count($min) != 0): ?>
                                        <div class="panel panel-default">
                                            <div class="panel-heading">
                                                <a class="panel-title" data-parent="#panel-ext" data-toggle="collapse" href="#<?php print $node->nid . "min"; ?>"> Minors </a>
                                            </div>
                                            <div class="panel-collapse collapse" id="<?php print $node->nid . "min"; ?>" style="height: auto;">
                                                <div class="panel-body">
                                                    <p>La oferta de posgrado en minor de <?php print $node->title; ?> está compuesta de la siguiente manera:</p>
                                                    <br>

                                                    <!--                        CADA PROGRAMA/CURSO                     -->
                                                    <div class="list-group">

                                                        <?php
                                                        for ($z = 0; $z < count($min); $z++) :
                                                            $pro = $min[$z];
                                                            $ID = $pro->nid;
                                                            $alias = drupal_get_path_alias('node/' . $ID);
                                                            ?>

                                                            <a class="list-group-item" href="<?php print($alias); ?>">
                                                                <?php if ($pro->field_nuevo): if (strcmp($pro->field_nuevo ["und"][0]["value"], '1') == 0): ?>
                                                                        <span class="badge pull-right badge-primary">Nuevo</span>    
                                                                    <?php endif; ?>  <?php endif; ?>

                                                                <?php if ($pro->field_modalidad): if (strcmp($pro->field_modalidad["und"][0]["value"], 'Virtual') == 0): ?>
                                                                        <span class="badge pull-right badge-brand">Virtual</span>    
                                                                    <?php endif; ?>  <?php endif; ?>

                                                                <?php
                                                                print($pro->title);
                                                                ?>                                    
                                                            </a>
                                                        <?php endfor; ?>

                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    <?php endif; ?>
                                    <?php if (count($tal) != 0): ?>
                                        <div class="panel panel-default">
                                            <div class="panel-heading">
                                                <a class="panel-title" data-parent="#panel-ext" data-toggle="collapse" href="#<?php print $node->nid . "tal"; ?>"> Talleres</a>
                                            </div>
                                            <div class="panel-collapse collapse" id="<?php print $node->nid . "tal"; ?>" style="height: auto;">
                                                <div class="panel-body">
                                                    <p>La oferta de posgrado en talleres de <?php print $node->title; ?> está compuesta de la siguiente manera:</p>
                                                    <br>

                                                    <!--                        CADA PROGRAMA/CURSO                     -->
                                                    <div class="list-group">

                                                        <?php
                                                        for ($z = 0; $z < count($tal); $z++) :
                                                            $pro = $tal[$z];
                                                            $ID = $pro->nid;
                                                            $alias = drupal_get_path_alias('node/' . $ID);
                                                            ?>

                                                            <a class="list-group-item" href="<?php print($alias); ?>">
                                                                <?php if ($pro->field_nuevo): if (strcmp($pro->field_nuevo ["und"][0]["value"], '1') == 0): ?>
                                                                        <span class="badge pull-right badge-primary">Nuevo</span>    
                                                                    <?php endif; ?>  <?php endif; ?>

                                                                <?php if ($pro->field_modalidad): if (strcmp($pro->field_modalidad["und"][0]["value"], 'Virtual') == 0): ?>
                                                                        <span class="badge pull-right badge-brand">Virtual</span>    
                                                                    <?php endif; ?>  <?php endif; ?>

                                                                <?php
                                                                print($pro->title);
                                                                ?>                                    
                                                            </a>
                                                        <?php endfor; ?>

                                                    </div>
                                                </div>
                                            </div>
                                        </div> 
                                    <?php endif; ?>
                                    <?php if (count($cha) != 0): ?>
                                        <div class="panel panel-default">
                                            <div class="panel-heading">
                                                <a class="panel-title" data-parent="#panel-ext" data-toggle="collapse" href="#<?php print $node->nid . "cha"; ?>"> Charlas </a>
                                            </div>
                                            <div class="panel-collapse collapse" id="<?php print $node->nid . "cha"; ?>" style="height: auto;">
                                                <div class="panel-body">
                                                    <p>La oferta de charlas de <?php print $node->title; ?> está compuesta de la siguiente manera:</p>
                                                    <br>

                                                    <!--                        CADA PROGRAMA/CURSO                     -->
                                                    <div class="list-group">

                                                        <?php
                                                        for ($z = 0; $z < count($cha); $z++) :
                                                            $pro = $cha[$z];
                                                            $ID = $pro->nid;
                                                            $alias = drupal_get_path_alias('node/' . $ID);
                                                            ?>

                                                            <a class="list-group-item" href="<?php print($alias); ?>">
                                                                <?php if ($pro->field_nuevo): if (strcmp($pro->field_nuevo ["und"][0]["value"], '1') == 0): ?>
                                                                        <span class="badge pull-right badge-primary">Nuevo</span>    
                                                                    <?php endif; ?>  <?php endif; ?>

                                                                <?php if ($pro->field_modalidad): if (strcmp($pro->field_modalidad["und"][0]["value"], 'Virtual') == 0): ?>
                                                                        <span class="badge pull-right badge-brand">Virtual</span>    
                                                                    <?php endif; ?>  <?php endif; ?>

                                                                <?php
                                                                print($pro->title);
                                                                ?>                                    
                                                            </a>
                                                        <?php endfor; ?>

                                                    </div>
                                                </div>
                                            </div>
                                        </div> 
                                    <?php endif; ?>
                                    <?php if (count($conf) != 0): ?>
                                        <div class="panel panel-default">
                                            <div class="panel-heading">
                                                <a class="panel-title" data-parent="#panel-ext" data-toggle="collapse" href="#<?php print $node->nid . "conf"; ?>"> Conferencias </a>
                                            </div>
                                            <div class="panel-collapse collapse" id="<?php print $node->nid . "conf"; ?>" style="height: auto;">
                                                <div class="panel-body">
                                                    <p>La oferta de conferencias de <?php print $node->title; ?> está compuesta de la siguiente manera:</p>
                                                    <br>

                                                    <!--                        CADA PROGRAMA/CURSO                     -->
                                                    <div class="list-group">

                                                        <?php
                                                        for ($z = 0; $z < count($conf); $z++) :
                                                            $pro = $conf[$z];
                                                            $ID = $pro->nid;
                                                            $alias = drupal_get_path_alias('node/' . $ID);
                                                            ?>

                                                            <a class="list-group-item" href="<?php print($alias); ?>">
                                                                <?php if ($pro->field_nuevo): if (strcmp($pro->field_nuevo ["und"][0]["value"], '1') == 0): ?>
                                                                        <span class="badge pull-right badge-primary">Nuevo</span>    
                                                                    <?php endif; ?>  <?php endif; ?>

                                                                <?php if ($pro->field_modalidad): if (strcmp($pro->field_modalidad["und"][0]["value"], 'Virtual') == 0): ?>
                                                                        <span class="badge pull-right badge-brand">Virtual</span>    
                                                                    <?php endif; ?>  <?php endif; ?>

                                                                <?php
                                                                print($pro->title);
                                                                ?>                                    
                                                            </a>
                                                        <?php endfor; ?>

                                                    </div>
                                                </div>
                                            </div>
                                        </div> 
                                    <?php endif; ?>
                                </div>

                            <?php endif; ?> 

                        </div>

                        <div class="tab-pane" id="investigacion">

                            <?php print($node->field_investigacion["und"][0]["safe_value"]); ?>

                            <?php
                            if ($node->field_grupo_de_investigacion["und"][0]["value"]) {
                                db_set_active('investigaciones');
                                for ($i = 0;; $i++) {
                                    $ID = $node->field_grupo_de_investigacion["und"][$i]["value"];
                                    if ($ID == 0) {
                                        break;
                                    }
                                    $grupo = node_load($ID);
                                    print $grupo->title . "<br>";
                                }
                                db_set_active();
                            }
                            ?>

                            <?php
                            if ($node->field_semilleros["und"][0]["value"]) {
                                db_set_active('investigaciones');
                                for ($i = 0;; $i++) {
                                    $ID = $node->field_semilleros["und"][$i]["value"];
                                    if ($ID == 0) {
                                        break;
                                    }
                                    $semillero = node_load($ID);
                                    print $semillero->title . "<br>";
                                }
                                db_set_active();
                            }
                            ?>

                        </div>

                        <div class="tab-pane" id="laboratorios">

                            <?php
                            if ($node->field_laboratorios["und"][0]["value"]) {
                                db_set_active('investigaciones');
                                for ($i = 0;; $i++) {
                                    $ID = $node->field_laboratorios["und"][$i]["value"];
                                    if ($ID == 0) {
                                        break;
                                    }
                                    $labs = node_load($ID);
                                    print $labs->title . "<br>";
                                }
                                db_set_active();
                            }
                            ?>

                        </div>

                        <div class="tab-pane" id="profesores">


                            <?php if ($node->field_profesores_tc["und"][0]["entity"]->nid) { ?>
                                <h3>Profesores de tiempo completo</h3>

                                <ul class="nav nav-tabs prog" data-tabs="tabs">                       
                                    <?php
                                    for ($i = 0;; $i++) {
                                        $ID = $node->field_profesores_tc["und"][$i]["entity"]->nid;
                                        if ($ID == 0) {
                                            break;
                                        }
                                        $alias = drupal_get_path_alias('node/' . $ID);
                                        if (strcmp("0", $node->field_profesores_tc["und"][$i]["entity"]->status) != 0) {
                                            ?>
                                            <div class="col-md-6 thumbnail" style="height: 270px; font-size: 13px;">      
                                                <a href="<?php print($alias); ?>">                                                
                                                    <?php print($node->field_profesores_tc["und"][$i]["entity"]->title . ", "); ?><?php print($node->field_profesores_tc["und"][$i]["entity"]->field_nivel_academico["und"][0]["value"]); ?>   
                                                </a><br/><br/>
                                                <div class="col-md-5">
                                                    <div style="margin-bottom: 10px;"> <?php echo theme('image_style', array('style_name' => 'thumbnail', 'path' => $node->field_profesores_tc["und"][$i]["entity"]->field_staffs_image['und'][0]['uri'], 'attributes' => array('class' => 'img-responsive'))); ?>        </div>                            
                                                    <div class="portada_profesor"> <?php print($node->field_profesores_tc["und"][$i]["entity"]->field_social["und"][0]["safe_value"]); ?> </div>
                                                </div>

                                                <div class="col-md-7">
                                                    <dl>  
                                                        <?php if (strlen($node->field_profesores_tc["und"][$i]["entity"]->field_enlace_al_cvlac["und"][0]['url']) > 1) { ?>
                                                            <dt>CvLAC:</dt>
                                                            <dd><a target="_blank" href="<?php print($node->field_profesores_tc["und"][$i]["entity"]->field_enlace_al_cvlac["und"][0]['url']) ?>"><?php print(" " . $node->field_profesores_tc["und"][$i]["entity"]->field_enlace_al_cvlac["und"][0]['title']); ?></a></dd>
                                                        <?php } ?>                    
                                                        <?php if ($node->field_profesores_tc["und"][$i]["entity"]->field_escalafon['und'][0]["value"] <> "No escalafonado") { ?>                       
                                                            <dt>Escalafón: </dt> 
                                                            <dd> <?php print(" " . $node->field_profesores_tc["und"][$i]["entity"]->field_escalafon['und'][0]["value"]); ?></dd>
                                                        <?php } ?>
                                                        <?php if (strlen($node->field_profesores_tc["und"][$i]["entity"]->field_mail["und"][0]["safe_value"]) > 1) { ?>   
                                                            <dt>Correo electrónico:</dt><dd><small><?php
                                                                    print(" " . $node->field_profesores_tc["und"][$i]["entity"]->field_mail["und"][0]["safe_value"]);
                                                                    ?></small></dd>
                                                        <?php } ?>
                                                        <?php if (strlen($node->field_profesores_tc["und"][$i]["entity"]->field_telefono["und"][0]["safe_value"]) > 1) { ?>
                                                            <dt>Teléfono:</dt><dd><?php
                                                                print(" " . $node->field_profesores_tc["und"][$i]["entity"]->field_telefono["und"][0]["safe_value"]);
                                                                ?></dd>
                                                        <?php } ?>
                                                        <?php if (strlen($node->field_profesores_tc["und"][$i]["entity"]->field_direccion["und"][0]["safe_value"]) > 1) { ?>
                                                            <dt>Dirección de Oficina:</dt>
                                                            <dd><?php print($node->field_profesores_tc["und"][$i]["entity"]->field_direccion["und"][0]["safe_value"]); ?></dd>
                                                        <?php } ?></dl>

                                                </div>
                                            </div>  

                                            <?php
                                        }
                                    }
                                    ?>


                                <?php }
                                ?>

                                <?php
//                                $bandera = TRUE;
                                $listado_profesores = array();
                                for ($i = 0;
                                ; $i++) {
                                    if (!isset($node->field_programas["und"][$i]["entity"]->nid)) {
                                        break;
                                    }
                                    for ($j = 0;
                                    ; $j++) {

                                        $ID = $node->field_programas["und"][$i]["entity"]->field_profesores_tc["und"][$j]["target_id"];
                                        if ($ID == 0) {
                                            break;
                                        }
                                        if (node_load($ID)) {
                                            $alias = drupal_get_path_alias('node/' . $ID);

//                                            for ($x = 0; $x < count($miarray); $x++) {
//                                                if ($ID == $miarray[$x]) {
//                                                    $bandera = false;
//                                                    $x = count($miarray);
//                                                }
//                                            }
                                            if (!in_array($ID, $listado_profesores)) {

                                                $listado_profesores[] = $ID;

                                                $nodetemp = node_load($ID);

                                                if (strcmp("0", $nodetemp->status) != 0) {
                                                    ?>
                                                    <div class="profesores">    
                                                        <div class="col-md-6 thumbnail" style="height: 270px; font-size: 13px;">
                                                            <a href="<?php print($alias); ?>">                                                
                                                                <?php print($nodetemp->title . ", "); ?><?php print($nodetemp->field_nivel_academico["und"][0]["value"]); ?>  </a>
                                                            <br/><br/>
                                                            <div class="col-md-5">
                                                                <div style="margin-bottom: 10px;">
                                                                    <?php echo theme('image_style', array('style_name' => 'thumbnail', 'path' => $nodetemp->field_staffs_image['und'][0]['uri'], 'attributes' => array('class' => 'img-responsive'))); ?>                                    
                                                                </div>
                                                                <div class="portada_profesor"> <?php print($nodetemp->field_social["und"][0]["safe_value"]); ?></div>
                                                            </div>
                                                            <div class="col-md-7">
                                                                <dl>  
                                                                    <?php if (strlen($nodetemp->field_enlace_al_cvlac["und"][0]['url']) > 1) { ?>
                                                                        <dt>CvLAC:</dt>
                                                                        <dd><a target="_blank" href="<?php print($nodetemp->field_enlace_al_cvlac["und"][0]['url']) ?>"><?php print(" " . $nodetemp->field_enlace_al_cvlac["und"][0]['title']); ?></a></dd>
                                                                    <?php } ?>                    
                                                                    <?php if ($nodetemp->field_escalafon['und'][0]["value"] <> "No escalafonado") { ?>                       
                                                                        <dt>Escalafón: </dt> 
                                                                        <dd> <?php print(" " . $nodetemp->field_escalafon['und'][0]["value"]); ?></dd>
                                                                    <?php } ?>
                                                                    <?php if (strlen($nodetemp->field_mail["und"][0]["safe_value"]) > 1) { ?>   
                                                                        <dt>Correo electrónico:</dt><dd><small><?php
                                                                                print(" " . $nodetemp->field_mail["und"][0]["safe_value"]);
                                                                                ?></small></dd>
                                                                    <?php } ?>
                                                                    <?php if (strlen($nodetemp->field_telefono["und"][0]["safe_value"]) > 1) { ?>
                                                                        <dt>Teléfono:</dt><dd><?php
                                                                            print(" " . $nodetemp->field_telefono["und"][0]["safe_value"]);
                                                                            ?></dd>
                                                                    <?php } ?>
                                                                    <?php if (strlen($nodetemp->field_direccion["und"][0]["safe_value"]) > 1) { ?>
                                                                        <dt>Dirección de Oficina:</dt>
                                                                        <dd><?php print($nodetemp->field_direccion["und"][0]["safe_value"]); ?></dd>
                                                                    <?php } ?></dl>

                                                            </div>
                                                        </div>
                                                    </div>

                                                    <?php
                                                }
                                            }
                                        }
                                    }
                                }
                                ?>

                        </div>

                        <div class="tab-pane" id="contacto">

                            <?php print($node->field_contacto["und"][0]["safe_value"]); ?>

                        </div>	  

                    </div>

                </div>

            </div>

        </div>

    </div>

</div>