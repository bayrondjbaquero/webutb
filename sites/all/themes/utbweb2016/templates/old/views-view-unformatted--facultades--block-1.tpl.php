<div class="panel panel-brand">

    <div class="panel-heading">

        <a class="panel-title" data-toggle="collapse" data-parent="#panel-ver-posgrado" href="#panel-element-ver-posgrado">
            Clic para ver Programas de Posgrado<span class="caret"></span>
        </a>
        <i class="fa fa-university fa-5x pull-right"></i>

        <div class="padding-x">
            Nuestros programas aportan una ventaja competitiva en el ámbito teórico y práctico de tu profesión, focalizando tu desarrollo en áreas específicas, en campos de investigación y de producción intelectual.
            En la UTB te ofrecemos las herramientas para continuar tu desarrollo profesional  hacia un mundo de oportunidades.
        </div>

        <div id="panel-element-ver-posgrado" class="panel-collapse collapse bg-brand">

            <div class="panel-body">	

                <div class="col-lg-12 thumbnail text-muted padding-x">

                    <div class="panel-group" id="panel-pos">

                        <?php foreach ($rows as $id => $row): ?>

                            <?php print $row; ?>

                        <?php endforeach; ?>

                    </div>				

                </div>	

            </div>

        </div>

    </div>	
</div>