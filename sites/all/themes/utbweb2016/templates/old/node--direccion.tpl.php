<div id="node-<?php print $node->nid; ?>" class="<?php print $classes; ?> clearfix"<?php print $attributes; ?>>

    <div class="row clearfix">

        <div class="col-md-12 column">

            <?php print($node->body["und"][0]['safe_summary']); ?>

            <div class="row" style="margin-bottom: 30px;">

                <!-- Navigation Buttons -->
                <div class="col-md-2">

                    <ul class="nav nav-pills nav-stacked" id="Programas">

                        <?php if ($node->body["und"][0]["safe_value"]) { ?> 
                            <li class="active"><a href="#descripcion"  data-toggle="tab">Descripción</a></li><?php } ?> 

                        <?php if ($node->field_director["und"][0]["entity"]) { ?> 
                            <li ><a href="#director" data-toggle="tab">Director</a></li><?php } ?>

                        <?php if ($node->field_diplomados["und"][0]["entity"]) { ?> 
                            <li><a href="#diplomados" data-toggle="tab">Diplomados</a></li><?php } ?>

                        <?php if ($node->field_cursos_presenciales["und"][0]["entity"]) { ?> 
                            <li><a href="#cursos_presenciales" data-toggle="tab">Cursos presenciales</a></li><?php } ?>

                        <?php if ($node->field_cursos_virtuales["und"][0]["entity"]) { ?> 
                            <li><a href="#cursos_virtuales" data-toggle="tab">Cursos virtuales</a></li><?php } ?>

                        <?php if ($node->field_cursos_para_ni_os["und"][0]["entity"]) { ?> 
                            <li><a href="#cursos_ninos" data-toggle="tab">Cursos para niños</a></li><?php } ?>

                        <?php if ($node->field_seminarios["und"][0]["entity"]) { ?> 
                            <li><a href="#seminarios" data-toggle="tab">Seminarios</a></li><?php } ?> 

                        <?php if ($node->field_certificaciones["und"][0]["safe_value"]) { ?> 
                            <li><a href="#certificaciones" data-toggle="tab">Certificaciones</a></li><?php } ?>

                        <?php if ($node->field_curso_contacto["und"][0]["safe_value"]) { ?>    
                            <li><a href="#contacto" data-toggle="tab">Contacto</a></li><?php } ?> 

                    </ul>

                </div>

                <!-- Content -->
                <div class="col-md-10">

                    <div class="tab-content">

                        <div class="tab-pane active" id="descripcion">
                            <?php print render($content['body']); ?>
                        </div>

                        <div class="tab-pane " id="director">
                            <a href="<?php echo url("node/" . $node->field_director["und"][0]["entity"]->nid);
                            ?>">
                                   <?php
                                   print("<h3>");
                                   print($node->field_director["und"][0]["entity"]->title);
                                   print(", ");
                                   print($node->field_director["und"][0]["entity"]->field_nivel_academico["und"][0]["value"]);
                                   print("</h3>");
                                   //@TODO: incluir redes sociales
                                   ?>
                            </a>
                            <div class="row ">
                                <div class="col-md-6">
                                    <div>
                                        <?php echo theme('image_style', array('style_name' => 'staff', 'path' => $node->field_director["und"][0]["entity"]->field_staffs_image['und'][0]['uri'], 'attributes' => array('class' => 'img-responsive'))); ?> 
                                        <?php print($node->field_director["und"][0]["entity"]->field_social["und"][0]["safe_value"]); ?>
                                    </div>
                                </div>

                                <div class="col-md-6">
                                    <div class="pull-left">
                                        <dl>  
                                            <?php if (strlen($node->field_director["und"][0]["entity"]->field_enlace_al_cvlac["und"][0]['url']) > 1) { ?>
                                                <dt>CvLAC:</dt>
                                                <dd><a target="_blank" href="<?php print($node->field_director["und"][0]["entity"]->field_enlace_al_cvlac["und"][0]['url']) ?>"><?php print(" " . $node->field_director["und"][0]["entity"]->field_enlace_al_cvlac["und"][0]['title']); ?></a></dd>
                                            <?php } ?>                    
                                            <?php if ($node->field_director["und"][0]["entity"]->field_escalafon['und'][0]["value"] <> "No escalafonado") { ?>                       
                                                <dt>Escalafón: </dt> 
                                                <dd> <?php print(" " . $node->field_director["und"][0]["entity"]->field_escalafon['und'][0]["value"]); ?></dd>
                                            <?php } ?>
                                            <?php if (strlen($node->field_director["und"][0]["entity"]->field_mail["und"][0]["safe_value"]) > 1) { ?>   
                                                <dt>Correo electrónico:</dt><dd><small><?php
                                                        print(" " . $node->field_director["und"][0]["entity"]->field_mail["und"][0]["safe_value"]);
                                                        ?></small></dd>
                                            <?php } ?>
                                            <?php if (strlen($node->field_director["und"][0]["entity"]->field_telefono["und"][0]["safe_value"]) > 1) { ?>
                                                <dt>Teléfono:</dt><dd><?php
                                                    print(" " . $node->field_director["und"][0]["entity"]->field_telefono["und"][0]["safe_value"]);
                                                    ?></dd>
                                            <?php } ?>
                                            <?php if (strlen($node->field_director["und"][0]["entity"]->field_direccion["und"][0]["safe_value"]) > 1) { ?>
                                                <dt>Dirección de Oficina:</dt>
                                                <dd><?php print($node->field_director["und"][0]["entity"]->field_direccion["und"][0]["safe_value"]); ?></dd>
                                            <?php } ?>
                                            <?php if (strlen($node->field_director["und"][0]["entity"]->field_web_page["und"][0]['url']) > 1) { ?>
                                                <dt>Web Personal:</dt>
                                                <dd><a target="_blank" href="<?php print($node->field_director["und"][0]["entity"]->field_web_page["und"][0]['url']) ?>"><?php print(" " . $node->field_director["und"][0]["entity"]->field_web_page["und"][0]['title']); ?></a></dd>                                      
                                            <?php } ?>
                                        </dl>
                                    </div>
                                </div>
                            </div>

                        </div>

                        <div class="tab-pane" id="diplomados">

                            <h3>Diplomados</h3> 
                            <ul class="nav list-group" data-tabs="tabs">                       
                                <?php
                                $f_bas = array();
                                $f_soc = array();
                                $f_eco = array();
                                $f_ing = array();
                                $f_edu = array();
                                $f_tyt = array();
                                $f_int = array();
                                $f_ver = array();


                                for ($i = 0;; $i++) {
                                    $entidad = $node->field_diplomados["und"][$i]["entity"];
                                    $ID = $entidad->nid;
                                    if ($ID == 0 or strcmp("0", $entidad->status) == 0) {
                                        break;
                                    }
                                    $term = taxonomy_term_load($entidad->field_unidad_de_contenido_curso['und'][$i]['tid']);
                                    $temp = $term->name;
                                    if (strcmp($temp, "Facultad de Ciencias Básicas") == 0) {
                                        array_push($f_bas, $entidad);
                                    } if (strcmp($temp, "Facultad de Ciencias Sociales y Humanas") == 0) {
                                        array_push($f_soc, $entidad);
                                    } if (strcmp($temp, "Facultad de Economía y Negocios") == 0) {
                                        array_push($f_eco, $entidad);
                                    } if (strcmp($temp, "Facultad de ingeniería") == 0) {
                                        array_push($f_ing, $entidad);
                                    } if (strcmp($temp, "Dirección de Educación") == 0) {
                                        array_push($f_edu, $entidad);
                                    } if (strcmp($temp, "Escuela de Estudios Técnicos y Tecnológicos") == 0) {
                                        array_push($f_tyt, $entidad);
                                    } if (strcmp($temp, "Intersemestral") == 0) {
                                        array_push($f_int, $entidad);
                                    } if (strcmp($temp, "Escuela de Verano") == 0) {
                                        array_push($f_ver, $entidad);
                                    }
                                }
                                ?>

<?php if (count($f_bas) != 0): ?>
                                    <div class="panel panel-default">
                                        <div class="panel-heading">
                                            <a class="panel-title" data-parent="#panel-ext" data-toggle="collapse" href="#<?php print $node->nid . "f_bas"; ?>"> Facultad de Ciencias Básicas </a>
                                        </div>
                                        <div class="panel-collapse collapse" id="<?php print $node->nid . "f_bas"; ?>" style="height: auto;">
                                            <div class="panel-body">
                                                <!--                        CADA PROGRAMA/CURSO                     -->
                                                <div class="list-group">

                                                    <?php
                                                    for ($z = 0; $z < count($f_bas); $z++) :
                                                        $pro = $f_bas[$z];
                                                        $ID = $pro->nid;
                                                        $alias = drupal_get_path_alias('node/' . $ID);
                                                        ?>

                                                        <a class="list-group-item" href="<?php print($alias); ?>">
                                                            <?php
                                                            print($pro->title);
                                                            ?>                                    
                                                        </a>
    <?php endfor; ?>

                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                <?php endif; ?>

<?php if (count($f_soc) != 0): ?>
                                    <div class="panel panel-default">
                                        <div class="panel-heading">
                                            <a class="panel-title" data-parent="#panel-ext" data-toggle="collapse" href="#<?php print $node->nid . "soc"; ?>"> Facultad de Ciencias Básicas </a>
                                        </div>
                                        <div class="panel-collapse collapse" id="<?php print $node->nid . "soc"; ?>" style="height: auto;">
                                            <div class="panel-body">
                                                <!--                        CADA PROGRAMA/CURSO                     -->
                                                <div class="list-group">

                                                    <?php
                                                    for ($z = 0; $z < count($f_soc); $z++) :
                                                        $pro = $f_soc[$z];
                                                        $ID = $pro->nid;
                                                        $alias = drupal_get_path_alias('node/' . $ID);
                                                        ?>

                                                        <a class="list-group-item" href="<?php print($alias); ?>">
                                                            <?php
                                                            print($pro->title);
                                                            ?>                                    
                                                        </a>
    <?php endfor; ?>

                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                <?php endif; ?>

<?php if (count($f_eco) != 0): ?>
                                    <div class="panel panel-default">
                                        <div class="panel-heading">
                                            <a class="panel-title" data-parent="#panel-ext" data-toggle="collapse" href="#<?php print $node->nid . "eco"; ?>"> Facultad de Ciencias Básicas </a>
                                        </div>
                                        <div class="panel-collapse collapse" id="<?php print $node->nid . "eco"; ?>" style="height: auto;">
                                            <div class="panel-body">
                                                <!--                        CADA PROGRAMA/CURSO                     -->
                                                <div class="list-group">

                                                    <?php
                                                    for ($z = 0; $z < count($f_eco); $z++) :
                                                        $pro = $f_eco[$z];
                                                        $ID = $pro->nid;
                                                        $alias = drupal_get_path_alias('node/' . $ID);
                                                        ?>

                                                        <a class="list-group-item" href="<?php print($alias); ?>">
                                                            <?php
                                                            print($pro->title);
                                                            ?>                                    
                                                        </a>
    <?php endfor; ?>

                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                <?php endif; ?>

<?php if (count($f_ing) != 0): ?>
                                    <div class="panel panel-default">
                                        <div class="panel-heading">
                                            <a class="panel-title" data-parent="#panel-ext" data-toggle="collapse" href="#<?php print $node->nid . "ing"; ?>"> Facultad de Ciencias Básicas </a>
                                        </div>
                                        <div class="panel-collapse collapse" id="<?php print $node->nid . "ing"; ?>" style="height: auto;">
                                            <div class="panel-body">
                                                <!--                        CADA PROGRAMA/CURSO                     -->
                                                <div class="list-group">

                                                    <?php
                                                    for ($z = 0; $z < count($f_ing); $z++) :
                                                        $pro = $f_ing[$z];
                                                        $ID = $pro->nid;
                                                        $alias = drupal_get_path_alias('node/' . $ID);
                                                        ?>

                                                        <a class="list-group-item" href="<?php print($alias); ?>">
                                                            <?php
                                                            print($pro->title);
                                                            ?>                                    
                                                        </a>
    <?php endfor; ?>

                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                <?php endif; ?>

<?php if (count($f_edu) != 0): ?>
                                    <div class="panel panel-default">
                                        <div class="panel-heading">
                                            <a class="panel-title" data-parent="#panel-ext" data-toggle="collapse" href="#<?php print $node->nid . "edu"; ?>"> Facultad de Ciencias Básicas </a>
                                        </div>
                                        <div class="panel-collapse collapse" id="<?php print $node->nid . "edu"; ?>" style="height: auto;">
                                            <div class="panel-body">
                                                <!--                        CADA PROGRAMA/CURSO                     -->
                                                <div class="list-group">

                                                    <?php
                                                    for ($z = 0; $z < count($f_edu); $z++) :
                                                        $pro = $f_edu[$z];
                                                        $ID = $pro->nid;
                                                        $alias = drupal_get_path_alias('node/' . $ID);
                                                        ?>

                                                        <a class="list-group-item" href="<?php print($alias); ?>">
                                                            <?php
                                                            print($pro->title);
                                                            ?>                                    
                                                        </a>
    <?php endfor; ?>

                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                <?php endif; ?>

<?php if (count($f_tyt) != 0): ?>
                                    <div class="panel panel-default">
                                        <div class="panel-heading">
                                            <a class="panel-title" data-parent="#panel-ext" data-toggle="collapse" href="#<?php print $node->nid . "tyt"; ?>"> Facultad de Ciencias Básicas </a>
                                        </div>
                                        <div class="panel-collapse collapse" id="<?php print $node->nid . "tyt"; ?>" style="height: auto;">
                                            <div class="panel-body">
                                                <!--                        CADA PROGRAMA/CURSO                     -->
                                                <div class="list-group">

                                                    <?php
                                                    for ($z = 0; $z < count($f_tyt); $z++) :
                                                        $pro = $f_tyt[$z];
                                                        $ID = $pro->nid;
                                                        $alias = drupal_get_path_alias('node/' . $ID);
                                                        ?>

                                                        <a class="list-group-item" href="<?php print($alias); ?>">
                                                            <?php
                                                            print($pro->title);
                                                            ?>                                    
                                                        </a>
    <?php endfor; ?>

                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                <?php endif; ?>

<?php if (count($f_int) != 0): ?>
                                    <div class="panel panel-default">
                                        <div class="panel-heading">
                                            <a class="panel-title" data-parent="#panel-ext" data-toggle="collapse" href="#<?php print $node->nid . "int"; ?>"> Facultad de Ciencias Básicas </a>
                                        </div>
                                        <div class="panel-collapse collapse" id="<?php print $node->nid . "int"; ?>" style="height: auto;">
                                            <div class="panel-body">
                                                <!--                        CADA PROGRAMA/CURSO                     -->
                                                <div class="list-group">

                                                    <?php
                                                    for ($z = 0; $z < count($f_int); $z++) :
                                                        $pro = $f_int[$z];
                                                        $ID = $pro->nid;
                                                        $alias = drupal_get_path_alias('node/' . $ID);
                                                        ?>

                                                        <a class="list-group-item" href="<?php print($alias); ?>">
                                                            <?php
                                                            print($pro->title);
                                                            ?>                                    
                                                        </a>
    <?php endfor; ?>

                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                <?php endif; ?>

<?php if (count($f_ver) != 0): ?>
                                    <div class="panel panel-default">
                                        <div class="panel-heading">
                                            <a class="panel-title" data-parent="#panel-ext" data-toggle="collapse" href="#<?php print $node->nid . "ver"; ?>"> Facultad de Ciencias Básicas </a>
                                        </div>
                                        <div class="panel-collapse collapse" id="<?php print $node->nid . "ver"; ?>" style="height: auto;">
                                            <div class="panel-body">
                                                <!--                        CADA PROGRAMA/CURSO                     -->
                                                <div class="list-group">

                                                    <?php
                                                    for ($z = 0; $z < count($f_ver); $z++) :
                                                        $pro = $f_ver[$z];
                                                        $ID = $pro->nid;
                                                        $alias = drupal_get_path_alias('node/' . $ID);
                                                        ?>

                                                        <a class="list-group-item" href="<?php print($alias); ?>">
                                                            <?php
                                                            print($pro->title);
                                                            ?>                                    
                                                        </a>
    <?php endfor; ?>

                                                </div>
                                            </div>
                                        </div>
                                    </div>
<?php endif; ?>

                            </ul>

                        </div>

                        <div class="tab-pane" id="cursos_presenciales">

                            <h3>Cursos Presenciales</h3> 
                            <ul class="nav list-group" data-tabs="tabs">                       
                                <?php
                                for ($i = 0;; $i++) {
                                    $ID = $node->field_cursos_presenciales["und"][$i]["entity"]->nid;

                                    if ($ID == 0 or strcmp("0", $node->status) == 0) {
                                        break;
                                    }
                                    if ($node->field_cursos_presenciales["und"][$i]["entity"]->status > 0) {
                                        $alias = drupal_get_path_alias('node/' . $ID);
                                        ?>
                                        <li class="list-group-item" >
                                            <a href="<?php print($alias); ?>">
                                                <div class=""> <?php print($node->field_cursos_presenciales["und"][$i]["entity"]->title); ?> 
                                                </div>
                                            </a>
                                        </li>
                                        <?php
                                    }
                                }
                                ?>
                            </ul>

                        </div>

                        <div class="tab-pane" id="cursos_virtuales">

                            <h3>Cursos Virtuales</h3> 
                            <ul class="nav list-group" data-tabs="tabs">                       
                                <?php
                                for ($i = 0;; $i++) {
                                    $ID = $node->field_cursos_virtuales["und"][$i]["entity"]->nid;

                                    if ($ID == 0 or strcmp("0", $node->status) == 0) {
                                        break;
                                    }
                                    if ($node->field_cursos_virtuales["und"][$i]["entity"]->status > 0) {
                                        $alias = drupal_get_path_alias('node/' . $ID);
                                        ?>
                                        <li class="list-group-item" >
                                            <a href="<?php print($alias); ?>">
                                                <div class=""> <?php print($node->field_cursos_virtuales["und"][$i]["entity"]->title); ?> 
                                                </div>
                                            </a>
                                        </li>
                                        <?php
                                    }
                                }
                                ?>
                            </ul>

                        </div>

                        <div class="tab-pane" id="cursos_ninos">

                            <h3>Cursos para Niños</h3> 
                            <ul class="nav list-group" data-tabs="tabs">                       
                                <?php
                                for ($i = 0;; $i++) {
                                    $ID = $node->field_cursos_para_ninos["und"][$i]["entity"]->nid;

                                    if ($ID == 0 or strcmp("0", $node->status) == 0) {
                                        break;
                                    }
                                    if ($node->field_cursos_para_ninos["und"][$i]["entity"]->status > 0) {
                                        $alias = drupal_get_path_alias('node/' . $ID);
                                        ?>
                                        <li class="list-group-item" >
                                            <a href="<?php print($alias); ?>">
                                                <div class=""> <?php print($node->field_cursos_para_ninos["und"][$i]["entity"]->title); ?> 
                                                </div>
                                            </a>
                                        </li>
                                        <?php
                                    }
                                }
                                ?>
                            </ul>

                        </div>

                        <div class="tab-pane" id="seminarios">

                            <h3>Cursos</h3> 
                            <ul class="nav list-group" data-tabs="tabs">                       
                                <?php
                                for ($i = 0;; $i++) {
                                    $ID = $node->field_seminarios["und"][$i]["entity"]->nid;

                                    if ($ID == 0 or strcmp("0", $node->status) == 0) {
                                        break;
                                    }
                                    if ($node->field_seminarios["und"][$i]["entity"]->status > 0) {
                                        $alias = drupal_get_path_alias('node/' . $ID);
                                        ?>
                                        <li class="list-group-item" >
                                            <a href="<?php print($alias); ?>">
                                                <div class=""> <?php print($node->field_seminarios["und"][$i]["entity"]->title); ?> 
                                                </div>
                                            </a>
                                        </li>
                                        <?php
                                    }
                                }
                                ?>
                            </ul>

                        </div>

                        <div class="tab-pane" id="certificaciones">

<?php print($node->field_certificaciones["und"][0]["safe_value"]); ?>

                        </div>


                        <div class="tab-pane" id="contacto">

<?php print($node->field_curso_contacto["und"][0]["safe_value"]); ?>

                        </div>	  

                    </div>

                </div>

            </div>

        </div>

    </div>

</div>
