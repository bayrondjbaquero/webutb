<div class="panel panel-primary">

    <div class="panel-heading">

        <a class="panel-title" data-toggle="collapse" data-parent="#panel-ver-pregrado" href="#panel-element-ver-pregrado">
            Clic para ver Programas de Pregrado<span class="caret"></span>
        </a>
        <i class="fa fa-graduation-cap fa-5x pull-right"></i>
        <div class="padding-x">
        <p>Ingresando a cualquiera de nuestros programas de pregrado, comenzarás a hacer parte de una comunidad talentosa, que busca solventar las nuevas exigencias del sector productivo, ofreciendo las mejores herramientas para ingresar a un mundo laboral lleno de oportunidades con el sello de excelencia de la Universidad Tecnológica de Bolívar.</p>     
        </div>

    </div>

    <div id="panel-element-ver-pregrado" class="panel-collapse collapse bg-primary">

        <div class="panel-body">

            <div class="col-lg-12 thumbnail text-muted padding-x">

                <div class="panel-group" id="panel-pregrado">

                    <h2 class="text-primary">Oferta de Pregrado </h2>

                    <?php foreach ($rows as $id => $row): ?>


                        <?php print $row; ?>

                    <?php endforeach; ?>

                </div>				

            </div>	

        </div>

    </div>

</div>