<div id="node-<?php print $node->nid; ?>" class="<?php print $classes; ?> clearfix"<?php print $attributes; ?>>

    <div class="row clearfix">

        <div class="col-md-12 column">

            <table class="table table-striped">
                <tbody>
                    <tr>
                        <th>Area de Conocimiento</th>
                        <th>Tipo de Usuario</th>
                        <th>Idioma del Curso</th>
                    </tr>      
                    <tr>
                        <td><?php print $node->field_area_conocimiento_curso_co["und"][0]['taxonomy_term']->name; ?></td>
                        <td><?php print $node->field_tipo_de_usuario["und"][0]["value"]; ?></td>
                        <td><?php print $node->field_idioma_del_curso["und"][0]['taxonomy_term']->name; ?></td>
                    </tr>
                    <tr>
                        <th>Fecha de Inscripcion</th>
                        <th></th>
                        <th>Fecha de Inicio</th>
                    </tr>      
                    <tr>
                        <td><?php
                            print format_date(strtotime($content['field_fecha_inscripcion_curso_co']['#items'][0]['value']), 'custom', 'F j, Y l');
                            ?></td>
                        <td></td>
                        <td><?php
                            print format_date(strtotime($content['field_fecha_inicio_curso_corto']['#items'][0]['value']), 'custom', 'F j, Y l');
                            ?></td>
                    </tr>
                </tbody>
            </table>

            <?php echo theme('image_style', array('style_name' => 'thumbnail', 'path' => $node->ffield_imagen_curso_corto['und'][0]['uri'])); ?>
            <?php print $node->body["und"][0]["safe_value"]; ?>



        </div>
    </div>
</div>