<?php
/**
 * @file
 * Default simple view template to display a list of rows.
 *
 * @ingroup views_templates
 */
?>   

<--generado por el editor-->
<div id="carousel-example" class="carousel slide" data-ride="carousel">
    <!-- Indicators -->
    <ol class="carousel-indicators">
        <li data-target="#carousel-example" data-slide-to="0" ></li>
        <li data-target="#carousel-example" data-slide-to="1" class="active"></li>
        <li data-target="#carousel-example" data-slide-to="2" ></li>
    </ol>

    <div class="carousel-inner">
        <?php $i = 0; ?>
        <?php
        foreach ($rows as $id => $row) {
            if ($i == 1) {
                ?> <div class="item active"> <?php } else { ?>
                    <div class="item">
                    <?php } $i++; ?>

                    <div<?php
                    if ($classes_array[$id]) {
                        print ' class="' . $classes_array[$id] . '"';
                    }
                    ?>>
                            <?php print $row; ?>
                    </div>  
                </div>  
            <?php } ?>        
        </div>        
    </div>

    <!-- Controls -->
    <a class="left carousel-control" href="javascript:void(0)" 
       data-slide="prev" data-target="#carousel-example">
        <span class="glyphicon glyphicon-chevron-left"></span>
    </a>
    <a class="right carousel-control" href="javascript:void(0)" 
       data-slide="next" data-target="#carousel-example">
        <span class="glyphicon glyphicon-chevron-right"></span>
    </a>
</div>
<--generado por el editor 2-->