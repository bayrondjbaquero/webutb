<div id="node-<?php print $node->nid; ?>" class="<?php print $classes; ?> clearfix"<?php print $attributes; ?>>

    <div class="row clearfix"> 

        <style type="text/css">
            /*////// RESET STYLES //////*/
            body, #bodyTable, #bodyCell{height:100% !important; margin:0; padding:0; width:100% !important;}
            table{border-collapse:collapse;}
            img, a img{border:0; outline:none; text-decoration:none;}
            h1, h2, h3, h4, h5, h6{margin:0; padding:0;}
            p{margin: 1em 0;}

            /*////// CLIENT-SPECIFIC STYLES //////*/
            .ReadMsgBody{width:100%;} .ExternalClass{width:100%;} /* Force Hotmail/Outlook.com to display emails at full width. */
            .ExternalClass, .ExternalClass p, .ExternalClass span, .ExternalClass font, .ExternalClass td, .ExternalClass div{line-height:100%;} /* Force Hotmail/Outlook.com to display line heights normally. */
            table, td{mso-table-lspace:0pt; mso-table-rspace:0pt;} /* Remove spacing between tables in Outlook 2007 and up. */
            #outlook a{padding:0;} /* Force Outlook 2007 and up to provide a "view in browser" message. */
            img{-ms-interpolation-mode: bicubic;} /* Force IE to smoothly render resized images. */
            body, table, td, p, a, li, blockquote{-ms-text-size-adjust:100%; -webkit-text-size-adjust:100%;} /* Prevent Windows- and Webkit-based mobile platforms from changing declared text sizes. */

            /*////// FRAMEWORK STYLES //////*/
            .flexibleContainerCell{padding-top:20px; padding-Right:30px; padding-Left:30px;}
            .flexibleImage{height:auto;}
            .bottomShim{padding-bottom:20px;}
            .imageContent, .imageContentLast{padding-bottom:20px;}
            .nestedContainerCell{padding-top:20px; padding-Right:20px; padding-Left:20px;}

            /*////// GENERAL STYLES //////*/
            body, #bodyTable{background-color:#F5F5F5;}
            #bodyCell{padding-top:40px; padding-bottom:40px;}
            #emailBody{background-color:#FFFFFF; border:1px solid #DDDDDD; border-collapse:separate; border-radius:4px;}
            h1, h2, h3, h4, h5, h6{color:#202020; font-family:Helvetica; font-size:20px; line-height:125%; text-align:Left;}
            .textContent, .textContentLast{color:#404040; font-family:Helvetica; font-size:16px; line-height:125%; text-align:Left; padding-bottom:20px;}
            .textContent a, .textContentLast a{color:#2C9AB7; text-decoration:underline;}
            .nestedContainer{background-color:#E5E5E5; border:1px solid #CCCCCC;}

            .nestedGreen{background-color:#4CAF50; border:1px solid #CCCCCC;}
            .nestedBlue{background-color:#0D47A1; border:1px solid #CCCCCC;}
            .nestedPurple{background-color:#4A148C; border:1px solid #CCCCCC;}
            .nestedOrange{background-color:#E65100; border:1px solid #CCCCCC;}
            .nestedComu{background-color:#1A237E; border:1px solid #CCCCCC;}

            .nestedPur{background-color:#0C124A; border:1px solid #CCCCCC;}
            .emailButton{background-color:#2C9AB7; border-collapse:separate; border-radius:4px;}
            .buttonContent{color:#FFFFFF; font-family:Helvetica; font-size:18px; font-weight:bold; line-height:100%; padding:15px; text-align:center;}
            .buttonContent a{color:#FFFFFF; display:block; text-decoration:none;}
            .emailCalendar{background-color:#FFFFFF; border:1px solid #CCCCCC;}
            .emailCalendarMonth{background-color:#2C9AB7; color:#FFFFFF; font-family:Helvetica, Arial, sans-serif; font-size:16px; font-weight:bold; padding-top:10px; padding-bottom:10px; text-align:center;}
            .emailCalendarDay{color:#2C9AB7; font-family:Helvetica, Arial, sans-serif; font-size:60px; font-weight:bold; line-height:100%; padding-top:20px; padding-bottom:20px; text-align:center;}
            .textWhite{color:#FFFFFF;}
            .textGreen{color:#4CAF50;}
            .textBlue{color:#0D47A1;}
            .textPurple{color:#4A148C;}
            .textOrange{color:#E65100;}
            .textPur{color:#1A237E;}




            /*////// MOBILE STYLES //////*/
            @media only screen and (max-width: 480px){			
                /*////// CLIENT-SPECIFIC STYLES //////*/
                body{width:100% !important; min-width:100% !important;} /* Force iOS Mail to render the email at full width. */

                /*////// FRAMEWORK STYLES //////*/
                /*
                        CSS selectors are written in attribute
                        selector format to prevent Yahoo Mail
                        from rendering media query styles on
                        desktop.
                */
                table[id="emailBody"], table[class="flexibleContainer"]{width:100% !important;}

                /*
                        The following style rule makes any
                        image classed with 'flexibleImage'
                        fluid when the query activates.
                        Make sure you add an inline max-width
                        to those images to prevent them
                        from blowing out. 
                */
                img[class="flexibleImage"]{height:auto !important; width:100% !important;}

                /*
                        Make buttons in the email span the
                        full width of their container, allowing
                        for left- or right-handed ease of use.
                */
                table[class="emailButton"]{width:100% !important;}
                td[class="buttonContent"]{padding:0 !important;}
                td[class="buttonContent"] a{padding:15px !important;}

                td[class="textContentLast"], td[class="imageContentLast"]{padding-top:20px !important;}

                /*////// GENERAL STYLES //////*/
                td[id="bodyCell"]{padding-top:10px !important; padding-Right:10px !important; padding-Left:10px !important;}
            }
        </style>


        <?php
        if ($node->field_tipo_mail["und"][0]['value']) {

            if (strcmp($node->field_tipo_mail["und"][0]['value'], "POR LA ACADEMIA") == 0) {
                $color = "Green";
            }
            if (strcmp($node->field_tipo_mail["und"][0]['value'], "SOLO PARA ESTUDIANTES") == 0) {
                $color = "Blue";
            }
            if (strcmp($node->field_tipo_mail["und"][0]['value'], "ALTO GOBIERNO") == 0) {
                $color = "Purple";
            }
            if (strcmp($node->field_tipo_mail["und"][0]['value'], "EMPLEADOS") == 0) {
                $color = "Orange";
            }
            if (strcmp($node->field_tipo_mail["und"][0]['value'], "EGRESADOS") == 0) {
                $color = "Pur";
            }
        }
        ?>



        <center>
            <table border="0" cellpadding="0" cellspacing="0" height="100%" width="100%" id="bodyTable">
                <tr>
                    <td align="center" valign="top" id="bodyCell">
                        <table border="0" cellpadding="0" cellspacing="0" width="700" id="emailBody">


                            <!-- MODULE ROW // -->
                            <tr>
                                <td align="center" valign="top" class="<?php print "nested" . $color; ?>">
                                    <!-- CENTERING TABLE // -->
                                    <table border="0" cellpadding="0" cellspacing="0" width="100%">
                                        <tr>
                                            <td align="center" valign="top">
                                                <!-- FLEXIBLE CONTAINER // -->
                                                
                                                            <?php
                                                            if ($node->field_tipo_mail["und"][0]['value']) {

                                                                if (strcmp($node->field_tipo_mail["und"][0]['value'], "POR LA ACADEMIA") == 0) {
                                                                    ?>

                                                                    <img src="http://www.unitecnologica.edu.co/newsletter/Conectate/imagen-mailing/porlaacademia.png" />

                                                                    <?php
                                                                }
                                                                if (strcmp($node->field_tipo_mail["und"][0]['value'], "SOLO PARA ESTUDIANTES") == 0) {
                                                                    ?>

                                                                    <img src="http://www.unitecnologica.edu.co/newsletter/Conectate/imagen-mailing/soloparaestudiantes.png" />

                                                                    <?php
                                                                }
                                                                if (strcmp($node->field_tipo_mail["und"][0]['value'], "ALTO GOBIERNO") == 0) {
                                                                    ?>

                                                                    <img src="http://www.unitecnologica.edu.co/newsletter/Conectate/imagen-mailing/altogobierno.png" />

                                                                    <?php
                                                                }
                                                                if (strcmp($node->field_tipo_mail["und"][0]['value'], "EMPLEADOS") == 0) {
                                                                    ?>

                                                                    <img src="http://www.unitecnologica.edu.co/newsletter/Conectate/imagen-mailing/empleados.png" />

                                                                    <?php
                                                                }
                                                                if (strcmp($node->field_tipo_mail["und"][0]['value'], "EGRESADOS") == 0) {
                                                                    ?>

                                                                    <img src="http://www.unitecnologica.edu.co/newsletter/Conectate/imagen-mailing/egresados.png" />

                                                                    <?php
                                                                }
                                                                if (strcmp($node->field_tipo_mail["und"][0]['value'], "PERIODISTAS") == 0) {
                                                                    ?>

                                                                    <img src="http://www.unitecnologica.edu.co/newsletter/Conectate/imagen-mailing/periodistas.png" />

                                                                    <?php
                                                                }
                                                            }
                                                            ?>

                                                     
                                                <!-- // FLEXIBLE CONTAINER -->
                                            </td>
                                        </tr>
                                    </table>
                                    <!-- // CENTERING TABLE -->
                                </td>
                            </tr>
                            <!-- // MODULE ROW -->

                            <!-- MODULE ROW // -->
                            <tr>
                                <td align="center" valign="top">
                                    <!-- CENTERING TABLE // -->
                                    <table border="0" cellpadding="0" cellspacing="0" width="100%">
                                        <tr>
                                            <td align="center" valign="top">
                                                <!-- FLEXIBLE CONTAINER // -->
                                                <table border="0" cellpadding="0" cellspacing="0" width="700" class="flexibleContainer">
                                                    <tr>
                                                        <td valign="top" width="700" class="flexibleContainerCell">


                                                            <!-- CONTENT TABLE // -->

                                                            <!-- // CONTENT TABLE -->

                                                            <!-- CONTENT TABLE // -->
                                                            <table align="Left" border="0" cellpadding="0" cellspacing="0" width="700" class="flexibleContainer">
                                                                <tr>
                                                                    <td valign="top" class="textContent">
                                                                        <h3><font class="<?php print "text" . $color; ?>"><strong><?php print $node->title; ?></strong></font></h3>
                                                                    </td>
                                                                </tr>
                                                            </table>
                                                            <!-- // CONTENT TABLE -->

                                                            <!-- CONTENT TABLE // -->

                                                            <!-- // CONTENT TABLE -->

                                                        </td>
                                                    </tr>
                                                </table>
                                                <!-- // FLEXIBLE CONTAINER -->
                                            </td>
                                        </tr>
                                    </table>
                                    <!-- // CENTERING TABLE -->
                                </td>
                            </tr>
                            <!-- // MODULE ROW -->

                            <!-- MODULE ROW // -->
                            <tr>
                                <td align="center" valign="top">
                                    <!-- CENTERING TABLE // -->
                                    <table border="0" cellpadding="0" cellspacing="0" width="100%">
                                        <tr>
                                            <td align="center" valign="top">
                                                <!-- FLEXIBLE CONTAINER // -->
                                                <table border="0" cellpadding="0" cellspacing="0" width="700" class="flexibleContainer">
                                                    <tr>
                                                        <td valign="top" width="700" class="flexibleContainerCell">


                                                            <!-- CONTENT TABLE // -->
                                                            <!--
                                                                In multi-column content blocks, the
                                                                content tables are given set widths
                                                                and the flexibleContainer class.
                                                            -->
                                                            <?php if ($node->body["und"][0]["safe_value"]) { ?>
                                                                <table align="Left" border="0" cellpadding="0" cellspacing="0" width="360" class="flexibleContainer">
                                                                    <tr>
                                                                        <td valign="top" class="textContent">
                                                                            <?php print $node->body["und"][0]["safe_value"]; ?>
                                                                        </td>
                                                                    </tr>
                                                                </table>
                                                            <?php } ?>
                                                            <!-- // CONTENT TABLE -->


                                                            <!-- CONTENT TABLE // -->
                                                            <?php if ($node->field_imagen_mail['und'][0]['uri']) { ?>
                                                                <table align="Right" border="0" cellpadding="0" cellspacing="0" width="160" class="flexibleContainer">
                                                                    <tr>
                                                                        <td valign="top" class="textContentLast">                                                                        
                                                                            <?php
                                                                            if ($node->field_imagen_mail['und'][0]['uri']) {
                                                                                $uriImagefromNode = $node->field_imagen_mail['und'][0]['uri'];
                                                                                $urlImagefromNode = file_create_url($uriImagefromNode);
                                                                            }
                                                                            ?>   
                                                                            <img src="<?php print $urlImagefromNode; ?>" style="width: 220px;" />
                                                                        </td>
                                                                    </tr>                                                                
                                                                </table>
                                                            <?php } ?>
                                                            <!-- // CONTENT TABLE -->


                                                        </td>
                                                    </tr>
                                                </table>
                                                <!-- // FLEXIBLE CONTAINER -->
                                            </td>
                                        </tr>
                                    </table>
                                    <!-- // CENTERING TABLE -->
                                </td>
                            </tr>
                            <!-- // MODULE ROW -->

                            <!-- MODULE ROW // -->
                            <?php if ($node->field_complemento["und"][0]["safe_value"]) { ?>
                                <tr>
                                    <td align="center" valign="top">
                                        <table border="0" cellpadding="0" cellspacing="0" width="100%">
                                            <tr>
                                                <td align="center" valign="top">
                                                    <table border="0" cellpadding="0" cellspacing="0" width="700" class="flexibleContainer">
                                                        <tr>
                                                            <td align="center" valign="top" width="700" class="flexibleContainerCell" >

                                                                <table border="0" cellpadding="0" cellspacing="0" width="100%">
                                                                    <tr>
                                                                        <td valign="top" class="textContent">

                                                                            <?php print $node->field_complemento["und"][0]["safe_value"]; ?>                                                                    

                                                                        </td>
                                                                    </tr>
                                                                </table>
                                                            </td>
                                                        </tr>
                                                    </table>
                                                </td>
                                            </tr>
                                        </table>
                                    </td>
                                </tr>
                            <?php } ?>
                            <!-- // MODULE ROW --> 




                            <?php
                            if (isset($node->field_archivo_adjunto)):
                                if (strcmp($node->field_archivo_adjunto["und"][0]["value"], '1') == 0):
                                    ?>
                                    <!-- MODULE ROW // -->
                                    <tr>
                                        <td align="center" valign="top">
                                            <!-- CENTERING TABLE // -->
                                            <table border="0" cellpadding="0" cellspacing="0" width="100%">
                                                <tr>
                                                    <td align="center" valign="top">
                                                        <!-- FLEXIBLE CONTAINER // -->
                                                        <table border="0" cellpadding="0" cellspacing="0" width="700" class="flexibleContainer">
                                                            <tr>
                                                                <td align="center" valign="top" width="700" class="flexibleContainerCell bottomShim">


                                                                    <!-- CONTENT TABLE // -->
                                                                    <!--
                                                                        The emailButton table's width can be changed
                                                                        to affect the look of the button. To make the
                                                                        button width dependent on the text inside, leave
                                                                        the width blank. When a button is placed in a column,
                                                                        it's helpful to set the width to 100%.
                                                                    -->
                                                                    <table border="0" cellpadding="0" cellspacing="0" width="260" class="emailButton">
                                                                        <tr>
                                                                            <td align="center" valign="middle" class="buttonContent">
                                                                                <a href="#" target="_blank">* Éste mailing tiene archivo adjunto, <strong>Descárgalo</strong></a>
                                                                            </td>
                                                                        </tr>
                                                                    </table>
                                                                    <!-- // CONTENT TABLE -->


                                                                </td>
                                                            </tr>
                                                        </table>
                                                        <!-- // FLEXIBLE CONTAINER -->
                                                    </td>
                                                </tr>
                                            </table>
                                            <!-- // CENTERING TABLE -->
                                        </td>
                                    </tr>
                                    <!-- // MODULE ROW -->
                                    <?php
                                endif;
                            endif;
                            ?>

                            <!-- MODULE ROW // -->
                            <tr>
                                <td align="center" valign="top"  class="nestedComu">
                                    <!-- CENTERING TABLE // -->
                                    <table border="0" cellpadding="0" cellspacing="0" width="100%">
                                        <tr>
                                            <td align="center" valign="top">
                                                <!-- FLEXIBLE CONTAINER // -->
                                                <table border="0" cellpadding="0" cellspacing="0" width="700" class="flexibleContainer">
                                                    <tr>
                                                        <td align="center" valign="top" width="700" class="flexibleContainerCell">


                                                            <!-- CONTENT TABLE // -->
                                                            <!--
                                                                The table cell imageContent has padding
                                                                applied to the bottom as part of the framework,
                                                                ensuring images space correctly in Android Gmail.
                                                            -->
                                                            <table border="0" cellpadding="0" cellspacing="0" width="100%">
                                                                <tr>
                                                                    <td valign="top" class="imageContent">
                                                                        <h2><font class="textWhite"><strong><?php print $node->field_departamento["und"][0]["value"] ?><strong></font></h2>
                                                                    </td>
                                                                </tr>
                                                            </table>
                                                            <!-- // CONTENT TABLE -->


                                                        </td>
                                                    </tr>
                                                </table>
                                                <!-- // FLEXIBLE CONTAINER -->
                                            </td>
                                        </tr>
                                    </table>
                                    <!-- // CENTERING TABLE -->
                                </td>
                            </tr>
                            <!-- // MODULE ROW -->   
























                        </table>
                        <!-- // EMAIL CONTAINER -->
                    </td>
                </tr>
            </table>
        </center>
    </div>
</div>