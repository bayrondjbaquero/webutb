<div id="node-<?php print $node->nid; ?>" class="<?php print $classes; ?> clearfix"<?php print $attributes; ?>>

    <div class="row clearfix">

        <div class="col-md-12 column">

            <table class="table table-striped table-bordered">
                <tbody>
                    <tr>
                        <th>Organizador</th>
                        <th>Link</th>
                        <th>Fecha de Inicio</th>
                    </tr>      
                    <tr>
                        <td><?php print $node->field_organizador["und"][0]["value"]; ?></td>
                        <td><a href="<?php print $node->field_link["und"][0]['url']; ?>" title="<?php print $node->field_organizador["und"][0]["value"]; ?>" target="_blank" rel="nofollow"><?php print $node->field_link["und"][0]['url']; ?></a></td>
                        <td><?php
                            print format_date(strtotime($content['field_fecha']['#items'][0]['value']), 'custom', 'F j, Y l');
                            ?></td>
                    </tr>
                </tbody>
            </table>
            <?php $my_image_url = file_create_url($field_logo[0]['uri']);  ?>            
            <div><?php if($node->field_logo["und"][0]['uri']){ ?><img src="<?php print $my_image_url; ?>"  class="margen-m responsive pull-right"/><?php } ?>
            <?php print $node->body["und"][0]["safe_value"]; ?></div>

            <?php if ($content['field_archivos_pdf']) { ?>
                <div class="panel panel-primary">
                    <div class="panel-heading">                    
                        <h3 class="panel-title"> <span class="glyphicon glyphicon-folder-open" style="font-size:2.5em;"></span>&nbsp;&nbsp;&nbsp; Descargas adicionales </h3> 
                    </div>
                    <div class="panel-body">
                        <?php print render($content['field_archivos_pdf']); ?>                              
                    </div>											
                </div>
            <?php } ?>
     