
<?php
$entity = node_load($fields['nid']->raw);
$uriImagefromNode = $entity->field_imagen['und'][0]['uri'];
$urlImagefromNode = file_create_url($uriImagefromNode);

 $path=drupal_get_path_alias('node/'.$fields['nid']->content);
?>
<style>
			.r-linea{
				max-width: 150px;
			}
			a[data-toggle="tab"]{
				text-align: center;
				font-size: 20px;
			}
			.bg-warning{
				background-color: #ba9f14;
				color: white;
			}
			.bg-primary,
			.bg-info,
			.bg-warning,
			.bg-danger{
				background-color: #002e82 !important;
				color: white;
			}
			.f17{
				font-size: 25px;
			}
			.text-verde,
			a[data-toggle="collapse"]{
				color: #43a047 !important;
				font-weight: bolder;
			}
			.cdf figure img{
				filter: grayscale(100%);
			}
			.title-galeria{
				background-color: #2d2d2d;
				padding: 10px 20px;
				color: white;
				text-align: center;
			}
			.slick-next::before, .slick-prev::before{
				color: #108f50;
			}
.grid figure {float: none;min-width: auto;

max-width: 100%;

width: 100%; margin: 0px;}
.grid { width: auto; max-width: 100%;}
		</style>
<div class="col-md-3">
				<a href="<?php print $path; ?>" class="text-dark">
					<figure>
						<img src="<?php print $urlImagefromNode; ?>" alt="" class="img-responsive" target="_blank">
					</figure>
					<div class="title-galeria">
						<?php print $entity->title; ?>
					</div>
				</a>
			</div>
