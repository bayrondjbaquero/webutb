<div class="panel panel-brand3">

    <div class="panel-heading">

        <a class="panel-title" data-toggle="collapse" data-parent="#panel-ver-virtual" href="#Virtual">
            Clic para ver Oferta de Educación a Distancia<span class="caret"></span>
        </a>
        <i class="fa fa-laptop fa-5x pull-right"></i>
        <div class="padding-x">
            La Universidad Tecnológica de Bolívar a través de su oferta de educación a distancia ofrece oportunidades de acceso a la educación superior a través de aulas virtuales que utilizan de manera eficiente las nuevas tecnologías de la información. Esta oferta educativa va dirigida a personas con limitantes de tiempo, ocupación o lugar para que puedan continuar de manera oportuna y profesional sus estudios académicos.
        </div>

    </div>

    <div id="Virtual" class="panel-collapse collapse bg-brand3">

        <div class="panel-body">	

            <div class="col-lg-12 thumbnail text-muted padding-x">

                <div class="panel-group" id="panel-cur">

                    <?php foreach ($rows as $id => $row): ?>

                        <?php print $row; ?>

                    <?php endforeach; ?>

                </div>				

            </div>	

        </div>

    </div>

</div>	