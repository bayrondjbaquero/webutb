<?php
/**
 * @file
 * Default simple view template to display a list of rows.
 *
 * @ingroup views_templates
 */
?>
<div class="panel panel-default">
    <div class="panel-heading">
        <h4 class="panel-title">
            <a data-toggle="collapse" data-parent="#accordion" href="#<?php print str_replace(" ", "_", $title); ?>">
                <?php if (!empty($title)): ?>
                    <?php print $title; ?>
                <?php endif; ?>
            </a>
        </h4>
    </div>
    <div id="<?php print str_replace(" ", "_", $title); ?>" class="panel-collapse collapse">
        <div class="panel-body">       

            <table class="table table-striped">
                <thead>
                    <tr>
                        <th>Titulo</th>
                        <th>Tipo</th>
                    </tr>
                </thead>
                <tbody>
                    <?php foreach ($rows as $id => $row): ?>  
                        <?php print $row; ?>
                    <?php endforeach; ?>
                </tbody>
            </table>

        </div>
    </div>
</div>