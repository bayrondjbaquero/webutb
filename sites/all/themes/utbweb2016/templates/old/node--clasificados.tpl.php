<?php

/**
 * @file
 * Default theme implementation to display a node.
 *
 * Available variables:
 * - $title: the (sanitized) title of the node.
 * - $content: An array of node items. Use render($content) to print them all,
 *   or print a subset such as render($content['field_example']). Use
 *   hide($content['field_example']) to temporarily suppress the printing of a
 *   given element.
 * - $user_picture: The node author's picture from user-picture.tpl.php.
 * - $date: Formatted creation date. Preprocess functions can reformat it by
 *   calling format_date() with the desired parameters on the $created variable.
 * - $name: Themed username of node author output from theme_username().
 * - $node_url: Direct URL of the current node.
 * - $display_submitted: Whether submission information should be displayed.
 * - $submitted: Submission information created from $name and $date during
 *   template_preprocess_node().
 * - $classes: String of classes that can be used to style contextually through
 *   CSS. It can be manipulated through the variable $classes_array from
 *   preprocess functions. The default values can be one or more of the
 *   following:
 *   - node: The current template type; for example, "theming hook".
 *   - node-[type]: The current node type. For example, if the node is a
 *     "Blog entry" it would result in "node-blog". Note that the machine
 *     name will often be in a short form of the human readable label.
 *   - node-teaser: Nodes in teaser form.
 *   - node-preview: Nodes in preview mode.
 *   The following are controlled through the node publishing options.
 *   - node-promoted: Nodes promoted to the front page.
 *   - node-sticky: Nodes ordered above other non-sticky nodes in teaser
 *     listings.
 *   - node-unpublished: Unpublished nodes visible only to administrators.
 * - $title_prefix (array): An array containing additional output populated by
 *   modules, intended to be displayed in front of the main title tag that
 *   appears in the template.
 * - $title_suffix (array): An array containing additional output populated by
 *   modules, intended to be displayed after the main title tag that appears in
 *   the template.
 *
 * Other variables:
 * - $node: Full node object. Contains data that may not be safe.
 * - $type: Node type; for example, story, page, blog, etc.
 * - $comment_count: Number of comments attached to the node.
 * - $uid: User ID of the node author.
 * - $created: Time the node was published formatted in Unix timestamp.
 * - $classes_array: Array of html class attribute values. It is flattened
 *   into a string within the variable $classes.
 * - $zebra: Outputs either "even" or "odd". Useful for zebra striping in
 *   teaser listings.
 * - $id: Position of the node. Increments each time it's output.
 *
 * Node status variables:
 * - $view_mode: View mode; for example, "full", "teaser".
 * - $teaser: Flag for the teaser state (shortcut for $view_mode == 'teaser').
 * - $page: Flag for the full page state.
 * - $promote: Flag for front page promotion state.
 * - $sticky: Flags for sticky post setting.
 * - $status: Flag for published status.
 * - $comment: State of comment settings for the node.
 * - $readmore: Flags true if the teaser content of the node cannot hold the
 *   main body content.
 * - $is_front: Flags true when presented in the front page.
 * - $logged_in: Flags true when the current user is a logged-in member.
 * - $is_admin: Flags true when the current user is an administrator.
 *
 * Field variables: for each field instance attached to the node a corresponding
 * variable is defined; for example, $node->body becomes $body. When needing to
 * access a field's raw values, developers/themers are strongly encouraged to
 * use these variables. Otherwise they will have to explicitly specify the
 * desired field language; for example, $node->body['en'], thus overriding any
 * language negotiation rule that was previously applied.
 *
 * @see template_preprocess()
 * @see template_preprocess_node()
 * @see template_process()
 *
 * @ingroup themeable
 */
?>
<div id="node-<?php print $node->nid; ?>" class="<?php print $classes; ?> clearfix"<?php print $attributes; ?>>

  <?php print $user_picture; ?>

  <?php print render($title_prefix); ?>
  <?php if (!$page): ?>
    <h2<?php print $title_attributes; ?>><a href="<?php print $node_url; ?>"><?php print $title; ?></a></h2>
  <?php endif; ?>
  <?php print render($title_suffix); ?>

  <?php if ($display_submitted): ?>
    <div class="submitted">
      <?php print $submitted; ?>
    </div>
  <?php endif; ?>

  <div class="content"<?php print $content_attributes; ?>>
    <?php
      // We hide the comments and links now so that we can render them later.
      hide($content['comments']);
      hide($content['links']);
      //print render($content);
	//$nodo = node_load($node->nid);
    ?>
	<div class="col-xs-12" style="padding-left: 0;">
		<div class="col-xs-5" style="padding-left: 0;">
			<div class="img_clasificado_wrapper">
				<a href="#" type="button" class="" data-toggle="modal" data-target="#modal<?php print $node->nid; ?>">
					<span class="overlay"><i class="fa fa-search"></i></span>
				<?php 
					$imagen = array(
									'style_name' => 'ancha_300x215', //330x215 the image style created, ancha_250x145
									'path' => $node->field_imagen['und'][0]['uri'],
									'attributes' => array(
															'class' => 'img-responsive'
														),
									);
					print theme('image_style', $imagen);
					$uriImagefromNode = $node->field_imagen['und'][0]['uri'];
					$urlImagefromNode = file_create_url($uriImagefromNode);
					//print render($content['field_imagen']);
				?>
				</a>
				<div class="modal fade" id="modal<?php print $node->nid; ?>" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">>
					<div class="modal-dialog modal-lg">
						<div class="modal-content">
							<div class="modal-header">
								<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">X</span></button>
								<h4 class="modal-title"><?php print $title; ?></h4>
							</div><!-- fin modal-header -->
							<div class="modal-body">
								<img alt="<?php print $node->field_imagen['und'][0]['alt']; ?>" src="<?php print $urlImagefromNode; ?>" class="img-responsive" />
							</div>
							<div class="modal-footer">
								<button type="button" class="btn btn-danger" data-dismiss="modal">Cerrar</button>
							</div>
						</div><!-- /.modal-content -->
					</div><!-- /.modal-dialog -->
				</div><!-- /.modal -->
			</div><!-- fin img_clasificado_wrapper -->
		</div><!-- fin col-xs-4 -->
		<div class="col-xs-7" style="font-size: 14px; font-family: Helvetica,Arial,sans-serif; margin-top: 10px;">
			<?php 
				//$info = field_info_instance('node','field_precio_salario','clasificados');
	 			//print $info['label'];
	 			$precio = field_get_items('node', $node, 'field_precio_salario');
				$precio = field_view_value('node', $node, 'field_precio_salario', $precio[0]);
				$etiqueta_precio = field_view_field('node', $node, 'field_precio_salario');
				
			?>
			<div class="precio_clasificado">
				<span class="etiqueta">
					<?php print render($etiqueta_precio['#title']); ?>:
				</span>&nbsp;<?php print render($precio); ?>
			</div>
			<div class="clearfix"></div>
			<hr style="border-top: 1px solid #ccc;">
			<div class="contacto_clasificado">
				<?php
					$contacto = field_get_items('node', $node, 'field_contacto');
					$contacto = field_view_value('node', $node, 'field_contacto', $contacto[0]);
					$etiqueta_contacto = field_view_field('node', $node, 'field_contacto');
				?>
				<i class="fa fa-user"></i>&nbsp;<span class="etiqueta"><?php print render($etiqueta_contacto['#title']); ?>: &nbsp;</span>
				<br>
				<?php print render($contacto); ?>
			</div>
			<hr style="border-top: 1px solid #ccc;">
			<?php print render($content['sharethis']); ?>
			<?php //print render($content['links']['statistics']['#links']['statistics_counter']['title']); ?>
		</div><!-- fin col-xs-8 -->
	</div><!-- fin col-xs-12 -->
	<div class="clearfix"></div>
	<div class="descripcion_clasificado" style="font-size: 14px; font-family: Helvetica,Arial,sans-serif;">
		<div class="padding-x linea-caja-top caja-sombra efecto-sombra-2">
			<h3 class="lined"><span>Descripción</span></h3>
			<?php print render($content['body']); ?>
		</div>
	</div>

  
	<?php print render($content['links']); ?>
	<?php print render($content['comments']); ?>

</div>
<hr>

<style>
h3.lined {
  position: relative;
  text-align: center;
}

h3.lined span {
  background: #fff;
  padding: 0 15px;
  position: relative;
  z-index: 1;
}

h3.lined:before {
  background: #ddd;
  content: "";
  display: block;
  height: 1px;
  position: absolute;
    top: 50%;
  width: 100%;
}

h3.lined:before {
  left: 0;
}
</style>


<!--
<div id="node-<?php print $node->nid; ?>" class="<?php print $classes; ?> clearfix"<?php print $attributes; ?>>

    <div class="row clearfix">

        <div class="col-md-12 thumbnail column">

            <table class="table table-bordered">
                <tbody>
                    <tr>
                        <th>Tipo:</th>
                        <th>Precio/Salario: </th>
                        <th>Contacto: </th>
                    </tr>      
                    <tr>
                        <td><?php //print ($node->field_tipo["und"][0]["value"]); ?></td>
                        <td><?php print ($node->field_precio_salario["und"][0]["safe_value"]); ?></td>
                        <td><?php print ($node->field_contacto["und"][0]["safe_value"]); ?></td>
                    </tr>                    
                </tbody>
            </table>


            <div class="col-md-3 column">
                <?php
                $uriImagefromNode = $node->field_imagen['und'][0]['uri'];
                $urlImagefromNode = file_create_url($uriImagefromNode);
                ?>
                <img alt="<?php print $node->field_imagen['und'][0]['alt']; ?>" src="<?php print $urlImagefromNode; ?>" class="img-responsive"/>            
            </div>
            <div class="col-md-9 column">
                <?php print ($node->body["und"][0]["safe_value"]); ?>
            </div>

        </div>
    </div>
</div>
-->



