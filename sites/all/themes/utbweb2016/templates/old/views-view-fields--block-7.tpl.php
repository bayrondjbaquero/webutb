<?php
/**
 * @file
 * Default simple view template to all the fields as a row.
 *
 * - $view: The view in use.
 * - $fields: an array of $field objects. Each one contains:
 *   - $field->content: The output of the field.
 *   - $field->raw: The raw data for the field, if it exists. This is NOT output safe.
 *   - $field->class: The safe class id to use.
 *   - $field->handler: The Views field handler object controlling this field. Do not use
 *     var_export to dump this object, as it can't handle the recursion.
 *   - $field->inline: Whether or not the field should be inline.
 *   - $field->inline_html: either div or span based on the above flag.
 *   - $field->wrapper_prefix: A complete wrapper containing the inline_html to use.
 *   - $field->wrapper_suffix: The closing tag for the wrapper.
 *   - $field->separator: an optional separator that may appear before a field.
 *   - $field->label: The wrap label text to use.
 *   - $field->label_html: The full HTML of the label to use including
 *     configured element type.
 * - $row: The raw result object from the query, with all data it fetched.
 *
 * @ingroup views_templates
 */
$ent = node_load($fields['nid']->raw);
if ($ent->field_foto_persona['und'][0]['uri']) {
    $uriImagefromNode = $ent->field_foto_persona['und'][0]['uri'];
    $urlImagefromNode = file_create_url($uriImagefromNode);
}
?>

<div class="col-md-12">

    <div class="col-md-6 col-md-offset-3">
        <?php if ($ent->field_foto_persona['und'][0]['uri']) { ?>
            <img class="margen-m img-responsive img-circle" src="<?php print $urlImagefromNode; ?>">
        <?php } ?>
    </div>

    <div class="col-md-12 col-sm-12">

        <blockquote class="blockquote-reverse">
            <i class="fa fa-quote-left fa-3x pull-left"></i>
            <div style="font-size: 13.5px; line-height: 1.25;"><?php print $ent->body['und'][0]['safe_summary']; ?></div>
            <i class="fa fa-quote-right fa-3x pull-right"></i>

            <footer>
                <cite title="<?php print print $ent->title; ?>"><?php print $ent->field_nombre_persona['und'][0]['safe_value']; ?></cite>
            </footer>

        </blockquote>

    </div>			

</div>