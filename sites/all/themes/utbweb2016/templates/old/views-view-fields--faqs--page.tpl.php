    <div class="panel panel-default">
        <div class="panel-heading">
            <h4 class="panel-title">
                <a data-toggle="collapse" data-parent="#faq" href="#faq-<?php print  $fields['nid']->content ?>"><?php print  $fields['title']->content ?></a>
            </h4>
        </div>
        <div id="faq-<?php print  $fields['nid']->content ?>" class="panel-collapse collapse">
            <div class="panel-body">
                <p><?php print  $fields['body']->content ?></p>
            </div>
        </div>
    </div>