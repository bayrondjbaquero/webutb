<?php
/**
 * @file
 * Default simple view template to display a list of rows.
 *
 * @ingroup views_templates
 */
?>


<div class = "blog">
    <?php if (!empty($title)): ?>
        <?php print $title; ?>
    <?php endif; ?>
    <hr class="savio-hr">
    <?php foreach ($rows as $id => $row): ?>
        <?php print $row; ?>
    <?php endforeach; ?>
</div>
