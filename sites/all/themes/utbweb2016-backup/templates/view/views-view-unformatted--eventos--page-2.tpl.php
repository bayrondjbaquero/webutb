<style>
    .rgba-greenUTB-strong {
        background-color: rgb(49, 128, 85);
    }

    .rgba-blueUTB-strong {
        background-color: rgb(43, 57, 136);
    }

    .card-min .card-label .label {
        font-size: 16px;
    }
</style>
<!--Post row-->
<div class="row">
    <div class="col-md-12 blog-column">
        <div class="view overlay hm-blue-slight z-depth-2">
            <a><img src="http://www.utb.edu.co/sites/web.unitecnologica.edu.co/files/descargas/agendate_banner.png" class="img-responsive">
                <div class="mask waves-effect waves-light "></div>
            </a>
        </div>

        <div class="card-panel bl-panel hoverable">

            <hr>

            <?php
            $i=0;
            foreach ($rows as $id => $row) {
                if($i==0){
                    print "<div class='row'>";
                }
                $i++;

                print $row;

                if($i==3){
                   print "</div>";
                    $i=0;
                }

            }

            ?> 

        </div>
    </div>
</div>
<!--/.Post row-->
