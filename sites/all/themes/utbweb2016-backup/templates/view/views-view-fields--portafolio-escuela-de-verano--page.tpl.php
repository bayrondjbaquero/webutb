<?php
$entity = node_load($fields['nid']->raw);
$uriImagefromNode = $entity->field_filtro_imagen['und'][0]['uri'];
$urlImagefromNode = file_create_url($uriImagefromNode);
?>
<?php
$num = strlen($entity->field_dirigido_a['und'][0]['value']);

$alias = drupal_get_path_alias('node/' . $entity->nid);
?>

<div class="mix col-md-4 item<?php print $num; ?>" style="min-height: 251px; max-height: 251px; margin-bottom: 10px;">
    <a href="<?php print $alias ?>" title="<?php print $entity->title; ?>">
        <div class="panel panel-primary z-depth-1">
            <div class="panel-heading">  <p><?php print $entity->title; ?></p></div>
            <div class="panel-body">
              <img alt="<?php print $temp->field_filtro_imagen['und'][0]['alt']; ?>" src="<?php print $urlImagefromNode; ?>" class="img-responsive"/>
              <hr>
              <p style="height: 10px;" >

                  <?php
                  if (isset($entity->field_nuevo)):
                  if (strcmp($entity->field_nuevo["und"][0]["value"], '1') == 0):
                  ?>
                  <span class="badge">Nuevo</span>
                  <?php endif; ?>
                  <?php endif; ?>

                  <?php
                  if (isset($entity->field_inscripciones)):
                  if (strcmp($entity->field_inscripciones["und"][0]["value"], '1') == 0):
                  ?>
                  <span class="badge badge-success">Inscripciones abiertas</span>
                  <?php endif; ?>
                  <?php endif; ?>

              </p>
            </div>
        </div>
    </a>
</div>
