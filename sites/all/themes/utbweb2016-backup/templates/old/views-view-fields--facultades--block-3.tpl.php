<?php
$entity = node_load($fields['nid']->raw);
if ($entity->field_cursos_virtuales["und"][0]["target_id"]) :
    ?>   
    <div class="panel panel-default">

        <div class="panel-heading">
            <a class="panel-title" data-toggle="collapse" data-parent="#panel-vir" href="#<?php print $entity->nid . "vir"; ?>">
                <?php print $entity->title; ?></a>
        </div>

        <div id="<?php print $entity->nid . "vir"; ?>" class="panel-collapse collapse">

            <div class="panel-body">
                <div class="panel-group" id="panel-pre-vir">

                    <p>La oferta de cursos en programas virtuales de <?php print $entity->title; ?> está compuesta de la siguiente manera:</p>
                    <br />                

                    <?php
                    $cur = array();
                    $sem = array();
                    $dip = array();
                    $min = array();
                    $tal = array();
                    $cha = array();
                    $conf = array();


                    for ($i = 0;; $i++) :
                        if (!$entity->field_cursos_virtuales["und"][$i]["target_id"]) :
                            break;
                        endif;
                        $programa = node_load($entity->field_cursos_virtuales["und"][$i]["target_id"]);

                        $temp = $programa->field_tipo["und"][0]['value'];


                        if (strcmp($temp, "Curso") == 0) {
                            array_push($cur, $programa);
                        }
                        if (strcmp($temp, "Seminario") == 0) {
                            array_push($sem, $programa);
                        }
                        if (strcmp($temp, "Diplomado") == 0) {
                            array_push($dip, $programa);
                        }
                        if (strcmp($temp, "Minor") == 0) {
                            array_push($min, $programa);
                        }
                        if (strcmp($temp, "Taller") == 0) {
                            array_push($tal, $programa);
                        }
                        if (strcmp($temp, "Charla") == 0) {
                            array_push($cha, $programa);
                        }
                        if (strcmp($temp, "Conferencia") == 0) {
                            array_push($conf, $programa);
                        }
                        ?>                   


                    <?php endfor; ?>
                    <!--                        CADA PROGRAMA/CURSO                     -->            
                    <?php if (count($cur) != 0):
     print count($cur)."hola";
                        ?>
                    
                        <div class="panel panel-default">
                            <div class="panel-heading">
                                <a class="panel-title" data-parent="#panel-pre-vir" data-toggle="collapse" href="#<?php print $entity->nid . "cur-vir"; ?>"> Cursos </a>
                            </div>
                            <div class="panel-collapse collapse" id="<?php print $entity->nid . "cur-vir"; ?>" style="height: auto;">
                                <div class="panel-body">
                                    <p>La oferta de Cursos de la <?php print $entity->title; ?> está compuesta de la siguiente manera:</p>
                                    <br>

                                    <!--                        CADA PROGRAMA/CURSO                     -->
                                    <div class="list-group">

                                        <?php
                                        for ($z = 0; $z < count($cur); $z++) :
                                            $pro = $cur[$z];
                                            $ID = $pro->nid;
                                            $alias = drupal_get_path_alias('node/' . $ID);
                                            ?>

                                            <a class="list-group-item" href="<?php print($alias); ?>">
                                                <?php if ($pro->field_nuevo): if (strcmp($pro->field_nuevo ["und"][0]["value"], '1') == 0): ?>
                                                        <span class="badge pull-right">Nuevo</span>    
                                                    <?php endif; ?>  <?php endif; ?><?php
                                                print($pro->title);
                                                ?>                                    
                                            </a>
                                        <?php endfor; ?>

                                    </div>
                                </div>
                            </div>
                        </div>
                    <?php endif; ?>
                    <?php if (count($sem) != 0): ?> 
                        <div class="panel panel-default">
                            <div class="panel-heading">
                                <a class="panel-title" data-parent="#panel-pre-vir" data-toggle="collapse" href="#<?php print $entity->nid . "sem-vir"; ?>"> Seminarios </a>
                            </div>
                            <div class="panel-collapse collapse" id="<?php print $entity->nid . "sem-vir"; ?>" style="height: auto;">
                                <div class="panel-body">
                                    <p>La oferta de Seminarios de la  <?php print $entity->title; ?> está compuesta de la siguiente manera:</p>
                                    <br>

                                    <!--                        CADA PROGRAMA/CURSO                     -->
                                    <div class="list-group">

                                        <?php
                                        for ($z = 0; $z < count($sem); $z++) :
                                            $pro = $sem[$z];
                                            $ID = $pro->nid;
                                            $alias = drupal_get_path_alias('node/' . $ID);
                                            ?>

                                            <a class="list-group-item" href="<?php print($alias); ?>">
                                                <?php if ($pro->field_nuevo): if (strcmp($pro->field_nuevo ["und"][0]["value"], '1') == 0): ?>
                                                        <span class="badge pull-right">Nuevo</span>    
                                                    <?php endif; ?>  <?php endif; ?><?php
                                                print($pro->title);
                                                ?>                                    
                                            </a>
                                        <?php endfor; ?>

                                    </div>
                                </div>
                            </div>
                        </div>
                    <?php endif; ?>
                    <?php if (count($dip) != 0): ?>               
                        <div class="panel panel-default">
                            <div class="panel-heading">
                                <a class="panel-title" data-parent="#panel-pre-vir" data-toggle="collapse" href="#<?php print $entity->nid . "dip-vir"; ?>"> Diplomados </a>
                            </div>
                            <div class="panel-collapse collapse" id="<?php print $entity->nid . "dip-vir"; ?>" style="height: auto;">
                                <div class="panel-body">
                                    <p>La oferta de diplomados de la <?php print $entity->title; ?> está compuesta de la siguiente manera:</p>
                                    <br>

                                    <!--                        CADA PROGRAMA/CURSO                     -->
                                    <div class="list-group">

                                        <?php
                                        for ($z = 0; $z < count($dip); $z++) :
                                            $pro = $dip[$z];
                                            $ID = $pro->nid;
                                            $alias = drupal_get_path_alias('node/' . $ID);
                                            ?>

                                            <a class="list-group-item" href="<?php print($alias); ?>">
                                                <?php if ($pro->field_nuevo): if (strcmp($pro->field_nuevo ["und"][0]["value"], '1') == 0): ?>
                                                        <span class="badge pull-right">Nuevo</span>    
                                                    <?php endif; ?>  <?php endif; ?><?php
                                                print($pro->title);
                                                ?>                                    
                                            </a>
                                        <?php endfor; ?>

                                    </div>
                                </div>
                            </div>
                        </div>
                    <?php endif; ?>               
                    <?php if (count($min) != 0): ?>
                        <div class="panel panel-default">
                            <div class="panel-heading">
                                <a class="panel-title" data-parent="#panel-pre-vir" data-toggle="collapse" href="#<?php print $entity->nid . "min-vir"; ?>"> Minors </a>
                            </div>
                            <div class="panel-collapse collapse" id="<?php print $entity->nid . "min-vir"; ?>" style="height: auto;">
                                <div class="panel-body">
                                    <p>La oferta de posgrado en minor de la <?php print $entity->title; ?> está compuesta de la siguiente manera:</p>
                                    <br>

                                    <!--                        CADA PROGRAMA/CURSO                     -->
                                    <div class="list-group">

                                        <?php
                                        for ($z = 0; $z < count($min); $z++) :
                                            $pro = $min[$z];
                                            $ID = $pro->nid;
                                            $alias = drupal_get_path_alias('node/' . $ID);
                                            ?>

                                            <a class="list-group-item" href="<?php print($alias); ?>">
                                                <?php if ($pro->field_nuevo): if (strcmp($pro->field_nuevo ["und"][0]["value"], '1') == 0): ?>
                                                        <span class="badge pull-right">Nuevo</span>    
                                                    <?php endif; ?>  <?php endif; ?><?php
                                                print($pro->title);
                                                ?>                                    
                                            </a>
                                        <?php endfor; ?>

                                    </div>
                                </div>
                            </div>
                        </div>
                    <?php endif; ?>
                    <?php if (count($tal) != 0): ?>
                        <div class="panel panel-default">
                            <div class="panel-heading">
                                <a class="panel-title" data-parent="#panel-pre-vir" data-toggle="collapse" href="#<?php print $entity->nid . "tal-vir"; ?>"> Talleres</a>
                            </div>
                            <div class="panel-collapse collapse" id="<?php print $entity->nid . "tal-vir"; ?>" style="height: auto;">
                                <div class="panel-body">
                                    <p>La oferta de posgrado en talleres de la <?php print $entity->title; ?> está compuesta de la siguiente manera:</p>
                                    <br>

                                    <!--                        CADA PROGRAMA/CURSO                     -->
                                    <div class="list-group">

                                        <?php
                                        for ($z = 0; $z < count($tal); $z++) :
                                            $pro = $tal[$z];
                                            $ID = $pro->nid;
                                            $alias = drupal_get_path_alias('node/' . $ID);
                                            ?>

                                            <a class="list-group-item" href="<?php print($alias); ?>">
                                                <?php if ($pro->field_nuevo): if (strcmp($pro->field_nuevo ["und"][0]["value"], '1') == 0): ?>
                                                        <span class="badge pull-right">Nuevo</span>    
                                                    <?php endif; ?>  <?php endif; ?><?php
                                                print($pro->title);
                                                ?>                                    
                                            </a>
                                        <?php endfor; ?>

                                    </div>
                                </div>
                            </div>
                        </div> 
                    <?php endif; ?>
                    <?php if (count($cha) != 0): ?>
                        <div class="panel panel-default">
                            <div class="panel-heading">
                                <a class="panel-title" data-parent="#panel-pre-vir" data-toggle="collapse" href="#<?php print $entity->nid . "cha-vir"; ?>"> Charlas </a>
                            </div>
                            <div class="panel-collapse collapse" id="<?php print $entity->nid . "cha-vir"; ?>" style="height: auto;">
                                <div class="panel-body">
                                    <p>La oferta de charlas de la <?php print $entity->title; ?> está compuesta de la siguiente manera:</p>
                                    <br>

                                    <!--                        CADA PROGRAMA/CURSO                     -->
                                    <div class="list-group">

                                        <?php
                                        for ($z = 0; $z < count($cha); $z++) :
                                            $pro = $cha[$z];
                                            $ID = $pro->nid;
                                            $alias = drupal_get_path_alias('node/' . $ID);
                                            ?>

                                            <a class="list-group-item" href="<?php print($alias); ?>">
                                                <?php if ($pro->field_nuevo): if (strcmp($pro->field_nuevo ["und"][0]["value"], '1') == 0): ?>
                                                        <span class="badge pull-right">Nuevo</span>    
                                                    <?php endif; ?>  <?php endif; ?><?php
                                                print($pro->title);
                                                ?>                                    
                                            </a>
                                        <?php endfor; ?>

                                    </div>
                                </div>
                            </div>
                        </div> 
                    <?php endif; ?>
                    <?php if (count($conf) != 0): ?>
                        <div class="panel panel-default">
                            <div class="panel-heading">
                                <a class="panel-title" data-parent="#panel-pre-vir" data-toggle="collapse" href="#<?php print $entity->nid . "conf-vir"; ?>"> Conferencias </a>
                            </div>
                            <div class="panel-collapse collapse" id="<?php print $entity->nid . "conf-vir"; ?>" style="height: auto;">
                                <div class="panel-body">
                                    <p>La oferta de conferencias de la <?php print $entity->title; ?> está compuesta de la siguiente manera:</p>
                                    <br>

                                    <!--                        CADA PROGRAMA/CURSO                     -->
                                    <div class="list-group">

                                        <?php
                                        for ($z = 0; $z < count($conf); $z++) :
                                            $pro = $conf[$z];
                                            $ID = $pro->nid;
                                            $alias = drupal_get_path_alias('node/' . $ID);
                                            ?>

                                            <a class="list-group-item" href="<?php print($alias); ?>">
                                                <?php if ($pro->field_nuevo): if (strcmp($pro->field_nuevo ["und"][0]["value"], '1') == 0): ?>
                                                        <span class="badge pull-right">Nuevo</span>    
                                                    <?php endif; ?>  <?php endif; ?><?php
                                                print($pro->title);
                                                ?>                                    
                                            </a>
                                        <?php endfor; ?>

                                    </div>
                                </div>
                            </div>
                        </div> 
                    <?php endif; ?>
                </div>
            </div>
        </div>  
    </div>
<?php endif; ?> 


<?php
if ($entity->field_programas_virtual["und"][0]["target_id"]) :
    ?>                             
    <div class="panel panel-default">

        <div class="panel-heading">
            <a class="panel-title" data-toggle="collapse" data-parent="#panel-pre-vir" href="#<?php print $entity->nid . "pre-vir"; ?>">
                <?php print $entity->title; ?></a>
        </div>

        <div id="<?php print $entity->nid . "pre-vir"; ?>" class="panel-collapse collapse">

            <div class="panel-body">

                <p>La oferta de pregrado en programas virtuales de <?php print $entity->title; ?> está compuesta de la siguiente manera:</p>
                <br />
                <div class="list-group">                  
                    <?php
                    $list_program = array();

                    for ($i = 0;; $i++) :
                        $ID = $entity->field_programas_virtual["und"][$i]["target_id"];
                        if ($ID == 0 or strcmp("0", $entity->status) == 0) :
                            break;
                        endif;
                        $program = node_load($entity->field_programas_virtual["und"][$i]["target_id"]);
                        array_push($list_program, $program);
                    endfor;
                    $titles = array();

                    for ($x = 0; $x < count($list_program); $x++) :
                        $titles[$x] = $list_program[$x]->title;
                    endfor;
                    asort($titles);
                    foreach ($titles as $key => $val):

                        $ID = $list_program[$key]->nid;
                        $alias = drupal_get_path_alias('node/' . $ID);
                        ?>

                        <a class="list-group-item" href="<?php print($alias); ?>">
                            <?php if ($list_program[$key]->field_nuevo):
                                if (strcmp($list_program[$key]->field_nuevo ["und"][0]["value"], '1') == 0): ?>
                                    <span class="badge pull-right">Nuevo</span>    
                                <?php endif; ?>  
                            <?php endif; ?><?php print $list_program[$key]->title; ?>                                    
                        </a>

                        <?php
                    endforeach;
                    ?>    
                </div>						

            </div>
        </div>
    </div>
<?php endif; ?> 