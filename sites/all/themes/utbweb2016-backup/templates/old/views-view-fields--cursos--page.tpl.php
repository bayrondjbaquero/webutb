<div class="col-md-11 col-md-offset-1 column">

    <div class="panel panel-primary">

        <div class="panel-heading">
            <h4 class="panel-title"><?php print  $fields['title']->content?></h4>
        </div>

        <div class="panel-body">

            <div class="img-responsive pull-right margen-m"> <?php print $fields['field_imagen_curso_corto']->content; ?> </div>

            <p>

              <?php print  $fields['body']->content?>

            </p>


        </div>

        <div class="panel-footer">

            <p>Area: <strong><?php  print  $fields['field_area_conocimiento_curso_co']->content ?></strong> 							
            <a class="btn btn-primary btn-default pull-right" href="<?php print "node/".$fields['nid']->content; ?>" ><span class="glyphicon glyphicon-arrow-right"></span> Ver Curso</a> 
            </p> 

        </div>

    </div>

</div>