<div id="node-<?php print $node->nid; ?>" class="<?php print $classes; ?> clearfix"<?php print $attributes; ?>>

    <div class="row clearfix">

        <div class="col-md-12 column">
            <?php
            print $node->body["und"][0]["safe_value"];
            $list_cursos = array();
            for ($i = 0;; $i++) {
                $id_curso = $node->field_cursos["und"][$i]["value"];
                if ($id_curso == 0) {
                    break;
                }
                array_push($list_cursos, $id_curso);
            }
            ?>
            <br /><br /><br />    
            <hr>
            <h3>Cursos Recomendados</h3>
            <br />
            <?php
            db_set_active('programas');

            for ($j = 0; $j < count($list_cursos); $j++) {
                $ID = $list_cursos[$j];
                $curso = node_load($ID);
                $uriImagefromNode = $curso->field_mini_foto['und'][0]['uri'];
                $semi_url = str_replace("public://", "", $uriImagefromNode);
                $URL = "http://programas.unitecnologica.edu.co/sites/programas.unitecnologica.edu.co/files/" . $semi_url;
                ?>
                <div class="col-md-6 thumbnail">
                    <h5 class="text-primary" >
                        <?php print $curso->title; ?></h5>
                    <div class="col-md-6">
                        <?php print $curso->body["und"][0]['safe_summary']; ?>
                        <a class="btn btn-primary" href="<?php print "http://programas.unitecnologica.edu.co/node/" . $curso->nid; ?>">
                            Ir al Curso ...  
                        </a>            
                    </div> 
                    <div class="col-md-6">
                        <img alt="<?php print $curso->field_mini_foto['und'][0]['alt']; ?>" src="<?php print $URL; ?>"  class="padding-m img-responsive"/>
                    </div>
                </div>
                <?php
            }

            db_set_active();
            ?>

        </div>

    </div>

</div>




