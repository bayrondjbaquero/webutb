<?php

/**
 * @file
 * Default simple view template to all the fields as a row.
 *
 * - $view: The view in use.
 * - $fields: an array of $field objects. Each one contains:
 *   - $field->content: The output of the field.
 *   - $field->raw: The raw data for the field, if it exists. This is NOT output safe.
 *   - $field->class: The safe class id to use.
 *   - $field->handler: The Views field handler object controlling this field. Do not use
 *     var_export to dump this object, as it can't handle the recursion.
 *   - $field->inline: Whether or not the field should be inline.
 *   - $field->inline_html: either div or span based on the above flag.
 *   - $field->wrapper_prefix: A complete wrapper containing the inline_html to use.
 *   - $field->wrapper_suffix: The closing tag for the wrapper.
 *   - $field->separator: an optional separator that may appear before a field.
 *   - $field->label: The wrap label text to use.
 *   - $field->label_html: The full HTML of the label to use including
 *     configured element type.
 * - $row: The raw result object from the query, with all data it fetched.
 *
 * @ingroup views_templates
 */
?>
<hr>
<div class="cold-xs-12">
	<div class="col-xs-4">
		<div class="img_clasificado_wrapper">
			<?php print $fields['field_imagen']->content; ?>
		</div>
	</div>
	<div class="col-xs-8">
		<div class="titulo_clasificado">
			<?php print $fields['title']->content; ?>
		</div>
		<div class="precio_clasificado">
			<span class="etiqueta"><?php print $fields['field_precio_salario']->label_html; ?></span>&nbsp;<?php print $fields['field_precio_salario']->content; ?>
		</div>
	</div>

</div><!-- fin col-md-12 -->
<div class="clearfix"></div>


<!--
<?php foreach ($fields as $id => $field): ?>
  <?php if (!empty($field->separator)): ?>
    <?php //print $field->separator; ?>
  <?php endif; ?>

  <?php //print $field->wrapper_prefix; ?>
    <?php //print $field->label_html; ?>
    <?php //print $fields['title']->content; ?>
  <?php //print $field->wrapper_suffix; ?>
<?php endforeach; ?>
-->
