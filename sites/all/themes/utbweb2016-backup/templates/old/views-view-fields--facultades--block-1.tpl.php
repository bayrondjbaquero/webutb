<?php
$entity = node_load($fields['nid']->raw);
if ($entity->field_programas_programaspos["und"][0]["target_id"]) :
    ?>   
    <div class="panel panel-default">

        <div class="panel-heading">
            <a class="panel-title" data-toggle="collapse" data-parent="#panel-pos" href="#<?php print $entity->nid . "pos"; ?>">
                <?php print $entity->title; ?></a>
        </div>
        <div id="<?php print $entity->nid . "pos"; ?>" class="panel-collapse collapse">

            <div class="panel-body">
                <div class="panel-group" id="panel-posg">
                    <p>La oferta de posgrados en programas profesionales de la <?php print $entity->title; ?> está compuesta de la siguiente manera:</p>
                    <br />                

                    <?php
                    $esp_tea = array();
                    $esp_teo = array();
                    $esp_pro = array();
                    $mae = array();
                    $doc = array();


                    for ($i = 0;; $i++) :
                        if (!$entity->field_programas_programaspos["und"][$i]["target_id"]) :
                            break;
                        endif;
                        $programa = node_load($entity->field_programas_programaspos["und"][$i]["target_id"]);

                        $temp = $programa->field_nivel["und"][0]['value'];

                        if (strcmp($temp, "Especialización Técnica") == 0) {
                            array_push($esp_tea, $programa);
                        }
                        if (strcmp($temp, "Especialización Tecnólogica") == 0) {
                            array_push($esp_teo, $programa);
                        }
                        if (strcmp($temp, "Especialización Profesional") == 0) {
                            array_push($esp_pro, $programa);
                        }
                        if (strcmp($temp, "Maestría") == 0) {
                            array_push($mae, $programa);
                        }
                        if (strcmp($temp, "Doctorado") == 0) {
                            array_push($doc, $programa);
                        }
                        ?>                   


                    <?php endfor; ?>
                    <!--                        CADA PROGRAMA/CURSO                     -->            

                    <?php if (count($esp_tea) != 0): ?>
                        <div class="panel panel-default">
                            <div class="panel-heading">
                                <a class="panel-title" data-parent="#panel-posg" data-toggle="collapse" href="#<?php print $entity->nid . "esp_tea"; ?>"> Especializaciones Técnicas </a>
                            </div>
                            <div class="panel-collapse collapse" id="<?php print $entity->nid . "esp_tea"; ?>" style="height: auto;">
                                <div class="panel-body">
                                    <p>La oferta de posgrado en especializaciones de la <?php print $entity->title; ?> está compuesta de la siguiente manera:</p>
                                    <br>

                                    <!--                        CADA PROGRAMA/CURSO                     -->
                                    <div class="list-group">

                                        <?php
                                        for ($z = 0; $z < count($esp_tea); $z++) :
                                            $pro = $esp_tea[$z];
                                            $ID = $pro->nid;
                                            $alias = drupal_get_path_alias('node/' . $ID);
                                            ?>

                                            <a class="list-group-item" href="<?php print($alias); ?>">
                                                <?php if ($pro->field_nuevo): if (strcmp($pro->field_nuevo ["und"][0]["value"], '1') == 0): ?>
                                                        <span class="badge pull-right">Nuevo</span>    
                                                    <?php endif; ?>  <?php endif; ?><?php
                                                print($pro->title);
                                                ?>                                    
                                            </a>
                                        <?php endfor; ?>

                                    </div>
                                </div>
                            </div>
                        </div>
                    <?php endif; ?>
                    <?php if (count($esp_teo) != 0): ?>
                        <div class="panel panel-default">
                            <div class="panel-heading">
                                <a class="panel-title" data-parent="#panel-posg" data-toggle="collapse" href="#<?php print $entity->nid . "esp_teo"; ?>"> Especializaciones Tecnólogicas </a>
                            </div>
                            <div class="panel-collapse collapse" id="<?php print $entity->nid . "esp_teo"; ?>" style="height: auto;">
                                <div class="panel-body">
                                    <p>La oferta de posgrado en especializaciones de la <?php print $entity->title; ?> está compuesta de la siguiente manera:</p>
                                    <br>

                                    <!--                        CADA PROGRAMA/CURSO                     -->
                                    <div class="list-group">

                                        <?php
                                        for ($z = 0; $z < count($esp_teo); $z++) :
                                            $pro = $esp_teo[$z];
                                            $ID = $pro->nid;
                                            $alias = drupal_get_path_alias('node/' . $ID);
                                            ?>

                                            <a class="list-group-item" href="<?php print($alias); ?>">
                                                <?php if ($pro->field_nuevo): if (strcmp($pro->field_nuevo ["und"][0]["value"], '1') == 0): ?>
                                                        <span class="badge pull-right">Nuevo</span>    
                                                    <?php endif; ?>  <?php endif; ?><?php
                                                print($pro->title);
                                                ?>                                    
                                            </a>
                                        <?php endfor; ?>

                                    </div>
                                </div>
                            </div>
                        </div>
                    <?php endif; ?>
                    <?php if (count($esp_pro) != 0): ?>               
                        <div class="panel panel-default">
                            <div class="panel-heading">
                                <a class="panel-title" data-parent="#panel-posg" data-toggle="collapse" href="#<?php print $entity->nid . "esp_pro"; ?>"> Especializaciones Profesionales </a>
                            </div>
                            <div class="panel-collapse collapse" id="<?php print $entity->nid . "esp_pro"; ?>" style="height: auto;">
                                <div class="panel-body">
                                    <p>La oferta de posgrado en especializaciones de la <?php print $entity->title; ?> está compuesta de la siguiente manera:</p>
                                    <br>

                                    <!--                        CADA PROGRAMA/CURSO                     -->
                                    <div class="list-group">

                                        <?php
                                        for ($z = 0; $z < count($esp_pro); $z++) :
                                            $pro = $esp_pro[$z];
                                            $ID = $pro->nid;
                                            $alias = drupal_get_path_alias('node/' . $ID);
                                            ?>

                                            <a class="list-group-item" href="<?php print($alias); ?>">
                                                <?php if ($pro->field_nuevo): if (strcmp($pro->field_nuevo ["und"][0]["value"], '1') == 0): ?>
                                                        <span class="badge pull-right">Nuevo</span>    
                                                    <?php endif; ?>  <?php endif; ?><?php
                                                print($pro->title);
                                                ?>                                    
                                            </a>
                                        <?php endfor; ?>

                                    </div>
                                </div>
                            </div>
                        </div>
                    <?php endif; ?>               
                    <?php if (count($mae) != 0): ?>
                        <div class="panel panel-default">
                            <div class="panel-heading">
                                <a class="panel-title" data-parent="#panel-posg" data-toggle="collapse" href="#<?php print $entity->nid . "mae"; ?>"> Maestrías </a>
                            </div>
                            <div class="panel-collapse collapse" id="<?php print $entity->nid . "mae"; ?>" style="height: auto;">
                                <div class="panel-body">
                                    <p>La oferta de posgrado en maestrías de la <?php print $entity->title; ?> está compuesta de la siguiente manera:</p>
                                    <br>

                                    <!--                        CADA PROGRAMA/CURSO                     -->
                                    <div class="list-group">

                                        <?php
                                        for ($z = 0; $z < count($mae); $z++) :
                                            $pro = $mae[$z];
                                            $ID = $pro->nid;
                                            $alias = drupal_get_path_alias('node/' . $ID);
                                            ?>

                                            <a class="list-group-item" href="<?php print($alias); ?>">
                                                <?php if ($pro->field_nuevo): if (strcmp($pro->field_nuevo ["und"][0]["value"], '1') == 0): ?>
                                                        <span class="badge pull-right">Nuevo</span>    
                                                    <?php endif; ?>  <?php endif; ?><?php
                                                print($pro->title);
                                                ?>                                    
                                            </a>
                                        <?php endfor; ?>

                                    </div>
                                </div>
                            </div>
                        </div>
                    <?php endif; ?>
                    <?php if (count($doc) != 0): ?>
                        <div class="panel panel-default">
                            <div class="panel-heading">
                                <a class="panel-title" data-parent="#panel-posg" data-toggle="collapse" href="#<?php print $entity->nid . "doc"; ?>"> Doctorados </a>
                            </div>
                            <div class="panel-collapse collapse" id="<?php print $entity->nid . "doc"; ?>" style="height: auto;">
                                <div class="panel-body">
                                    <p>La oferta de posgrado en doctorado de la <?php print $entity->title; ?> está compuesta de la siguiente manera:</p>
                                    <br>

                                    <!--                        CADA PROGRAMA/CURSO                     -->
                                    <div class="list-group">

                                        <?php
                                        for ($z = 0; $z < count($doc); $z++) :
                                            $pro = $doc[$z];
                                            $ID = $pro->nid;
                                            $alias = drupal_get_path_alias('node/' . $ID);
                                            ?>

                                            <a class="list-group-item" href="<?php print($alias); ?>">
                                                <?php if ($pro->field_nuevo): if (strcmp($pro->field_nuevo ["und"][0]["value"], '1') == 0): ?>
                                                        <span class="badge pull-right">Nuevo</span>    
                                                    <?php endif; ?>  <?php endif; ?><?php
                                                print($pro->title);
                                                ?>                                    
                                            </a>
                                        <?php endfor; ?>

                                    </div>
                                </div>
                            </div>
                        </div> 
                    <?php endif; ?>
                </div>
            </div>
        </div>

    </div>
<?php endif; ?> 
