<div id="node-<?php print $node->nid; ?>" class="<?php print $classes; ?> clearfix"<?php print $attributes; ?>>

    <div class="row clearfix">

        <div class="col-md-12 column">

            <table class="table table-striped">
                <tbody>
                    <tr>
                        <th>Fecha Inicio</th>
                        <th>Fecha Final</th>
                    </tr>      
                    <tr>
                        <td><?php
                            print format_date(strtotime($content['field_fecha_inicio_mision']['#items'][0]['value']), 'custom', 'F j, Y l');
                            ?></td>
                        <td><?php
                            print format_date(strtotime($content['field_fecha_final_mision']['#items'][0]['value']), 'custom', 'F j, Y l');
                            ?></td>
                    </tr>
                </tbody>
            </table>
            <div class="pull-left margen-s"><?php print $fields['field_imagen_mision']->content; ?></div>
            <?php print $node->body["und"][0]["safe_value"]; ?>
        </div>

    </div>
</div>






