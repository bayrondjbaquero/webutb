<?php
$entity = node_load($fields['nid']->raw);
$uriImagefromNode = $entity->field_imagen['und'][0]['uri'];
$urlImagefromNode = file_create_url($uriImagefromNode);

 $path=drupal_get_path_alias('node/'.$fields['nid']->content);
?>
<div class="col-md-4">
    <figure class="effect-ming">
        <a href="<?php print $path; ?>">
            <img target="_blank" src="<?php print $urlImagefromNode; ?>" clas="img-responsive" style=" height: 210px; "/>
            <figcaption>
                <p><strong><?php print $entity->title; ?></strong></p>        
            </figcaption>
        </a>
    </figure>
</div>