<div id="node-<?php print $node->nid; ?>" class="<?php print $classes; ?> clearfix"<?php print $attributes; ?>>
 
    <div class="row clearfix">

        <div class="col-md-12 column">

            <table class="table table-striped">
                <tbody>
                    <tr>
                        <th>Entidad</th>
                        <th>Tipo de Usuario</th>
                        <th>Fecha de Inicio</th>
                    </tr>      
                    <tr>
                        <td><?php print $node->field_entidad["und"][0]['taxonomy_term']->name; ?></td>
                        <td><?php print $node->field_tipo_usuario_beca["und"][0]["value"]; ?></td>
                        <td><?php
                            print format_date(strtotime($content['field_fecha_convocatoria_']['#items'][0]['value']), 'custom', 'F j, Y l');
                            ?></td>
                    </tr>
                    <tr>
                        <th>Link de apoyo</th>
                        <th></th>
                        <th></th>
                    </tr>      
                    <tr>
                        <td><a href="<?php print $node->field_informacion_adicional_url["und"][0]["url"] ?>"><?php print $node->field_informacion_adicional_url["und"][0]["url"] ?></a></td>
                        <td></td><td></td>
                    </tr>
                </tbody>
            </table>
            <?php
            if($node->field_imagen_beca['und'][0]['uri']):
            $uriImagefromNode = $node->field_imagen_beca['und'][0]['uri'];
            $urlImagefromNode = file_create_url($uriImagefromNode);            
            ?>
            <img src="<?php print $urlImagefromNode; ?>" style=" min-height: 150px; " class="img-responsive"/>
            
            <?php
            endif;
            print $node->body["und"][0]["safe_value"]; 
            ?>
           <?php if ($content['field_adjuntos_beca']){ ?>
            <div class="panel panel-primary">
                <div class="panel-heading">                    
                    <h3 class="panel-title"> <span class="glyphicon glyphicon-folder-open" style="font-size:2.5em;"></span>&nbsp;&nbsp;&nbsp; Descargas adicionales </h3> 
                </div>
                <div class="panel-body">
                    <?php print render($content['field_adjuntos_beca']); ?>                              
                </div>											
            </div>
          <?php }   ?>
        </div>
    </div>
</div>


