<?php $valor = 0; 
foreach ($rows as $id => $row):  
    $valor = $id; 
    if($valor!=0):
        break;
    endif;
     endforeach; ?>
<div class="container panel-group" id="grados">
    <div class="panel panel-default">
        <div class="panel-heading">
            <a class="panel-title collapsed" data-toggle="collapse" data-parent="#grados" href='#actividad<?php print $valor; ?>'><h4>Grados en: <?php print $title; ?></h4></a>
        </div>
        <div id='actividad<?php print $valor; ?>' class="panel-collapse collapse" style="height: 0px;">
            <div class="panel-body">
                <table class="table table-striped">
                    <thead>
                        <tr>
                            <th>Actividad</th>
                            <th>Fecha inicio (Mes/Día/Año)</th>
                            <th>Fecha fin (Mes/Día/Año)</th>
                            <th>Adjunto</th>
                        </tr>
                    </thead>
                    <tbody>
                        <?php foreach ($rows as $id => $row): ?>
                            <?php print $row; ?>
                        <?php endforeach; ?>
                    </tbody>
                </table>
            </div>
        </div>
    </div>
</div>