<div id="article-<?php print $node->nid; ?>" class="article <?php
print $classes;
if ($display_submitted)
?>clearfix" <?php print $attributes; ?> >
    <div class="row col-md-12 ">  
        
    
  <!-- Modal -->
  <div class="modal fade" id="myModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
    <div class="modal-dialog">
      <div class="modal-content" style="width: 802.222222328186px;">
        <div class="modal-header">
          <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
          <h4 class="modal-title"> <?php
            print("<h1><small> ");
            print($node->title);
            print(", ");
            print($node->field_nivel_academico["und"][0]["value"]);
            print("</small></h1> ");
            //@TODO: incluir redes sociales
            ?></h4>
        </div>
        <div class="modal-body">
          <?php echo theme('image_style', array('style_name' => 'large', 'path' => $node->field_foto['und'][0]['uri'], 'class' => 'thumbnail')); ?> 
        </div>
        <div class="modal-footer">
          <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
        </div>
      </div><!-- /.modal-content -->
    </div><!-- /.modal-dialog -->
  </div><!-- /.modal -->
        
        
        <a href="<?php echo url("node/" . $node->nid); ?>">
            <?php
            print("<h1><small> ");
            print($node->title);
            print("</small></h1> ");
            //@TODO: incluir redes sociales
            ?>
        </a>
        <br/>
        <div class="row">
            <div class="col-md-6">
                <a href="#myModal" class="imagen" role="button" data-toggle="modal"> <?php echo theme('image_style', array('style_name' => 'staff', 'path' => $node->field_foto['und'][0]['uri'], 'class' => 'imagen', 'attributes' => array('class' => 'img-responsive'))); ?> </a>                 
            </div>

            <div class="col-md-6 centrado">
                <div class="pull-left">
                    <dl>  
                        <?php if (strlen($node->field_url_perfil["und"][0]['url']) > 1) { ?>
                        <dt>Perfil Investigador:</dt>
                        <dd><a class="btn btn-default" target="_blank" href="<?php print($node->field_url_perfil["und"][0]['url']) ?>">Información adicional</a></dd>
                        <?php } ?>                     
                        
                        <?php if (strlen($node->field_email["und"][0]["safe_value"]) > 1) { ?>   
                        <dt>Correo electrónico:</dt><dd><?php
                            print(" " . $node->field_email["und"][0]["safe_value"]);
                            ?></dd>
                        <?php } ?>
                        <?php if (strlen($node->field_telefono["und"][0]["safe_value"]) > 1) { ?>
                        <dt>Teléfono:</dt><dd><?php
                            print(" " . $node->field_telefono["und"][0]["safe_value"]);
                            ?></dd>
                        <?php } ?>
                        <?php if (strlen($node->field_direccion_oficina["und"][0]["safe_value"]) > 1) { ?>
                        <dt>Dirección de Oficina:</dt>
                        <dd><?php print($node->field_direccion_oficina["und"][0]["safe_value"]); ?></dd>
                        <?php } ?>
                        <?php if (strlen($node->field_tipo_de_investigador["und"][0]["safe_value"]) > 1) { ?>
                        <dt>Web Personal:</dt>
                        <dd><?php print($node->field_tipo_de_investigador["und"][0]["safe_value"]); ?></dd>                                      
                        <?php } ?>
<!--
                        <dt>Temáticas de Interés:</dt>-->
                        <dd><?php
                            print render($content['field_grupo_de_investigaci_n']);                            
                            ?></dd>                                      

                    </dl>
                </div>
            </div>
        </div>
        <br/>
        <hr>
        <br/>        
        <div class="row"
             <div class="col-md-12">
                <?php print($node->body["und"][0]["safe_value"]); ?>
                
            </div>
        </div>
    </div>    