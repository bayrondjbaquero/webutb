<?php

/**
 * @file
 * Default simple view template to display a list of rows.
 *
 * @ingroup views_templates
 */
?>
<?php if (!empty($title)): ?>
  <div class="row clearfix fecha">

    <h3><span class="glyphicon glyphicon-calendar"></span><?php print $title; ?></h3>
    <hr>

</div>
  
<?php endif; ?>
<div class="panel-group">
<?php foreach ($rows as $id => $row): ?>
  <div<?php if ($classes_array[$id]) { print ' class="' . $classes_array[$id] .'"';  } ?>>
    <?php print $row; ?>
  </div>
<?php endforeach; ?>
</div>