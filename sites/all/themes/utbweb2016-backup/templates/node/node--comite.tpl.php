<style>
.img-responsive{
    min-height: 200px !important;
  }
  .elegant-card .card-content p {
    font-size: 0.89em !important;
  }
  .elegant-card .card-content {
      font-weight: bold !important;
      min-height: 106px;
    }
</style>

<?php if (!empty($body)): ?>

    <article id="node-<?php print $node->nid; ?>" class="<?php print $classes; ?> clearfix"<?php print $attributes; ?>> <?php print $user_picture; ?> <?php print render($title_prefix); ?>

    </div>
<?php endif; ?>


<br/>
<hr/>
<br/>

<h5 class="section-title st-blue"> Consejo Superior    <small><?php print $node->field_contenido_superior['und'][0]['safe_value']; ?></small></h5>

<div class="row ">
    <?php
    for ($i = 0;; $i++) {
        $ID = $node->field_superior["und"][$i]["entity"]->nid;

        if ($ID == 0 or strcmp("0", $node->status) == 0) {
            break;
        }

        if ($node->field_superior["und"][$i]["entity"]->field_staffs_image["und"][0]["uri"]) {

            $uriImagefromNode = $node->field_superior["und"][$i]["entity"]->field_staffs_image["und"][0]["uri"];
            $urlImagefromNode = file_create_url($uriImagefromNode);
            $URL = file_create_url($uriImagefromNode);
        }
        ?>



        <div class="col-md-3">
            <div class="elegant-card z-depth-1 hoverable">
                <!-- Image wrapper -->
                <div class="card-up view overlay hm-white-slight">
                    <p class="card-label"> <span class="label rgba-black-light">  </span></p>
                    <a><img src="<?php
                        print $URL;
                        $URL = null;
                        ?>" class="img-responsive"></a>
                    <div class="mask waves-effect waves-light"> </div>
                </div>

                <!-- Button -->
                <a class="btn-floating btn-large waves-effect waves-light stylish-color"><i class="fa fa-certificate"></i></a>
                <!-- Content -->
                <div class="card-content">
                    <?php print $node->field_superior["und"][$i]["entity"]->title; ?>
                    <p><?php print $node->field_superior["und"][$i]["entity"]->field_cargo["und"][0]["value"]; ?></p>
                </div>
            </div>
        </div>



        <?php
    }
    ?>
</div>


<br/>
<hr/>
<br/>




<h5 class="section-title st-blue"> CONSEJO ACADÉMICO  <small><?php print $node->field_contenido_academico['und'][0]['safe_value']; ?> </small></h5>

<div class="row">
    <?php
    for ($i = 0;; $i++) {
        $ID = $node->field_academico["und"][$i]["entity"]->nid;
        if ($ID == 0 or strcmp("0", $node->status) == 0) {
            break;
        }

        if ($node->field_academico["und"][$i]["entity"]->field_staffs_image["und"][0]["uri"]) {

            $uriImagefromNode = $node->field_academico["und"][$i]["entity"]->field_staffs_image["und"][0]["uri"];
            $urlImagefromNode = file_create_url($uriImagefromNode);
            $URL = file_create_url($uriImagefromNode);
        }
        ?>



        <div class="col-md-3">
            <div class="elegant-card z-depth-1 hoverable">
                <!-- Image wrapper -->
                <div class="card-up view overlay hm-white-slight">
                    <p class="card-label"> <span class="label rgba-black-light"> </span></p>
                    <a><img src="<?php
                        print $URL;
                        $URL = null;
                        ?>" class="img-responsive"></a>
                    <div class="mask waves-effect waves-light"> </div>
                </div>

                <!-- Button -->
                <a class="btn-floating btn-large waves-effect waves-light stylish-color"><i class="fa fa-university"></i></a>
                <!-- Content -->
                <div class="card-content">
                    <?php print $node->field_academico["und"][$i]["entity"]->title; ?>
                    <p><?php print $node->field_academico["und"][$i]["entity"]->field_cargo["und"][0]["value"]; ?></p>
                </div>
            </div>
        </div>

        <?php
    }
    ?>
</div>


<br/>
<hr/>
<br/>

<h5 class="section-title st-blue"> CONSEJO ADMINISTRATIVO <small><?php print $node->field_contenido_administrativo['und'][0]['safe_value']; ?></small></h5>

<div class="row">
    <?php
    for ($i = 0;; $i++) {
        $ID = $node->field_administrativo["und"][$i]["entity"]->nid;
        if ($ID == 0 or strcmp("0", $node->status) == 0) {
            break;
        }

        if ($node->field_administrativo["und"][$i]["entity"]->field_staffs_image["und"][0]["uri"]) {

            $uriImagefromNode = $node->field_administrativo["und"][$i]["entity"]->field_staffs_image["und"][0]["uri"];
            $urlImagefromNode = file_create_url($uriImagefromNode);
            $URL = file_create_url($uriImagefromNode);
        }
        ?>

        <div class="col-md-3">
            <div class="elegant-card z-depth-1 hoverable">
                <!-- Image wrapper -->
                <div class="card-up view overlay hm-white-slight">
                    <p class="card-label"> <span class="label rgba-black-light">  </span></p>
                    <a><img src="<?php
                        print $URL;
                        $URL = null;
                        ?>" class="img-responsive"></a>
                    <div class="mask waves-effect waves-light"> </div>
                </div>

                <!-- Button -->
                <a class="btn-floating btn-large waves-effect waves-light stylish-color"><i class="fa fa-users"></i></a>
                <!-- Content -->
                <div class="card-content">
                    <?php print $node->field_administrativo["und"][$i]["entity"]->title; ?>
                    <p><?php print $node->field_administrativo["und"][$i]["entity"]->field_cargo["und"][0]["value"]; ?></p>
                </div>
            </div>
        </div>


        <?php
    }
    ?>
</div>
