<?php
$pasos = array();
for ($i = 0;; $i++) :
    $ID = $node->field_pasos["und"][$i]["entity"]->nid;

    if ($ID == 0 or strcmp("0", $node->status) == 0) :
        break;
    endif;

    if ($node->field_pasos["und"][$i]["entity"]->status > 0) :
        $paso = $node->field_pasos["und"][$i]["entity"];
        array_push($pasos, $paso);

    endif;
endfor;
?>
<div class="clearfix"><?php print $node->body["und"][0]["safe_value"]; ?></div>
<br>
<!--Numeros paso a paso-->

<ul class="nav nav-tabs tabs-  <?php count($pasos); ?>">

  <?php for ($i = 0; $i < count($pasos); $i++): ?>
      <?php if ($i == 0): ?>
          <li class="active">
          <?php else: ?>
          <li>
          <?php endif; ?>
          <a data-toggle="tab" href="#paso<?php print $node->nid.$i; ?>" role="tab" data-toggle="pill"><?php print $i + 1; ?></a>
      </li>
  <?php endfor; ?>

</ul>


<div class="tab-content card-panel blue-grey lighten-5">
    <?php for ($z = 0; $z < count($pasos); $z++): ?>
        <?php if ($z == 0): ?>
            <div class="tab-pane fade in active" id="paso<?php print $node->nid.$z; ?>">
            <?php else: ?>
                <div class="tab-pane fade" id="paso<?php print $node->nid.$z; ?>">
                <?php endif; ?>

                <?php print $pasos[$z]->body["und"][0]["safe_value"]; ?>

            </div>
        <?php endfor; ?>

    </div>

</div>

                    <br>
                    <div class="clearfix"><?php print $node->field_pie["und"][0]["safe_value"]; ?>
                    </div>
