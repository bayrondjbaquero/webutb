<?php
        $noticias = array();
        for ($i = 0;; $i++) :
        $ID = $node->field_noticia["und"][$i]["entity"]->nid;
        if ($ID == 0) :
        break;
        endif;

        if ($node->field_noticia["und"][$i]["entity"]->status > 0) :
        $noticia = $node->field_noticia["und"][$i]["entity"];
        array_push($noticias, $noticia);
        endif;

        endfor;

        ?>




        <?php
        $uriImagefromNode = $node->field_fotonoticia['und'][0]['uri'];
        $urlImagefromNode = file_create_url($uriImagefromNode);
        ?>









        <html xmlns="http://www.w3.org/1999/xhtml" xmlns="http://www.w3.org/1999/xhtml">
            <head>
                <meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
                <meta name="viewport" content="width=device-width" />
                <title>Boletin CEaD - Boletín # </title>
                <script async src="https://www.googletagmanager.com/gtag/js?id=UA-107190981-1"></script>
                <script>
                    window.dataLayer = window.dataLayer || [];
                    function gtag() {
                        dataLayer.push(arguments);
                    }
                    ;
                    gtag('js', new Date());
                    gtag('config', 'UA-107190981-1');
                </script>

                <style>
                    body{
                        height: 100% !important;margin: 0;padding: 0;width: 100% !important;-ms-text-size-adjust: 100%;-webkit-text-size-adjust: 100%;font-family: helvetica;
                    }
                    .global-mso-configuration-padding-margin{
                        padding: 0;height: 100% !important;margin: 0;width: 100% !important;mso-table-lspace: 0;mso-table-rspace: 0;-ms-text-size-adjust: 100%;-webkit-text-size-adjust: 100%;
                    }
                    .global-mso-configuration-collapse{
                        border-collapse: collapse;mso-table-lspace: 0;mso-table-rspace: 0;-ms-text-size-adjust: 100%;-webkit-text-size-adjust: 100%;
                    }
                    .table-collapse{
                        border-collapse: collapse;
                    }

                    .td-configuration{
                        padding: 0;mso-table-lspace: 0;mso-table-rspace: 0;-ms-text-size-adjust: 100%;-webkit-text-size-adjust: 100%;
                    }
                    .td-configuration-padding{
                        padding-top: 10px;padding-bottom: 0;padding: 0;mso-table-lspace: 0;mso-table-rspace: 0;-ms-text-size-adjust: 100%;-webkit-text-size-adjust: 100%;
                    }
                    /*
                        Ajuste para Outlook
                    */
                    .global-mso-configuration{
                        mso-table-lspace: 0;mso-table-rspace: 0;-ms-text-size-adjust: 100%;-webkit-text-size-adjust: 100%
                    }
                    .padding-20{
                        padding-top: 20px;padding-right: 20px;padding-left: 20px;
                    }
                    /*
                        CSS DE LOS TITULOS DE LAS SECCIONES
                    */
                    .h3-titles{
                        color: white;margin: 0;padding: 0;font-family: Helvetica;font-size: 20px;line-height: 125%;text-align: Left;
                    }
                    /*
                        CSS DE LOS TD PERTENECIENTE BOTONES LEER MAS
                    */
                    .td-readmore{
                        padding: 15px;color: #FFF;font-family: Helvetica;font-size: 18px;font-weight: bold;line-height: 100%;text-align: center;
                    }
                    /*
                        CSS DE LOS TD PERTENIENTE A LAS IMAGENES DE LAS SECCIONES
                    */
                    .td-img-padding-0{
                        padding-bottom: 0;padding-top: 0 !important;padding: 0;
                    }
                    /*
                        CSS DE LOS TD PERTENECIENTE A LOS TEXTOS DE LAS SECCIONES
                    */
                    .td-section-text{
                        padding: 10px;color: #404040;font-family: Helvetica;font-size: 16px;line-height: 125%;text-align: Left;padding-bottom: 20px
                    }
                    /*
                        CSS DE LOS TITULOS DE LAS NOTICIAS DE LAS SECCIONES
                    */
                    .h3-new-title{
                        color: #006699;font-size: 26px;margin: 0;padding: 0;font-family: Helvetica;line-height: 125%;text-align: Left;
                    }
                    .td-padding-r-t-l-20-b-10{
                        padding: 0;padding-bottom: 10px;padding-right: 10px;padding-top: 20px;padding-left: 20px;
                    }
                    /*
                        CSS TABLA BOTONES LEER MAS
                    */
                    .table-readmore{
                        border-collapse: separate;background-color: #2C9AB7;border-radius: 4px;
                    }
                    /*
                        CSS IMGS
                    */
                    .img-styles{
                        border: 0;outline: none;text-decoration: none;-ms-interpolation-mode: bicubic;
                    }
                    /*
                         CSS NOMBRE DE LA SECCION
                    */
                    .section-title{
                        padding-top: 8px;padding-bottom: 0;padding: 0;
                    }
                    /*
                        CSS TEXTO NOTICIA
                    */
                    .p-news-text{
                        margin: 1em 0;-ms-text-size-adjust: 100%;-webkit-text-size-adjust: 100%;font-size: 14px;
                    }
                    /*
                        CSS LINKS LEER MAS
                    */
                    .a-readmore{
                        -ms-text-size-adjust: 100%;-webkit-text-size-adjust: 100%;color: #FFF;display: block;text-decoration: none;
                    }
                    .email-footer{
                        text-decoration: none;
                        color: white;
                    }
                    .links-socialnets{
                        text-decoration: none;
                    }
                </style>
            </head>
            <body>
                <center>
                    <table border="0" cellpadding="0" cellspacing="0" height="100%" width="100%" id="bodyTable" class="global-mso-configuration-padding-margin table-collapse" >
                        <tr>
                            <td valign="top" id="bodyCell" class="global-mso-configuration-padding-margin" style="padding-top: 40px;padding-bottom: 40px">
                                <!-- EMAIL CONTAINER // -->
                                <!--
                                        The table "emailBody" is the email's container.
                                    Its width can be set to 100% for a color band
                                    that spans the width of the page.
                                -->
                                <center>
                                    <table border="0" cellpadding="0" cellspacing="0" width="600"  id="emailBody" class="global-mso-configuration" style="border-collapse: separate; background-image: url('./assets/imgs/bg.png'); border: 1px solid #DDD;border-radius: 4px;">
                                        <!-- MODULE ROW MENSAJE// -->
                                        <tr style="height: 35px;background: #73B220;">
                                            <td>
                                                <p style="position: absolute; background: #0a2e38; width:450px; margin: -10px 170px 0; padding: 10px; color: white; font-size:12px; text-align: center;-ms-text-size-adjust: 100%;-webkit-text-size-adjust: 100%; border-radius:2px; ">
                                                    Si no puede visualizar correctamente este boletín haga clic <a style="color: white;-ms-text-size-adjust: 100%;-webkit-text-size-adjust: 100%" href="./">aquí</a>.
                                                </p>
                                            </td>

                                        </tr>
                                        <!-- // MODULE ROW MENSAJE-->

                                        <!-- MODULE ROW Cabecera // -->
                                        <tr>
                                            <td align="center" valign="top" class="td-configuration">
                                                <!-- CENTERING TABLE // -->
                                                <table border="0" cellpadding="0" cellspacing="0" width="100%" class="global-mso-configuration table-collapse" style=" margin-top: 20px;">
                                                    <tr>
                                                        <td width="300px" align="Left" valign="top" class="imageContent logocontainer global-mso-configuration" style="padding-top: 5px;padding: 0;padding-bottom: 20px">
                                                            <img src="http://www.utbvirtual.edu.co/sites/utbvirtual.edu.co/files/logoutbbol.png" class="flexibleImage logoutb img-styles" style="width: 130%;" />
                                                        </td>

                                                        <td align="Right" valign="top" class="imageContent socialicons" style="padding: 0;" class="global-mso-configuration">
                                                            <a href="https://www.youtube.com/channel/UCOyPJhsZ9nYnNU8A-jas-Mw" style="-ms-text-size-adjust: 100%;-webkit-text-size-adjust: 100%" class="links-socialnets">
                                                                <img src="http://www.utbvirtual.edu.co/sites/utbvirtual.edu.co/files/icono_youtube.png" class="img-styles" alt="Youtube"  />
                                                            </a>
                                                            <a href="http://twitter.com/utbvirtual" style="-ms-text-size-adjust: 100%;-webkit-text-size-adjust: 100%" class="links-socialnets">
                                                                <img src="http://www.utbvirtual.edu.co/sites/utbvirtual.edu.co/files/icono_twitter.png" class="img-styles" alt="Twitter"  />
                                                            </a>
                                                            <a href="http://www.facebook.com/UTBVirtual" style="-ms-text-size-adjust: 100%;-webkit-text-size-adjust: 100%" class="links-socialnets">
                                                                <img src="http://www.utbvirtual.edu.co/sites/utbvirtual.edu.co/files/icono_facebook-.png" class="img-styles" alt="Facebook" />
                                                            </a>
                                                        </td>
                                                    </tr>
                                                </table>
                                            </td>
                                        </tr>

                                        <tr>
                                            <td align="center" valign="top" class="td-configuration">
                                                <!-- CENTERING TABLE // -->
                                                <table border="0" cellpadding="0" cellspacing="0" width="100%" class="global-mso-configuration table-collapse">
                                                    <tr>
                                                        <td width="200px" align="Left" valign="top" class="imageContent logocontainer global-mso-configuration" style="padding-top: 5px;padding: 0;padding-bottom: 20px">
                                                            <img src="<?php print $urlImagefromNode; ?>" class="flexibleImage logoutb img-styles" />
                                                        </td>


                                                    </tr>
                                                </table>
                                            </td>
                                        </tr>
































            <?php
            for ($z = 0; $z < count($noticias); $z++):
            $uriImagefromNodeCap = $noticias[$z]->field_image['und'][0]['uri'];
            $urlImagefromNodeCap = file_create_url($uriImagefromNodeCap);
            ?>

            <?php

            $ID = $noticias[$z]->nid;
                $alias = drupal_get_path_alias('node/' . $ID);
                ?>

            <!-- MODULE ROW ACTUALIDAD // -->
                                        <tr>
                                            <td align="center" valign="top" class="td-configuration">
                                                <!-- CENTERING TABLE // -->
                                                <table border="0" cellpadding="0" cellspacing="0" width="100%" class="global-mso-configuration-collapse">
                                                    <tr>
                                                        <td align="center" valign="top" class="td-configuration">
                                                            <!-- FLEXIBLE CONTAINER // -->
                                                            <table border="0" cellpadding="0" cellspacing="0" width="700" class="flexibleContainer" class="global-mso-configuration-collapse">
                                                                <tr>
                                                                    <td align="center" valign="top" width="600" class="flexibleContainerCell" td-configuration padding-20>


                                                                        <!-- CONTENT TABLE // -->
                                                                        <table border="0" cellpadding="0" cellspacing="0" width="100%"  class="global-mso-configuration-collapse">
                                                                            <tr>
                                                                                <td colspan="2" valign="top"  class="imageContent global-mso-configuration td-img-padding-0">
                                                                                    <img src="<?php print $urlImagefromNodeCap; ?>" style="height: auto; width: 100%;" class="flexibleImage img-styles"/>
                                                                                </td>
                                                                            </tr>
                                                                            <tr>
                                                                                <td style="padding-left:30px;"></td>
                                                                                <td colspan="2" valign="top" class="global-mso-configuration td-section-text"  style="padding-left: 50px; ">
                                                                                    <h3 class="h3-new-title">  <a href="<?php print($alias); ?>"><?php print $noticias[$z]->title; ?></a></h3><br />

                                                                                    <p class="p-news-text">
                                                                                      <?php
                                                                                          print $noticias[$z]->body["und"][0]['safe_summary'];
                                                                                          ?>
                                                                                                                                                                            </p>

                                                                                </td>
                                                                            </tr>
                                                                            <tr>
                                                                                <td style="width:0.1%;"></td>
                                                                                <td colspan="2" align="Right" valign="top" width="600" class="flexibleContainerCell bottomShim global-mso-configuration td-padding-r-t-l-20-b-10" >


                                                                                    <!-- CONTENT TABLE // -->
                                                                                    <!--
                                                                                        The emailButton table's width can be changed
                                                                                        to affect the look of the button. To make the
                                                                                        button width dependent on the text inside, leave
                                                                                        the width blank. When a button is placed in a column,
                                                                                        it's helpful to set the width to 100%.
                                                                                    -->



                                                                                    <table border="0" cellpadding="0" cellspacing="0" width="150" class="emailButton global-mso-configuration table-readmore" >
                                                                                        <tr bgcolor="#73B220">
                                                                                            <td align="center" valign="middle" class="buttonContent global-mso-configuration td-readmore" >
                                                                                                <a href="<?php print($alias); ?>" class="a-readmore">Leer más</a>
                                                                                            </td>
                                                                                        </tr>
                                                                                    </table>
                                                                                    <!-- // CONTENT TABLE -->


                                                                                </td>
                                                                            </tr>
                                                                        </table>
                                                                        <!-- // CONTENT TABLE -->


                                                                    </td>
                                                                </tr>
                                                            </table>
                                                            <!-- // FLEXIBLE CONTAINER -->
                                                        </td>
                                                    </tr>
                                                </table>
                                                <!-- // CENTERING TABLE -->
                                            </td>
                                        </tr>
                                        <!-- // MODULE ROW ACTUALIDAD-->


            <!--/.1 Column horizontal listing-->
            <?php endfor; ?>
















            <!-- MODULE ROW FOOTER// -->
                                           <tr>
                                               <td align="center" valign="top" class="td-configuration">
                                                   <!-- CENTERING TABLE // -->
                                                   <table border="0" cellpadding="0" cellspacing="0" width="100%" class="global-mso-configuration-collapse">
                                                       <tr>
                                                           <td align="center" valign="top" class="td-configuration" style="background: #0a2e38">
                                                               <!-- CONTENT TABLE // -->
                                                               <table border="0" cellpadding="0" cellspacing="0" width="100%" class="global-mso-configuration-collapse">
                                                                   <tr>
                                                                       <td>
                                                                           <h3 style="color:white !important;text-align:center;font-size: 34px;margin-top: 15px; margin-bottom: 0px !important;"><a style="color: white !important;" href="<?php print $node->field_creditos["und"][0]['url']; ?>">Créditos</a></h3>
                                                                           <p style="color: white;text-align: center;-ms-text-size-adjust: 100%;-webkit-text-size-adjust: 100%;font-size: 13px">
                                                                               Universidad Tecnológica de Bolívar Copyright © 2013 - Cartagena - Bolívar - Colombia
                                                                               <br />
                                                                               Parque Industrial y Tecnológico Carlos Vélez Pombo, Km 1 Vía Turbaco.  <br>
                                                                                   Pbx: 57-5-6535200 -
                                                                                   Fax: 57-5- 6619240.
                                                                                   <br />
                                                                                   <br>
                                                                                       Campus Casa Lemaitre: Calle del Bouquet cra.21 #25-92, barrio Manga
                                                                                       <br>
                                                                                           Tels.: (5) 6606041-42-43
                                                                                           <br>
                                                                                               <br>
                                                                                                   Contáctenos
                                                                                                   <br />
                                                                                                   PBX: +57 6535200 ext. 130
                                                                                                   <br />
                                                                                                   Email: <strong><a href="mailto:soporte@utbvirtual.edu.co " class="email-footer">soporte@utbvirtual.edu.co </a></strong>
                                                                                                   <br>
                                                                                                       <img src="http://www.utbvirtual.edu.co/sites/utbvirtual.edu.co/files/logoutb_blanco.png" alt=""  height="60%">
                                                                                                           </p>

                                                                                                           </td>
                                                                                                           </tr>
                                                                                                           </table>
                                                                                                           <!-- // CONTENT TABLE -->
                                                                                                           </td>
                                                                                                           </tr>
                                                                                                           </table>
                                                                                                           <!-- // CENTERING TABLE -->
                                                                                                           </td>
                                                                                                           </tr>
                                                                                                           <!-- // MODULE ROW FOOTER-->

                                                                                                           </table>
                                                                                                           </center>
                                                                                                           <!-- // EMAIL CONTAINER -->
                                                                                                           </td>
                                                                                                           </tr>
                                                                                                           </table>
                                                                                                           </center>
                                                                                                           </body>
                                                                                                           </html>
