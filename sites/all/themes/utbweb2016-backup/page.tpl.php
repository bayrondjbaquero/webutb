<div id="menuauxiliar">
    <a name="start"></a> <section>

        <nav class="navbar z-depth-1 mer-pos" style="margin-bottom: 0px;">
            <div class="container ">
                <div class="navbar-header">
                    <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#menu-auxiliar">
                        <span class="sr-only">Toggle navigation</span>
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                    </button>
                    <a class="navbar-brand waves-effect waves-light" href="http://www.utb.edu.co"><strong>UTB</strong></a>
                </div>

                <div class="navbar-right collapse navbar-collapse" id="menu-auxiliar">

                    <?php print render($page['menu_auxiliar']); ?>

                </div>
            </div>
        </nav>

    </section>
</div>


<div class="container">
    <div id="header-utb">
        <div class="row clearfix">
            <div class="col-lg-8 col-md-12 col-sm-12 col-xs-12">
                <br>
                <?php if ($logo): ?>
                    <div class="col-lg-5 col-md-6 col-sm-6 col-xs-12 text-center">
                        <a href="/" title="<?php print t('Home'); ?>" rel="home"> <img  src="<?php print $logo; ?>" alt="<?php print $site_name; ?>" role="presentation" /> </a>
                    </div>
                <?php endif; ?>

                <div class="col-lg-7 col-md-6 col-sm-6 col-xs-12 text-center">
                    <hgroup>
                        <h1 style="font-size: 18px; margin-bottom: 10px;"><strong><?php print $site_name; ?></strong></h1>

                        <h2 style="font-size: 15px; margin-top: 10px;"><?php print $site_slogan; ?></h2>
                    </hgroup>
                </div>
            </div>

            <?php if ($page['encabezado_principal-2']): ?>
                <div class="col-lg-4 col-md-12 col-sm-12 col-xs-12 text-center">
                    <section>
                        <?php print render($page['encabezado_principal-2']); ?>
                    </section>
                </div>
            <?php endif; ?>
        </div>
    </div>

    <?php if (($page['encabezado_auxiliar-1']) || ($page['encabezado_auxiliar-2'])): ?>
        <div class="row clearfix">
            <?php if ($page['encabezado_auxiliar-1']): ?>
                <div class="col-md-6 column">
                    <section>
                        <?php print render($page['encabezado_auxiliar-1']); ?>
                    </section>
                </div>
            <?php endif; ?>
            <?php if ($page['encabezado_auxiliar-2']): ?>
                <div class="col-md-6 column">
                    <section>
                        <?php print render($page['encabezado_auxiliar-2']); ?>
                    </section>
                </div>
            <?php endif; ?>
        </div>
    <?php endif; ?>

    <?php if ($page['jumbo_1']): ?>
    </div>
    <?php print render($page['jumbo_1']) ?>
    <div class="container">
    <?php endif; ?>
</div>
<div class="clearfix">


    <div class="col-md-12 column" style="padding-right: 0px;padding-left: 0px;">

        <section>
            <?php if (!empty($page['main_menu'])): ?>


                <nav class="navbar z-depth-1 inst-strong" style="margin-bottom: 0px;">
                    <div class="container ">
                        <div class="navbar-header">
                            <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#main-menu">
                                <span class="sr-only">Toggle navigation</span>
                                <span class="icon-bar"></span>
                                <span class="icon-bar"></span>
                                <span class="icon-bar"></span>
                            </button>
                            <a class="navbar-brand waves-effect waves-light" href="<?php print $front_page; ?>"><i class="fa fa-home"></i></a>
                        </div>

                        <div class="collapse navbar-collapse" id="main-menu">
                            <?php print render($page['main_menu']); ?>
                        </div>
                    </div>
                </nav>

            <?php endif; ?>
        </section>

    </div>




</div>

<?php if ($messages): ?>
    <div class="container">
        <div class="row clearfix">
            <div class="row clearfix">
                <div class="col-md-12 column">
                    <section>
                        <?php print render($messages); ?>
                    </section>
                </div>
            </div>
        </div>
    </div>
<?php endif; ?>

<?php if (!empty($page['menu_secundario'])): ?>
    <div class="row clearfix">
        <div class="row clearfix">
            <div class="col-md-12 column">
                <section>
                    <?php if (!empty($page['menu_secundario'])): ?>
                        <?php print render($page['menu_secundario']); ?>
                    <?php endif; ?>
                </section>
            </div>
        </div>
    </div>
<?php endif; ?>


<div class="container">
    <?php if (($page['galeria'])): ?>
        <div class="row clearfix">
            <div class="col-md-12 column">
                <section>
                    <?php print render($page['galeria']); ?>
                </section>
            </div>
        </div>
    <?php endif; ?>


    <?php if (($page['galeria_prog'])): ?>
    </div>

    <div class="clearfix">
        <div class="col-md-12 column" style="padding-right: 0px;padding-left: 0px;">
            <section>
                <?php print render($page['galeria_prog']); ?>
            </section>
        </div>
    </div>


    <div class="container">

    <?php endif; ?>

    <?php if (($page['especial-completa-1'])): ?>
        <div class="row clearfix">
            <div class="col-md-12 column">
                <?php print render($page['especial-completa-1']); ?>
            </div>
        </div>
    <?php endif; ?>

    <?php if ($page['jumbo_2']): ?>
    </div>
    <?php print render($page['jumbo_2']) ?>
    <div class="container">
    <?php endif; ?>

    <?php if (($page['anuncios_destacados']) || ($page['noticias_destacadas'])): ?>
        <div class="row clearfix">
            <section>
                <?php if ($page['anuncios_destacados']): ?>
                    <div class="col-md-8 column">
                        <?php print render($page['anuncios_destacados']); ?>
                    </div>
                <?php endif; ?>
                <?php if ($page['noticias_destacadas']): ?>
                    <div class="col-md-4 column">
                        <?php print render($page['noticias_destacadas']); ?>
                    </div>
                <?php endif; ?>
            </section>
        </div>
    <?php endif; ?>


    <?php if (($page['complemento_portada_superior-1']) || ($page['complemento_portada_superior-2']) || ($page['complemento_portada_superior-3'])): ?>
        <div class="row clearfix">
            <?php if (($page['complemento_portada_superior-1'])): ?>
                <div class="col-md-4 column">
                    <?php print render($page['complemento_portada_superior-1']); ?></div>
            <?php endif; ?>
            <?php if (($page['complemento_portada_superior-2'])): ?>
                <div class="col-md-4 column">
                    <?php print render($page['complemento_portada_superior-2']); ?></div>
            <?php endif; ?>
            <?php if (($page['complemento_portada_superior-3'])): ?>
                <div class="col-md-4 column">
                    <?php print render($page['complemento_portada_superior-3']); ?></div>
            <?php endif; ?>
        </div>
    <?php endif; ?>

    <?php if (($page['migas_pan']) || ($breadcrumb)): ?>
        <div class="row clearfix">
            <section>
                <?php if (($page['migas_pan'])): ?>
                    <?php print render($page['migas_pan']); ?>
                <?php endif; ?>
                <div>
                    <?php if ($breadcrumb): ?>
                        <div id="breadcrumb"><?php // print $breadcrumb;                       ?></div>
                    <?php endif; ?>
                </div>
            </section>
        </div>
    <?php endif; ?>

    <?php if (($page['destacados-1']) || ($page['destacados-2']) || ($page['destacados-3']) || ($page['destacados-4']) || ($page['destacados-5']) || ($page['destacados-6'])): ?>
        <div class="row clearfix">
            <section>
                <?php if ($page['destacados-1']): ?>
                    <div class="col-md-2">
                        <?php print render($page['destacados-1']); ?>
                    </div>
                <?php endif; ?>
                <?php if ($page['destacados-2']): ?>
                    <div class="col-md-2">
                        <?php print render($page['destacados-2']); ?>
                    </div>
                <?php endif; ?>
                <?php if ($page['destacados-3']): ?>
                    <div class="col-md-2">
                        <?php print render($page['destacados-3']); ?>
                    </div>
                <?php endif; ?>
                <?php if ($page['destacados-4']): ?>
                    <div class="col-md-2">
                        <?php print render($page['destacados-4']); ?>
                    </div>
                <?php endif; ?>
                <?php if ($page['destacados-5']): ?>
                    <div class="col-md-2">
                        <?php print render($page['destacados-5']); ?>
                    </div>
                <?php endif; ?>
                <?php if ($page['destacados-6']): ?>
                    <div class="col-md-2">
                        <?php print render($page['destacados-6']); ?>
                    </div>
                <?php endif; ?>
            </section>
        </div>
    <?php endif; ?>

    <?php if (($page['especial-1']) || ($page['especial-2'])): ?>
        <div class="row clearfix">
            <section>
                <?php if ($page['especial-1']): ?>
                    <div class="col-md-8 column">
                        <?php print render($page['especial-1']); ?>
                    </div>
                <?php endif; ?>
                <?php if ($page['especial-2']): ?>
                    <div class="col-md-4 column">
                        <?php print render($page['especial-2']); ?>
                    </div>
                <?php endif; ?>
            </section>
        </div>
    <?php endif; ?>

    <?php if (($page['conceptos-1']) || ($page['conceptos-2']) || ($page['conceptos-3']) || ($page['conceptos-4'])): ?>
        <div class="row clearfix">
            <section>
                <?php if ($page['conceptos-1']): ?>
                    <div class="col-md-3 column">
                        <?php print render($page['conceptos-1']); ?>
                    </div>
                <?php endif; ?>
                <?php if ($page['conceptos-2']): ?>
                    <div class="col-md-3 column">
                        <?php print render($page['conceptos-2']); ?>
                    </div>
                <?php endif; ?>

                <?php if ($page['conceptos-3']): ?>
                    <div class="col-md-3 column">
                        <?php print render($page['conceptos-3']); ?>
                    </div>
                <?php endif; ?>
                <?php if ($page['conceptos-4']): ?>
                    <div class="col-md-3 column">
                        <?php print render($page['conceptos-4']); ?>
                    </div>
                <?php endif; ?>
            </section>
        </div>
    <?php endif; ?>

    <?php if (($page['especial-completa-2'])): ?>
        <div class="row clearfix">
            <div class="col-md-12 column">
                <?php print render($page['especial-completa-2']); ?>
            </div>
        </div>
    <?php endif; ?>

    <?php if (($page['informacion-1']) || ($page['informacion-2'])): ?>
        <div class="row clearfix">
            <section>
                <?php if ($page['informacion-1']): ?>
                    <div class="col-md-6 column">
                        <?php print render($page['informacion-1']); ?>
                    </div>
                <?php endif; ?>
                <?php if ($page['informacion-2']): ?>
                    <div class="col-md-6 column">
                        <?php print render($page['informacion-2']); ?>
                    </div>
                <?php endif; ?>
            </section>
        </div>
    <?php endif; ?>

    <?php if (($page['complemento_portada_medio-1']) || ($page['complemento_portada_medio-2']) || ($page['complemento_portada_medio-3'])): ?>
        <div class="row clearfix">
            <?php if (($page['complemento_portada_medio-1'])): ?>
                <div class="col-md-4 column">
                    <?php print render($page['complemento_portada_medio-1']); ?></div>
            <?php endif; ?>

            <?php if (($page['complemento_portada_medio-2'])): ?>
                <div class="col-md-4 column">
                    <?php print render($page['complemento_portada_medio-2']); ?></div>
            <?php endif; ?>

            <?php if (($page['complemento_portada_medio-3'])): ?>
                <div class="col-md-4 column">
                    <?php print render($page['complemento_portada_medio-3']); ?></div>
            <?php endif; ?>
        </div>
    <?php endif; ?>

    <?php if ($page['jumbo_3']): ?>
    </div>
    <?php print render($page['jumbo_3']) ?>
    <div class="container">
    <?php endif; ?>

    <?php if ($tabs): ?>
        <div class="tabs">
            <?php print render($tabs); ?>
        </div>
    <?php endif; ?>


    <?php if (($page['left_sidebar']) || ($page['informacion_prioritaria']) || ($page['informacion_complementaria']) || ($page['content']) || ($page['right_sidebar'])): ?>
        <div class="row clearfix">
            <?php if ($page['left_sidebar']): ?>
                <aside>
                    <div class="col-md-3 col-xs-12 column">
                        <?php print render($page['left_sidebar']); ?>
                    </div>
                </aside>
            <?php endif; ?>

            <?php
            $class_content = "col-md-12";
            if ($page['left_sidebar'] && $page['right_sidebar']):
                $class_content = "col-md-6 column";
            endif;
            if ($page['left_sidebar'] && !$page['right_sidebar']):
                $class_content = "col-md-9 column";
            endif;
            if (!$page['left_sidebar'] && $page['right_sidebar']):
                $class_content = "col-md-9 column";
            endif;
            if (!$page['left_sidebar'] && !$page['right_sidebar']):
                $class_content = "col-md-12 column";
            endif;
            ?>
            <div class="<?php echo $class_content; ?>">
                <?php if ($page['highlighted']): ?>
                    <aside>
                        <div class="col-md-12 column">
                            <?php print render($page['highlighted']); ?>
                        </div>
                    </aside>
                <?php endif; ?>

                <?php if ($page['help']): ?>
                    <aside>
                        <div class="col-md-12 column">
                            <?php print render($page['help']); ?>
                            <?php print $messages; ?>
                        </div>
                    </aside>
                <?php endif; ?>

                <?php if ($page['informacion_prioritaria']): ?>
                    <aside>
                        <div class="col-md-12 column">
                            <?php print render($page['informacion_prioritaria']); ?>
                        </div>
                    </aside>
                <?php endif; ?>
                <?php if (($page['noticias-1']) || ($page['noticias-2']) || ($page['noticias-3'])): ?>
                    <div class="panel panel-default jumbo-gallery">
                        <div class="panel-heading">
                            <a class="panel-title collapsed" data-parent="#nuevas" data-toggle="collapse" href="#nuevo"> Noticias otros portales UTB <span class="caret"></span> </a>
                        </div>
                        <div class="panel-collapse collapse" id="nuevo" style="height: 199px;">
                            <div class="panel-body">
                                <p>Últimas noticias :</p>
                                <br>
                                <!--                        CADA PROGRAMA/CURSO                     -->
                                <div class="list-group">

                                    <div class="row clearfix">
                                        <section>

                                            <?php if ($page['noticias-1']): ?>
                                                <div class="col-md-4 column">
                                                    <?php print render($page['noticias-1']); ?>
                                                </div>
                                            <?php endif; ?>

                                            <?php if ($page['noticias-2']): ?>
                                                <div class="col-md-4 column">
                                                    <?php print render($page['noticias-2']); ?>
                                                </div>
                                            <?php endif; ?>


                                            <?php if ($page['noticias-3']): ?>
                                                <div class="col-md-4 column">
                                                    <?php print render($page['noticias-3']); ?>
                                                </div>
                                            <?php endif; ?>
                                        </section>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>

                <?php endif; ?>
                <?php if (!drupal_is_front_page()) : ?>
                    <?php if ($page['content']): ?>
                        <article>
                            <div class="col-md-12 column">
                                <?php print render($page['content']); ?>
                            </div>

                        </article>
                    <?php endif; ?>
                <?php endif; ?>

                <?php if ($page['informacion_complementaria']): ?>
                    <aside>
                        <div class="col-md-12 column">
                            <?php print render($page['informacion_complementaria']); ?>
                        </div>
                    </aside>
                <?php endif; ?>
            </div>
            <?php if ($page['right_sidebar']): ?>
                <div class="col-md-3 column">
                    <aside>
                        <?php print render($page['right_sidebar']); ?>
                    </aside>
                </div>

            <?php endif; ?>
        </div>
    <?php endif; ?>




    <?php if ($page['jumbo_4']): ?>
    </div>
    <?php print render($page['jumbo_4']) ?>
    <div class="container">
    <?php endif; ?>

    <?php if (($page['novedades-1']) || ($page['novedades-2']) || ($page['novedades-3']) || ($page['novedades-4'])): ?>
        <div class="row clearfix">
            <section>

                <?php if ($page['novedades-1']): ?>
                    <div class="col-md-3 column">
                        <?php print render($page['novedades-1']); ?>
                    </div>
                <?php endif; ?>

                <?php if ($page['novedades-2']): ?>
                    <div class="col-md-3 column">
                        <?php print render($page['novedades-2']); ?>
                    </div>
                <?php endif; ?>


                <?php if ($page['novedades-3']): ?>
                    <div class="col-md-3 column">
                        <?php print render($page['novedades-3']); ?>
                    </div>
                <?php endif; ?>
                <?php if ($page['novedades-4']): ?>
                    <div class="col-md-3 column">
                        <?php print render($page['novedades-4']); ?>
                    </div>
                <?php endif; ?>
            </section>
        </div>
    <?php endif; ?>

    <?php if (($page['resaltados-1']) || ($page['resaltados-2']) || ($page['resaltados-3']) || ($page['resaltados-4']) || ($page['resaltados-5']) || ($page['resaltados-6'])): ?>
        <div class="row clearfix">
            <section>
                <?php if ($page['resaltados-1']): ?>
                    <div class="col-md-2">
                        <?php print render($page['resaltados-1']); ?>
                    </div>
                <?php endif; ?>

                <?php if ($page['resaltados-2']): ?>
                    <div class="col-md-2">
                        <?php print render($page['resaltados-2']); ?>
                    </div>
                <?php endif; ?>

                <?php if ($page['resaltados-3']): ?>
                    <div class="col-md-2">
                        <?php print render($page['resaltados-3']); ?>
                    </div>
                <?php endif; ?>

                <?php if ($page['resaltados-4']): ?>
                    <div class="col-md-2">
                        <?php print render($page['resaltados-4']); ?>
                    </div>
                <?php endif; ?>

                <?php if ($page['resaltados-5']): ?>
                    <div class="col-md-2">
                        <?php print render($page['resaltados-5']); ?>
                    </div>
                <?php endif; ?>

                <?php if ($page['resaltados-6']): ?>
                    <div class="col-md-2">
                        <?php print render($page['resaltados-6']); ?>
                    </div>
                <?php endif; ?>
            </section>
        </div>
    <?php endif; ?>

    <?php if (($page['notas-1']) || ($page['notas-2'])): ?>
        <div class="row clearfix">
            <section>
                <?php if ($page['notas-1']): ?>
                    <div class="col-md-6 column">
                        <?php print render($page['notas-1']); ?>
                    </div>
                <?php endif; ?>
                <?php if ($page['notas-2']): ?>
                    <div class="col-md-6 column">
                        <?php print render($page['notas-2']); ?>
                    </div>
                <?php endif; ?>
            </section>
        </div>
    <?php endif; ?>

    <?php if (($page['catalogo-1']) || ($page['catalogo-2']) || ($page['catalogo-3']) || ($page['catalogo-4']) || ($page['catalogo-5'])): ?>
        <div class="row clearfix">
            <?php if ($page['catalogo-1']): ?>
                <div class="col-md-2 col-md-offset-1">
                    <?php print render($page['catalogo-1']); ?>
                </div>
            <?php endif; ?>
            <?php if ($page['catalogo-2']): ?>
                <div class="col-md-2">
                    <?php print render($page['catalogo-2']); ?>
                </div>
            <?php endif; ?>

            <?php if ($page['catalogo-3']): ?>
                <div class="col-md-2">
                    <?php print render($page['catalogo-3']); ?>
                </div>
            <?php endif; ?>

            <?php if ($page['catalogo-4']): ?>
                <div class="col-md-2">
                    <?php print render($page['catalogo-4']); ?>
                </div>
            <?php endif; ?>

            <?php if ($page['catalogo-5']): ?>
                <div class="col-md-2">
                    <?php print render($page['catalogo-5']); ?>
                </div>
            <?php endif; ?>
        </div>
    <?php endif; ?>

    <?php if (($page['especial-completa-3'])): ?>
        <div class="row clearfix">
            <div class="col-md-12 column">
                <?php print render($page['especial-completa-3']); ?>
            </div>
        </div>
    <?php endif; ?>

    <?php if (($page['inferior_especial-1']) || ($page['inferior_especial-2'])): ?>
        <div class="row clearfix">
            <?php if ($page['inferior_especial-1']): ?>
                <div class="col-md-4 column">
                    <?php print render($page['inferior_especial-1']); ?>
                </div>
            <?php endif; ?>

            <?php if ($page['inferior_especial-2']): ?>
                <div class="col-md-8 column">
                    <?php print render($page['inferior_especial-2']); ?>
                </div>
            <?php endif; ?>
        </div>
    <?php endif; ?>

    <?php if ($page['jumbo_5']): ?>
    </div>
    <?php print render($page['jumbo_5']) ?>
    <div class="container">
    <?php endif; ?>

    <?php if (($page['opciones-1']) || ($page['opciones-2']) || ($page['opciones-3']) || ($page['opciones-4'])): ?>
        <div class="row clearfix">
            <?php if ($page['opciones-1']): ?>
                <div class="col-md-3 column">
                    <?php print render($page['opciones-1']); ?>
                </div>
            <?php endif; ?>

            <?php if ($page['opciones-2']): ?>
                <div class="col-md-3 column">
                    <?php print render($page['opciones-2']); ?>
                </div>
            <?php endif; ?>

            <?php if ($page['opciones-3']): ?>
                <div class="col-md-3 column">
                    <?php print render($page['opciones-3']); ?>
                </div>
            <?php endif; ?>

            <?php if ($page['opciones-4']): ?>
                <div class="col-md-3 column">
                    <?php print render($page['opciones-4']); ?>
                </div>
            <?php endif; ?>
        </div>
    <?php endif; ?>

    <?php if (($page['complemento-1']) || ($page['complemento-2'])): ?>
        <div class="row clearfix">
            <?php if ($page['complemento-1']): ?>
                <div class="col-md-6 column">
                    <?php print render($page['complemento-1']); ?>
                </div>
            <?php endif; ?>
            <?php if ($page['complemento-2']): ?>
                <div class="col-md-6 column">
                    <?php print render($page['complemento-2']); ?>
                </div>
            <?php endif; ?>
        </div>
    <?php endif; ?>

    <?php if (($page['especial-completa-4'])): ?>
        <div class="row clearfix">
            <div class="col-md-12 column">
                <?php print render($page['especial-completa-4']); ?>
            </div>
        </div>
    <?php endif; ?>
    <?php if (($page['especial-completa-5'])): ?>
        <div class="row clearfix">
            <div class="col-md-12 column">
                <?php print render($page['especial-completa-5']); ?>
            </div>
        </div>
    <?php endif; ?>
    <br>

    <?php if (($page['publi-1']) || ($page['publi-2']) || ($page['publi-3']) || ($page['publi-4'])): ?>
        <div class="row clearfix">
            <?php if ($page['publi-1']): ?>
                <div class="col-md-3 column">
                    <?php print render($page['publi-1']); ?>
                </div>
            <?php endif; ?>

            <?php if ($page['publi-2']): ?>
                <div class="col-md-3 column">
                    <?php print render($page['publi-2']); ?>
                </div>
            <?php endif; ?>

            <?php if ($page['publi-3']): ?>
                <div class="col-md-3 column">
                    <?php print render($page['publi-3']); ?>
                </div>
            <?php endif; ?>

            <?php if ($page['publi-4']): ?>
                <div class="col-md-3 column">
                    <?php print render($page['publi-4']); ?>
                </div>
            <?php endif; ?>
        </div>
    <?php endif; ?>

    <?php if (($page['complemento_portada_inferior-1']) || ($page['complemento_portada_inferior-2']) || ($page['complemento_portada_inferior-3'])): ?>
        <div class="row clearfix">
            <?php if (($page['complemento_portada_inferior-1'])): ?>
                <div class="col-md-4 column">
                    <?php print render($page['complemento_portada_inferior-1']); ?>
                </div>
            <?php endif; ?>
            <?php if (($page['complemento_portada_inferior-2'])): ?>
                <div class="col-md-4 column">
                    <?php print render($page['complemento_portada_inferior-2']); ?>
                </div>
            <?php endif; ?>
            <?php if (($page['complemento_portada_inferior-3'])): ?>
                <div class="col-md-4 column">
                    <?php print render($page['complemento_portada_inferior-3']); ?>
                </div>
            <?php endif; ?>
        </div>
    <?php endif; ?>

    <?php if ($page['social']): ?>
        <div class="row clearfix text-right">
            <p class="small"><strong>¡Comparte este contenido en tus redes sociales!</strong></p>
            <?php print render($page['social']) ?>
        </div>
    <?php endif; ?>

</div>

<?php if ($page['jumbo_6']): ?>
    <?php print render($page['jumbo_6']) ?>
<?php endif; ?>
<!--<a data-toggle="modal" href="#myModal" style="background-color:black;" class="btn btn-primary">Reporta Error</a>-->
<footer id="footer" >
    <div class="col-md-12 column" style="padding-right: 0px;padding-left: 0px;">
        <div id="menupoliticas">
            <nav class="navbar z-depth-1 mer-pos" style="margin-bottom: 0px;">
                <div class="container ">
                    <div class="navbar-header">
                        <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#menu-politicas">
                            <span class="sr-only">Toggle navigation</span>
                            <span class="icon-bar"></span>
                            <span class="icon-bar"></span>
                            <span class="icon-bar"></span>
                        </button>
                        <a class="navbar-brand social" href="#start"><span class="glyphicon glyphicon-arrow-up" style=" color: white; "></span></a>
                    </div>

                    <div class="navbar-right collapse navbar-collapse" id="menu-politicas">
                        <?php print render($page['menu_politicas']); ?>
                    </div>
                </div>
            </nav>
            </section>
        </div>
    </div>

    <?php if ($page['jumbo_7']): ?>
        <?php print render($page['jumbo_7']) ?>
    <?php endif; ?>

    <?php if (($page['footer_left']) || ($page['footer_center']) || ($page['footer_right']) || ($page['footer_completo'])): ?>
        <div class="clearfix inst-strong text-white padding-m">
            <div class="container">
                <?php if ($page['footer_left']): ?>
                    <div class="col-md-4 column padding-m ">
                        <?php print render($page['footer_left']); ?>
                    </div>
                <?php endif; ?>
                <?php if ($page['footer_center']): ?>
                    <div class="col-md-4 column padding-m ">
                        <?php print render($page['footer_center']); ?>
                    </div>
                <?php endif; ?>
                <?php if ($page['footer_right']): ?>
                    <div class="col-md-4 column padding-m ">
                        <?php print render($page['footer_right']); ?>
                    </div>
                <?php endif; ?>

                <?php if ($page['footer_completo']): ?>
                    <div class="col-md-12 column padding-m ">
                        <?php print render($page['footer_completo']) ?>
                    </div>
                <?php endif; ?>

            </div>
        </div>
    <?php endif; ?>

    <?php if ($page['jumbo_8']): ?>
        <?php print render($page['jumbo_8']) ?>
    <?php endif; ?>

    <?php if (($page['copyright'])): ?>
        <div class="clearfix text-muted text-center mer-pos text-white small padding-s ">
            <div class="col-md-12 column">
                <?php print render($page['copyright']); ?>
            </div>
        </div>
    <?php endif; ?>

</footer>
