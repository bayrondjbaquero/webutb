<?php
if ($form):
    ?>
    <h1>
        <?php print drupal_render_children($form); ?>
    </h1>
    <?php
    db_set_active('investigaciones');


    $result1 = db_query("SELECT nid, title FROM {node} WHERE type = :type", array(
        ':type' => 'grupo_de_investigacion',
    ));

    $result2 = db_query("SELECT nid, title FROM {node} WHERE type = :type", array(
        ':type' => 'laboratorios',
    ));

    $result3 = db_query("SELECT nid, title FROM {node} WHERE type = :type", array(
        ':type' => 'semillero',
    ));

    db_set_active();

endif;
drupal_add_css(drupal_get_path('theme', 'SUBTX1000') . '/css/bootstrap.min.css');
drupal_add_css(drupal_get_path('theme', 'SUBTX1000') . '/css/utb.css');
drupal_add_js(drupal_get_path('theme', 'SUBTX1000') . '/js/bootstrap.min.js');
drupal_add_js(drupal_get_path('theme', 'SUBTX1000') . '/js/jquery.min.js');
?>

<button type="button" class="btn btn-primary btn-lg" data-toggle="modal" data-target="#Grupos">
    Grupos de Investigación DB INVESTIGACIONES
</button>

<!-- Modal -->
<div class="modal fade" id="Grupos" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true" >
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>
                <h1 class="modal-title" id="myModalLabel">Semilleros UTB</h1>
            </div>
            <div class="modal-body"  style="height: 400px;overflow: scroll;" >
                <?php
                if ($result1) {
                    print "<table class='table'><thead><tr><th>ID</th>";
                    print "<th>TITULO</th></tr></thead><tbody>";
                    while ($row = $result1->fetchAssoc()) {
                        print "<tr><td><h3>" . $row['nid'] . "</h3></td>";
                        print "<td><h3>" . $row['title'] . "</h3></td></tr>";
                    }
                    print "</tbody></table>";
                }
                ?>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
            </div>
        </div>
    </div>
</div>

<button type="button" class="btn btn-primary btn-lg" data-toggle="modal" data-target="#Laboratorios">
    Laboratorios DB INVESTIGACIONES
</button>

<!-- Modal -->
<div class="modal fade" id="Laboratorios" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true" >
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>
                <h1 class="modal-title" id="myModalLabel">Semilleros UTB</h1>
            </div>
            <div class="modal-body"  style="height: 400px;overflow: scroll;" >
                <?php
                if ($result2) {
                    print "<table class='table'><thead><tr><th>ID</th>";
                    print "<th>TITULO</th></tr></thead><tbody>";
                    while ($row = $result2->fetchAssoc()) {
                        print "<tr><td><h3>" . $row['nid'] . "</h3></td>";
                        print "<td><h3>" . $row['title'] . "</h3></td></tr>";
                    }
                    print "</tbody></table>";
                }
                ?>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
            </div>
        </div>
    </div>
</div>

<!-- Button trigger modal -->
<button type="button" class="btn btn-primary btn-lg" data-toggle="modal" data-target="#Semilleros">
    Semilleros DB INVESTIGACIONES
</button>

<!-- Modal -->
<div class="modal fade" id="Semilleros" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true" >
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>
                <h1 class="modal-title" id="myModalLabel">Semilleros UTB</h1>
            </div>
            <div class="modal-body"  style="height: 400px;overflow: scroll;" >
                <?php
                if ($result3) {
                    print "<table class='table'><thead><tr><th>ID</th>";
                    print "<th>TITULO</th></tr></thead><tbody>";
                    while ($row = $result3->fetchAssoc()) {
                        print "<tr><td><h3>" . $row['nid'] . "</h3></td>";
                        print "<td><h3>" . $row['title'] . "</h3></td></tr>";
                    }
                    print "</tbody></table>";
                }
                ?>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
            </div>
        </div>
    </div>
</div>





